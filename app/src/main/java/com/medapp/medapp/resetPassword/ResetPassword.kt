package com.medapp.medapp.resetPassword

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Window
import android.view.WindowManager
import android.widget.Button
import android.widget.EditText
import com.google.gson.JsonParser
import com.medapp.medapp.R
import com.medapp.medapp.config.ServerConfig
import com.muddzdev.styleabletoastlibrary.StyleableToast
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.util.concurrent.TimeUnit

class ResetPassword : AppCompatActivity() {
    private var s_v_code: String? = null
    private var type: String? = "i"
    private var phoneNumber: String? = "0"
    private var AUTH_KEY: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reset_password)

        //make status bar invisible
        val w: Window = getWindow()
        w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        //get data from intent
        val i_p = getIntent()
        type = i_p.getStringExtra("type")

        //phone verification
        findViewById<Button>(R.id.getCode).setOnClickListener {
            phoneNumber = findViewById<EditText>(R.id.familyID).text.toString()

            if (phoneNumber!!.length != 0) {
                isAccountExist()
            }
        }
        //on next clicked
        onNextClicked()
    }

    fun isAccountExist() {
        doAsync {
            val client: OkHttpClient = OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build()

            val urlBuilder = HttpUrl.parse(ServerConfig().isAccountExist_resetPassword())!!.newBuilder()
            urlBuilder.addQueryParameter("phoneNumber", phoneNumber)
            urlBuilder.addQueryParameter("type", type)

            val url = urlBuilder.build().toString()

            val request = Request.Builder()
                    .url(url)
                    .build()

            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()

            uiThread {
                Log.d("ResetPass", responce_string.toString())

                if (responce_string == "exist") {
                    startPhoneVerification(phoneNumber!!)
                } else {
                    showMessageAfterSignup(getString(R.string.no_such_account_exist), Color.RED)
                }

            }
        }
    }

    fun showMessageAfterSignup(msg_text: String, color: Int) {
        val st = StyleableToast.Builder(this)
                .text(msg_text)
                .textColor(Color.WHITE)
                .backgroundColor(color)
                .build()
        st.show()
    }

    fun startPhoneVerification(phoneNumber: String) {
        sendOtp()
        /*var mCallbacks = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            override fun onVerificationCompleted(credential: PhoneAuthCredential) {
                if ((credential.smsCode) != null) {
                    findViewById<EditText>(R.id.smsCode).setText(credential.smsCode)
                    findViewById<TextView>(R.id.note).setText("Verification code detected")
                } else {
                    startFinalActivity()
                    findViewById<TextView>(R.id.note).setText("Your phone number is verified, \nThere is no need of entering verification code.")
                }
            }

            override fun onVerificationFailed(e: FirebaseException) {
                findViewById<TextView>(R.id.note).setText("Unable to send verification code")
            }

            override fun onCodeSent(verificationId: String?,
                                    token: PhoneAuthProvider.ForceResendingToken?) {
                s_v_code = verificationId
                findViewById<TextView>(R.id.note).setText("Verification code is sent on your mobile number")
            }
        }

        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                120,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks)     // OnVerificationStateChangedCallbacks*/
    }

    fun sendOtp() {
        doAsync {
            val client = OkHttpClient()

            //GET AUTH CODE FROM PRIVATE SERVERS
            val urlBuilder = HttpUrl.parse(ServerConfig().getMsg91_securityCode_from_webServer())!!.newBuilder()
            urlBuilder.addQueryParameter("security_code", "241999")
            val url = urlBuilder.build().toString()
            val request = Request.Builder()
                    .url(url)
                    .build()
            val response: Response = client.newCall(request).execute()
            val auth_code = response.body()!!.string()
            AUTH_KEY = auth_code
            //My Auth Key
            //233436AcdEJOX35b7f9b31
            //changing the auth key as the previous accout limit finished. . .
            //SEND OTP USING Msg91 API
            val urlBuilder_2 = HttpUrl.parse("http://control.msg91.com/api/sendotp.php")!!.newBuilder()
            urlBuilder_2.addQueryParameter("authkey", AUTH_KEY)
            urlBuilder_2.addQueryParameter("message", "Your Verification Code is ##OTP##")
            urlBuilder_2.addQueryParameter("sender", "Medapp")
            urlBuilder_2.addQueryParameter("mobile", getCountryCode() + phoneNumber)
            val url_2 = urlBuilder_2.build().toString()
            val request_2 = Request.Builder()
                    .url(url_2)
                    .build()
            val response_2: Response = client.newCall(request_2).execute()
            val responce_string = response_2.body()!!.string()
            Log.d("ResetPassword", "Response String: $responce_string")
        }
    }

    fun onNextClicked() {
        findViewById<Button>(R.id.nextButton).setOnClickListener {
            checkCredintials()
        }
    }

    fun checkCredintials() {
        /*var user_sms_code = findViewById<EditText>(R.id.smsCode).getText().toString()
        val credential = PhoneAuthProvider.getCredential(s_v_code!!, user_sms_code)
        signInWithPhoneAuthCredential(credential)*/
        verifyOtp()
    }

    fun verifyOtp() {
        doAsync {
            val client = OkHttpClient()

            val urlBuilder = HttpUrl.parse("https://control.msg91.com/api/verifyRequestOTP.php")!!.newBuilder()
            urlBuilder.addQueryParameter("authkey", "233436AcdEJOX35b7f9b31")
            urlBuilder.addQueryParameter("otp", findViewById<EditText>(R.id.smsCode).text.toString())
            urlBuilder.addQueryParameter("mobile", getCountryCode() + phoneNumber)
            val url = urlBuilder.build().toString()
            val request = Request.Builder()
                    .url(url)
                    .build()
            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()
            uiThread {
                Log.d("Reset_Password", responce_string)
                val parser = JsonParser()
                val j_o = parser.parse(responce_string).getAsJsonObject();
                val resp_msg = j_o.get("message")
                if (resp_msg.toString() == "\"otp_verified\"") {
                    startFinalActivity()
                }
            }
        }
    }

    /*private fun signInWithPhoneAuthCredential(credential: PhoneAuthCredential) {
        FirebaseAuth.getInstance().signInWithCredential(credential)
                .addOnCompleteListener(this, OnCompleteListener<AuthResult> { task ->
                    if (task.isSuccessful) {
                        startFinalActivity()
                    } else {
                        showError()
                    }
                })
    }*/

    fun startFinalActivity() {
        val i = Intent(this, ResetPassword_2::class.java)
        i.putExtra("type", type)
        i.putExtra("phoneNumber", phoneNumber)
        startActivity(i)
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        finish()
    }

    fun showError() {
        val st = StyleableToast.Builder(this)
                .text(getString(R.string.invalid_verification_code))
                .textColor(Color.WHITE)
                .backgroundColor(Color.RED)
                .build()
        st.show()
    }

    fun getCountryCode(): String {
        //val m_Codes = arrayOf("376", "971", "93", "355", "374", "599", "244", "672", "54", "43", "61", "297", "994", "387", "880", "32", "226", "359", "973", "257", "229", "590", "673", "591", "55", "975", "267", "375", "501", "1", "61", "243", "236", "242", "41", "225", "682", "56", "237", "86", "57", "506", "53", "238", "61", "357", "420", "49", "253", "45", "213", "593", "372", "20", "291", "34", "251", "358", "679", "500", "691", "298", "33", "241", "44", "995", "233", "350", "299", "220", "224", "240", "30", "502", "245", "592", "852", "504", "385", "509", "36", "62", "353", "972", "44", "91", "964", "98", "39", "962", "81", "254", "996", "855", "686", "269", "850", "82", "965", "7", "856", "961", "423", "94", "231", "266", "370", "352", "371", "218", "212", "377", "373", "382", "261", "692", "389", "223", "95", "976", "853", "222", "356", "230", "960", "265", "52", "60", "258", "264", "687", "227", "234", "505", "31", "47", "977", "674", "683", "64", "968", "507", "51", "689", "675", "63", "92", "48", "508", "870", "1", "351", "680", "595", "974", "40", "381", "7", "250", "966", "677", "248", "249", "46", "65", "290", "386", "421", "232", "378", "221", "252", "597", "239", "503", "963", "268", "235", "228", "66", "992", "690", "670", "993", "216", "676", "90", "688", "886", "255", "380", "256", "1", "598", "998", "39", "58", "84", "678", "681", "685", "967", "262", "27", "260", "263")

        Log.d("Reset_Password", "+91")
        return "+91"
    }
}
