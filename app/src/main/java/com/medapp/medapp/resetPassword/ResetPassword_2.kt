package com.medapp.medapp.resetPassword

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Button
import android.widget.EditText
import com.medapp.medapp.R
import com.medapp.medapp.config.ServerConfig
import com.medapp.medapp.loginSignup.family_login
import com.medapp.medapp.loginSignup.login
import com.muddzdev.styleabletoastlibrary.StyleableToast
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.util.concurrent.TimeUnit

class ResetPassword_2 : AppCompatActivity() {
    private var type: String? = "i"
    private var phoneNumber: String? = "0"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reset_password_2)

        //get data from intent
        val i_p = getIntent()
        type = i_p.getStringExtra("type")
        phoneNumber = i_p.getStringExtra("phoneNumber")

        //make next button work
        onClickNextButton()
    }

    //onclick next button
    fun onClickNextButton() {
        findViewById<Button>(R.id.nextButton).setOnClickListener {
            val newPassword = findViewById<EditText>(R.id.newPassword).text.toString()
            val confirmNewPassword = findViewById<EditText>(R.id.confirmNewPassword).text.toString()
            if (newPassword.length != 0 && confirmNewPassword.length != 0) {
                if (newPassword == confirmNewPassword) {
                    resetPassword(newPassword.toString())
                } else {
                    showMessageAfterSignup(getString(R.string.retyped_password_must_be_same), Color.RED)
                }
            } else {
                showMessageAfterSignup(getString(R.string.all_fields_are_compulsory), Color.RED)
            }
        }
    }

    //make api request to reset password
    fun resetPassword(newPassword: String) {
        doAsync {
            val client: OkHttpClient = OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build()

            val urlBuilder = HttpUrl.parse(ServerConfig().updatePassword())!!.newBuilder()
            urlBuilder.addQueryParameter("phoneNumber", phoneNumber)
            urlBuilder.addQueryParameter("type", type)
            urlBuilder.addQueryParameter("newPassword", newPassword)

            val url = urlBuilder.build().toString()

            val request = Request.Builder()
                    .url(url)
                    .build()

            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()

            uiThread {
                Log.d("mess", responce_string.toString())

                if (responce_string != "dataMissing") {
                    if (responce_string != "serverError") {
                        if (responce_string == "ok") {
                            showMessageAfterSignup(getString(R.string.password_updated), Color.GREEN)

                            gotoLogin()
                        } else {
                            showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                        }
                    } else {
                        showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                    }
                } else {
                    showMessageAfterSignup(getString(R.string.something_goes_wrong), Color.RED)
                }
            }
        }
    }

    //goto login
    fun gotoLogin() {
        if (type == "i") {
            val i = Intent(this@ResetPassword_2, login::class.java)
            startActivity(i)
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            finish()
        }
        if (type == "f") {
            val i = Intent(this@ResetPassword_2, family_login::class.java)
            startActivity(i)
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            finish()
        }
    }

    //show message
    fun showMessageAfterSignup(msg_text: String, color: Int) {
        val st = StyleableToast.Builder(this)
                .text(msg_text)
                .textColor(Color.WHITE)
                .backgroundColor(color)
                .build()
        st.show()
    }

}
