package com.medapp.medapp;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.medapp.medapp.config.ServerConfig;

import java.io.IOException;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class verifyAddToFamily extends AppCompatActivity {

    Button accept, reject;
    com.facebook.drawee.view.SimpleDraweeView simpleDraweeView;
    TextView view_1, view_num, view_num_admin;

    SharedPreferences loginInfo;

    OkHttpClient client;
    HttpUrl.Builder urlBuilder;

    Response response;
    Request request;

    String familyID, phoneNumber, responseString;

    String TAG = "verifyAddToFamily";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fresco.initialize(this);
        setContentView(R.layout.activity_verify_add_to_family);

        loginInfo = getApplicationContext().getSharedPreferences("loginInfo", Context.MODE_PRIVATE);
        phoneNumber = loginInfo.getString("phoneNumber", "");

        simpleDraweeView = findViewById(R.id.admin_pic);
        view_1 = findViewById(R.id.view_1);
        view_num = findViewById(R.id.view_num);
        view_num_admin = findViewById(R.id.view_num_admin);
        accept = findViewById(R.id.accept);
        reject = findViewById(R.id.reject);

        Intent intent = getIntent();
        familyID = intent.getStringExtra("familyID");
        Log.d("VerifyAddToFamily", familyID);

        getInviteInfo inviteInfo = new getInviteInfo();
        inviteInfo.execute(familyID);

        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //accept invite . . .
                acceptInvite invite = new acceptInvite();
                invite.execute(familyID, phoneNumber);
            }
        });

        reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //reject invite
                rejectInvite invite = new rejectInvite();
                invite.execute(familyID, phoneNumber);
            }
        });
    }

    @SuppressLint("StaticFieldLeak")
    public class getInviteInfo extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            String id = strings[0];
            Log.d(TAG, "ID: " + id);

            client = new OkHttpClient.Builder().connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build();

            urlBuilder = Objects.requireNonNull(HttpUrl.parse(new ServerConfig().getAdminDetails())).newBuilder();
            urlBuilder.addQueryParameter("familyID", id);

            String url = urlBuilder.build().toString();

            request = new Request.Builder().url(url).build();

            try {
                response = client.newCall(request).execute();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response.body() != null) {
                try {
                    responseString = response.body().string();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return responseString;
        }

        @Override
        protected void onPostExecute(String s) {
            Log.d(TAG, "Response server:" + s);
            String[] data = s.split(",");
            String name = data[0];
            String number = data[1];
            String profile = data[2];

            Uri uri = Uri.parse(new ServerConfig().getRootAddress()
                    + "upload/" + profile + "?notCache=" + profile);

            simpleDraweeView.setImageURI(uri);

            view_1.setText(String.format(getString(R.string.invite_join_family), name));
            view_num.setText(String.format(getString(R.string.phone_number_invite), name));
            view_num_admin.setText(number);

            Log.d(TAG, "name:" + name + " number:" + number + " profile:" + profile);
        }
    }

    @SuppressLint("StaticFieldLeak")
    public class acceptInvite extends AsyncTask<String, String, String> {

        String responseResult;

        @Override
        protected String doInBackground(String... strings) {
            String id = strings[0];
            String num = strings[1];

            client = new OkHttpClient.Builder().connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build();
            urlBuilder = Objects.requireNonNull(HttpUrl.parse(new ServerConfig().acceptInvite())).newBuilder();
            urlBuilder.addQueryParameter("familyID", id);
            urlBuilder.addQueryParameter("userNum", num);

            String url = urlBuilder.build().toString();

            request = new Request.Builder().url(url).build();

            try {
                response = client.newCall(request).execute();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (response.body() != null) {
                try {
                    responseResult = response.body().string();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return responseResult;
        }

        @Override
        protected void onPostExecute(String s) {
            Log.d(TAG, "Response String:" + s);
            if (s.equals("success")) {
                Toast.makeText(getBaseContext(), R.string.successfully_added, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getBaseContext(), R.string.problem_join_family, Toast.LENGTH_SHORT).show();
            }

            Intent intent = new Intent(verifyAddToFamily.this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            verifyAddToFamily.this.finish();
        }
    }

    @SuppressLint("StaticFieldLeak")
    public class rejectInvite extends AsyncTask<String, String, String> {

        String responseResult;

        @Override
        protected String doInBackground(String... strings) {
            String id = strings[0];
            String num = strings[1];

            client = new OkHttpClient.Builder().connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build();

            urlBuilder = Objects.requireNonNull(HttpUrl.parse(new ServerConfig().rejectInvite())).newBuilder();
            urlBuilder.addQueryParameter("familyID", id);
            urlBuilder.addQueryParameter("userNum", num);

            String url = urlBuilder.build().toString();

            request = new Request.Builder().url(url).build();

            try {
                response = client.newCall(request).execute();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (response.body() != null) {
                try {
                    responseResult = response.body().string();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return responseResult;
        }

        @Override
        protected void onPostExecute(String s) {
            Log.d(TAG, "Response String:" + s);
            if (s.equals("success")) {
                Toast.makeText(getBaseContext(), R.string.invite_rejected, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getBaseContext(), R.string.error_rejecting_invite, Toast.LENGTH_SHORT).show();
            }

            Intent intent = new Intent(verifyAddToFamily.this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            verifyAddToFamily.this.finish();
        }
    }
}
