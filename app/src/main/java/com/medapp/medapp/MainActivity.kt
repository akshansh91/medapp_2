package com.medapp.medapp

import android.app.Activity
import android.app.Dialog
import android.app.job.JobInfo
import android.app.job.JobScheduler
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.RectF
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.PersistableBundle
import android.os.StrictMode
import android.provider.MediaStore
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.widget.Button
import android.widget.ImageView
import com.afollestad.materialdialogs.DialogAction
import com.afollestad.materialdialogs.MaterialDialog
import com.github.angads25.filepicker.controller.DialogSelectionListener
import com.github.angads25.filepicker.model.DialogConfigs
import com.github.angads25.filepicker.model.DialogProperties
import com.github.angads25.filepicker.view.FilePickerDialog
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.medapp.medapp.database.DB_HELPER
import com.medapp.medapp.services.*
import com.medapp.medapp.utils.individual_family_login
import com.medapp.medapp.utils.left_drawer
import com.mikepenz.materialdrawer.Drawer
import com.squareup.picasso.Picasso
import com.theartofdev.edmodo.cropper.CropImage
import com.zhihu.matisse.Matisse
import com.zhihu.matisse.MimeType
import com.zhihu.matisse.engine.impl.PicassoEngine
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.vudroid.core.DecodeServiceBase
import org.vudroid.pdfdroid.codec.PdfContext
import org.vudroid.pdfdroid.codec.PdfPage
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*

class MainActivity : AppCompatActivity() {
    private var drawerResult: Drawer? = null
    private var jobschedular: JobScheduler? = null
    private var jobschedularCode: Int = 1
    private var phoneNumber: String? = null
    private var toolbar: Toolbar? = null
    private var familyId: String? = null

    val TAG: String? = "Activity_Name"
    val REQUEST_IMAGE_CAPTURE = 1
    val REQUEST_CODE_FOR_GALLERY_CAPTURE = 2
    var photoFile: File? = null
    var progressDialog: Dialog? = null
    private var doubleBackToExitPressedOnce = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)

        Log.d(TAG, "Inside MainActivity")
        //onclick listener for open camera
        onclickListenerForOpenCamera()

        //starting the services here  . .
        val service_checkAddedtoFamily = Intent(this, checkAddedToFamily::class.java)
        startService(service_checkAddedtoFamily)
        val service_checkDocsToBeVerified = Intent(this, checkDocsToBeVerified::class.java)
        startService(service_checkDocsToBeVerified)

        //onclick listener for select image button
        attach_onclick_listener_to_add_photos_from_gallery()

        //onclick listener for select pdf files
        onclickListenerForSelectPdfFile()
        //get toolbar for drawer
        toolbar = findViewById(R.id.toolbar_tabs)

        //get phone number
        val loginInfo = applicationContext.getSharedPreferences("loginInfo", Context.MODE_PRIVATE)
        phoneNumber = loginInfo.getString("phoneNumber", "")

        //onclick listener for upload button
        //onclickListenerForUploadButton()

        //onclick listener for retrieve button
        onclickListenerForRetrieveButton()

        //on click permanent diseases button
        //onclickPermanentDiseasesButtton()

        //navigation drawer
        left_drawer(this, this@MainActivity, toolbar!!).createNavigationDrawer()

        //verify auto upload
        verifyAutoLoginInformation()

        //create Sqlite database
        DB_HELPER(this@MainActivity).writableDatabase

        //get job schedular service
        jobschedular = applicationContext.getSystemService(Context.JOB_SCHEDULER_SERVICE) as JobScheduler
        schedulTheJobForHealthGoals()
        schedulTheJobForHealthInsurance()
        setPreferencesForNutrition()
        schedulTheJobForNutrition()
        schedulTheJobForSyncNutritionOnline()
    }

    /*override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            return
        }
        this.doubleBackToExitPressedOnce = true
        Toast.makeText(this, "Press back again to exit", Toast.LENGTH_SHORT).show()
        Handler().postDelayed(Runnable { doubleBackToExitPressedOnce = false }, 2000)
    }*/

    //job schedular
    fun schedulTheJobForHealthGoals() {
        val builder = JobInfo.Builder(jobschedularCode, ComponentName(this@MainActivity, health_goals_services::class.java))
                .setPersisted(true)
                .setPeriodic(5000)
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED)
                .setRequiresCharging(false)
                .setRequiresDeviceIdle(false)

        val bundle = PersistableBundle()
        bundle.putString("key", "value")
        builder.setExtras(bundle)

        val s_response = jobschedular!!.schedule(builder.build())
        if (s_response <= 0) {
            //something goes wrong
        }
    }

    fun schedulTheJobForHealthInsurance() {
        val builder = JobInfo.Builder(jobschedularCode, ComponentName(this@MainActivity, health_insurance_service::class.java))
                .setPersisted(true)
                .setPeriodic(5000)
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED)
                .setRequiresCharging(false)
                .setRequiresDeviceIdle(false)

        val bundle = PersistableBundle()
        bundle.putString("key", "value")
        builder.setExtras(bundle)

        val s_response = jobschedular!!.schedule(builder.build())
        if (s_response <= 0) {
            //something goes wrong
        }
    }

    fun schedulTheJobForNutrition() {
        val builder = JobInfo.Builder(jobschedularCode, ComponentName(this@MainActivity, nutrition_service::class.java))
                .setPersisted(true)
                .setPeriodic(5000) //change to 1 hour
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED)
                .setRequiresCharging(false)
                .setRequiresDeviceIdle(false)

        val bundle = PersistableBundle()
        bundle.putString("key", "value")
        builder.setExtras(bundle)

        val s_response = jobschedular!!.schedule(builder.build())
        if (s_response <= 0) {
            //something goes wrong
        }
    }

    fun setPreferencesForNutrition() {
        val nutritionInfo = getSharedPreferences("nutrition", Context.MODE_PRIVATE)
        val editor = nutritionInfo.edit()
        editor.putString("breakFastTime_Hour", "7")
        editor.putString("lunchTime_Hour", "14") //TODO: change to 13
        editor.putString("DinnerTime_Hour", "20")
        editor.commit()
    }

    fun schedulTheJobForSyncNutritionOnline() {
        val builder = JobInfo.Builder(jobschedularCode, ComponentName(this@MainActivity, sync_nutrition_online::class.java))
                .setPersisted(true)
                .setPeriodic(5000)
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED)
                .setRequiresCharging(false)
                .setRequiresDeviceIdle(false)

        val bundle = PersistableBundle()
        bundle.putString("key", "value")
        builder.setExtras(bundle)

        val s_response = jobschedular!!.schedule(builder.build())
        if (s_response <= 0) {
            //something goes wrong
        }
    }


    //buttons on home screen
    /*fun onclickListenerForUploadButton(){
        findViewById<ImageView>(R.id.uploadButton).setOnClickListener{
            openModeOfUploadActivity()
        }
    }*/
    fun onclickListenerForRetrieveButton() {
        findViewById<Button>(R.id.retrieveButton).setOnClickListener {
            openHistoryActivity()
        }
    }

    /*fun onclickPermanentDiseasesButtton(){
        findViewById<Button>(R.id.permanentDiseasesButton).setOnClickListener{
            openPermanentDiseases()
        }
    }*/
    /*fun openModeOfUploadActivity(){
        val intent = Intent(this,MainActivity::class.java)
        startActivity(intent)
    }*/
    fun openHistoryActivity() {
        val intent = Intent(this, history_pickFamilyMember::class.java)
        startActivity(intent)
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
    }

    /*fun openPermanentDiseases(){
        val intent = Intent(this,permanentDiseaese::class.java)
        startActivity(intent)
    }
*/
    //verify auto login information
    fun verifyAutoLoginInformation() {
        val loginInfo = applicationContext.getSharedPreferences("loginInfo", Context.MODE_PRIVATE)

        if (loginInfo.contains("familyOrIndividual") == true) {

            //for family
            if (loginInfo.getString("familyOrIndividual", "").toString() == "f") {
                if (loginInfo.contains("phoneNumber") == true && loginInfo.contains("password") == true) {
                    val phoneNumber = loginInfo.getString("phoneNumber", "")
                    val password = loginInfo.getString("password", "")
                    individual_family_login(this@MainActivity).makeFamilyLoginApiRequest(phoneNumber, password)
                } else {
                    left_drawer(this, this@MainActivity, toolbar!!).makeUserLogOut()
                }
            }

            //for individual
            if (loginInfo.getString("familyOrIndividual", "").toString() == "i") {
                if (loginInfo.contains("phoneNumber") == true && loginInfo.contains("password") == true) {
                    val phoneNumber = loginInfo.getString("phoneNumber", "")
                    val password = loginInfo.getString("password", "")
                    individual_family_login(this@MainActivity).makeLoginApiRequest(phoneNumber, password)
                } else {
                    left_drawer(this, this@MainActivity, toolbar!!).makeUserLogOut()
                }
            }

            //for security
            if (loginInfo.getString("familyOrIndividual", "").toString() != "i" && loginInfo.getString("familyOrIndividual", "").toString() != "f") {
                left_drawer(this, this@MainActivity, toolbar!!).makeUserLogOut()
            }

        } else {
            left_drawer(this, this@MainActivity, toolbar!!).makeUserLogOut()
        }
    }


    //camera
    fun onclickListenerForOpenCamera() {
        findViewById<ImageView>(R.id.openCamera).setOnClickListener {
            get_permissions_camera()
        }
    }

    fun _openCamera() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            try {
                photoFile = createImageFile()
            } catch (ex: Exception) {
                Log.d("Errors__", ex.toString())
            }
            if (photoFile != null) {
                val builder: StrictMode.VmPolicy.Builder = StrictMode.VmPolicy.Builder()
                StrictMode.setVmPolicy(builder.build())
                val photoURI: Uri = Uri.fromFile(photoFile!!)
                Log.d("URI__Profile", photoURI.toString())
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
            }
        }
    }

    fun createImageFile(): File {
        val mCurrentPhotoPath: String
        val imageFileName: String = "camera"
        val storageDir: File = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        )

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath()
        return image
    }

    //file selector
    fun onclickListenerForSelectPdfFile() {
        findViewById<ImageView>(R.id.selectPdfFile).setOnClickListener {
            get_permissions_fileExplorer()
        }
    }

    fun openFileSelector() {
        val properties = DialogProperties()

        properties.selection_mode = DialogConfigs.MULTI_MODE;
        properties.selection_type = DialogConfigs.FILE_SELECT;
        properties.root = File(DialogConfigs.DEFAULT_DIR);
        properties.error_dir = File(DialogConfigs.DEFAULT_DIR);
        properties.offset = File(DialogConfigs.DEFAULT_DIR);
        properties.extensions = null;

        val dialog: FilePickerDialog = FilePickerDialog(this@MainActivity, properties)
        dialog.setTitle("Select a File")

        dialog.setDialogSelectionListener(object : DialogSelectionListener {
            override fun onSelectedFilePaths(files: Array<out String>?) {
                convertPdfToImages(files!!)
            }
        })

        dialog.show()
    }

    fun convertPdfToImages(files: Array<out String>) {
        showProcessProgress()

        doAsync {
            var uriList: MutableList<Uri>? = mutableListOf()
            val no_of_files = files.size
            var counter = 0

            while (counter < no_of_files) {
                var pdfFile = File(files[counter])

                val decodeService = DecodeServiceBase(PdfContext())
                decodeService.setContentResolver(applicationContext.getContentResolver())
                decodeService.open(Uri.fromFile(pdfFile))
                val pageCount: Int = decodeService.getPageCount()
                var i = 0
                while (i < pageCount) {
                    val page: PdfPage = decodeService.getPage(i) as PdfPage
                    val rectF = RectF(0.toFloat(), 0.toFloat(), 1.toFloat(), 1.toFloat())

                    // do a fit center to 1920x1080
                    val scaleBy = 1
                    val with: Int = (page.getWidth() * scaleBy)
                    val height: Int = (page.getHeight() * scaleBy)

                    val bitmap: Bitmap = page.renderBitmap(with, height, rectF)

                    try {
                        val outputFile = File(applicationContext.externalCacheDir,
                                System.currentTimeMillis().toString() + ".jpg")
                        val outputStream = FileOutputStream(outputFile)

                        // a bit long running
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream)

                        uriList!!.add(Uri.fromFile(outputFile))

                        outputStream.close()
                    } catch (e: IOException) {
                    }

                    i++
                }
                counter++
            }
            uiThread {
                progressDialog!!.hide()
                openPreview(uriList!!)
                Log.d("mess", "size: " + uriList.size + " " + uriList.toString())
            }
        }
    }

    //select image
    fun attach_onclick_listener_to_add_photos_from_gallery() {
        findViewById<ImageView>(R.id.selectImage).setOnClickListener {
            get_permissions_gallery()
        }
    }

    fun open_selector() {
        Matisse.from(this)
                .choose(MimeType.allOf())
                .countable(true)
                .maxSelectable(1)
                .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
                .thumbnailScale(0.85f)
                .imageEngine(PicassoEngine())
                .forResult(REQUEST_CODE_FOR_GALLERY_CAPTURE)
    }

    //activity results
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        try {
            if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
                if (photoFile != null) {
                    var uriOfImage = Uri.fromFile(photoFile)
                    Log.e("Main", "uri Main : " + uriOfImage.toString())

                    //start croper
                    CropImage.activity(uriOfImage)
                            .start(this)
                } else {
                    Log.e("Main", "Uri == NULL")
                }
            }
            if (requestCode == REQUEST_CODE_FOR_GALLERY_CAPTURE && resultCode == Activity.RESULT_OK) {
                var selected_images = Matisse.obtainResult(data)
                openPreview(selected_images!!)
            }

            //for croper
            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                val result: CropImage.ActivityResult = CropImage.getActivityResult(data)
                if (resultCode == RESULT_OK) {
                    doAsync {
                        val builder: StrictMode.VmPolicy.Builder = StrictMode.VmPolicy.Builder()
                        StrictMode.setVmPolicy(builder.build())
                        var resultUri: Uri = result.getUri()

                        //save cropped image for persisitance
                        val croppedImage = createImageFile() //empty
                        val outputStream = FileOutputStream(croppedImage)
                        // a bit long running
                        (Picasso.with(this@MainActivity)
                                .load(resultUri)
                                .get()
                                ).compress(Bitmap.CompressFormat.JPEG, 100, outputStream)

                        resultUri = Uri.fromFile(croppedImage)
                        outputStream.close()
                        uiThread {
                            //add to mu list
                            val mu_list = ArrayList<Uri>(1)
                            mu_list.add(resultUri)
                            Log.d("Main", "camera uri" + resultUri.toString())
                            openPreview(mu_list)
                        }
                    }
                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    val error: Exception = result.getError();
                }
            }
        } catch (e: Exception) {
            Log.d("Main", e.toString())
        }
    }

    //preview
    fun openPreview(list: MutableList<Uri>) {
        val _object = list
        val i = Intent(this, typeOfDocument::class.java)
        val args = Bundle()
        args.putSerializable("ARRAYLIST", _object as java.io.Serializable)
        i.putExtra("BUNDLE", args)
        startActivity(i)
        finish()
    }

    //get permissions
    //Camera
    fun get_permissions_camera() {
        if (ContextCompat.checkSelfPermission(this@MainActivity, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

            MaterialDialog.Builder(this@MainActivity)
                    .title("Camera permission")
                    .content("Camera permissions are required for opening Camera")
                    .negativeText("Cancel")
                    .onNegative(object : MaterialDialog.SingleButtonCallback {
                        override fun onClick(dialog: MaterialDialog, which: DialogAction) {

                        }
                    })
                    .positiveText("Give Permissions")
                    .onPositive(object : MaterialDialog.SingleButtonCallback {
                        override fun onClick(dialog: MaterialDialog, which: DialogAction) {
                            getPermissionsUsingDexter_camera(
                                    android.Manifest.permission.CAMERA
                            )
                        }
                    })
                    .show()

        } else {
            _openCamera()
        }
    }

    fun getPermissionsUsingDexter_camera(permissionString: String) {
        Dexter.withActivity(this)
                .withPermissions(
                        permissionString
                ).withListener(object : MultiplePermissionsListener {
                    override fun onPermissionRationaleShouldBeShown(permissions: MutableList<PermissionRequest>?, token: PermissionToken?) {

                    }

                    override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                        if (report!!.areAllPermissionsGranted() == true) {
                            _openCamera()

                            Log.d("mess", "permission given")
                        } else {
                            Log.d("mess", "permission not granted")
                        }
                    }
                })
                .check()
    }

    //gallery
    fun get_permissions_gallery() {
        if (ContextCompat.checkSelfPermission(this@MainActivity, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            MaterialDialog.Builder(this@MainActivity)
                    .title("Storage permission")
                    .content("Storage permissions are required for opening the Gallery")
                    .negativeText("Cancel")
                    .onNegative(object : MaterialDialog.SingleButtonCallback {
                        override fun onClick(dialog: MaterialDialog, which: DialogAction) {

                        }
                    })
                    .positiveText("Give Permissions")
                    .onPositive(object : MaterialDialog.SingleButtonCallback {
                        override fun onClick(dialog: MaterialDialog, which: DialogAction) {
                            getPermissionsUsingDexter_gallery(
                                    android.Manifest.permission.READ_EXTERNAL_STORAGE
                            )
                        }
                    })
                    .show()

        } else {
            open_selector()
        }

    }

    fun getPermissionsUsingDexter_gallery(permissionString: String) {
        Dexter.withActivity(this)
                .withPermissions(
                        permissionString
                ).withListener(object : MultiplePermissionsListener {
                    override fun onPermissionRationaleShouldBeShown(permissions: MutableList<PermissionRequest>?, token: PermissionToken?) {

                    }

                    override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                        if (report!!.areAllPermissionsGranted() == true) {
                            open_selector()

                            Log.d("mess", "permission given")
                        } else {
                            Log.d("mess", "permission not granted")
                        }
                    }
                })
                .check()
    }

    //file exploer
    fun get_permissions_fileExplorer() {
        if (ContextCompat.checkSelfPermission(this@MainActivity, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            MaterialDialog.Builder(this@MainActivity)
                    .title("Storage permission")
                    .content("Storage access permissions are required for opening File Explorer")
                    .negativeText("Cancel")
                    .onNegative(object : MaterialDialog.SingleButtonCallback {
                        override fun onClick(dialog: MaterialDialog, which: DialogAction) {

                        }
                    })
                    .positiveText("Give Permissions")
                    .onPositive(object : MaterialDialog.SingleButtonCallback {
                        override fun onClick(dialog: MaterialDialog, which: DialogAction) {
                            getPermissionsUsingDexter_fileExplores(
                                    android.Manifest.permission.READ_EXTERNAL_STORAGE
                            )
                        }
                    })
                    .show()

        } else {
            openFileSelector()
        }

    }

    fun getPermissionsUsingDexter_fileExplores(permissionString: String) {
        Dexter.withActivity(this)
                .withPermissions(
                        permissionString
                ).withListener(object : MultiplePermissionsListener {
                    override fun onPermissionRationaleShouldBeShown(permissions: MutableList<PermissionRequest>?, token: PermissionToken?) {

                    }

                    override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                        if (report!!.areAllPermissionsGranted() == true) {
                            openFileSelector()

                            Log.d("mess", "permission given")
                        } else {
                            Log.d("mess", "permission not granted")
                        }
                    }
                })
                .check()
    }

    //progress bar
    fun showProcessProgress() {
        progressDialog = MaterialDialog.Builder(this)
                .title("Please Wait")
                .content("Converting Pdf to Images")
                .progress(true, 0)
                .show()
    }

}
