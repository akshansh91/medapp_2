package com.medapp.medapp;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.medapp.medapp.addNewFamilyMemberViaFamilyTreeSection.phoneNumberOfNewFamilyMember;
import com.medapp.medapp.config.ServerConfig;
import com.medapp.medapp.utils.left_drawer;
import com.medapp.medapp.utils.utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.blox.graphview.BaseGraphAdapter;
import de.blox.graphview.Graph;
import de.blox.graphview.GraphView;
import de.blox.graphview.Node;
import de.blox.graphview.tree.BuchheimWalkerAlgorithm;
import de.blox.graphview.tree.BuchheimWalkerConfiguration;

public class familyTree_2 extends AppCompatActivity {

    GraphView graphView;
    //int nodeCount = 1;
    ArrayList<String> family_names;
    ArrayList<String> family_relation;
    ArrayList<String> family_profile_pic;
    ArrayList<Node> nodeArrayList;
    int size;
    Button addMembers;
    Toolbar toolbar;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(familyTree_2.this, MainActivity.class));
        overridePendingTransition(R.anim.fade_out, R.anim.fade_in);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_family_tree_2);

        try {
            Log.d("Check_familyTree2", "Inside onCreate()");

            toolbar = findViewById(R.id.familyTreeToolBar);
            graphView = findViewById(R.id.graph);
            addMembers = findViewById(R.id.addMember);

            new left_drawer(this, familyTree_2.this, toolbar).createNavigationDrawer();
            nodeArrayList = new ArrayList<>();

            Intent i = getIntent();
            family_names = i.getStringArrayListExtra("familyNames");
            family_relation = i.getStringArrayListExtra("familyRelation");
            family_profile_pic = i.getStringArrayListExtra("familyProfilePic");

            family_names.add(0, "");
            family_relation.add(0, "");
            family_profile_pic.add(0, "");

            size = family_names.size();

            Log.d("familyTree__", "Size: " + size);

            Graph graph = new Graph();

            addMembers.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(familyTree_2.this, phoneNumberOfNewFamilyMember.class);
                    startActivity(i);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });

            for (int j = 0; j < size; j++) {
                String nodeName = "node_" + j;
                Log.d("familyTree__", "Node name: " + nodeName);
                nodeArrayList.add(new Node(nodeName));
            }

            try {
                if (size == 2) {
                    //for just single element. . .
                    graph.addEdge(nodeArrayList.get(0), nodeArrayList.get(1));
                } else if (size == 3) {
                    graph.addEdge(nodeArrayList.get(0), nodeArrayList.get(1));
                    graph.addEdge(nodeArrayList.get(1), nodeArrayList.get(2));
                    graph.addEdge(nodeArrayList.get(1), nodeArrayList.get(3));
                } else if (size == 4) {
                    graph.addEdge(nodeArrayList.get(0), nodeArrayList.get(1));
                    graph.addEdge(nodeArrayList.get(1), nodeArrayList.get(2));
                    graph.addEdge(nodeArrayList.get(1), nodeArrayList.get(3));
                    graph.addEdge(nodeArrayList.get(2), nodeArrayList.get(4));
                } else if (size == 5) {
                    graph.addEdge(nodeArrayList.get(0), nodeArrayList.get(1));
                    graph.addEdge(nodeArrayList.get(1), nodeArrayList.get(2));
                    graph.addEdge(nodeArrayList.get(1), nodeArrayList.get(3));
                    graph.addEdge(nodeArrayList.get(2), nodeArrayList.get(4));
                    graph.addEdge(nodeArrayList.get(3), nodeArrayList.get(5));
                } else if (size == 6) {
                    graph.addEdge(nodeArrayList.get(0), nodeArrayList.get(1));
                    graph.addEdge(nodeArrayList.get(1), nodeArrayList.get(2));
                    graph.addEdge(nodeArrayList.get(1), nodeArrayList.get(3));
                    graph.addEdge(nodeArrayList.get(2), nodeArrayList.get(4));
                    graph.addEdge(nodeArrayList.get(2), nodeArrayList.get(5));
                    graph.addEdge(nodeArrayList.get(3), nodeArrayList.get(6));
                } else if (size == 7) {
                    graph.addEdge(nodeArrayList.get(0), nodeArrayList.get(1));
                    graph.addEdge(nodeArrayList.get(1), nodeArrayList.get(2));
                    graph.addEdge(nodeArrayList.get(1), nodeArrayList.get(3));
                    graph.addEdge(nodeArrayList.get(2), nodeArrayList.get(4));
                    graph.addEdge(nodeArrayList.get(2), nodeArrayList.get(5));
                    graph.addEdge(nodeArrayList.get(3), nodeArrayList.get(6));
                    graph.addEdge(nodeArrayList.get(3), nodeArrayList.get(7));
                } else if (size == 8) {
                    graph.addEdge(nodeArrayList.get(0), nodeArrayList.get(1));
                    graph.addEdge(nodeArrayList.get(1), nodeArrayList.get(2));
                    graph.addEdge(nodeArrayList.get(1), nodeArrayList.get(3));
                    graph.addEdge(nodeArrayList.get(2), nodeArrayList.get(4));
                    graph.addEdge(nodeArrayList.get(2), nodeArrayList.get(5));
                    graph.addEdge(nodeArrayList.get(3), nodeArrayList.get(6));
                    graph.addEdge(nodeArrayList.get(3), nodeArrayList.get(7));
                    graph.addEdge(nodeArrayList.get(4), nodeArrayList.get(8));
                } else if (size == 9) {
                    graph.addEdge(nodeArrayList.get(0), nodeArrayList.get(1));
                    graph.addEdge(nodeArrayList.get(1), nodeArrayList.get(2));
                    graph.addEdge(nodeArrayList.get(1), nodeArrayList.get(3));
                    graph.addEdge(nodeArrayList.get(2), nodeArrayList.get(4));
                    graph.addEdge(nodeArrayList.get(2), nodeArrayList.get(5));
                    graph.addEdge(nodeArrayList.get(3), nodeArrayList.get(6));
                    graph.addEdge(nodeArrayList.get(3), nodeArrayList.get(7));
                    graph.addEdge(nodeArrayList.get(4), nodeArrayList.get(8));
                    graph.addEdge(nodeArrayList.get(4), nodeArrayList.get(9));
                } else if (size == 10) {
                    graph.addEdge(nodeArrayList.get(0), nodeArrayList.get(1));
                    graph.addEdge(nodeArrayList.get(1), nodeArrayList.get(2));
                    graph.addEdge(nodeArrayList.get(1), nodeArrayList.get(3));
                    graph.addEdge(nodeArrayList.get(2), nodeArrayList.get(4));
                    graph.addEdge(nodeArrayList.get(2), nodeArrayList.get(5));
                    graph.addEdge(nodeArrayList.get(3), nodeArrayList.get(6));
                    graph.addEdge(nodeArrayList.get(3), nodeArrayList.get(7));
                    graph.addEdge(nodeArrayList.get(4), nodeArrayList.get(8));
                    graph.addEdge(nodeArrayList.get(4), nodeArrayList.get(9));
                    graph.addEdge(nodeArrayList.get(5), nodeArrayList.get(10));
                } else if (size == 11) {
                    graph.addEdge(nodeArrayList.get(0), nodeArrayList.get(1));
                    graph.addEdge(nodeArrayList.get(1), nodeArrayList.get(2));
                    graph.addEdge(nodeArrayList.get(1), nodeArrayList.get(3));
                    graph.addEdge(nodeArrayList.get(2), nodeArrayList.get(4));
                    graph.addEdge(nodeArrayList.get(2), nodeArrayList.get(5));
                    graph.addEdge(nodeArrayList.get(3), nodeArrayList.get(6));
                    graph.addEdge(nodeArrayList.get(3), nodeArrayList.get(7));
                    graph.addEdge(nodeArrayList.get(4), nodeArrayList.get(8));
                    graph.addEdge(nodeArrayList.get(4), nodeArrayList.get(9));
                    graph.addEdge(nodeArrayList.get(5), nodeArrayList.get(10));
                    graph.addEdge(nodeArrayList.get(5), nodeArrayList.get(11));
                } else if (size == 12) {
                    graph.addEdge(nodeArrayList.get(0), nodeArrayList.get(1));
                    graph.addEdge(nodeArrayList.get(1), nodeArrayList.get(2));
                    graph.addEdge(nodeArrayList.get(1), nodeArrayList.get(3));
                    graph.addEdge(nodeArrayList.get(2), nodeArrayList.get(4));
                    graph.addEdge(nodeArrayList.get(2), nodeArrayList.get(5));
                    graph.addEdge(nodeArrayList.get(3), nodeArrayList.get(6));
                    graph.addEdge(nodeArrayList.get(3), nodeArrayList.get(7));
                    graph.addEdge(nodeArrayList.get(4), nodeArrayList.get(8));
                    graph.addEdge(nodeArrayList.get(4), nodeArrayList.get(9));
                    graph.addEdge(nodeArrayList.get(5), nodeArrayList.get(10));
                    graph.addEdge(nodeArrayList.get(5), nodeArrayList.get(11));
                    graph.addEdge(nodeArrayList.get(6), nodeArrayList.get(12));
                } else if (size == 13) {
                    graph.addEdge(nodeArrayList.get(0), nodeArrayList.get(1));
                    graph.addEdge(nodeArrayList.get(1), nodeArrayList.get(2));
                    graph.addEdge(nodeArrayList.get(1), nodeArrayList.get(3));
                    graph.addEdge(nodeArrayList.get(2), nodeArrayList.get(4));
                    graph.addEdge(nodeArrayList.get(2), nodeArrayList.get(5));
                    graph.addEdge(nodeArrayList.get(3), nodeArrayList.get(6));
                    graph.addEdge(nodeArrayList.get(3), nodeArrayList.get(7));
                    graph.addEdge(nodeArrayList.get(4), nodeArrayList.get(8));
                    graph.addEdge(nodeArrayList.get(4), nodeArrayList.get(9));
                    graph.addEdge(nodeArrayList.get(5), nodeArrayList.get(10));
                    graph.addEdge(nodeArrayList.get(5), nodeArrayList.get(11));
                    graph.addEdge(nodeArrayList.get(6), nodeArrayList.get(12));
                    graph.addEdge(nodeArrayList.get(6), nodeArrayList.get(13));
                } else if (size == 14) {
                    graph.addEdge(nodeArrayList.get(0), nodeArrayList.get(1));
                    graph.addEdge(nodeArrayList.get(1), nodeArrayList.get(2));
                    graph.addEdge(nodeArrayList.get(1), nodeArrayList.get(3));
                    graph.addEdge(nodeArrayList.get(2), nodeArrayList.get(4));
                    graph.addEdge(nodeArrayList.get(2), nodeArrayList.get(5));
                    graph.addEdge(nodeArrayList.get(3), nodeArrayList.get(6));
                    graph.addEdge(nodeArrayList.get(3), nodeArrayList.get(7));
                    graph.addEdge(nodeArrayList.get(4), nodeArrayList.get(8));
                    graph.addEdge(nodeArrayList.get(4), nodeArrayList.get(9));
                    graph.addEdge(nodeArrayList.get(5), nodeArrayList.get(10));
                    graph.addEdge(nodeArrayList.get(5), nodeArrayList.get(11));
                    graph.addEdge(nodeArrayList.get(6), nodeArrayList.get(12));
                    graph.addEdge(nodeArrayList.get(6), nodeArrayList.get(13));
                    graph.addEdge(nodeArrayList.get(7), nodeArrayList.get(14));
                }
            } catch (Exception e) {
                Log.d("FamilyTree__", "Error: " + String.valueOf(e));
            }

            final BaseGraphAdapter<ViewHolder> adapter = new BaseGraphAdapter<ViewHolder>(familyTree_2.this, R.layout.nodetree, graph) {
                @NonNull
                @Override
                public ViewHolder onCreateViewHolder(View view) {
                    return new ViewHolder(view);
                }

                @SuppressLint("SetTextI18n")
                @Override
                public void onBindViewHolder(ViewHolder viewHolder, Object data, final int position) {
                    try {
                        if (family_relation.get(position).equals("") && position == 0) {
                            viewHolder.relation.setVisibility(View.GONE);
                            viewHolder.mTextView.setVisibility(View.GONE);
                        } else if (family_relation.get(position).equals("") && position == 1) {
                            viewHolder.relation.setText(R.string.admin);
                            viewHolder.mTextView.setText(family_names.get(position));
                        } else if (family_relation.get(position).equals("")) {
                            viewHolder.relation.setText(R.string.family_member);
                            viewHolder.mTextView.setText(family_names.get(position));
                        } else {
                            viewHolder.relation.setText(family_relation.get(position));
                            viewHolder.mTextView.setText(family_names.get(position));
                        }
                    } catch (Exception e) {
                        Log.d("familyTree__", "on bind... Exception: " + e);
                    }
                    /*if (family_relation.get(position).equals("") && position == 0) {
                        viewHolder.relation.setText("Admin");
                    } else if (family_relation.get(position).equals("")) {
                        viewHolder.relation.setText("Family Member");
                    } else {
                        viewHolder.relation.setText(family_relation.get(position));
                    }*/
                    //viewHolder.circleImageView.setImageDrawable(R.drawable.cloud_upload_3);

                    if (family_profile_pic.get(position).equals("")) {
                        try {
                            Log.d("Check_familyTree2", "Inside onBindViewHolder()");
                            if (position != 0) {
                                Picasso.with(familyTree_2.this)
                                        .load(R.drawable.icon_man)
                                        .transform(new utils().getCircularTransformation(familyTree_2.this))
                                        .fit().into(viewHolder.circleImageView);
                            } else {
                                Picasso.with(familyTree_2.this)
                                        .load(R.drawable.family_1)
                                        .transform(new utils().getCircularTransformation(familyTree_2.this))
                                        .fit().into(viewHolder.circleImageView);
                            }
                        } catch (Exception e) {
                            Log.d("Check_familyTree2", "Inside onBindViewHolder() ... Exception: " + e);
                        }
                    } else {
                        Picasso.with(familyTree_2.this).load(new ServerConfig()
                                .getRootAddress() + "upload/" + family_profile_pic.get(position) + "?notCache=" + family_profile_pic.get(position))
                                .transform(new utils().getCircularTransformation(familyTree_2.this))
                                .fit().into(viewHolder.circleImageView);
                    }
                }
            };

            graphView.setAdapter(adapter);

            final BuchheimWalkerConfiguration configuration = new BuchheimWalkerConfiguration.Builder()
                    .setSiblingSeparation(40)
                    .setLevelSeparation(80)
                    .setSubtreeSeparation(60)
                    .setOrientation(BuchheimWalkerConfiguration.ORIENTATION_TOP_BOTTOM)
                    .build();
            adapter.setAlgorithm(new BuchheimWalkerAlgorithm(configuration));
        } catch (Exception e) {
            Log.d("Check_familyTree2", "Inside onCreate()... Exception: " + e);
        }
    }

    class ViewHolder {
        TextView mTextView, relation;
        ImageView circleImageView;

        ViewHolder(View view) {
            mTextView = view.findViewById(R.id.text);
            circleImageView = view.findViewById(R.id.pic);
            relation = view.findViewById(R.id.relation);
        }
    }
}