package com.medapp.medapp.loginSignup

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.Spinner
import com.medapp.medapp.R
import com.medapp.medapp.config.ServerConfig
import com.muddzdev.styleabletoastlibrary.StyleableToast
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.util.concurrent.TimeUnit

class family_signup : AppCompatActivity() {
    private var type: String? = "i"
    private var phoneNumber: String? = null
    private var country_code: String? = null
    private var fullName: String? = null
    private var password: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        try {
            Log.d("Check_family_signup", "Inside onCreate")
            super.onCreate(savedInstanceState)
            setContentView(R.layout.activity_family_signup)
            Log.d("ActivityName", "family_signup")

            //get data from intent
            val i_p = getIntent()
            type = i_p.getStringExtra("type")

            //skipActivity()
            onNextClicked()
        } catch (e: Exception) {
            Log.d("Check_family_signup", "Inside onCreate()...Exception: $e")
        }
    }

    override fun onBackPressed() {
        val i = Intent(this, individualOrFamily::class.java)
        startActivity(i)
        finish()
    }

    fun skipActivity() {
        val i = Intent(this, family_signup_2::class.java)
        i.putExtra("noOfFamilyMembers", "1")
        //i.putExtra("type", type)
        startActivity(i)
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
    }

    fun onNextClicked() {
        try {
            Log.d("Check_family_signup", "Inside onNextClicked()")
            val spinner = findViewById<Spinner>(R.id.country_code_spinner2)
            /*findViewById<Button>(R.id.nextButton).setOnClickListener {
                //get value from spinner
                val noOfFamilyMemberSpinner = findViewById<Spinner>(R.id.noOfFamilyMember)
                val noOfFamilyMembers = noOfFamilyMemberSpinner.selectedItem.toString()

                if (noOfFamilyMembers.length < 3) {
                    val i = Intent(this, family_signup_2::class.java)
                    i.putExtra("noOfFamilyMembers", noOfFamilyMembers)
                    startActivity(i)
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                } else {
                    showError()
                }
            }*/

            findViewById<Button>(R.id.nextButton).setOnClickListener {
                fullName = findViewById<EditText>(R.id.fullName).getText().toString()
                phoneNumber = findViewById<EditText>(R.id.phoneNumber).getText().toString()
                country_code = spinner.getSelectedItem().toString()
                password = findViewById<EditText>(R.id.password).getText().toString()

                if (phoneNumber!!.length == 10 && password!!.length > 0 && country_code!!.length > 0 && fullName!!.length > 0) {
                    isAccountExist()
                } else {
                    if (phoneNumber!!.length < 10) {
                        showErrorMsg(getString(R.string.enter_your_phone_number), ContextCompat.getColor(this, R.color.red))
                    } else {
                        showErrorMsg(getString(R.string.enter_your_password), ContextCompat.getColor(this, R.color.red))
                    }
                }

                /*if (fullName.length > 0) {
                    val i = Intent(this, signup_2::class.java)
                    i.putExtra("fullName", fullName)
                    startActivity(i)
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                } else {
                    showError()
                }*/
                /*if (phoneNumber!!.length > 0) {
                    val i = Intent(this, signup_2::class.java)
                    i.putExtra("phoneNumber", phoneNumber)
                    i.putExtra("countryCode", country_code.toString())
                    startActivity(i)
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                } else {
                    showError()
                }*/
            }
        } catch (e: Exception) {
            Log.d("Check_family_signup", "Inside onNextClicked()...Exception: $e")
        }
    }

    private fun isAccountExist() {
        doAsync {
            try {
                Log.d("Check_family_signup", "Inside isAccountExist()")
                val client: OkHttpClient = OkHttpClient.Builder()
                        .connectTimeout(60, TimeUnit.SECONDS)
                        .writeTimeout(60, TimeUnit.SECONDS)
                        .readTimeout(60, TimeUnit.SECONDS)
                        .build()

                val urlBuilder = HttpUrl.parse(ServerConfig().isAccountExist_resetPassword())!!.newBuilder()
                urlBuilder.addQueryParameter("phoneNumber", phoneNumber)
                urlBuilder.addQueryParameter("type", type)

                val url = urlBuilder.build().toString()

                val request = Request.Builder()
                        .url(url)
                        .build()

                val response: Response = client.newCall(request).execute()
                val responce_string = response.body()!!.string()

                uiThread {
                    Log.d("SignUp", responce_string.toString())

                    if (responce_string == "exist") {
                        //startPhoneVerification(phoneNumber!!)
                        val st = StyleableToast.Builder(this@family_signup)
                                .text(getString(R.string.account_already_exist))
                                .textColor(Color.WHITE)
                                .backgroundColor(Color.RED)
                                .build()
                        st.show()

                        val i = Intent(this@family_signup, individualOrFamily::class.java)
                        i.putExtra("phoneNumber", phoneNumber)
                        i.putExtra("countryCode", country_code.toString())
                        startActivity(i)
                        finish()
                    } else {

                        var result_full_names_array: ArrayList<String> = ArrayList()
                        result_full_names_array.add(fullName.toString())

                        var result_phone_number_array: java.util.ArrayList<String> = java.util.ArrayList()
                        result_phone_number_array.add(phoneNumber.toString())


                        val args = Bundle()
                        args.putSerializable("ARRAYLIST_fullNames", result_full_names_array as java.io.Serializable)
                        args.putSerializable("ARRAYLIST_phoneNumbers", result_phone_number_array as java.io.Serializable)

                        val i = Intent(this@family_signup, family_signup_2::class.java)
                        i.putExtra("noOfFamilyMembers", "1")
                        i.putExtra("countryCode", country_code.toString())
                        i.putExtra("password", password)
                        i.putExtra("BUNDLE", args)

                        startActivity(i)
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                    }
                }
            } catch (e: Exception) {
                Log.d("Check_family_signup", "Inside isAccountExist()...Exception: $e")
            }
        }
    }


    fun showError() {
        val st = StyleableToast.Builder(this)
                .text(getString(R.string.enter_valid_values))
                .textColor(Color.WHITE)
                .backgroundColor(Color.RED)
                .build()
        st.show()
    }

    fun showErrorMsg(msg_text: String, color: Int) {
        val st = StyleableToast.Builder(this)
                .text(msg_text)
                .textColor(Color.WHITE)
                .backgroundColor(color)
                .build()
        st.show()
    }
}
