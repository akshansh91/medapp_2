package com.medapp.medapp.loginSignup

import android.content.Intent
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.constraint.ConstraintSet
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.medapp.medapp.R
import org.jetbrains.anko.backgroundDrawable
import org.jetbrains.anko.find
import org.jetbrains.anko.padding
import org.jetbrains.anko.textColor
import java.util.*

class family_signup_3 : AppCompatActivity() {
    private var c_layout: ViewGroup? = null
    private var noOfFamilyMembers: Int = 0
    private var phoneNumber: String? = null
    private var country_code: String? = null
    private var password: String? = null
    private var fullNameList: java.util.ArrayList<*>? = null
    private var phoneNumberList: java.util.ArrayList<*>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_family_signup_3)
        Log.d("ActivityName", "family_signup_3")
        //get subpart_c_layout
        c_layout = findViewById(R.id.subparent_c_layout)

        //get info from previous activity
        val intent = intent
        val args = intent.getBundleExtra("BUNDLE")
        fullNameList = (args.getSerializable("ARRAYLIST_fullNames")) as java.util.ArrayList<*>
        phoneNumberList = (args.getSerializable("ARRAYLIST_phoneNumbers")) as java.util.ArrayList<*>

        password = intent.getStringExtra("password").toString()

        noOfFamilyMembers = intent.getStringExtra("noOfFamilyMembers").toInt()
        country_code = intent.getStringExtra("countryCode")

        //create email fields
        createEmailFieldsForEachFamilyMember()

        //on next clicked
        onNextClicked()

        skipThisStep()
    }


    fun createEmailFieldsForEachFamilyMember() {
        val size = noOfFamilyMembers
        var counter = 1
        while (counter <= size) {
            createEditText(counter, counter.toString())
            counter++
        }
    }

    fun createEditText(id: Int, personNumber: String) {
        val ed = EditText(this)
        ed.hint = getString(R.string.email_member) + personNumber + " )"
        ed.backgroundDrawable = ContextCompat.getDrawable(this@family_signup_3, R.drawable.login_field_background)
        ed.id = id
        ed.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(this, R.drawable.ic_email_white_24dp), null, null, null)
        ed.padding = 20
        ed.textColor = ContextCompat.getColor(this, R.color.white)
        ed.width = 725

        c_layout!!.addView(ed)

        val constraintSet = ConstraintSet()
        constraintSet.clone(c_layout as ConstraintLayout)
        constraintSet.connect(id, ConstraintSet.RIGHT, c_layout!!.id, ConstraintSet.RIGHT, 0)
        constraintSet.connect(id, ConstraintSet.LEFT, c_layout!!.id, ConstraintSet.LEFT, 0)
        if (id == 1) {
            constraintSet.connect(id, ConstraintSet.TOP, findViewById<TextView>(R.id.welcomeHeading).id, ConstraintSet.BOTTOM, 36)
        } else {
            constraintSet.connect(id, ConstraintSet.TOP, id - 1, ConstraintSet.BOTTOM, 16)
        }
        constraintSet.constrainDefaultHeight(id, 200)
        constraintSet.applyTo(c_layout!! as ConstraintLayout)
    }

    fun onNextClicked() {
        findViewById<Button>(R.id.nextButton).setOnClickListener {
            var result_email_array: ArrayList<String> = ArrayList()
            var counter = 1
            val size = noOfFamilyMembers
            while (counter <= size) {
                var g = find<EditText>(counter).getText().toString()
                if (g.length == 0) {
                    g = " "
                }
                result_email_array.add(g)
                counter++
            }

            val i = Intent(this, family_signup_4::class.java)
            i.putExtra("noOfFamilyMembers", noOfFamilyMembers.toString())
            i.putExtra("phoneNumber", phoneNumber)
            i.putExtra("countryCode", country_code.toString())
            val args = Bundle()
            args.putSerializable("ARRAYLIST_emails", result_email_array as java.io.Serializable)
            args.putSerializable("ARRAYLIST_fullNames", fullNameList as java.io.Serializable)
            i.putExtra("BUNDLE", args)
            startActivity(i)
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        }

        findViewById<ImageView>(R.id.skipIcon).setOnClickListener {
            var result_email_array: ArrayList<String> = ArrayList()
            var counter = 1
            val size = noOfFamilyMembers
            while (counter <= size) {
                var g = find<EditText>(counter).getText().toString()
                if (g.length == 0) {
                    g = " "
                }
                result_email_array.add(g)
                counter++
            }

            val i = Intent(this, family_signup_4::class.java)
            i.putExtra("noOfFamilyMembers", noOfFamilyMembers.toString())
            i.putExtra("phoneNumber", phoneNumber)
            i.putExtra("countryCode", country_code.toString())
            val args = Bundle()
            args.putSerializable("ARRAYLIST_emails", result_email_array as java.io.Serializable)
            args.putSerializable("ARRAYLIST_fullNames", fullNameList as java.io.Serializable)
            i.putExtra("BUNDLE", args)
            startActivity(i)
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        }
        findViewById<TextView>(R.id.skipTextView).setOnClickListener {
            var result_email_array: ArrayList<String> = ArrayList()
            var counter = 1
            val size = noOfFamilyMembers
            while (counter <= size) {
                var g = find<EditText>(counter).getText().toString()
                if (g.length == 0) {
                    g = " "
                }
                result_email_array.add(g)
                counter++
            }

            val i = Intent(this, family_signup_4::class.java)
            i.putExtra("noOfFamilyMembers", noOfFamilyMembers.toString())
            i.putExtra("phoneNumber", phoneNumber)
            i.putExtra("countryCode", country_code.toString())
            val args = Bundle()
            args.putSerializable("ARRAYLIST_emails", result_email_array as java.io.Serializable)
            args.putSerializable("ARRAYLIST_fullNames", fullNameList as java.io.Serializable)
            i.putExtra("BUNDLE", args)
            startActivity(i)
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        }
    }

    fun skipThisStep() {
        var result_email_array: ArrayList<String> = ArrayList()
        var counter = 1
        val size = noOfFamilyMembers
        while (counter <= size) {
            var g = find<EditText>(counter).getText().toString()
            if (g.length == 0) {
                g = " "
            }
            result_email_array.add(g)
            counter++
        }

        val i = Intent(this, family_signup_4::class.java)
        i.putExtra("noOfFamilyMembers", noOfFamilyMembers.toString())
//        i.putExtra("phoneNumber", phoneNumber)
        i.putExtra("countryCode", country_code.toString())
        i.putExtra("password", password)

        val args = Bundle()
        args.putSerializable("ARRAYLIST_emails", result_email_array as java.io.Serializable)
        args.putSerializable("ARRAYLIST_fullNames", fullNameList as java.io.Serializable)
        args.putSerializable("ARRAYLIST_phoneNumbers", phoneNumberList as java.io.Serializable)
        i.putExtra("BUNDLE", args)
        startActivity(i)
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        finish()
    }
}
