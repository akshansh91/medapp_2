package com.medapp.medapp.loginSignup

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.ViewGroup
import com.medapp.medapp.R
import com.muddzdev.styleabletoastlibrary.StyleableToast
import java.util.*


class family_signup_6 : AppCompatActivity() {
    private var c_layout: ViewGroup? = null
    private var noOfFamilyMembers: Int = 0
    private var fullNameList: java.util.ArrayList<*>? = null
    private var emailList: java.util.ArrayList<*>? = null
    private var PhoneNumberList: java.util.ArrayList<*>? = null
    private var password: String? = null
    private var countryCode: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_family_signup_6)

        Log.d("ActivityName", "family_signup_6")
        //get subpart_c_layout
        c_layout = findViewById(R.id.subparent_c_layout)

        //get info from previous activity
        val intent = intent
        val args = intent.getBundleExtra("BUNDLE")
        fullNameList = (args.getSerializable("ARRAYLIST_fullNames")) as java.util.ArrayList<*>
        emailList = (args.getSerializable("ARRAYLIST_emails")) as java.util.ArrayList<*>
        PhoneNumberList = (args.getSerializable("ARRAYLIST_phoneNumbers")) as java.util.ArrayList<*>
        noOfFamilyMembers = intent.getStringExtra("noOfFamilyMembers").toInt()
        password = intent.getStringExtra("password").toString()
        countryCode = intent.getStringExtra("countryCode").toString()

        /*skip This Step*/
        skipThisStep()

        //create birth date input fields
        ////createBirthDatesFieldsForEachFamilyMember()

        //on next clicked
        ////onNextClicked()

        //on skip clicked
        ////onSkipClicked()

    }

    /*
    fun createBirthDatesFieldsForEachFamilyMember(){
        val size = noOfFamilyMembers
        var counter =1
        while(counter <= (size*4)) {
            createEditText(counter, counter+1,counter+2,counter+3)
            counter=counter+4
        }
    }

    fun createEditText(id_1:Int,id_2:Int,id_3:Int,id_4:Int){
        //create heading textview
        val tv_1= TextView(applicationContext)
        tv_1.id=id_1
        tv_1.setGravity(Gravity.CENTER_VERTICAL or Gravity.CENTER_HORIZONTAL)
        tv_1.padding=10
        tv_1.textColor= ContextCompat.getColor(this,R.color.white)
        tv_1.width= 600
        tv_1.setText("Member "+((id_1/4)+1).toString())

        c_layout!!.addView(tv_1)

        val constraintSet_1 = ConstraintSet()
        constraintSet_1.clone(c_layout as ConstraintLayout)
        constraintSet_1.connect(id_1, ConstraintSet.RIGHT, c_layout!!.id , ConstraintSet.RIGHT, 0)
        constraintSet_1.connect(id_1, ConstraintSet.LEFT, c_layout!!.id , ConstraintSet.LEFT, 0)
        if(id_1 == 1) {
            constraintSet_1.connect(id_1, ConstraintSet.TOP, findViewById<TextView>(R.id.welcomeHeading).id, ConstraintSet.BOTTOM, 16)
        }else{
            constraintSet_1.connect(id_1, ConstraintSet.TOP, id_1-3, ConstraintSet.BOTTOM, 16)
        }
        constraintSet_1.constrainDefaultHeight(id_1, 200)
        constraintSet_1.applyTo(c_layout!! as ConstraintLayout)

        //create month editext
        val ed_1= EditText(this)
        ed_1.hint="Month"
        ed_1.backgroundDrawable= ContextCompat.getDrawable(applicationContext, R.drawable.login_field_background)
        ed_1.id=id_2
        ed_1.setGravity(Gravity.CENTER_VERTICAL or Gravity.CENTER_HORIZONTAL)
        ed_1.inputType= InputType.TYPE_CLASS_NUMBER
        ed_1.padding=20
        ed_1.setFilters(arrayOf<InputFilter>(InputFilter.LengthFilter(2)))
        ed_1.textColor= ContextCompat.getColor(this,R.color.white)
        ed_1.width= 200

        c_layout!!.addView(ed_1)

        val constraintSet_2 = ConstraintSet()
        constraintSet_2.clone(c_layout as ConstraintLayout)
        constraintSet_2.connect(id_2, ConstraintSet.RIGHT, c_layout!!.id , ConstraintSet.RIGHT, 0)
        constraintSet_2.connect(id_2, ConstraintSet.LEFT, c_layout!!.id , ConstraintSet.LEFT, 0)
        constraintSet_2.connect(id_2, ConstraintSet.TOP, id_1, ConstraintSet.BOTTOM, 6)
        constraintSet_2.constrainDefaultHeight(id_2, 200)
        constraintSet_2.applyTo(c_layout!! as ConstraintLayout)

        //create Day editext
        val ed_2= EditText(this)
        ed_2.hint="Day"
        ed_2.backgroundDrawable= ContextCompat.getDrawable(applicationContext, R.drawable.login_field_background)
        ed_2.id=id_3
        ed_2.setGravity(Gravity.CENTER_VERTICAL or Gravity.CENTER_HORIZONTAL)
        ed_2.inputType= InputType.TYPE_CLASS_NUMBER
        ed_2.padding=20
        ed_2.setFilters(arrayOf<InputFilter>(InputFilter.LengthFilter(2)))
        ed_2.textColor= ContextCompat.getColor(this,R.color.white)
        ed_2.width= 200

        c_layout!!.addView(ed_2)

        val constraintSet_3 = ConstraintSet()
        constraintSet_3.clone(c_layout as ConstraintLayout)
        constraintSet_3.connect(id_3, ConstraintSet.RIGHT, id_2 , ConstraintSet.LEFT, 0)
        constraintSet_3.connect(id_3, ConstraintSet.TOP, id_2, ConstraintSet.TOP, 0)
        constraintSet_3.constrainDefaultHeight(id_3, 200)
        constraintSet_3.applyTo(c_layout!! as ConstraintLayout)

        //create Year editext
        val ed_3= EditText(this)
        ed_3.hint="Year"
        ed_3.backgroundDrawable= ContextCompat.getDrawable(applicationContext, R.drawable.login_field_background)
        ed_3.id=id_4
        ed_3.setGravity(Gravity.CENTER_VERTICAL or Gravity.CENTER_HORIZONTAL)
        ed_3.inputType= InputType.TYPE_CLASS_NUMBER
        ed_3.padding=20
        ed_3.setFilters(arrayOf<InputFilter>(InputFilter.LengthFilter(4)))
        ed_3.textColor= ContextCompat.getColor(this,R.color.white)
        ed_3.width= 200

        c_layout!!.addView(ed_3)

        val constraintSet_4 = ConstraintSet()
        constraintSet_4.clone(c_layout as ConstraintLayout)
        constraintSet_4.connect(id_4, ConstraintSet.LEFT, id_2 , ConstraintSet.RIGHT, 0)
        constraintSet_4.connect(id_4, ConstraintSet.TOP, id_2, ConstraintSet.TOP, 0)
        constraintSet_4.connect(id_4, ConstraintSet.BOTTOM, id_2, ConstraintSet.BOTTOM, 0)
        constraintSet_4.constrainDefaultHeight(id_4, 200)
        constraintSet_4.applyTo(c_layout!! as ConstraintLayout)

    }


    fun onNextClicked(){
        findViewById<Button>(R.id.nextButton).setOnClickListener {
            var c = 2
            val s = noOfFamilyMembers
            var allAreFields = true
            while (c <= (s*4)) {

                var g = find<EditText>(c).getText().toString()
                c = c+1
                var h = find<EditText>(c).getText().toString()
                c = c+1
                var k = find<EditText>(c).getText().toString()
                if (g.length == 0 || h.length == 0 || k.length == 0) {
                    allAreFields = false
                    break
                }
                c = c+2
            }

            if (allAreFields == true) {
                var result_day_array: ArrayList<String> = ArrayList()
                var result_month_array: ArrayList<String> = ArrayList()
                var result_year_array: ArrayList<String> = ArrayList()
                var counter = 2
                val size = noOfFamilyMembers
                while (counter <= (size*4)) {

                    var m = find<EditText>(counter).getText().toString()
                    var d = find<EditText>(counter+1).getText().toString()
                    var y = find<EditText>(counter+2).getText().toString()
                    Log.d("mess","day:"+d+"month"+m+"year"+y)
                    result_day_array.add(d)
                    result_month_array.add(m)
                    result_year_array.add(y)

                    counter = counter + 4
                }


                val i = Intent(this, family_signup_7::class.java)
                i.putExtra("noOfFamilyMembers", noOfFamilyMembers.toString())
                i.putExtra("password", password.toString())
                val args = Bundle()
                args.putSerializable("ARRAYLIST_phoneNumbers", PhoneNumberList as java.io.Serializable)
                args.putSerializable("ARRAYLIST_emails", emailList as java.io.Serializable)
                args.putSerializable("ARRAYLIST_fullNames", fullNameList as java.io.Serializable)
                args.putSerializable("ARRAYLIST_days", result_day_array as java.io.Serializable)
                args.putSerializable("ARRAYLIST_months", result_month_array as java.io.Serializable)
                args.putSerializable("ARRAYLIST_years", result_year_array as java.io.Serializable)
                i.putExtra("BUNDLE", args)
                startActivity(i)
            }else{
                showError()
            }
        }
    }

    fun onSkipClicked(){
        findViewById<TextView>(R.id.skipTextView).setOnClickListener {
            var c = 2
            val s = noOfFamilyMembers
            var allAreFields = true
            while (c <= (s*4)) {

                    find<EditText>(c).setText(" ")
                c = c+1
                    find<EditText>(c).setText(" ")
                c = c+1
                    find<EditText>(c).setText(" ")

                c = c+2
            }

            if (allAreFields == true) {
                var result_day_array: ArrayList<String> = ArrayList()
                var result_month_array: ArrayList<String> = ArrayList()
                var result_year_array: ArrayList<String> = ArrayList()
                var counter = 2
                val size = noOfFamilyMembers
                while (counter <= (size*4)) {

                    var m = find<EditText>(counter).getText().toString()
                    var d = find<EditText>(counter+1).getText().toString()
                    var y = find<EditText>(counter+2).getText().toString()
                    Log.d("mess","day:"+d+"month"+m+"year"+y)
                    result_day_array.add(d)
                    result_month_array.add(m)
                    result_year_array.add(y)

                    counter = counter + 4
                }


                val i = Intent(this, family_signup_7::class.java)
                i.putExtra("noOfFamilyMembers", noOfFamilyMembers.toString())
                i.putExtra("password", password.toString())
                val args = Bundle()
                args.putSerializable("ARRAYLIST_phoneNumbers", PhoneNumberList as java.io.Serializable)
                args.putSerializable("ARRAYLIST_emails", emailList as java.io.Serializable)
                args.putSerializable("ARRAYLIST_fullNames", fullNameList as java.io.Serializable)
                args.putSerializable("ARRAYLIST_days", result_day_array as java.io.Serializable)
                args.putSerializable("ARRAYLIST_months", result_month_array as java.io.Serializable)
                args.putSerializable("ARRAYLIST_years", result_year_array as java.io.Serializable)
                i.putExtra("BUNDLE", args)
                startActivity(i)
            }else{
                showError()
            }
        }
    }


  */

    fun showError() {
        val st = StyleableToast.Builder(this)
                .text(getString(R.string.enter_family_member_birth_date))
                .textColor(Color.WHITE)
                .backgroundColor(Color.RED)
                .build()
        st.show()
    }

    fun skipThisStep() {
        val result_day_array: ArrayList<String> = ArrayList()
        val result_month_array: ArrayList<String> = ArrayList()
        val result_year_array: ArrayList<String> = ArrayList()
        result_day_array.add("0")
        result_month_array.add("0")
        result_year_array.add("0000")

        val i = Intent(this, family_signup_7::class.java)
        i.putExtra("noOfFamilyMembers", noOfFamilyMembers.toString())
        i.putExtra("password", password.toString())
        val args = Bundle()
        args.putSerializable("ARRAYLIST_phoneNumbers", PhoneNumberList as java.io.Serializable?)
        args.putSerializable("ARRAYLIST_emails", emailList as java.io.Serializable?)
        args.putSerializable("ARRAYLIST_fullNames", fullNameList as java.io.Serializable?)
        args.putSerializable("ARRAYLIST_days", result_day_array as java.io.Serializable?)
        args.putSerializable("ARRAYLIST_months", result_month_array as java.io.Serializable?)
        args.putSerializable("ARRAYLIST_years", result_year_array as java.io.Serializable?)
        i.putExtra("BUNDLE", args)
        i.putExtra("countryCode", countryCode)
        startActivity(i)
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        finish()
    }

}
