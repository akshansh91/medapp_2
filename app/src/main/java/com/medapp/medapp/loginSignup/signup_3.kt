package com.medapp.medapp.loginSignup

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Window
import android.view.WindowManager
import android.widget.Button
import android.widget.EditText
import android.widget.Spinner
import com.medapp.medapp.R
import com.muddzdev.styleabletoastlibrary.StyleableToast

class signup_3 : AppCompatActivity() {
    //private var fullName: String? = null
    private var phoneNumber: String? = null
    private var email: String? = null
    private var country_code: String? = null
    private var userName: String? = null
    private var password: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup_3)
        Log.d("ActivityName", "signup_3")
        overridePendingTransition(R.xml.slide_in, R.xml.slide_out)

        //make status bar invisible
        val w: Window = getWindow()
        w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)

        //get fullName and phone number
        val p_i = getIntent()
        //fullName = p_i.getStringExtra("fullName")
        phoneNumber = p_i.getStringExtra("phoneNumber")
        email = p_i.getStringExtra("email")
        country_code = p_i.getStringExtra("countryCode").toString()
        userName = p_i.getStringExtra("fullName")
        password = p_i.getStringExtra("password")

        //onclick next button
        onClickNextButton()

        /*skip this step*/
        skipThisStep()
    }

    fun onClickNextButton() {
        findViewById<Button>(R.id.nextButton).setOnClickListener {
            val fullName = findViewById<EditText>(R.id.familyID).getText().toString()
            val password = findViewById<EditText>(R.id.password).getText().toString()
            val spinner = findViewById<Spinner>(R.id.country_code_spinner)
            //val country_code = spinner.getSelectedItem().toString()

            if (phoneNumber!!.length > 0 && password.length > 0 && country_code!!.length > 0) {
                val i = Intent(this, signup_4::class.java)
                i.putExtra("fullName", fullName)
                i.putExtra("phoneNumber", phoneNumber)
                i.putExtra("email", email)
                i.putExtra("countryCode", country_code.toString())
                i.putExtra("password", password)
                startActivity(i)
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            } else {
                showError()
            }
        }
    }

    fun showError() {
        val st = StyleableToast.Builder(this)
                .text(getString(R.string.enter_your_phone_number_and_password))
                .textColor(Color.WHITE)
                .backgroundColor(Color.RED)
                .build()
        st.show()
    }

    fun skipThisStep() {
        val i = Intent(this, signup_4::class.java)
        //i.putExtra("fullName", fullName)
        i.putExtra("phoneNumber", phoneNumber)
        i.putExtra("email", email)
        i.putExtra("countryCode", country_code.toString())
        i.putExtra("fullName", userName)
        i.putExtra("password", password)

        startActivity(i)
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        finish()
    }
}
