package com.medapp.medapp.loginSignup

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Button
import android.widget.EditText
import com.medapp.medapp.R
import com.muddzdev.styleabletoastlibrary.StyleableToast

class join_family_signup_3 : AppCompatActivity() {
    private var familyId: String? = null
    private var password: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_join_family_signup_3)
        Log.d("ActivityName", "join_family_signup_3")
        //get family id from intent
        val p_i = getIntent()
        familyId = p_i.getStringExtra("familyId")
        password = p_i.getStringExtra("password")

        //on next button clicked
        onNextClicked()
    }

    fun onNextClicked() {
        findViewById<Button>(R.id.nextButton).setOnClickListener {
            var fullName = findViewById<EditText>(R.id.fullName).getText().toString()
            var email = findViewById<EditText>(R.id.email).getText().toString()
            var phoneNumber = findViewById<EditText>(R.id.familyID).getText().toString()
            var day = findViewById<EditText>(R.id.day).getText().toString()
            var month = findViewById<EditText>(R.id.month).getText().toString()
            var year = findViewById<EditText>(R.id.year).getText().toString()

            if (fullName.length != 0) {
                if (email.length != 0) {
                    //don't do anything
                } else {
                    email = " "
                }
                if (phoneNumber.length != 10) {
                    if (day.length != 0 && month.length != 0 && year.length != 0) {
                        //don't do anything
                    } else {
                        day = " "
                        month = " "
                        year = " "
                    }
                    goToPhoneVerificationActivity(familyId!!, fullName, email, phoneNumber, password!!, day, month, year)

                } else {
                    showError(getString(R.string.enter_your_phone_number), Color.RED)
                }
            } else {
                showError(getString(R.string.enter_your_full_name), Color.RED)
            }
        }
    }

    fun goToPhoneVerificationActivity(f_id: String, fullName: String, email: String, phoneNumber: String, password: String, day: String, month: String, year: String) {
        //val i = Intent(this, join_family_signup_4::class.java)
        //directly creating the account without verifying the number. . . .
        //verification taking too much time . . . a lot

        val i = Intent(this, join_family_signup_5::class.java)
        i.putExtra("familyId", f_id)
        i.putExtra("fullName", fullName)
        i.putExtra("email", email)
        i.putExtra("phoneNumber", phoneNumber)
        i.putExtra("password", password)
        i.putExtra("day", day)
        i.putExtra("month", month)
        i.putExtra("year", year)

        startActivity(i)
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
    }

    fun showError(msg: String, color: Int) {
        val st = StyleableToast.Builder(this)
                .text(msg)
                .textColor(Color.WHITE)
                .backgroundColor(color)
                .build()
        st.show()
    }
}
