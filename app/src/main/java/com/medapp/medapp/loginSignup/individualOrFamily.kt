package com.medapp.medapp.loginSignup

import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.content.res.Resources
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.DisplayMetrics
import android.util.Log
import android.widget.Button
import com.medapp.medapp.R
import java.util.*

class individualOrFamily : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //for fullscreen
        /*window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)*/

        setContentView(R.layout.activity_individual_or_family)
        Log.d("ActivityName", "individual_or_family")
        //auto login
        autoLogin()

        checkLang()

        findViewById<Button>(R.id.individualButton).setOnClickListener {
            startIndividualLogin()
        }

        findViewById<Button>(R.id.familyButton).setOnClickListener {
            startFamilyLogin()
        }
    }

    private fun checkLang() {
        val language = applicationContext.getSharedPreferences("lang", Context.MODE_PRIVATE)
        val lang: String = language.getString("language", "")

        var myLocale = Locale(lang)
        var res: Resources = resources
        var dm: DisplayMetrics = res.displayMetrics
        var conf: Configuration = res.configuration
        conf.locale = myLocale
        res.updateConfiguration(conf, dm)
    }


    fun autoLogin() {
        val loginInfo = applicationContext.getSharedPreferences("loginInfo", Context.MODE_PRIVATE)
        if (loginInfo.contains("familyOrIndividual")) {
            if (loginInfo.getString("familyOrIndividual", null).toString() == "f") {
                startFamilyLogin()
            }
            if (loginInfo.getString("familyOrIndividual", null).toString() == "i") {
                startIndividualLogin()
            }
        } else {
            startIndividualLogin()
        }
    }

    override fun onBackPressed() {
        finishAffinity()
        finish()
    }

    fun startFamilyLogin() {
        startActivity(Intent(this, family_login::class.java))
        finish()
    }

    fun startIndividualLogin() {
        startActivity(Intent(this, login::class.java))
        finish()
    }
}
