package com.medapp.medapp.loginSignup

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Window
import android.view.WindowManager
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.medapp.medapp.R
import com.muddzdev.styleabletoastlibrary.StyleableToast

//entering DOB
class signup_4 : AppCompatActivity() {
    private var fullName: String? = null
    private var phoneNumber: String? = null
    private var email: String? = null
    private var password: String? = null
    private var country_code: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup_4)
        Log.d("ActivityName", "signup_4")
        overridePendingTransition(R.xml.slide_in, R.xml.slide_out);

        //make status bar invisible
        val w: Window = getWindow()
        w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        //get fullname phonenumber email password
        val p_i = getIntent()
        fullName = p_i.getStringExtra("fullName")
        phoneNumber = p_i.getStringExtra("phoneNumber")
        email = p_i.getStringExtra("email")
        password = p_i.getStringExtra("password")
        country_code = p_i.getStringExtra("countryCode").toString()

        //on next button clicked
        onClickNextButton()

        /*skip this step*/
        skipThisStep()
    }

    fun onClickNextButton() {
        findViewById<Button>(R.id.nextButton).setOnClickListener {
            val day = findViewById<EditText>(R.id.day).getText().toString()
            val month = findViewById<EditText>(R.id.month).getText().toString()
            val year = findViewById<EditText>(R.id.year).getText().toString()

            if (day.length > 0 && month.length > 0 && year.length > 0) {
                val i = Intent(this, signup_5::class.java)
                i.putExtra("fullName", fullName)
                i.putExtra("phoneNumber", phoneNumber)
                i.putExtra("email", email)
                i.putExtra("password", password)
                i.putExtra("day", day)
                i.putExtra("month", month)
                i.putExtra("year", year)
                i.putExtra("countryCode", country_code)
                startActivity(i)
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            } else {
                showError()
            }
        }

        findViewById<TextView>(R.id.skipTextView).setOnClickListener {
            val i = Intent(this, signup_5::class.java)
            i.putExtra("fullName", fullName)
            i.putExtra("phoneNumber", phoneNumber)
            i.putExtra("email", email)
            i.putExtra("password", password)
            i.putExtra("day", " ")
            i.putExtra("month", " ")
            i.putExtra("year", " ")
            startActivity(i)
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        }
        findViewById<ImageView>(R.id.skipIcon).setOnClickListener {
            val i = Intent(this, signup_5::class.java)
            i.putExtra("fullName", fullName)
            i.putExtra("phoneNumber", phoneNumber)
            i.putExtra("email", email)
            i.putExtra("password", password)
            i.putExtra("day", " ")
            i.putExtra("month", " ")
            i.putExtra("year", " ")
            startActivity(i)
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        }
    }

    fun showError() {
        val st = StyleableToast.Builder(this)
                .text(getString(R.string.enter_your_birth_date))
                .textColor(Color.WHITE)
                .backgroundColor(Color.RED)
                .build()
        st.show()
    }

    fun skipThisStep() {
        val i = Intent(this, signup_5::class.java)
        i.putExtra("fullName", fullName)
        i.putExtra("phoneNumber", phoneNumber)
        i.putExtra("email", email)
        i.putExtra("password", password)
        i.putExtra("day", " ")
        i.putExtra("month", " ")
        i.putExtra("year", " ")
        i.putExtra("countryCode", country_code)
        startActivity(i)
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        finish()
    }
}
