package com.medapp.medapp.loginSignup

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.constraint.ConstraintSet
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.text.InputType
import android.util.Log
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.medapp.medapp.R
import com.muddzdev.styleabletoastlibrary.StyleableToast
import org.jetbrains.anko.backgroundDrawable
import org.jetbrains.anko.find
import org.jetbrains.anko.padding
import org.jetbrains.anko.textColor

class family_signup_5 : AppCompatActivity() {
    private var c_layout: ViewGroup? = null
    private var noOfFamilyMembers: Int = 0
    private var fullNameList: java.util.ArrayList<*>? = null
    private var emailList: java.util.ArrayList<*>? = null
    private var PhoneNumberList: java.util.ArrayList<*>? = null
    private var countryCode: String? = null
    private var password: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_family_signup_5)
        Log.d("ActivityName", "family_signup_5")
        //get subpart_c_layout
        c_layout = findViewById(R.id.subparent_c_layout)

        //get info from previous activity
        val intent = intent
        val args = intent.getBundleExtra("BUNDLE")
        fullNameList = (args.getSerializable("ARRAYLIST_fullNames")) as java.util.ArrayList<*>
        emailList = (args.getSerializable("ARRAYLIST_emails")) as java.util.ArrayList<*>
        PhoneNumberList = (args.getSerializable("ARRAYLIST_phoneNumbers")) as java.util.ArrayList<*>
        noOfFamilyMembers = intent.getStringExtra("noOfFamilyMembers").toInt()
        countryCode = intent.getStringExtra("countryCode").toString()
        password = intent.getStringExtra("password").toString()

        //create password input field
        createPasswordFieldsForEachFamilyMember()

        //on next clicked
        onNextClicked()
        skipThis()
    }

    private fun skipThis() {
        val i = Intent(this, family_signup_6::class.java)
        i.putExtra("noOfFamilyMembers", noOfFamilyMembers.toString())
        i.putExtra("password", password.toString())
        val args = Bundle()
        args.putSerializable("ARRAYLIST_phoneNumbers", PhoneNumberList as java.io.Serializable)
        args.putSerializable("ARRAYLIST_emails", emailList as java.io.Serializable)
        args.putSerializable("ARRAYLIST_fullNames", fullNameList as java.io.Serializable)
        i.putExtra("BUNDLE", args)
        i.putExtra("countryCode", countryCode)
        startActivity(i)
    }

    fun createPasswordFieldsForEachFamilyMember() {
        createEditText(1, 1.toString())
    }

    fun createEditText(id: Int, personNumber: String) {
        val ed = EditText(this)
        ed.hint = getString(R.string.password)
        ed.backgroundDrawable = ContextCompat.getDrawable(this@family_signup_5, R.drawable.login_field_background)
        ed.id = id
        ed.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(this, R.drawable.ic_security_black_24dp), null, null, null)
        ed.inputType = InputType.TYPE_TEXT_VARIATION_PASSWORD
        ed.padding = 20
        ed.textColor = ContextCompat.getColor(this, R.color.black)
        ed.width = 725

        c_layout!!.addView(ed)

        val constraintSet = ConstraintSet()
        constraintSet.clone(c_layout as ConstraintLayout)
        constraintSet.connect(id, ConstraintSet.RIGHT, c_layout!!.id, ConstraintSet.RIGHT, 0)
        constraintSet.connect(id, ConstraintSet.LEFT, c_layout!!.id, ConstraintSet.LEFT, 0)
        if (id == 1) {
            constraintSet.connect(id, ConstraintSet.TOP, findViewById<TextView>(R.id.welcomeHeading).id, ConstraintSet.BOTTOM, 36)
        } else {
            constraintSet.connect(id, ConstraintSet.TOP, id - 1, ConstraintSet.BOTTOM, 16)
        }
        constraintSet.constrainDefaultHeight(id, 200)
        constraintSet.applyTo(c_layout!! as ConstraintLayout)
    }

    fun onNextClicked() {
        findViewById<Button>(R.id.nextButton).setOnClickListener {
            var g = find<EditText>(1).getText().toString()


            if (g.length != 0) {
                var password = g

                val i = Intent(this, family_signup_6::class.java)
                i.putExtra("noOfFamilyMembers", noOfFamilyMembers.toString())
                i.putExtra("password", password.toString())
                val args = Bundle()
                args.putSerializable("ARRAYLIST_phoneNumbers", PhoneNumberList as java.io.Serializable)
                args.putSerializable("ARRAYLIST_emails", emailList as java.io.Serializable)
                args.putSerializable("ARRAYLIST_fullNames", fullNameList as java.io.Serializable)
                i.putExtra("BUNDLE", args)
                i.putExtra("countryCode", countryCode)
                startActivity(i)
            } else {
                showError()
            }
        }
    }

    fun showError() {
        val st = StyleableToast.Builder(this)
                .text(getString(R.string.enter_family_password))
                .textColor(Color.WHITE)
                .backgroundColor(Color.RED)
                .build()
        st.show()
    }

}
