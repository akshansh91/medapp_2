package com.medapp.medapp.loginSignup

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Button
import android.widget.EditText
import com.medapp.medapp.R
import com.medapp.medapp.config.ServerConfig
import com.muddzdev.styleabletoastlibrary.StyleableToast
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.util.concurrent.TimeUnit

class join_family_signup : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_join_family_signup)

        Log.d("ActivityName", "join_family_signup")
        //on next clicked
        onClickedNextButton()
    }

    //We'll now get the family id instead of phone number . . .

    fun onClickedNextButton() {
        findViewById<Button>(R.id.nextButton).setOnClickListener {
            var ID_family = findViewById<EditText>(R.id.familyID).getText().toString()
            if (ID_family.length > 0) {
                makeRequestToDetectFamilyApi(ID_family)
            } else {
                showError()
            }
        }
    }

    override fun onBackPressed() {
        val i = Intent(this, individualOrFamily::class.java)
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(i)
        finish()
    }

    fun makeRequestToDetectFamilyApi(pn: String) {
        doAsync {
            val client: OkHttpClient = OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build()

            //val urlBuilder = HttpUrl.parse(ServerConfig().getDetectFamilyApi())!!.newBuilder()
            //urlBuilder.addQueryParameter("phoneNumber", pn)

            val urlBuilder = HttpUrl.parse(ServerConfig().getDetectFamilUsingFamilyIdyApi())!!.newBuilder()
            urlBuilder.addQueryParameter("IdFamily", pn)

            val url = urlBuilder.build().toString()

            val request = Request.Builder()
                    .url(url)
                    .build()

            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()

            uiThread {
                Log.d("mess", responce_string.toString())

                if (responce_string != null && responce_string.length < 420) {
                    if (responce_string == "dataMissing") {
                        showMessageerror(getString(R.string.something_goes_wrong), ContextCompat.getColor(this@join_family_signup, R.color.red))
                    } else {
                        if (responce_string == "notAssoc") {
                            showMessageerror(getString(R.string.this_family_id_is_not_associated_with_any_family), ContextCompat.getColor(this@join_family_signup, R.color.red))
                        } else {
                            if (responce_string == "serverError") {
                                showMessageerror(getString(R.string.server_error), ContextCompat.getColor(this@join_family_signup, R.color.red))
                            } else {
                                processResponse(responce_string)
                            }
                        }
                    }
                } else {
                    showMessageerror(getString(R.string.server_error), Color.RED)
                }
            }
        }
    }

    fun processResponse(r_s: String) {
        val arr_r_s = r_s.split("___")
        val family_id_and_password = arr_r_s[0]
        val arr_family_id_and_password = family_id_and_password.split("***")
        val family_id = arr_family_id_and_password[0]
        val password = arr_family_id_and_password[1]

        var new_string = r_s.replace("___", "\n")
        var new_string_with_no_family_id = new_string.replace(family_id + "***" + password, " ")
        openJoinFamily_2(new_string_with_no_family_id, family_id, password)
    }

    fun openJoinFamily_2(str: String, f_id: String, password: String) {
        var i = Intent(this, join_family_signup_2::class.java)
        i.putExtra("familyMemberNames", str)
        i.putExtra("familyId", f_id)
        i.putExtra("password", password)
        startActivity(i)
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
    }


    fun showMessageerror(msg_text: String, color: Int) {
        val st = StyleableToast.Builder(this)
                .text(msg_text)
                .textColor(Color.WHITE)
                .backgroundColor(color)
                .build()
        st.show()
    }

    fun showError() {
        val st = StyleableToast.Builder(this)
                .text(getString(R.string.enter_phone_number_of_your_family_member))
                .textColor(Color.WHITE)
                .backgroundColor(Color.RED)
                .build()
        st.show()
    }
}
