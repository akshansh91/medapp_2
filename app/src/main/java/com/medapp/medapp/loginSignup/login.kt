package com.medapp.medapp.loginSignup

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ValueAnimator
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.databinding.DataBindingUtil
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Bundle
import android.os.Handler
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.*
import android.widget.*
import android.widget.AdapterView.OnItemSelectedListener
import com.medapp.medapp.Main2Activity
import com.medapp.medapp.R
import com.medapp.medapp.config.ServerConfig
import com.medapp.medapp.databinding.ActivityLoginBinding
import com.medapp.medapp.resetPassword.ResetPassword
import com.muddzdev.styleabletoastlibrary.StyleableToast
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.util.*


class login : AppCompatActivity() {

    private var doubleBackToExitPressedOnce = false
    private var ActivityLoginBinding: ActivityLoginBinding? = null
    private var frameLayout: FrameLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        Log.d("ActivityName", "login")

        ActivityLoginBinding = DataBindingUtil.setContentView(this@login, R.layout.activity_login)

        //make status bar invisible
        val w: Window = window
        w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
        frameLayout = findViewById(R.id.sign_in_btn)

        frameLayout!!.setOnClickListener {
            load()
        }

        //auto login
        autoLogin()

        //register link
        openNewAccountRegister()

        //attach login button function
        //onClickLoginButton()

        //make reset link work
        makeResetLinkWork()

        //detect spinner change
        spinnerListener()
    }

    fun load() {
        Log.d("login_anim", "inside load...")

        val phoneNumber = findViewById<EditText>(R.id.familyID).text.toString()
        val password = findViewById<EditText>(R.id.password).text.toString()

        findViewById<Button>(R.id.registerLink).visibility = View.INVISIBLE

        if (phoneNumber.length == 10) {
            if (password.isNotEmpty()) {
                makeLoginApiRequest(phoneNumber, password)
            } else {
                showMessageAfterLogin(getString(R.string.enter_your_password), Color.RED)
            }
        } else {
            showMessageAfterLogin(getString(R.string.enter_your_phone_number), Color.RED)
        }

        animateButtonWidth()
        fadeOutTextAndSetProgressDialog()
        nextAction()
    }

    fun animateButtonWidth() {
        Log.d("login_anim", "inside animateButtonWidth...")
        var anim: ValueAnimator = ValueAnimator.ofInt(ActivityLoginBinding!!.signInBtn.measuredWidth, getFinalWidth())
        anim.addUpdateListener {
            object : ValueAnimator.AnimatorUpdateListener {
                override fun onAnimationUpdate(animation: ValueAnimator?) {
                    var value: Int = animation?.animatedValue as Int
                    var layoutParams: ViewGroup.LayoutParams = ActivityLoginBinding!!.signInBtn.layoutParams
                    layoutParams.width = value
                    ActivityLoginBinding!!.signInBtn.requestLayout()
                }
            }
            anim.duration = 250
            anim.start()
        }
    }

    fun fadeOutTextAndSetProgressDialog() {
        Log.d("login_anim", "inside fadeOutTextAndSetProgressDialog()...")
        ActivityLoginBinding!!.signInText.animate().alpha(0f).setDuration(250)
                .setListener(object : AnimatorListenerAdapter() {
                    override fun onAnimationEnd(animation: Animator?) {
                        super.onAnimationEnd(animation)
                        showProgressDialog()
                    }
                }).start()
    }

    fun nextAction() {
        Log.d("login_anim", "inside nextAction()...")
        Handler().postDelayed(object : Runnable {
            override fun run() {
                revealButton()
                fadeOutProgressDialog()
                delayedStartNextActivity()
            }
        }, 2000)
    }

    fun getFinalWidth(): Int {
        Log.d("login_anim", "inside getFinalWidth()...")
        return resources.getDimension(R.dimen.get_width).toInt()
    }

    fun delayedStartNextActivity() {
        Log.d("login_anim", "inside delayedStartNextActivity()...")
        Handler().postDelayed(object : Runnable {
            override fun run() {
                var intent = Intent(this@login, Main2Activity::class.java)
                startActivity(intent)
                finish()
            }
        }, 100)
    }

    fun revealButton() {
        Log.d("login_anim", "inside revealButton()...")
        ActivityLoginBinding!!.signInBtn.elevation = 0f
        ActivityLoginBinding!!.revealView.visibility = View.VISIBLE

        var x: Int = ActivityLoginBinding!!.revealView.width
        var y: Int = ActivityLoginBinding!!.revealView.height

        var startX: Int = (getFinalWidth() / 2 + ActivityLoginBinding!!.signInBtn.x).toInt()
        var startY: Int = (getFinalWidth() / 2 + ActivityLoginBinding!!.signInBtn.y).toInt()

        var radius: Float = Math.max(x, y) * 1.2f

        var reveal: Animator = ViewAnimationUtils.createCircularReveal(ActivityLoginBinding!!.revealView, startX, startY
                , getFinalWidth().toFloat(), radius)

        reveal.duration = 350

        reveal.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator?) {
                super.onAnimationEnd(animation)
                finish()
            }
        })
        reveal.start()
    }

    fun showProgressDialog() {
        Log.d("login_anim", "inside showProgressDialog()...")
        ActivityLoginBinding!!.signInProgressBar.indeterminateDrawable.setColorFilter(resources.getColor(R.color.white), PorterDuff.Mode.SRC_IN)
        ActivityLoginBinding!!.signInProgressBar.visibility = View.VISIBLE
    }

    fun fadeOutProgressDialog() {
        Log.d("login_anim", "inside fadeOutProgressDialog()...")
        ActivityLoginBinding!!.signInProgressBar.animate().alpha(0f).setDuration(200).start()
    }

    /*override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            return
        }
        this.doubleBackToExitPressedOnce = true
        Toast.makeText(this, "Press back again to exit", Toast.LENGTH_SHORT).show()
        Handler().postDelayed(Runnable { doubleBackToExitPressedOnce = false }, 2000)
    }*/

    fun getStringByLocal(context: Activity, id: Int, locale: String): Array<out String>? {
        var configuration: Configuration = Configuration(getResources().getConfiguration())
        configuration.setLocale(Locale(locale))
        return context.createConfigurationContext(configuration).getResources().getStringArray(id)
    }

    fun spinnerListener() {
        val spinner = findViewById<Spinner>(R.id.familyOrIndividualspinner)

        val roozArrayValue = getStringByLocal(this@login, R.array.indivisualOrFamilySpinner, "en")
        Log.d("language_spinner_", "Array: ${Arrays.toString(roozArrayValue)}")

        spinner.setSelection(0)

        spinner.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(parentView: AdapterView<*>, selectedItemView: View, position: Int, id: Long) {
                val selectedItem = roozArrayValue!![position]

                Log.d("language_spinner_", "selectedItem: $selectedItem")

                //val SelectedItem = spinner.selectedItem.toString()

                if (selectedItem == "Family") {
                    val i = Intent(this@login, family_login::class.java)
                    startActivity(i)
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                    finish()
                } else if (selectedItem == "Join Family") {
                    val i = Intent(this@login, join_family_signup::class.java)
                    startActivity(i)
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                    finish()
                }
            }

            override fun onNothingSelected(parentView: AdapterView<*>) {
                val SelectedItem = spinner.selectedItem.toString()
                if (SelectedItem == "Family") {
                    val i = Intent(this@login, family_login::class.java)
                    startActivity(i)
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                    finish()
                } else if (SelectedItem == "Join Family") {
                    val i = Intent(this@login, join_family_signup::class.java)
                    startActivity(i)
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                    finish()
                }
            }
        }
    }

    fun openNewAccountRegister() {
        findViewById<Button>(R.id.registerLink).setOnClickListener {
            val i = Intent(this, signup::class.java)
            i.putExtra("type", "i")
            startActivity(i)
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        }
    }

    fun onClickLoginButton() {
        //findViewById<Button>(R.id.loginButton).setOnClickListener {
        findViewById<FrameLayout>(R.id.sign_in_btn).setOnClickListener {
            val phoneNumber = findViewById<EditText>(R.id.familyID).text.toString()
            val password = findViewById<EditText>(R.id.password).text.toString()

            if (phoneNumber.length > 0) {
                if (password.length > 0) {
                    makeLoginApiRequest(phoneNumber, password)
                } else {
                    showMessageAfterLogin(getString(R.string.enter_your_password), Color.RED)
                }
            } else {
                showMessageAfterLogin(getString(R.string.enter_your_phone_number), Color.RED)
            }
        }
    }

    fun showMessageAfterLogin(msg_text: String, color: Int) {
        val st = StyleableToast.Builder(this)
                .text(msg_text)
                .textColor(Color.WHITE)
                .backgroundColor(color)
                .build()
        st.show()
    }

    fun makeLoginApiRequest(phoneNu: String, password: String) {
        doAsync {
            val client = OkHttpClient()

            val urlBuilder = HttpUrl.parse(ServerConfig().getLoginApi())!!.newBuilder()
            urlBuilder.addQueryParameter("phoneNumber", phoneNu)
            urlBuilder.addQueryParameter("password", password)

            val url = urlBuilder.build().toString()

            val request = Request.Builder()
                    .url(url)
                    .build()

            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()

            uiThread {
                if (responce_string == "login") {
                    showMessageAfterLogin(getString(R.string.successfully_logged_in), ContextCompat.getColor(this@login, R.color.green))
                    storeInPersistentData(phoneNu, password)
                }
                if (responce_string == "dataMissing") {
                    showMessageAfterLogin(getString(R.string.something_goes_wrong), Color.RED)
                }
                if (responce_string == "noSuchAccount") {
                    showMessageAfterLogin(getString(R.string.no_such_account_exist), Color.RED)
                }
                if (responce_string == "serverError") {
                    showMessageAfterLogin(getString(R.string.server_error), Color.RED)
                }
                if (responce_string == "invalidPhoneNumber") {
                    showMessageAfterLogin(getString(R.string.invalid_phone_number), Color.RED)
                }
                if (responce_string != "login"
                        && responce_string != "dataMissing"
                        && responce_string != "invalidPhoneNumber"
                        && responce_string != "noSuchAccount"
                        && responce_string != "serverError") {
                    showMessageAfterLogin(getString(R.string.server_error), Color.RED)
                }
            }
        }
    }

    fun storeInPersistentData(phoneNu: String, password: String) {
        val loginInfo = getSharedPreferences("loginInfo", Context.MODE_PRIVATE)
        val editor = loginInfo.edit()
        editor.putString("password", password)
        editor.putString("phoneNumber", phoneNu)
        editor.putString("familyId", "0")
        editor.putString("familyOrIndividual", "i")
        editor.apply()

        startMainActivity()
    }

    fun autoLogin() {
        val loginInfo = applicationContext.getSharedPreferences("loginInfo", Context.MODE_PRIVATE)
        if (loginInfo.contains("familyOrIndividual") == true && loginInfo.contains("familyId") == true) {
            if (loginInfo.getString("familyId", "").toString() == "0") {
                startMainActivity()
            }
        }
    }

    fun startMainActivity() {
        val i = Intent(this, Main2Activity::class.java)
        startActivity(i)
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
    }

    //make reset button work
    fun makeResetLinkWork() {
        findViewById<TextView>(R.id.forgottenPasswordTextView).setOnClickListener {
            val i = Intent(this@login, ResetPassword::class.java)
            i.putExtra("type", "i")
            startActivity(i)
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            finish()
        }
    }
}
