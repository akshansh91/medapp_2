package com.medapp.medapp.loginSignup

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.constraint.ConstraintSet
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.text.InputType
import android.util.Log
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Spinner
import android.widget.TextView
import com.medapp.medapp.R
import com.muddzdev.styleabletoastlibrary.StyleableToast
import org.jetbrains.anko.backgroundDrawable
import org.jetbrains.anko.find
import org.jetbrains.anko.padding
import org.jetbrains.anko.textColor
import java.util.*

class family_signup_4 : AppCompatActivity() {
    private var c_layout: ViewGroup? = null
    private var noOfFamilyMembers: Int = 0
    private var fullNameList: java.util.ArrayList<*>? = null
    private var emailList: java.util.ArrayList<*>? = null
    private var phoneNumberList: java.util.ArrayList<*>? = null
    private var phoneNumber: String? = null
    private var country_code: String? = null
    private var password: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_family_signup_4)
        Log.d("ActivityName", "family_signup_4")
        //get subpart_c_layout
        c_layout = findViewById<ViewGroup>(R.id.subparent_c_layout)

        //get info from previous activity
        val intent = intent
        val args = intent.getBundleExtra("BUNDLE")
        fullNameList = (args.getSerializable("ARRAYLIST_fullNames")) as java.util.ArrayList<*>
        emailList = (args.getSerializable("ARRAYLIST_emails")) as java.util.ArrayList<*>
        phoneNumberList = (args.getSerializable("ARRAYLIST_phoneNumbers")) as java.util.ArrayList<*>

        noOfFamilyMembers = intent.getStringExtra("noOfFamilyMembers").toInt()
//        phoneNumber = intent.getStringExtra("phoneNumber")
        password = intent.getStringExtra("password").toString()
        country_code = intent.getStringExtra("countryCode")

        //create phone number fields
        createPhoneNumberFieldsForEachFamilyMember()

        //on next clicked
        //onNextClicked()

        skipThis()
    }

    private fun skipThis() {
        var result_phone_number_array: ArrayList<String> = ArrayList()
        result_phone_number_array.add(phoneNumber.toString())

        val i = Intent(this, family_signup_5::class.java)
        i.putExtra("noOfFamilyMembers", noOfFamilyMembers.toString())
        i.putExtra("password", password)

        val args = Bundle()
        args.putSerializable("ARRAYLIST_phoneNumbers", phoneNumberList as java.io.Serializable)
        args.putSerializable("ARRAYLIST_emails", emailList as java.io.Serializable)
        args.putSerializable("ARRAYLIST_fullNames", fullNameList as java.io.Serializable)
        i.putExtra("BUNDLE", args)
        i.putExtra("countryCode", country_code.toString())
        startActivity(i)
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
    }


    fun createPhoneNumberFieldsForEachFamilyMember() {
        val size = noOfFamilyMembers
        var counter = 1
        while (counter <= size) {
            createEditText(counter, counter.toString())
            counter++
        }
    }

    fun createEditText(id: Int, personNumber: String) {
        val ed = EditText(this)
        ed.hint = getString(R.string.phone_number)
        ed.backgroundDrawable = ContextCompat.getDrawable(this@family_signup_4, R.drawable.login_field_background)
        ed.id = id
        ed.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(this, R.drawable.ic_phone_white_24dp), null, null, null)
        ed.inputType = InputType.TYPE_CLASS_NUMBER
        ed.padding = 20
        ed.textColor = ContextCompat.getColor(this, R.color.white)
        ed.width = 725

        c_layout!!.addView(ed)

        val constraintSet = ConstraintSet()
        constraintSet.clone(c_layout as ConstraintLayout)
        constraintSet.connect(id, ConstraintSet.RIGHT, c_layout!!.id, ConstraintSet.RIGHT, 0)
        constraintSet.connect(id, ConstraintSet.LEFT, c_layout!!.id, ConstraintSet.LEFT, 0)
        if (id == 1) {
            constraintSet.connect(id, ConstraintSet.TOP, findViewById<TextView>(R.id.welcomeHeading).id, ConstraintSet.BOTTOM, 36)
        } else {
            constraintSet.connect(id, ConstraintSet.TOP, id - 1, ConstraintSet.BOTTOM, 16)
        }
        constraintSet.constrainDefaultHeight(id, 200)
        constraintSet.applyTo(c_layout!! as ConstraintLayout)
    }


    fun onNextClicked() {
        findViewById<Button>(R.id.nextButton).setOnClickListener {
            var c = 1
            val s = noOfFamilyMembers
            var allAreFields = true
            while (c <= s) {
                var g = find<EditText>(c).getText().toString()
                if (g.length == 0) {
                    allAreFields = false
                    break
                }
                c++
            }

            if (allAreFields == true) {
                var result_phone_number_array: ArrayList<String> = ArrayList()
                var counter = 1
                val size = noOfFamilyMembers
                while (counter <= size) {
                    var g = find<EditText>(counter).getText().toString()
                    result_phone_number_array.add(g)
                    counter++
                }

                val i = Intent(this, family_signup_5::class.java)
                i.putExtra("noOfFamilyMembers", noOfFamilyMembers.toString())
                val args = Bundle()
                args.putSerializable("ARRAYLIST_phoneNumbers", result_phone_number_array as java.io.Serializable)
                args.putSerializable("ARRAYLIST_emails", emailList as java.io.Serializable)
                args.putSerializable("ARRAYLIST_fullNames", fullNameList as java.io.Serializable)
                i.putExtra("BUNDLE", args)
                val spinner = findViewById<Spinner>(R.id.country_code_spinner)
                val country_code = spinner.getSelectedItem().toString()
                i.putExtra("countryCode", country_code.toString())
                startActivity(i)
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            } else {
                showError()
            }
        }
    }

    fun showError() {
        val st = StyleableToast.Builder(this)
                .text(getString(R.string.enter_phone_number))
                .textColor(Color.WHITE)
                .backgroundColor(Color.RED)
                .build()
        st.show()
    }
}
