package com.medapp.medapp.loginSignup

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.constraint.ConstraintSet
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.text.InputType
import android.util.Log
import android.view.Gravity
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.medapp.medapp.R
import com.muddzdev.styleabletoastlibrary.StyleableToast
import org.jetbrains.anko.backgroundDrawable
import org.jetbrains.anko.find
import org.jetbrains.anko.padding
import org.jetbrains.anko.textColor


class family_signup_2 : AppCompatActivity() {
    private var noOfFamilyMembers = 0
    private var c_layout: ViewGroup? = null
    private var password: String? = null
    private var country_code: String? = null
    private var fullNameList: java.util.ArrayList<*>? = null
    private var phoneNumberList: java.util.ArrayList<*>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_family_signup_2)
        Log.d("ActivityName", "family_signup_2")
        //get subpart_c_layout
        c_layout = findViewById(R.id.subparent_c_layout)

        //get no of familt memebers
        val p_i = getIntent()
        val args = p_i.getBundleExtra("BUNDLE")

        fullNameList = (args.getSerializable("ARRAYLIST_fullNames")) as java.util.ArrayList<*>
        phoneNumberList = (args.getSerializable("ARRAYLIST_phoneNumbers")) as java.util.ArrayList<*>
        password = p_i.getStringExtra("password").toString()

        noOfFamilyMembers = p_i.getStringExtra("noOfFamilyMembers").toInt()
        //phoneNumber = p_i.getStringExtra("phoneNumber")
        country_code = p_i.getStringExtra("countryCode")

        //create full name fields
        createFullNameFieldsForEachFamilyMember()

        //onNextClicked
        onNextClicked()

        skipThisStep()
    }

    private fun skipThisStep() {
        val i = Intent(this, family_signup_3::class.java)

        i.putExtra("countryCode", country_code.toString())

        val args = Bundle()
        args.putSerializable("ARRAYLIST_fullNames", fullNameList as java.io.Serializable)
        args.putSerializable("ARRAYLIST_phoneNumbers", phoneNumberList as java.io.Serializable)

        i.putExtra("noOfFamilyMembers", "1")
        i.putExtra("countryCode", country_code.toString())
        i.putExtra("password", password)
        i.putExtra("BUNDLE", args)

        startActivity(i)
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
    }

    fun createFullNameFieldsForEachFamilyMember() {
        val size = noOfFamilyMembers
        var counter = 1
        while (counter <= size) {
            createEditText(counter, counter.toString())
            counter++
        }
    }

    fun createEditText(id: Int, personNumber: String) {
        val ed = EditText(this)

        //ed.hint = "Full Name ( Member $personNumber )"
        ed.hint = getString(R.string.full_name)
        ed.backgroundDrawable = ContextCompat.getDrawable(this@family_signup_2, R.drawable.login_field_background)
        ed.id = id
        ed.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(this, R.drawable.ic_perm_identity_black_2_24dp), null, null, null)
        ed.padding = 20
        ed.textColor = ContextCompat.getColor(this, R.color.black)
        ed.width = 725
        ed.gravity = Gravity.CENTER_HORIZONTAL
        ed.inputType = InputType.TYPE_CLASS_TEXT

        c_layout!!.addView(ed)

        val constraintSet = ConstraintSet()
        constraintSet.clone(c_layout as ConstraintLayout)
        constraintSet.connect(id, ConstraintSet.RIGHT, c_layout!!.id, ConstraintSet.RIGHT, 0)
        constraintSet.connect(id, ConstraintSet.LEFT, c_layout!!.id, ConstraintSet.LEFT, 0)
        if (id == 1) {
            constraintSet.connect(id, ConstraintSet.TOP, findViewById<TextView>(R.id.welcomeHeading).id, ConstraintSet.BOTTOM, 36)
        } else {
            constraintSet.connect(id, ConstraintSet.TOP, id - 1, ConstraintSet.BOTTOM, 16)
        }
        constraintSet.constrainDefaultHeight(id, 200)
        constraintSet.applyTo(c_layout!! as ConstraintLayout)
    }

    fun onNextClicked() {
        findViewById<Button>(R.id.nextButton).setOnClickListener {
            var c = 1
            val s = noOfFamilyMembers
            var allAreFields = true
            while (c <= s) {
                var g = find<EditText>(c).getText().toString()
                if (g.length == 0) {
                    allAreFields = false
                    break
                }
                c++
            }

            if (allAreFields == true) {
                var result_full_names_array: ArrayList<String> = ArrayList()
                var counter = 1
                val size = noOfFamilyMembers
                while (counter <= size) {
                    var g = find<EditText>(counter).getText().toString()
                    result_full_names_array.add(g)
                    counter++
                }

                val i = Intent(this, family_signup_3::class.java)
                i.putExtra("noOfFamilyMembers", noOfFamilyMembers.toString())
                //i.putExtra("phoneNumber", phoneNumber)
                i.putExtra("countryCode", country_code.toString())

                val args = Bundle()
                args.putSerializable("ARRAYLIST", result_full_names_array as java.io.Serializable)
                i.putExtra("BUNDLE", args)
                startActivity(i)
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            } else {
                showError()
            }
        }
    }

    fun showError() {
        val st = StyleableToast.Builder(this)
                .text(getString(R.string.enter_full_name))
                .textColor(Color.WHITE)
                .backgroundColor(Color.RED)
                .build()
        st.show()
    }
}
