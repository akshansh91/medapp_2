package com.medapp.medapp.loginSignup

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Button
import android.widget.EditText
import com.google.gson.JsonParser
import com.medapp.medapp.R
import com.medapp.medapp.config.ServerConfig
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

//OTP verification . . .
class signup_5 : AppCompatActivity() {
    private var AUTH_KEY: String? = null
    private var fullName: String? = null
    private var phoneNumber: String? = null
    private var email: String? = null
    private var password: String? = null
    private var day: String? = null
    private var month: String? = null
    private var year: String? = null
    private var TAG: String = "OTP"
    private var country_code: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup_5)
        Log.d("ActivityName", "signup_5")
        //GET DATA FROM PREVIOUS ACTIVITY
        getDataFromPreviousActivity()

        //SEND OTP
        sendOtp()

        //ONCLICK RESEND OTP BUTTON
        onClickResendOtpButton()

        //ONCLICK NEXT BUTTON
        onClickNextButton()
    }

    //GET DATA FROM PREVIOUS ACTIVITY
    fun getDataFromPreviousActivity() {
        val p_i = getIntent()
        fullName = p_i.getStringExtra("fullName")
        phoneNumber = p_i.getStringExtra("phoneNumber")
        email = p_i.getStringExtra("email")
        password = p_i.getStringExtra("password")
        day = p_i.getStringExtra("day")
        month = p_i.getStringExtra("month")
        year = p_i.getStringExtra("year")
        country_code = p_i.getStringExtra("countryCode").toString()
    }

    //SEND OTP
    fun sendOtp() {
        doAsync {
            val client = OkHttpClient()

            //GET AUTH CODE FROM PRIVATE SERVERS
            val urlBuilder = HttpUrl.parse(ServerConfig().getMsg91_securityCode_from_webServer())!!.newBuilder()
            urlBuilder.addQueryParameter("security_code", "241999")
            val url = urlBuilder.build().toString()
            val request = Request.Builder()
                    .url(url)
                    .build()
            val response: Response = client.newCall(request).execute()
            val auth_code = response.body()!!.string()
            AUTH_KEY = auth_code

            //SEND OTP USING Msg91 API
            val urlBuilder_2 = HttpUrl.parse("http://control.msg91.com/api/sendotp.php")!!.newBuilder()
            urlBuilder_2.addQueryParameter("authkey", AUTH_KEY)
            urlBuilder_2.addQueryParameter("message", "Your Verification Code is ##OTP##")
            urlBuilder_2.addQueryParameter("sender", "Medapp")
            urlBuilder_2.addQueryParameter("mobile", getCountryCode() + phoneNumber)
            val url_2 = urlBuilder_2.build().toString()
            val request_2 = Request.Builder()
                    .url(url_2)
                    .build()
            val response_2: Response = client.newCall(request_2).execute()
            val responce_string = response_2.body()!!.string()
        }
    }

    //RESEND OTP
    fun resendOtp() {
        doAsync {
            val client = OkHttpClient()

            //GET AUTH CODE FROM PRIVATE SERVERS
            val urlBuilder = HttpUrl.parse(ServerConfig().getMsg91_securityCode_from_webServer())!!.newBuilder()
            urlBuilder.addQueryParameter("security_code", "241999")
            val url = urlBuilder.build().toString()
            val request = Request.Builder()
                    .url(url)
                    .build()
            val response: Response = client.newCall(request).execute()
            val auth_code = response.body()!!.string()
            AUTH_KEY = auth_code

            //SEND OTP USING Msg91 API
            val urlBuilder_2 = HttpUrl.parse("http://control.msg91.com/api/retryotp.php")!!.newBuilder()
            urlBuilder_2.addQueryParameter("authkey", AUTH_KEY)
            urlBuilder_2.addQueryParameter("mobile", getCountryCode() + phoneNumber)
            val url_2 = urlBuilder_2.build().toString()
            val request_2 = Request.Builder()
                    .url(url_2)
                    .build()
            val response_2: Response = client.newCall(request_2).execute()
            val responce_string = response_2.body()!!.string()
        }
    }

    //ONCLICK RESEND OTP BUTTON
    fun onClickResendOtpButton() {
        findViewById<Button>(R.id.resendOtpButton).setOnClickListener {
            resendOtp()
        }
    }

    //VERIFY OTP
    fun verifyOtp() {
        doAsync {
            val client = OkHttpClient()

            val urlBuilder = HttpUrl.parse("https://control.msg91.com/api/verifyRequestOTP.php")!!.newBuilder()
            urlBuilder.addQueryParameter("authkey", AUTH_KEY)
            urlBuilder.addQueryParameter("otp", findViewById<EditText>(R.id.smsCode).text.toString())
            urlBuilder.addQueryParameter("mobile", getCountryCode() + phoneNumber)
            val url = urlBuilder.build().toString()
            val request = Request.Builder()
                    .url(url)
                    .build()
            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()
            uiThread {
                Log.d(TAG, responce_string)
                val parser = JsonParser()
                val j_o = parser.parse(responce_string).getAsJsonObject();
                val resp_msg = j_o.get("message")
                if (resp_msg.toString() == "\"otp_verified\"") {
                    startFinalActivity()
                }
            }
        }
    }

    //ONCLICK NEXT BUTTON
    fun onClickNextButton() {
        findViewById<Button>(R.id.nextButton).setOnClickListener {
            verifyOtp()
        }
    }

    //START FINAL ACTIVTY
    fun startFinalActivity() {
        val i = Intent(this, signup_6::class.java)
        i.putExtra("fullName", fullName)
        i.putExtra("phoneNumber", phoneNumber)
        i.putExtra("email", email)
        i.putExtra("password", password)
        i.putExtra("day", day)
        i.putExtra("month", month)
        i.putExtra("year", year)
        startActivity(i)
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
    }

    //GET COUNTRY CODE
    fun getCountryCode(): String {
        //val m_Codes = arrayOf("376", "971", "93", "355", "374", "599", "244", "672", "54", "43", "61", "297", "994", "387", "880", "32", "226", "359", "973", "257", "229", "590", "673", "591", "55", "975", "267", "375", "501", "1", "61", "243", "236", "242", "41", "225", "682", "56", "237", "86", "57", "506", "53", "238", "61", "357", "420", "49", "253", "45", "213", "593", "372", "20", "291", "34", "251", "358", "679", "500", "691", "298", "33", "241", "44", "995", "233", "350", "299", "220", "224", "240", "30", "502", "245", "592", "852", "504", "385", "509", "36", "62", "353", "972", "44", "91", "964", "98", "39", "962", "81", "254", "996", "855", "686", "269", "850", "82", "965", "7", "856", "961", "423", "94", "231", "266", "370", "352", "371", "218", "212", "377", "373", "382", "261", "692", "389", "223", "95", "976", "853", "222", "356", "230", "960", "265", "52", "60", "258", "264", "687", "227", "234", "505", "31", "47", "977", "674", "683", "64", "968", "507", "51", "689", "675", "63", "92", "48", "508", "870", "1", "351", "680", "595", "974", "40", "381", "7", "250", "966", "677", "248", "249", "46", "65", "290", "386", "421", "232", "378", "221", "252", "597", "239", "503", "963", "268", "235", "228", "66", "992", "690", "670", "993", "216", "676", "90", "688", "886", "255", "380", "256", "1", "598", "998", "39", "58", "84", "678", "681", "685", "967", "262", "27", "260", "263")

        Log.d(TAG, country_code.toString())
        return country_code.toString()
    }
}
