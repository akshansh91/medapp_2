package com.medapp.medapp.loginSignup

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.medapp.medapp.R
import com.medapp.medapp.config.ServerConfig
import com.muddzdev.styleabletoastlibrary.StyleableToast
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class family_signup_8 : AppCompatActivity() {
    private var s_v_code: String? = null
    private var c_layout: ViewGroup? = null
    private var noOfFamilyMembers: Int = 0
    private var fullNameList: java.util.ArrayList<*>? = null
    private var emailList: java.util.ArrayList<*>? = null
    private var PhoneNumberList: java.util.ArrayList<*>? = null
    private var dayList: java.util.ArrayList<*>? = null
    private var monthList: java.util.ArrayList<*>? = null
    private var yearList: java.util.ArrayList<*>? = null
    private var password: String? = null
    private var adminPhoneNumber: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_family_signup_8)
        Log.d("ActivityName", "family_signup_8")
        //start loading animations
        findViewById<com.wang.avi.AVLoadingIndicatorView>(R.id.avi).show()

        //get subpart_c_layout
        c_layout = findViewById<ViewGroup>(R.id.subparent_c_layout)

        //get info from previous activity
        val intent = intent
        val args = intent.getBundleExtra("BUNDLE")
        fullNameList = (args.getSerializable("ARRAYLIST_fullNames")) as java.util.ArrayList<*>
        emailList = (args.getSerializable("ARRAYLIST_emails")) as java.util.ArrayList<*>
        PhoneNumberList = (args.getSerializable("ARRAYLIST_phoneNumbers")) as java.util.ArrayList<*>
        dayList = (args.getSerializable("ARRAYLIST_days")) as java.util.ArrayList<*>
        monthList = (args.getSerializable("ARRAYLIST_months")) as java.util.ArrayList<*>
        yearList = (args.getSerializable("ARRAYLIST_years")) as java.util.ArrayList<*>
        noOfFamilyMembers = intent.getStringExtra("noOfFamilyMembers").toInt()
        password = intent.getStringExtra("password").toString()
        adminPhoneNumber = intent.getStringExtra("adminPhoneNumber").toString()

        //signup the family
        makeFamilySignupApiRequest()

    }

    fun convertArrayToString(arr: java.util.ArrayList<*>): String {
        var counter = 0
        val size = arr.size
        var result_string = ""
        while (counter < size) {
            result_string = result_string + "__" + arr[counter]
            counter++
        }
        return result_string
    }

    fun makeFamilySignupApiRequest() {
        doAsync {
            val client: OkHttpClient = OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build()

            val urlBuilder = HttpUrl.parse(ServerConfig().getFamilySignupApi())!!.newBuilder()
            urlBuilder.addQueryParameter("fullName", convertArrayToString(fullNameList!!))
            urlBuilder.addQueryParameter("phoneNumber", convertArrayToString(PhoneNumberList!!))
            urlBuilder.addQueryParameter("email", convertArrayToString(emailList!!))
            urlBuilder.addQueryParameter("password", password)
            urlBuilder.addQueryParameter("day", convertArrayToString(dayList!!))
            urlBuilder.addQueryParameter("month", convertArrayToString(monthList!!))
            urlBuilder.addQueryParameter("year", convertArrayToString(yearList!!))
            urlBuilder.addQueryParameter("dateOfRegister", getTodaysDate())
            urlBuilder.addQueryParameter("adminPhoneNumber", adminPhoneNumber)

            val url = urlBuilder.build().toString()

            val request = Request.Builder()
                    .url(url)
                    .build()

            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()

            uiThread {
                Log.d("mess_Family", responce_string.toString())

                if (responce_string == "AccountCreated") {
                    showMessageAfterSignup(getString(R.string.account_created_successfully), ContextCompat.getColor(this@family_signup_8, R.color.green))
                    openLoginPage()
                }
                if (responce_string == "dataMissing") {
                    showMessageAfterSignup(getString(R.string.something_goes_wrong), Color.RED)
                }
                if (responce_string == "issue") {
                    showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                }
                if (responce_string == "accountAlreadyExist") {
                    showMessageAfterSignup(getString(R.string.phone_number_already_registered), Color.RED)
                    findViewById<TextView>(R.id.error).text = getString(R.string.these_phone_numbers_already_registered)
                    findViewById<Button>(R.id.joinFamilyButton).visibility = View.VISIBLE

                    //changing this a bit
                    //showing the join family sign up page. . .
                    findViewById<Button>(R.id.joinFamilyButton).setOnClickListener {
                        //openLoginPage()
                        val i = Intent(this@family_signup_8, join_family_signup::class.java)
                        startActivity(i)
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                        finish()
                    }
                }
                if (responce_string != "AccountCreated" && responce_string != "dataMissing"
                        && responce_string != "issue" && responce_string != "accountAlreadyExist") {
                    showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                }
            }
        }
    }

    fun getTodaysDate(): String {
        val c: Calendar = Calendar.getInstance()
        val df: SimpleDateFormat = SimpleDateFormat("dd-MM-yyyy")
        val formattedDate: String = df.format(c.time).toString()
        return formattedDate
    }

    fun showMessageAfterSignup(msg_text: String, color: Int) {
        val st = StyleableToast.Builder(this)
                .text(msg_text)
                .textColor(Color.WHITE)
                .backgroundColor(color)
                .build()
        st.show()
    }

    fun openLoginPage() {
        val i = Intent(this, family_login::class.java)
        startActivity(i)
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        finish()
    }

}
