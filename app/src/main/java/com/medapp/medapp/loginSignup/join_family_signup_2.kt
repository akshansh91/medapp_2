package com.medapp.medapp.loginSignup

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Button
import android.widget.TextView
import com.medapp.medapp.R

class join_family_signup_2 : AppCompatActivity() {
    private var familyId: String? = null
    private var password: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_join_family_signup_2)
        Log.d("ActivityName", "join_family_signup_2")
        //get family member names from intent
        val p_i = getIntent()
        val familyMemberNames = p_i.getStringExtra("familyMemberNames")
        familyId = p_i.getStringExtra("familyId")
        password = p_i.getStringExtra("password")

        //set family members names
        findViewById<TextView>(R.id.familyMemberNames).setText(familyMemberNames)

        //on yes clicked
        onYesClicked()

        //on no clicked
        onNoClicked()

        Log.d("mess", familyId + "  " + password)
    }

    fun onYesClicked() {
        findViewById<Button>(R.id.iWantToJoinThisFamilyButton).setOnClickListener {
            val i = Intent(this, join_family_signup_3::class.java)
            i.putExtra("familyId", familyId)
            i.putExtra("password", password)
            startActivity(i)
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            finish()
        }
    }

    fun onNoClicked() {
        findViewById<Button>(R.id.iDontWantToJoinThisFamilyButton).setOnClickListener {
            val i = Intent(this, join_family_signup::class.java)
            startActivity(i)
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            finish()
        }
    }
}
