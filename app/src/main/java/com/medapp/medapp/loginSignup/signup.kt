package com.medapp.medapp.loginSignup

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Window
import android.view.WindowManager
import android.widget.Button
import android.widget.EditText
import android.widget.Spinner
import com.medapp.medapp.R
import com.medapp.medapp.config.ServerConfig
import com.muddzdev.styleabletoastlibrary.StyleableToast
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.util.concurrent.TimeUnit


class signup : AppCompatActivity() {

    private var type: String? = "i"
    private var phoneNumber: String? = null
    private var userName: String? = null
    private var password: String? = null
    private var country_code: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)

        Log.d("ActivityName", "signup")

        //get data from intent
        val i_p = getIntent()
        type = i_p.getStringExtra("type")

        overridePendingTransition(R.xml.slide_in, R.xml.slide_out)

        //make status bar invisible
        val w: Window = getWindow()
        w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)

        //next button
        onClickNextButton()
    }

    override fun onBackPressed() {
        val i = Intent(this, individualOrFamily::class.java)
        startActivity(i)
        finish()
    }

    fun onClickNextButton() {
        val spinner = findViewById<Spinner>(R.id.country_code_spinner2)
        findViewById<Button>(R.id.nextButton).setOnClickListener {
            //val fullName = findViewById<EditText>(R.id.fullName).getText().toString()
            phoneNumber = findViewById<EditText>(R.id.phoneNumber).getText().toString()
            userName = findViewById<EditText>(R.id.fullName).getText().toString()
            password = findViewById<EditText>(R.id.password).getText().toString()
            country_code = spinner.getSelectedItem().toString()

            if (phoneNumber!!.length == 10 && password!!.length > 0 && country_code!!.length > 0 && userName!!.length > 0) {
                isAccountExist()
            } else {
                if (phoneNumber!!.length < 10) {
                    showErrorMsg(getString(R.string.enter_your_phone_number), ContextCompat.getColor(this, R.color.red))
                } else {
                    showErrorMsg(getString(R.string.enter_your_password), ContextCompat.getColor(this, R.color.red))
                }
            }

            //isAccountExist()
            /*if (fullName.length > 0) {
                val i = Intent(this, signup_2::class.java)
                i.putExtra("fullName", fullName)
                startActivity(i)
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            } else {
                showError()
            }*/
            /*if (phoneNumber!!.length > 0) {
                val i = Intent(this, signup_2::class.java)
                i.putExtra("phoneNumber", phoneNumber)
                i.putExtra("countryCode", country_code.toString())
                startActivity(i)
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            } else {
                showError()
            }*/
        }
    }

    private fun isAccountExist() {
        doAsync {
            val client: OkHttpClient = OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build()

            val urlBuilder = HttpUrl.parse(ServerConfig().isAccountExist_resetPassword())!!.newBuilder()
            urlBuilder.addQueryParameter("phoneNumber", phoneNumber)
            urlBuilder.addQueryParameter("type", type)

            val url = urlBuilder.build().toString()

            val request = Request.Builder()
                    .url(url)
                    .build()

            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()

            uiThread {
                Log.d("SignUp", responce_string.toString())

                if (responce_string == "exist") {
                    //startPhoneVerification(phoneNumber!!)
                    val st = StyleableToast.Builder(this@signup)
                            .text(getString(R.string.account_already_exist))
                            .textColor(Color.WHITE)
                            .backgroundColor(Color.RED)
                            .build()
                    st.show()

                    val i = Intent(this@signup, individualOrFamily::class.java)
                    i.putExtra("phoneNumber", phoneNumber)
                    i.putExtra("countryCode", country_code.toString())
                    startActivity(i)
                    finish()
                } else {
                    val i = Intent(this@signup, signup_2::class.java)
                    i.putExtra("phoneNumber", phoneNumber)
                    i.putExtra("countryCode", country_code.toString())
                    i.putExtra("fullName", userName)
                    i.putExtra("password", password)
                    startActivity(i)
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                }
            }
        }
    }

    fun showError() {
        val st = StyleableToast.Builder(this)
                .text(getString(R.string.enter_valid_values))
                .textColor(Color.WHITE)
                .backgroundColor(Color.RED)
                .build()
        st.show()
    }

    fun showErrorMsg(msg_text: String, color: Int) {
        val st = StyleableToast.Builder(this)
                .text(msg_text)
                .textColor(Color.WHITE)
                .backgroundColor(color)
                .build()
        st.show()
    }
}
