package com.medapp.medapp.loginSignup

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.medapp.medapp.R
import com.medapp.medapp.config.ServerConfig
import com.muddzdev.styleabletoastlibrary.StyleableToast
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class join_family_signup_5 : AppCompatActivity() {
    private var s_v_code: String? = null
    private var familyId: String? = null
    private var fullName: String? = null
    private var email: String? = null
    private var phoneNumber: String? = null
    private var password: String? = null
    private var day: String? = null
    private var month: String? = null
    private var year: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_join_family_signup_5)
        Log.d("ActivityName", "join_family_signup_5")
        //start loading animations
        findViewById<com.wang.avi.AVLoadingIndicatorView>(R.id.avi).show()

        //get data from intent
        val p_i = getIntent()
        familyId = p_i.getStringExtra("familyId")
        fullName = p_i.getStringExtra("fullName")
        email = p_i.getStringExtra("email")
        password = p_i.getStringExtra("password")
        phoneNumber = p_i.getStringExtra("phoneNumber")
        day = p_i.getStringExtra("day")
        month = p_i.getStringExtra("month")
        year = p_i.getStringExtra("year")

        //join family
        makeJoinFamilyApiRequest()
    }

    fun makeJoinFamilyApiRequest() {
        doAsync {
            val client: OkHttpClient = OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build()

            val urlBuilder = HttpUrl.parse(ServerConfig().getJoinFamilyApi())!!.newBuilder()
            urlBuilder.addQueryParameter("familyId", familyId)
            urlBuilder.addQueryParameter("fullName", fullName)
            urlBuilder.addQueryParameter("email", email)
            urlBuilder.addQueryParameter("phoneNumber", phoneNumber)
            urlBuilder.addQueryParameter("password", password)
            urlBuilder.addQueryParameter("day", day)
            urlBuilder.addQueryParameter("month", month)
            urlBuilder.addQueryParameter("year", year)
            urlBuilder.addQueryParameter("dateOfRegister", getTodaysDate())

            val url = urlBuilder.build().toString()

            val request = Request.Builder()
                    .url(url)
                    .build()

            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()

            uiThread {
                Log.d("mess", responce_string.toString())

                if (responce_string == "AccountCreated") {
                    showMessageAfterSignup(getString(R.string.account_created_successfully), ContextCompat.getColor(this@join_family_signup_5, R.color.green))
                    openLoginPage()
                } else if (responce_string == "dataMissing" || responce_string == "issue") {
                    showMessageAfterSignup(getString(R.string.something_went_wrong), Color.RED)
                } else if (responce_string == "accountAlreadyExist") {
                    showMessageAfterSignup(getString(R.string.you_are_a_member_of_this_family), Color.RED)
                    findViewById<TextView>(R.id.error).text = getString(R.string.you_can_login_to_your_family_account_by_clicking_on_log_in)
                    findViewById<Button>(R.id.joinFamilyButton).setVisibility(View.VISIBLE)
                    findViewById<Button>(R.id.joinFamilyButton).setOnClickListener {
                        openLoginPage()
                    }
                } else {
                    showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                }
            }
        }
    }

    fun getTodaysDate(): String {
        val c: Calendar = Calendar.getInstance()
        val df: SimpleDateFormat = SimpleDateFormat("dd-MM-yyyy")
        val formattedDate: String = df.format(c.getTime()).toString()
        return formattedDate
    }

    fun showMessageAfterSignup(msg_text: String, color: Int) {
        val st = StyleableToast.Builder(this)
                .text(msg_text)
                .textColor(Color.WHITE)
                .backgroundColor(color)
                .build()
        st.show()
    }

    fun openLoginPage() {
        val i = Intent(this, family_login::class.java)
        startActivity(i)
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        finish()
    }


}
