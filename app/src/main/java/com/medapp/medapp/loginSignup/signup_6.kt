package com.medapp.medapp.loginSignup

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.medapp.medapp.R
import com.medapp.medapp.config.ServerConfig
import com.muddzdev.styleabletoastlibrary.StyleableToast
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.text.SimpleDateFormat
import java.util.*

//showing status of the account created . . .

class signup_6 : AppCompatActivity() {
    private var s_v_code: String? = null
    private var fullName: String? = null
    private var phoneNumber: String? = null
    private var email: String? = null
    private var password: String? = null
    private var day: String? = null
    private var month: String? = null
    private var year: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup_6)
        Log.d("ActivityName", "signup_6")

        //start loading animations
        findViewById<com.wang.avi.AVLoadingIndicatorView>(R.id.avi).show()

        //get data from intent
        val p_i = intent
        fullName = p_i.getStringExtra("fullName")
        phoneNumber = p_i.getStringExtra("phoneNumber")
        email = p_i.getStringExtra("email")
        password = p_i.getStringExtra("password")
        day = p_i.getStringExtra("day")
        month = p_i.getStringExtra("month")
        year = p_i.getStringExtra("year")

        //signup user
        makeSignupApiRequest()
    }

    fun makeSignupApiRequest() {
        doAsync {
            val client = OkHttpClient()

            val urlBuilder = HttpUrl.parse(ServerConfig().getSignupApi())!!.newBuilder()
            urlBuilder.addQueryParameter("fullName", fullName)
            urlBuilder.addQueryParameter("phoneNumber", phoneNumber)
            urlBuilder.addQueryParameter("email", email)
            urlBuilder.addQueryParameter("password", password)
            urlBuilder.addQueryParameter("day", day)
            urlBuilder.addQueryParameter("month", month)
            urlBuilder.addQueryParameter("year", year)
            urlBuilder.addQueryParameter("dateOfRegister", getTodaysDate())

            val url = urlBuilder.build().toString()

            val request = Request.Builder()
                    .url(url)
                    .build()

            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()

            uiThread {
                if (responce_string == "AccountCreated") {
                    showMessageAfterSignup(getString(R.string.account_created_successfully), ContextCompat.getColor(this@signup_6, R.color.green))
                    openLoginPage()
                }
                if (responce_string == "dataMissing") {
                    showMessageAfterSignup(getString(R.string.something_goes_wrong), Color.RED)
                }
                if (responce_string == "issue") {
                    showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                }
                if (responce_string == "accountAlreadyExist") {
                    showMessageAfterSignup(getString(R.string.this_number_is_already_registered), Color.RED)
                    findViewById<TextView>(R.id.error).text = getString(R.string.this_number_is_already_registered)
                    findViewById<Button>(R.id.LogInButton).visibility = View.VISIBLE
                    findViewById<Button>(R.id.LogInButton).setOnClickListener {
                        openLoginPage()
                    }
                }
                if (responce_string != "AccountCreated"
                        && responce_string != "dataMissing"
                        && responce_string != "issue"
                        && responce_string != "accountAlreadyExist") {
                    showMessageAfterSignup("Server Error", Color.RED)
                }
            }
        }
    }

    fun getTodaysDate(): String {
        val c: Calendar = Calendar.getInstance()
        val df: SimpleDateFormat = SimpleDateFormat("dd-MM-yyyy")
        val formattedDate: String = df.format(c.time).toString()
        return formattedDate
    }

    fun showMessageAfterSignup(msg_text: String, color: Int) {
        val st = StyleableToast.Builder(this)
                .text(msg_text)
                .textColor(Color.WHITE)
                .backgroundColor(color)
                .build()
        st.show()
    }

    fun openLoginPage() {
        val i = Intent(this, login::class.java)
        startActivity(i)
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
    }
}
