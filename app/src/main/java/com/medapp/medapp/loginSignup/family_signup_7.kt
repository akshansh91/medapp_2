package com.medapp.medapp.loginSignup

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import com.google.gson.JsonParser
import com.medapp.medapp.R
import com.medapp.medapp.config.ServerConfig
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class family_signup_7 : AppCompatActivity() {
    private var AUTH_KEY: String? = null
    private var c_layout: ViewGroup? = null
    private var noOfFamilyMembers: Int = 0
    private var fullNameList: java.util.ArrayList<*>? = null
    private var emailList: java.util.ArrayList<*>? = null
    private var PhoneNumberList: java.util.ArrayList<*>? = null
    private var dayList: java.util.ArrayList<*>? = null
    private var monthList: java.util.ArrayList<*>? = null
    private var yearList: java.util.ArrayList<*>? = null
    private var password: String? = null
    private var TAG: String = "OTP"
    private var phoneNumber: String? = null
    private var country_code: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_family_signup_7)
        Log.d("ActivityName", "family_signup_7")
        //GET DATA FROM PREVIOUS ACTIVITY
        getDataFromPreviousActivity()

        //SEND OTP
        sendOtp()

        //ONCLICK RESEND OTP BUTTON
        onClickResendOtpButton()

        //ONCLICK NEXT BUTTON
        onClickNextButton()
    }

    //GET DATA FROM PREVIOUS ACTIVITY
    fun getDataFromPreviousActivity() {
        //get subpart_c_layout
        c_layout = findViewById(R.id.subparent_c_layout)

        //get info from previous activity
        val intent = intent
        val args = intent.getBundleExtra("BUNDLE")
        fullNameList = (args.getSerializable("ARRAYLIST_fullNames")) as java.util.ArrayList<*>?
        emailList = (args.getSerializable("ARRAYLIST_emails")) as java.util.ArrayList<*>?
        PhoneNumberList = (args.getSerializable("ARRAYLIST_phoneNumbers")) as java.util.ArrayList<*>?
        dayList = (args.getSerializable("ARRAYLIST_days")) as java.util.ArrayList<*>?
        monthList = (args.getSerializable("ARRAYLIST_months")) as java.util.ArrayList<*>?
        yearList = (args.getSerializable("ARRAYLIST_years")) as java.util.ArrayList<*>?
        noOfFamilyMembers = intent.getStringExtra("noOfFamilyMembers").toInt()
        password = intent.getStringExtra("password").toString()
        country_code = intent.getStringExtra("countryCode").toString()

        phoneNumber = PhoneNumberList!![0].toString()
    }

    //SEND OTP
    fun sendOtp() {
        doAsync {
            val client = OkHttpClient()

            //GET AUTH CODE FROM PRIVATE SERVERS
            val urlBuilder = HttpUrl.parse(ServerConfig().getMsg91_securityCode_from_webServer())!!.newBuilder()
            urlBuilder.addQueryParameter("security_code", "241999")
            val url = urlBuilder.build().toString()
            val request = Request.Builder()
                    .url(url)
                    .build()
            val response: Response = client.newCall(request).execute()
            val auth_code = response.body()!!.string()
            AUTH_KEY = auth_code

            //SEND OTP USING Msg91 API
            val urlBuilder_2 = HttpUrl.parse("http://control.msg91.com/api/sendotp.php")!!.newBuilder()
            urlBuilder_2.addQueryParameter("authkey", auth_code)
            urlBuilder_2.addQueryParameter("message", "Your Verification Code is ##OTP##")
            urlBuilder_2.addQueryParameter("sender", "Medapp")
            urlBuilder_2.addQueryParameter("mobile", getCountryCode() + phoneNumber)
            val url_2 = urlBuilder_2.build().toString()
            val request_2 = Request.Builder()
                    .url(url_2)
                    .build()
            val response_2: Response = client.newCall(request_2).execute()
            val responce_string = response_2.body()!!.string()
        }
    }

    //RESEND OTP
    fun resendOtp() {
        doAsync {
            val client = OkHttpClient()

            //GET AUTH CODE FROM PRIVATE SERVERS
            val urlBuilder = HttpUrl.parse(ServerConfig().getMsg91_securityCode_from_webServer())!!.newBuilder()
            urlBuilder.addQueryParameter("security_code", "241999")
            val url = urlBuilder.build().toString()
            val request = Request.Builder()
                    .url(url)
                    .build()
            val response: Response = client.newCall(request).execute()
            val auth_code = response.body()!!.string()
            AUTH_KEY = auth_code

            //SEND OTP USING Msg91 API
            val urlBuilder_2 = HttpUrl.parse("http://control.msg91.com/api/retryotp.php")!!.newBuilder()
            urlBuilder_2.addQueryParameter("authkey", auth_code)
            urlBuilder_2.addQueryParameter("mobile", getCountryCode() + phoneNumber)
            val url_2 = urlBuilder_2.build().toString()
            val request_2 = Request.Builder()
                    .url(url_2)
                    .build()
            val response_2: Response = client.newCall(request_2).execute()
            val responce_string = response_2.body()!!.string()
        }
    }

    //ONCLICK RESEND OTP BUTTON
    fun onClickResendOtpButton() {
        findViewById<Button>(R.id.resendOtpButton).setOnClickListener {
            resendOtp()
        }
    }

    //VERIFY OTP
    fun verifyOtp() {
        doAsync {
            val client = OkHttpClient()

            val urlBuilder = HttpUrl.parse("https://control.msg91.com/api/verifyRequestOTP.php")!!.newBuilder()
            urlBuilder.addQueryParameter("authkey", AUTH_KEY)
            urlBuilder.addQueryParameter("otp", findViewById<EditText>(R.id.smsCode).text.toString())
            urlBuilder.addQueryParameter("mobile", getCountryCode() + phoneNumber)
            val url = urlBuilder.build().toString()
            val request = Request.Builder()
                    .url(url)
                    .build()
            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()
            uiThread {
                Log.d(TAG, responce_string)
                val parser = JsonParser()
                val j_o = parser.parse(responce_string).getAsJsonObject();
                val resp_msg = j_o.get("message")
                if (resp_msg.toString() == "\"otp_verified\"") {
                    startFinalActivity()
                }
            }
        }
    }

    //ONCLICK NEXT BUTTON
    fun onClickNextButton() {
        findViewById<Button>(R.id.nextButton).setOnClickListener {
            verifyOtp()
        }
    }

    //START FINAL ACTIVTY
    fun startFinalActivity() {
        val i = Intent(this, family_signup_8::class.java)
        i.putExtra("noOfFamilyMembers", noOfFamilyMembers.toString())
        i.putExtra("password", password.toString())
        i.putExtra("adminPhoneNumber", phoneNumber)

        val args = Bundle()
        args.putSerializable("ARRAYLIST_phoneNumbers", PhoneNumberList as java.io.Serializable)
        args.putSerializable("ARRAYLIST_emails", emailList as java.io.Serializable)
        args.putSerializable("ARRAYLIST_fullNames", fullNameList as java.io.Serializable)
        args.putSerializable("ARRAYLIST_days", dayList as java.io.Serializable)
        args.putSerializable("ARRAYLIST_months", monthList as java.io.Serializable)
        args.putSerializable("ARRAYLIST_years", yearList as java.io.Serializable)
        i.putExtra("BUNDLE", args)
        startActivity(i)
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
    }

    //GET COUNTRY CODE
    fun getCountryCode(): String {
        return country_code.toString()
    }
}