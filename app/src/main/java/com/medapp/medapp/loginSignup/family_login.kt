package com.medapp.medapp.loginSignup

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ValueAnimator
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.databinding.DataBindingUtil
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Bundle
import android.os.Handler
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.view.ViewAnimationUtils
import android.view.ViewGroup
import android.widget.*
import com.medapp.medapp.Main2Activity
import com.medapp.medapp.MainActivity
import com.medapp.medapp.R
import com.medapp.medapp.config.ServerConfig
import com.medapp.medapp.databinding.ActivityFamilyLoginBinding
import com.medapp.medapp.resetPassword.ResetPassword
import com.muddzdev.styleabletoastlibrary.StyleableToast
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.util.*
import java.util.concurrent.TimeUnit

class family_login : AppCompatActivity() {
    private var familyId: String? = null
    private var phoneNumber: String? = null
    private var password: String? = null
    private var frameLayout: FrameLayout? = null

    //private var ActivityFamilyLoginBindingImpl: ActivityFamilyLoginBinding? = null
    private var ActivityFamilyLoginBinding: ActivityFamilyLoginBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_family_login)

        Log.d("Check_family_login", "Inside family_login Activity")

        ActivityFamilyLoginBinding = DataBindingUtil.setContentView(this@family_login, R.layout.activity_family_login)

        frameLayout = findViewById(R.id.sign_in_btn)

        frameLayout!!.setOnClickListener {
            load()
        }
        //auto login
        autoLogin()

        //attach register link

        registerLink()

        //attch join family link
        //JoinFamilyLink()

        //attach function to login button
        //Commented for now . ..
        //OnClickedLoginButton()

        //make reset link work
        makeResetLinkWork()

        //detect spinner change
        spinnerListener()
    }

    fun load() {
        Log.d("login_anim", "inside load...")

        var l_phoneNumber = findViewById<EditText>(R.id.familyID).getText().toString()
        var l_password = findViewById<EditText>(R.id.password).getText().toString()

        findViewById<Button>(R.id.registerLink).visibility = View.INVISIBLE

        if (l_phoneNumber.length == 10 && l_password.length != 0) {
            login(l_phoneNumber, l_password)
        } else {
            if (l_phoneNumber.length < 10) {
                showErrorMsg(getString(R.string.enter_your_phone_number), ContextCompat.getColor(this, R.color.red))
            } else {
                showErrorMsg(getString(R.string.enter_your_password), ContextCompat.getColor(this, R.color.red))
            }
        }

        animateButtonWidth()
        fadeOutTextAndSetProgressDialog()
        nextAction()
    }

    fun animateButtonWidth() {
        Log.d("login_anim", "inside animateButtonWidth...")
        var anim: ValueAnimator = ValueAnimator.ofInt(ActivityFamilyLoginBinding!!.signInBtn.measuredWidth, getFinalWidth())
        anim.addUpdateListener {
            object : ValueAnimator.AnimatorUpdateListener {
                override fun onAnimationUpdate(animation: ValueAnimator?) {
                    var value: Int = animation?.getAnimatedValue() as Int
                    var layoutParams: ViewGroup.LayoutParams = ActivityFamilyLoginBinding!!.signInBtn.getLayoutParams()
                    layoutParams.width = value
                    ActivityFamilyLoginBinding!!.signInBtn.requestLayout()
                }
            }
            anim.setDuration(250)
            anim.start()
        }
    }

    fun fadeOutTextAndSetProgressDialog() {
        Log.d("login_anim", "inside fadeOutTextAndSetProgressDialog()...")
        ActivityFamilyLoginBinding!!.signInText.animate().alpha(0f).setDuration(250)
                .setListener(object : AnimatorListenerAdapter() {
                    override fun onAnimationEnd(animation: Animator?) {
                        super.onAnimationEnd(animation)
                        showProgressDialog()
                    }
                }).start()
    }

    fun nextAction() {
        Log.d("login_anim", "inside nextAction()...")
        Handler().postDelayed(object : Runnable {
            override fun run() {
                revealButton()
                fadeOutProgressDialog()
                delayedStartNextActivity()
            }
        }, 2000)
    }

    fun getFinalWidth(): Int {
        Log.d("login_anim", "inside getFinalWidth()...")
        return resources.getDimension(R.dimen.get_width).toInt()
    }

    fun delayedStartNextActivity() {
        Log.d("login_anim", "inside delayedStartNextActivity()...")
        Handler().postDelayed(object : Runnable {
            override fun run() {
                var intent = Intent(this@family_login, Main2Activity::class.java)
                startActivity(intent)
                finish()
            }
        }, 100)
    }

    fun revealButton() {
        Log.d("login_anim", "inside revealButton()...")
        ActivityFamilyLoginBinding!!.signInBtn.setElevation(0f)
        ActivityFamilyLoginBinding!!.revealView.setVisibility(View.VISIBLE)

        var x: Int = ActivityFamilyLoginBinding!!.revealView.width
        var y: Int = ActivityFamilyLoginBinding!!.revealView.height

        var startX: Int = (getFinalWidth() / 2 + ActivityFamilyLoginBinding!!.signInBtn.x).toInt()
        var startY: Int = (getFinalWidth() / 2 + ActivityFamilyLoginBinding!!.signInBtn.y).toInt()

        var radius: Float = Math.max(x, y) * 1.2f

        var reveal: Animator = ViewAnimationUtils.createCircularReveal(ActivityFamilyLoginBinding!!.revealView, startX, startY
                , getFinalWidth().toFloat(), radius)

        reveal.setDuration(350)

        reveal.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator?) {
                super.onAnimationEnd(animation)
                finish()
            }
        })
        reveal.start()
    }

    fun showProgressDialog() {
        Log.d("login_anim", "inside showProgressDialog()...")
        ActivityFamilyLoginBinding!!.signInProgressBar.indeterminateDrawable.setColorFilter(resources.getColor(R.color.white), PorterDuff.Mode.SRC_IN)
        ActivityFamilyLoginBinding!!.signInProgressBar.visibility = View.VISIBLE
    }

    fun fadeOutProgressDialog() {
        Log.d("login_anim", "inside fadeOutProgressDialog()...")
        ActivityFamilyLoginBinding!!.signInProgressBar.animate().alpha(0f).setDuration(200).start()
    }


    fun getStringByLocal(context: Activity, id: Int, locale: String): Array<out String>? {
        var configuration: Configuration = Configuration(getResources().getConfiguration())
        configuration.setLocale(Locale(locale))
        return context.createConfigurationContext(configuration).getResources().getStringArray(id)
    }

    fun spinnerListener() {
        try {
            Log.d("Check_family_login", "Inside spinnerListener()")
            val spinner = findViewById<Spinner>(R.id.familyOrIndividualspinner)

            val roozArrayValue = getStringByLocal(this@family_login, R.array.indivisualOrFamilySpinner, "en")

            spinner.setSelection(1)

            spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parentView: AdapterView<*>, selectedItemView: View, position: Int, id: Long) {
                    //val SelectedItem = spinner.selectedItem.toString()

                    val selectedItem = roozArrayValue!![position]

                    if (selectedItem == "Individual") {
                        val i = Intent(this@family_login, login::class.java)
                        startActivity(i)
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                        finish()
                    } else if (selectedItem == "Join Family") {
                        val i = Intent(this@family_login, join_family_signup::class.java)
                        startActivity(i)
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                        finish()
                    }
                }

                override fun onNothingSelected(parentView: AdapterView<*>) {
                    val SelectedItem = spinner.selectedItem.toString()
                    if (SelectedItem == "Individual") {
                        val i = Intent(this@family_login, login::class.java)
                        startActivity(i)
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                        finish()
                    } else if (SelectedItem == "Join Family") {
                        val i = Intent(this@family_login, join_family_signup::class.java)
                        startActivity(i)
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                        finish()
                    }
                }

            }
        } catch (e: Exception) {
            Log.d("Check_family_login", "Inside spinnerListener()... Exception: $e")
        }
    }

    fun registerLink() {
        try {
            Log.d("Check_family_login", "Inside registerLink()")
            findViewById<TextView>(R.id.registerLink).setOnClickListener {
                val i = Intent(this, family_signup::class.java)
                i.putExtra("type", "f")
                startActivity(i)
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            }
        } catch (e: Exception) {
            Log.d("Check_family_login", "Inside registerLink() ... Exception: $e")
        }
    }

    /*
    fun JoinFamilyLink(){
        findViewById<TextView>(R.id.joinFamilyLink).setOnClickListener{
            startActivity(Intent(this,join_family_signup::class.java))
        }
    }
    */

    fun OnClickedLoginButton() {
        try {
            Log.d("Check_family_login", "Inside OnClickedLoginButton()")
            //findViewById<Button>(R.id.loginButton).setOnClickListener {
            findViewById<Button>(R.id.sign_in_btn).setOnClickListener {
                var l_phoneNumber = findViewById<EditText>(R.id.familyID).getText().toString()
                var l_password = findViewById<EditText>(R.id.password).getText().toString()
                if (l_phoneNumber.length != 0 && l_password.length != 0) {
                    login(l_phoneNumber, l_password)
                } else {
                    if (l_phoneNumber.length == 0) {
                        showErrorMsg(getString(R.string.enter_your_phone_number), ContextCompat.getColor(this, R.color.red))
                    } else {
                        showErrorMsg(getString(R.string.enter_your_password), ContextCompat.getColor(this, R.color.red))
                    }
                }
            }
        } catch (e: Exception) {
            Log.d("Check_family_login", "Inside OnClickedLoginButton() ... Exception: $e")
        }
    }

    fun login(l_p: String, l_pass: String) {
        makeFamilyLoginApiRequest(l_p, l_pass)
    }

    fun makeFamilyLoginApiRequest(l_phoneNumber: String, l_password: String) {
        doAsync {
            val client: OkHttpClient = OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build()

            val urlBuilder = HttpUrl.parse(ServerConfig().getFamilyLoginApi())!!.newBuilder()
            urlBuilder.addQueryParameter("l_phoneNumber", l_phoneNumber)
            urlBuilder.addQueryParameter("l_password", l_password)

            val url = urlBuilder.build().toString()

            val request = Request.Builder()
                    .url(url)
                    .build()

            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()

            uiThread {
                Log.d("mess", responce_string.toString())

                if (responce_string == "login") {
                    showErrorMsg(getString(R.string.successfully_logged_in), ContextCompat.getColor(this@family_login, R.color.green))
                    phoneNumber = l_phoneNumber
                    password = l_password
                    getFamilyId()
                }
                if (responce_string == "dataMissing") {
                    showErrorMsg(getString(R.string.something_goes_wrong), Color.RED)
                }
                if (responce_string == "noSuchAccount") {
                    showErrorMsg(getString(R.string.no_such_account_exist), Color.RED)
                }
                if (responce_string == "serverError") {
                    showErrorMsg(getString(R.string.server_error), Color.RED)
                }
                if (responce_string == "invalidPhoneNumber") {
                    showErrorMsg(getString(R.string.invalid_phone_number), Color.RED)
                }
                if (responce_string != "login"
                        && responce_string != "dataMissing"
                        && responce_string != "invalidPhoneNumber"
                        && responce_string != "noSuchAccount"
                        && responce_string != "serverError") {
                    showErrorMsg(getString(R.string.server_error), Color.RED)
                }
            }
        }
    }

    fun storeInPersistentData() {
        val loginInfo = getSharedPreferences("loginInfo", Context.MODE_PRIVATE)
        val editor = loginInfo.edit()
        editor.putString("password", password)
        editor.putString("phoneNumber", phoneNumber)
        editor.putString("familyId", familyId!!)
        editor.putString("familyOrIndividual", "f")
        editor.apply()

        startMainActivity()
    }

    fun getFamilyId() {
        doAsync {
            val client: OkHttpClient = OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build()

            val urlBuilder = HttpUrl.parse(ServerConfig().getFamilyIdApi())!!.newBuilder()
            urlBuilder.addQueryParameter("phoneNumber", phoneNumber)

            val url = urlBuilder.build().toString()

            val request = Request.Builder()
                    .url(url)
                    .build()

            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()

            uiThread {
                Log.d("mess", responce_string.toString())

                familyId = responce_string
                storeInPersistentData()
            }
        }
    }

    fun showErrorMsg(msg_text: String, color: Int) {
        val st = StyleableToast.Builder(this)
                .text(msg_text)
                .textColor(Color.WHITE)
                .backgroundColor(color)
                .build()
        st.show()
    }

    fun autoLogin() {
        try {
            Log.d("Check_family_Login", "Inside autoLogin")
            val loginInfo = applicationContext.getSharedPreferences("loginInfo", Context.MODE_PRIVATE)
            if (loginInfo.contains("familyOrIndividual") == true && loginInfo.contains("familyId") == true) {
                if (loginInfo.getString("familyId", "").toString() != "0") {
                    startMainActivity()
                }
            }
        } catch (e: Exception) {
            Log.d("Check_family_Login", "Inside autoLogin...Exception: $e")
        }
    }

    fun startMainActivity() {

        //val i = Intent(this, MainActivity::class.java)
        val i = Intent(this, Main2Activity::class.java)
        startActivity(i)
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        finish()
    }

    //make reset button work
    fun makeResetLinkWork() {
        try {
            Log.d("Check_family_Login", "Inside makeResetLinkWork")
            findViewById<TextView>(R.id.forgottenPasswordTextView).setOnClickListener {
                val i = Intent(this@family_login, ResetPassword::class.java)
                i.putExtra("type", "f")
                startActivity(i)
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            }
        } catch (e: Exception) {
            Log.d("Check_family_Login", "Inside makeResetLinkWork...Exception: $e")
        }
    }
}
