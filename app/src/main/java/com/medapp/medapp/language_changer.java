package com.medapp.medapp;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.medapp.medapp.IntroSlider.WelcomeActivity;
import com.medapp.medapp.loginSignup.individualOrFamily;

import java.util.List;
import java.util.Locale;

public class language_changer extends AppCompatActivity {

    Button eng, hin;
    SharedPreferences language;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.language_changer);

        boolean isFirstRun_1 = getSharedPreferences("PREFERENCE_1", MODE_PRIVATE)
                .getBoolean("isFirstRun", true);

        if (isFirstRun_1) {
            Log.d("First_", "Loading for the first time. . .");
            getSharedPreferences("PREFERENCE_1", MODE_PRIVATE).edit()
                    .putBoolean("isFirstRun", false).apply();
        } else {
            Log.d("First_", "Loading for the second time. . .");
            launchHomeScreen();
        }

        checkPhone_n_SMS();

        eng = findViewById(R.id.english);
        hin = findViewById(R.id.hindi);

        language = this.getSharedPreferences("lang", Context.MODE_PRIVATE);

        eng.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                changeLanguage changeLanguage = new changeLanguage();
                changeLanguage.execute("en");
            }
        });

        hin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                changeLanguage changeLanguage = new changeLanguage();
                changeLanguage.execute("hi");
            }
        });
    }

    private void checkPhone_n_SMS() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS)
                != PackageManager.PERMISSION_GRANTED) {

            new MaterialDialog.Builder(this)
                    .title(R.string.sms_permission)
                    .content(R.string.sms_perm_required)
                    .negativeText(R.string.cancel)
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                        }
                    })
                    .positiveText(R.string.give_permission)
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            getPermissionsUsingDexter_readSMS();
                        }
                    })
                    .show();
        }
    }

    private void getPermissionsUsingDexter_readSMS() {
        Dexter.withActivity(this)
                .withPermissions(
                        android.Manifest.permission.READ_SMS
                ).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {
                if (report.areAllPermissionsGranted()) {
                    Log.d("mess", "permission given");
                } else {
                    Log.d("mess", "permission not granted");
                }
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {

            }
        }).check();
    }

    private void launchHomeScreen() {
        startActivity(new Intent(language_changer.this, individualOrFamily.class));
        finish();
    }

    private void moveToNext() {
        startActivity(new Intent(language_changer.this, WelcomeActivity.class));
        finish();
    }

    @SuppressLint("StaticFieldLeak")
    class changeLanguage extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {

            String lang = strings[0];

            Locale myLocale = new Locale(lang);
            Resources res = getResources();
            DisplayMetrics dm = res.getDisplayMetrics();
            Configuration conf = res.getConfiguration();
            conf.locale = myLocale;
            res.updateConfiguration(conf, dm);

            SharedPreferences.Editor editor = language.edit();
            editor.putString("language", lang);
            editor.apply();
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            moveToNext();
        }
    }
}
