package com.medapp.medapp

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.text.InputType
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.afollestad.materialdialogs.DialogAction
import com.afollestad.materialdialogs.MaterialDialog
import com.medapp.medapp.config.ServerConfig
import com.medapp.medapp.database.DB_HELPER
import com.medapp.medapp.utils.left_drawer
import com.muddzdev.styleabletoastlibrary.StyleableToast
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.util.concurrent.TimeUnit

class HealthInsurance : AppCompatActivity() {
    private var healthInsurance_recyclerview: RecyclerView? = null
    private var result_list_for_healthInsurance: ArrayList<healthInsurance_store> = ArrayList<healthInsurance_store>()
    private var healthInsurance_adapter_holder: RecyclerView.Adapter<healthInsurance_adapter.NumberViewHolder>? = null
    private var phoneNumber: String? = null
    val TAG: String? = "Activity_Name"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_health_insurance)

        Log.d(TAG, "Inside HealthInsurance")

        findViewById<Toolbar>(R.id.healthInsuranceToolBar).setTitle(getString(R.string.health_insurance))
        findViewById<Toolbar>(R.id.healthInsuranceToolBar).setTitleTextColor(ContextCompat.getColor(this@HealthInsurance, R.color.white))

        //navigation drawer
        left_drawer(this, this@HealthInsurance, findViewById(R.id.healthInsuranceToolBar)!!).createNavigationDrawer()

        //on add clicked
        onclickAdd()

        //attach health insurance adapter to recyclerview
        attach_healthInsurance_adapter_to_recyclerview()

        //get phone number
        val loginInfo = applicationContext.getSharedPreferences("loginInfo", Context.MODE_PRIVATE)
        phoneNumber = loginInfo.getString("phoneNumber", "")

        //load insurance reminders
        loadInsuranceReminder(phoneNumber!!)
    }

    //on click add button
    fun onclickAdd() {
        findViewById<Button>(R.id.addHealthInsuranceButton).setOnClickListener {
            val i = Intent(this@HealthInsurance, addHealthInsuranceDetails::class.java)
            startActivity(i)
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        }
    }

    //onclick cancel reminder button
    fun onclickCancelReminder(policyNumber: String) {
        MaterialDialog.Builder(this)
                .title(getString(R.string.cancel_reminder))
                .content(getString(R.string.do_you_really_want_to_cancel_the_reminder))
                .inputType(InputType.TYPE_CLASS_TEXT)
                .positiveText(getString(R.string.yes))
                .negativeText(getString(R.string.no))
                .onPositive(object : MaterialDialog.SingleButtonCallback {
                    override fun onClick(dialog: MaterialDialog, which: DialogAction) {
                        cancelReminder(policyNumber)
                    }
                })
                .onNegative(object : MaterialDialog.SingleButtonCallback {
                    override fun onClick(dialog: MaterialDialog, which: DialogAction) {

                    }
                })
                .show()
    }

    fun cancelReminder(policyNumber: String) {
        doAsync {
            val client: OkHttpClient = OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build()

            val urlBuilder = HttpUrl.parse(ServerConfig().cancelInsuranceReminderApi())!!.newBuilder()
            urlBuilder.addQueryParameter("policyNumber", policyNumber)

            val url = urlBuilder.build().toString()

            val request = Request.Builder()
                    .url(url)
                    .build()

            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()

            uiThread {
                Log.d("mess", responce_string.toString())

                if (responce_string != "dataMissing") {
                    if (responce_string == "ok") {
                        //update expiry flag in local database
                        DB_HELPER(this@HealthInsurance).ActivateExpiryFlagInHealthInsurance(policyNumber)

                        loadInsuranceReminder(phoneNumber!!)
                    } else {
                        showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                    }
                } else {
                    showMessageAfterSignup(getString(R.string.something_goes_wrong), Color.RED)
                }
            }
        }
    }

    //profile recyclerview
    fun attach_healthInsurance_adapter_to_recyclerview() {
        val healthInsurance_recyclerView: RecyclerView = findViewById<RecyclerView>(R.id.healthInsuranceRecyclerView)
        healthInsurance_recyclerview = healthInsurance_recyclerView
        val layoutManager = LinearLayoutManager(applicationContext)
        healthInsurance_recyclerView.layoutManager = layoutManager
        healthInsurance_recyclerView.setHasFixedSize(false)
        healthInsurance_recyclerView.isNestedScrollingEnabled = false
        healthInsurance_adapter_holder = healthInsurance_adapter()
        healthInsurance_recyclerView.adapter = healthInsurance_adapter_holder
    }

    inner class healthInsurance_store(phoneNumber: String, policyNumber: String, renewDay: String, renewMonth: String, renewYear: String) {
        private var inner_phoneNumber: String = phoneNumber
        private var inner_policyNumber: String = policyNumber
        private var inner_renewDay: String = renewDay
        private var inner_renewMonth: String = renewMonth
        private var inner_renewYear: String = renewYear

        fun get_phoneNumber(): String {
            return inner_phoneNumber
        }

        fun get_policyNumber(): String {
            return inner_policyNumber
        }

        fun get_renewDate(): String {
            return inner_renewDay + "/" + inner_renewMonth + "/" + inner_renewYear
        }
    }

    private inner class healthInsurance_adapter() : RecyclerView.Adapter<healthInsurance_adapter.NumberViewHolder>() {

        override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): NumberViewHolder {
            val context = viewGroup.context
            val layoutIdForListItem = R.layout.health_insurance_blueprint
            val inflater = LayoutInflater.from(applicationContext)
            val shouldAttachToParentImmediately = false

            val view = inflater.inflate(layoutIdForListItem, viewGroup, shouldAttachToParentImmediately)
            val viewHolder = NumberViewHolder(view)

            return viewHolder
        }

        override fun onBindViewHolder(holder: NumberViewHolder, position: Int) {
            val current_object = result_list_for_healthInsurance.get(position)
            holder.bind(current_object)
        }

        override fun getItemCount(): Int {
            return result_list_for_healthInsurance.size
        }

        inner class NumberViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            private var policyNumberTextView: TextView? = null
            private var renewDateTextView: TextView? = null
            private var healthInsuranceIcon: ImageView? = null
            private var dontRemindMeButton: Button? = null

            init {
                policyNumberTextView = itemView.findViewById(R.id.healthInsurancePolicyNumberValue)
                renewDateTextView = itemView.findViewById(R.id.renewDateValue)
                healthInsuranceIcon = itemView.findViewById(R.id.healthInsuranceIcon)
                dontRemindMeButton = itemView.findViewById(R.id.dontRemindMeButton)
            }

            fun bind(current_object: healthInsurance_store) {
                policyNumberTextView!!.setText(current_object.get_policyNumber())
                renewDateTextView!!.setText(current_object.get_renewDate())
                healthInsuranceIcon!!.setImageResource(R.drawable.health_insurance_icon)

                dontRemindMeButton!!.setOnClickListener {
                    onclickCancelReminder(current_object.get_policyNumber())
                }
            }

        }

    }

    //load insurance reminders
    fun loadInsuranceReminder(phoneNumber: String) {
        doAsync {
            val client: OkHttpClient = OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build()

            val urlBuilder = HttpUrl.parse(ServerConfig().getMyInsuranceReminderApi())!!.newBuilder()
            urlBuilder.addQueryParameter("phoneNumber", phoneNumber)

            val url = urlBuilder.build().toString()

            val request = Request.Builder()
                    .url(url)
                    .build()

            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()

            uiThread {
                Log.d("mess", responce_string.toString())

                if (responce_string != "dataMissing") {
                    if (responce_string != "noReminders") {
                        processResponse(responce_string)
                    } else {
                        showMessageAfterSignup(getString(R.string.no_reminders_are_set_yet), Color.GREEN)
                    }
                } else {
                    showMessageAfterSignup(getString(R.string.something_goes_wrong), Color.RED)
                }
            }
        }
    }

    fun showMessageAfterSignup(msg_text: String, color: Int) {
        val st = StyleableToast.Builder(this)
                .text(msg_text)
                .textColor(Color.WHITE)
                .backgroundColor(color)
                .build()
        st.show()
    }

    fun processResponse(responseString: String) {
        result_list_for_healthInsurance.clear()
        val chunkOfData = responseString.split("___")
        var counter = 1
        val size = chunkOfData.size

        while (counter < size) {
            val singleDataSet = chunkOfData[counter]

            val policyNumber = singleDataSet.split("*")[0]
            val renewDay = (singleDataSet.split("*")[1]).split("^")[0]
            val renewMonth = ((singleDataSet.split("*")[1]).split("^")[1]).split("??")[0]
            val renewYear = (((singleDataSet.split("*")[1]).split("^")[1]).split("??")[1])

            result_list_for_healthInsurance.add(healthInsurance_store(phoneNumber.toString(), policyNumber, renewDay, renewMonth, renewYear))
            healthInsurance_adapter_holder!!.notifyDataSetChanged()

            counter++
        }
    }
}
