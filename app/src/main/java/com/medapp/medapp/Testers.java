package com.medapp.medapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.ImageView;

public class Testers extends AppCompatActivity {

    Button button;
    ImageView view;
    int REQUEST_CODE = 99;
//    int preference = ScanConstants.OPEN_CAMERA;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_testers);
        // Toast.makeText(this, "Testers activity started", Toast.LENGTH_SHORT).show();
        button = findViewById(R.id.scan);
        view = findViewById(R.id.img);


       /* button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    //Toast.makeText(Testers.this, "Button clicked", Toast.LENGTH_SHORT).show();
                    if (ContextCompat.checkSelfPermission(Testers.this, android.Manifest.permission.READ_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_GRANTED &&
                            ContextCompat.checkSelfPermission(Testers.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        // Permission is not granted

                        Intent intent = new Intent(Testers.this, ScanActivity.class);
                        intent.putExtra(ScanConstants.OPEN_INTENT_PREFERENCE, preference);
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        startActivityForResult(intent, REQUEST_CODE);
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    } else {
                        requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE}, 1);


                    }

                } catch (Exception e) {
                    Toast.makeText(Testers.this, "Some error on click", Toast.LENGTH_SHORT).show();
                    Log.d("Error___", String.valueOf(e));
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri uri = Objects.requireNonNull(data.getExtras()).getParcelable(ScanConstants.SCANNED_RESULT);
            Bitmap bitmap;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                if (uri != null) {
                    getContentResolver().delete(uri, null, null);
                }
                //Toast.makeText(this, "Came back successfully", Toast.LENGTH_SHORT).show();
                view.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
       *//* else
            Toast.makeText(this, "result code"+resultCode, Toast.LENGTH_SHORT).show();*/
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        /*switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Intent intent = new Intent(Testers.this, ScanActivity.class);
                    intent.putExtra(ScanConstants.OPEN_INTENT_PREFERENCE, preference);
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                    startActivityForResult(intent, REQUEST_CODE);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {
                    new MaterialDialog.Builder(Testers.this).title("Permission Denied")
                            .content("The application requires storage permission to work efficiently. Kindly grant the permissions manually from settings to use this feature.")
                            .negativeText("OK")
                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                                }
                            }).show();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }*/
    }
}
