package com.medapp.medapp

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.StrictMode
import android.provider.ContactsContract
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.text.InputType
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.afollestad.materialdialogs.DialogAction
import com.afollestad.materialdialogs.MaterialDialog
import com.amulyakhare.textdrawable.TextDrawable
import com.amulyakhare.textdrawable.util.ColorGenerator
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.medapp.medapp.config.ServerConfig
import com.medapp.medapp.utils.left_drawer
import com.muddzdev.styleabletoastlibrary.StyleableToast
import com.squareup.picasso.Picasso
import okhttp3.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.io.BufferedOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class previewImages : AppCompatActivity() {
    private var result_list_for_off_study_show: java.util.ArrayList<off_study_show_store> = java.util.ArrayList<off_study_show_store>()
    private var off_study_show_adapter_holder: RecyclerView.Adapter<off_study_show_adapter.NumberViewHolder>? = null
    private var imagesForPreview: java.util.ArrayList<*>? = null
    private var g_recyclerView: RecyclerView? = null
    val TAG: String? = "Activity_Name"
    private var phoneNumber: String? = null
    private var familyId: String? = null
    private var familyMemberName: String? = "0"
    private var typeOfDocument: String? = null
    private var categoryOfDocument: String? = null
    private var familyMemberPhoneNumber: String? = "0"
    private var PICK_CONTACT_REQUEST: Int = 1
    var progressDialog: Dialog? = null

    private var clickedItemCount: Int? = null
    private var clickedFamilyMemberName: String? = null

    private var name: String? = null
    private var number: String? = null

    var allowedByFamilyMembersFlag: String = "1"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_preview_images)
        setTitle("Step 4 - Preview")
        Log.d(TAG, "Inside preview_images")
        Log.d("Picking_Docs", "Inside previewImages activity . . ")
        //overridePendingTransition(R.xml.slide_in, R.xml.slide_out)

        left_drawer(this, this@previewImages, findViewById(R.id.toolbar_tabs2)!!).createNavigationDrawer()

        //get phone number and family id
        try {
            val loginInfo = applicationContext.getSharedPreferences("loginInfo", Context.MODE_PRIVATE)
            phoneNumber = loginInfo.getString("phoneNumber", "")
            familyId = loginInfo.getString("familyId", "")

            //get images for preview , category ,type
            val intent = intent
            val args = intent.getBundleExtra("BUNDLE")
            imagesForPreview = (args.getSerializable("ARRAYLIST")) as java.util.ArrayList<*>
            typeOfDocument = intent.getStringExtra("typeOfDocument")
            categoryOfDocument = intent.getStringExtra("categoryOfDocument")


            //for selecting from contacts . . .
            findViewById<Button>(R.id.contactPicker).setOnClickListener {
                //Now going to select a contact for which we want to upload . . .
                //getting permission to read Contacts . . .
                getPermissiontoReadContact()
                //pickContact()
            }

            //for opening images in gallery
            if (Build.VERSION.SDK_INT >= 24) {
                try {
                    val m = StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
                    m.invoke(null)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            //onclick listener for upload button
            onclickListenerForUploadButton()

            //atttach off study show adpater to recyclerview
            attach_off_study_show_adapter_to_recyclerview()

            //load images for preview
            load_images_for_preview()

            //load pick family members
            if (familyId != "0") {
                LoadFamilyMembersNames()
            } else {
                //we will be displaying the toolbar for individual too
                findViewById<Toolbar>(R.id.pickFamilyMembersToolBar).setVisibility(View.VISIBLE)
                findViewById<TextView>(R.id.chooseFamilyMemberHeading).setVisibility(View.VISIBLE)
                findViewById<TextView>(R.id.divider).setVisibility(View.VISIBLE)
                findViewById<ImageView>(R.id.chooseFamilyMemberDot).setVisibility(View.VISIBLE)
            }
        } catch (e: Exception) {
            Log.d("Picking_Docs", "Inside previewImages onCreate ...$e ")
        }
    }

    private fun getPermissiontoReadContact() {
        if (ContextCompat.checkSelfPermission(this@previewImages, android.Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {

            MaterialDialog.Builder(this@previewImages)
                    .title(getString(R.string.read_contact_permission))
                    .content(getString(R.string.permission_to_read_contacts_required))
                    .negativeText(R.string.cancel)
                    .onNegative(object : MaterialDialog.SingleButtonCallback {
                        override fun onClick(dialog: MaterialDialog, which: DialogAction) {

                        }
                    })
                    .positiveText(R.string.give_permission)
                    .onPositive(object : MaterialDialog.SingleButtonCallback {
                        override fun onClick(dialog: MaterialDialog, which: DialogAction) {
                            getPermissionsUsingDexter_contacts(
                                    android.Manifest.permission.READ_CONTACTS
                            )
                        }
                    })
                    .show()

        } else {
            pickContact()
        }
    }

    fun getPermissionsUsingDexter_contacts(permissionString: String) {
        Dexter.withActivity(this)
                .withPermissions(
                        permissionString
                ).withListener(object : MultiplePermissionsListener {
                    override fun onPermissionRationaleShouldBeShown(permissions: MutableList<PermissionRequest>?, token: PermissionToken?) {

                    }

                    override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                        if (report!!.areAllPermissionsGranted() == true) {
                            pickContact()

                            Log.d("mess", "permission given")
                        } else {
                            Log.d("mess", "permission not granted")
                        }
                    }
                })
                .check()
    }

    private fun pickContact() {
        val pickContactIntent = Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI)
        //pickContactIntent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE) // Show user only contacts w/ phone numbers
        startActivityForResult(pickContactIntent, PICK_CONTACT_REQUEST)
        //overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == PICK_CONTACT_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {

                val contactData = data?.data
                val c = contentResolver.query(contactData!!, null, null, null, null)
                if (c!!.moveToFirst()) {

                    name = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME))
                    val contactId = c.getString(c.getColumnIndex(ContactsContract.Contacts._ID))

                    var hasPhone = c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))

                    if (hasPhone.equals("1", ignoreCase = true))
                        hasPhone = "true"
                    else
                        hasPhone = "false"

                    if (java.lang.Boolean.parseBoolean(hasPhone)) {
                        val phones = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId, null, null)
                        while (phones!!.moveToNext()) {
                            number = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
                        }
                        phones.close()
                    }

                    // Find Email Addresses
                    /*val emails = contentResolver.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = " + contactId, null, null)
                    while (emails!!.moveToNext()) {
                        emailAddress = emails.getString(emails.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA))
                    }
                    emails.close()
*/
                    //mainActivity.onBackPressed();
                    // Toast.makeText(mainactivity, "go go go", Toast.LENGTH_SHORT).show();

                    Log.d("NumberSelected", "$name num$number ")
                }
                c.close()
                showMessageAfterSignup(name + R.string.is_selected, R.color.primary)
                familyId = "0"
                familyMemberName = name

                var tempNum: String = number!!.trim().replace(" ", "")

                if (tempNum.length == 10) {
                    familyMemberPhoneNumber = tempNum
                } else if (tempNum.length == 11) {
                    familyMemberPhoneNumber = tempNum.substring(1)
                } else if (tempNum.length == 12) {
                    familyMemberPhoneNumber = tempNum.substring(2)
                } else if (tempNum.length == 13) {
                    familyMemberPhoneNumber = tempNum.substring(3)
                }

                allowedByFamilyMembersFlag = "0"

                Log.d("NumberSelected", "$familyMemberName num $familyMemberPhoneNumber ")
                Log.d("NumberSelected", "flag:$allowedByFamilyMembersFlag")
                //onclick listener for upload button
                onclickListenerForUploadButton()
            }
        }
    }

    //offline study show recyclerview
    fun attach_off_study_show_adapter_to_recyclerview() {
        var off_study_show_recyclerView: RecyclerView = findViewById<RecyclerView>(R.id.offline_study_show_recyclerView)
        g_recyclerView = off_study_show_recyclerView
        val layoutManager = LinearLayoutManager(applicationContext)
        off_study_show_recyclerView.layoutManager = layoutManager
        off_study_show_recyclerView.setHasFixedSize(false)
        off_study_show_recyclerView.isNestedScrollingEnabled = false
        off_study_show_adapter_holder = off_study_show_adapter()
        off_study_show_recyclerView.adapter = off_study_show_adapter_holder
    }

    inner class off_study_show_store(
            img_file: File,
            img_file_uri: Uri
    ) {
        private var inner_file: File = img_file
        private var inner_file_uri: Uri = img_file_uri
        var specificCategoryValue: String? = "none"

        fun get_file(): File {
            return inner_file
        }

        fun get_file_uri(): Uri {
            //inner_file_uri = Uri.parse("file://"+inner_file_uri)
            return inner_file_uri
        }

        fun get_specificCategory(): String {
            return specificCategoryValue!!
        }

    }

    private inner class off_study_show_adapter() : RecyclerView.Adapter<off_study_show_adapter.NumberViewHolder>() {

        override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): NumberViewHolder {
            val context = viewGroup.context
            val layoutIdForListItem = R.layout.preview_blueprint
            val inflater = LayoutInflater.from(context)
            val shouldAttachToParentImmediately = false

            val view = inflater.inflate(layoutIdForListItem, viewGroup, shouldAttachToParentImmediately)
            val viewHolder = NumberViewHolder(view)

            return viewHolder
        }

        override fun onBindViewHolder(holder: NumberViewHolder, position: Int) {
            var current_object = result_list_for_off_study_show.get(position)
            holder.bind(current_object, position)
        }

        override fun getItemCount(): Int {
            return result_list_for_off_study_show.size
        }

        inner class NumberViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            private var preview: ImageView? = null
            private var specificCategoryContent: TextView? = null
            private var specificCategoryEditIcon: ImageView? = null
            private var imageNumberImageView: ImageView? = null

            init {
                preview = itemView.findViewById(R.id.off_study_photo_preview)
                specificCategoryContent = itemView.findViewById(R.id.specificCategoryContent)
                specificCategoryEditIcon = itemView.findViewById(R.id.editCategoryIcon)
                imageNumberImageView = itemView.findViewById(R.id.documentNumberImage)
            }

            fun bind(current_object: off_study_show_store, position: Int) {
                specificCategoryEditIcon!!.setOnClickListener {
                    createSpecificCategoryDialog(current_object, specificCategoryContent!!)
                }

                val drawable = TextDrawable.builder().buildRoundRect((position + 1).toString(), R.color.colorPrimaryDark,
                        300)
                imageNumberImageView!!.setImageDrawable(drawable)

                Picasso.with(applicationContext)
                        .load(current_object.get_file())
                        .fit()
                        .into(preview)

                preview!!.setOnClickListener {
                    //if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
                    val intent = Intent()
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                    intent.action = Intent.ACTION_VIEW
                    intent.setDataAndType(current_object.get_file_uri(), "image/*")
                    startActivity(intent)
                    //overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                    /*} else {
                        val intent = Intent()
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                        intent.action = Intent.ACTION_VIEW
                        val photoUri = FileProvider.getUriForFile(this@previewImages, BuildConfig.APPLICATION_ID + ".provider", current_object.get_file())
                        intent.data = photoUri
                        startActivity(intent)
                    }
                    */
                }
            }
        }
    }

    //load images for preview
    fun load_images_for_preview() {
        try {
            Log.d("imageGallery__", "Inside previewImages.....load_images_for_preview")
            var counter = 0
            var size = imagesForPreview!!.size
            //new introduced
            //var bitmapImg: Bitmap? = null

            doAsync {
                try {
                    Log.d("imageGallery__", "Inside previewImages.....load_images_for_preview...Inside doAsync ")
                    while (counter < size) {
                        try {
                            Log.d("imageGallery__", "Inside doAsync while loop ")

                            var bitmapImg = Picasso
                                    .with(applicationContext)
                                    .load(Uri.parse(imagesForPreview!![counter].toString()))
                                    .resize(350, 350)
                                    .get()

                            //create a file to write bitmap data
                            var f: File = File(applicationContext.getCacheDir(), System.currentTimeMillis().toString() + System.nanoTime().toString());
                            var os: OutputStream = BufferedOutputStream(FileOutputStream(f))
                            //bitmapImg.compress(Bitmap.CompressFormat.JPEG, 100, os)
                            bitmapImg?.compress(Bitmap.CompressFormat.JPEG, 100, os)
                            os.close()

                            val file = f
                            result_list_for_off_study_show.add(off_study_show_store(file, Uri.parse(imagesForPreview!![counter].toString())))

                            counter++
                        } catch (e: Exception) {
                            Log.d("imageGallery__", "Inside previewImages.....load_images_for_preview...Inside doAsync...Exception: $e")
                        }
                    }

                    uiThread {
                        off_study_show_adapter_holder!!.notifyDataSetChanged()
                    }
                } catch (e: Exception) {
                    Log.d("imageGallery__", "Inside previewImages.....load_images_for_preview...Inside doAsync..Exception: $e")
                }
            }
        } catch (e: Exception) {
            Log.d("imageGallery__", "Inside load_images_for_preview ..... Exception: $e")
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this@previewImages, Main2Activity::class.java)
        startActivity(intent)
        finish()
    }

    //upload button
    fun onclickListenerForUploadButton() {
        findViewById<Button>(R.id.uploadButton).setOnClickListener {

            if (familyId != "0") {
                //if family admin logs in
                if (familyMemberName != "0" && familyMemberPhoneNumber != "0") {
                    (findViewById(R.id.uploadButton) as Button).setEnabled(false)
                    (findViewById(R.id.uploadButton) as Button).setBackgroundColor(Color.GRAY)

                    showProcessProgress(getString(R.string.initialization))
                    startUpload()
                } else {
                    showMessageAfterSignup(getString(R.string.select_family_member), Color.RED)
                }
            } else {
                //if the individual is logged in
                (findViewById(R.id.uploadButton) as Button).setEnabled(false)
                (findViewById(R.id.uploadButton) as Button).setBackgroundColor(Color.GRAY)

                showProcessProgress(getString(R.string.initialization))
                startUpload()
            }

        }
    }

    fun startUpload() {
        val MEDIA_TYPE_PNG: MediaType = MediaType.parse("file/*")!!
        val size = imagesForPreview!!.size
        var counter = 0
        val c: Calendar = Calendar.getInstance()
        val df1: SimpleDateFormat = SimpleDateFormat("dd")
        val df2: SimpleDateFormat = SimpleDateFormat("MM")
        val df3: SimpleDateFormat = SimpleDateFormat("yyyy")
        var uploadDay: String = df1.format(c.getTime()).toString()
        if (uploadDay[0].toString() == "0") {
            uploadDay = (uploadDay.replaceFirst("0", "")).toString()
        }
        var uploadMonth: String = df2.format(c.getTime()).toString()
        if (uploadMonth[0].toString() == "0") {
            uploadMonth = (uploadMonth.replaceFirst("0", "")).toString()
        }
        val uploadYear: String = df3.format(c.getTime()).toString()

        doAsync {
            while (counter < size) {
                /*________image upload______________________________*/
                uiThread {
                    progressDialog!!.dismiss()
                    showProcessProgress(getString(R.string.upload_started_doc) + counter + 1)
                }
                var bitmapImg = Picasso
                        .with(applicationContext)
                        .load(Uri.parse(imagesForPreview!![counter].toString()))
                        .resize(350, 350)
                        .get()

                /*get image description*/
                val child = g_recyclerView!!.getChildAt(counter)
                var descriptionOfGivenImage = child.findViewById<EditText>(R.id.descriptionText).getText().toString()
                if (descriptionOfGivenImage.length == 0) {
                    descriptionOfGivenImage = " "
                }

                /*get specific category*/
                var specificCategory = child.findViewById<TextView>(R.id.specificCategoryContent).text.toString()

                /*________create file______________________________*/
                uiThread {
                    progressDialog!!.dismiss()
                    showProcessProgress(getString(R.string.processing))
                }
                //create a file to write bitmap data
                var f: File = File(applicationContext.getCacheDir(), System.currentTimeMillis().toString());
                var os: OutputStream = BufferedOutputStream(FileOutputStream(f))
                bitmapImg.compress(Bitmap.CompressFormat.JPEG, 100, os)
                os.close()
                var imageFile = f
                var imageName = counter.toString() + ".jpg"
                var image = imageFile


                /*upload api request*/
                uiThread {
                    progressDialog!!.dismiss()
                    showProcessProgress(getString(R.string.uploading))
                }

                var response: Response? = null
                try {
                    Log.d("tester", "Flag:" + allowedByFamilyMembersFlag)
                    Log.d("tester", "Family Id:" + familyId)
                    val client: OkHttpClient = OkHttpClient.Builder()
                            .connectTimeout(60, TimeUnit.SECONDS)
                            .writeTimeout(60, TimeUnit.SECONDS)
                            .readTimeout(60, TimeUnit.SECONDS)
                            .build()

                    val requestBody: RequestBody = MultipartBody.Builder().setType(MultipartBody.FORM)
                            .addFormDataPart("file", imageName, RequestBody.create(MEDIA_TYPE_PNG, image))
                            .addFormDataPart("familyId", familyId!!)
                            .addFormDataPart("familyMemberName", familyMemberName!!)
                            .addFormDataPart("phoneNumber", phoneNumber!!)
                            .addFormDataPart("familyMemberPhoneNumber", familyMemberPhoneNumber!!)
                            .addFormDataPart("description", descriptionOfGivenImage)
                            .addFormDataPart("typeOfDocument", typeOfDocument!!)
                            .addFormDataPart("categoryOfDocument", categoryOfDocument!!)
                            .addFormDataPart("specificCategory", specificCategory)
                            .addFormDataPart("uploadDay", uploadDay)
                            .addFormDataPart("uploadMonth", uploadMonth)
                            .addFormDataPart("uploadYear", uploadYear)
                            .addFormDataPart("memberFlag", allowedByFamilyMembersFlag)
                            .build()
                    val request: Request = Request.Builder().url(ServerConfig().getStorageServerAddress())
                            .post(requestBody).build()
                    response = client.newCall(request).execute()
                } catch (e: UnknownHostException) {
                    uiThread {
                        val toast: Toast = Toast.makeText(applicationContext, e.toString(), Toast.LENGTH_LONG)
                        toast.show()
                    }
                } catch (e: SocketTimeoutException) {
                    uiThread {
                        val toast: Toast = Toast.makeText(applicationContext, e.toString(), Toast.LENGTH_LONG)
                        toast.show()
                    }
                }
                val responce_string = response!!.body()!!.string()


                /*all images are uploaded*/
                if (counter == (size - 1)) {
                    uiThread {
                        progressDialog!!.dismiss()
                        //Log.d("mess", "responce from upload.php: for others" + allowedByFamilyMembersFlag)
                        Log.d("mess", "responce from upload.php: " + responce_string)

                        if (responce_string == "true") {
                            val toast: Toast = Toast.makeText(applicationContext, getString(R.string.document_uploaded_successfully), Toast.LENGTH_LONG)
                            toast.show()
                        } else {
                            val toast: Toast = Toast.makeText(applicationContext, getString(R.string.server_error), Toast.LENGTH_LONG)
                            toast.show()
                        }
                        openHome()
                        finish()
                    }
                }
                counter++
            }
        }
    }

    fun showProcessProgress(msg: String) {
        progressDialog = MaterialDialog.Builder(this)
                .title(R.string.please_wait)
                .content(msg)
                .progress(true, 0)
                .show()
    }

    //create specific category dialog
    fun createSpecificCategoryDialog(current_object: off_study_show_store, specificCategoryTextView: TextView) {
        MaterialDialog.Builder(this)
                .title(getString(R.string.add_category))
                .items(R.array.specificCategoryItems)
                .itemsCallbackSingleChoice(-1, object : MaterialDialog.ListCallbackSingleChoice {
                    override fun onSelection(dialog: MaterialDialog?, itemView: View?, which: Int, text: CharSequence?): Boolean {
                        if (text == "Create Custom Category") {
                            createInputDialogForCustomCategory(current_object, specificCategoryTextView)
                        } else {
                            val category: String = text.toString().replace(" ", "")
                            current_object.specificCategoryValue = category
                            //text.toString()
                            specificCategoryTextView.setText(current_object.specificCategoryValue)
                        }

                        Log.d("mess", current_object.specificCategoryValue)

                        return true
                    }

                })
                .positiveText(R.string.add)
                .show()
    }

    fun createInputDialogForCustomCategory(current_object: off_study_show_store, specificCategoryTextView: TextView) {
        MaterialDialog.Builder(this)
                .title(getString(R.string.custom_category))
                .content(R.string.enter_custom_category)
                .inputType(InputType.TYPE_CLASS_TEXT)
                .positiveText(getString(R.string.create))
                .input(getString(R.string.enter_custom_category), "", object : MaterialDialog.InputCallback {
                    override fun onInput(dialog: MaterialDialog, input: CharSequence?) {
                        current_object.specificCategoryValue = input.toString()
                        specificCategoryTextView.setText(current_object.specificCategoryValue)

                        Log.d("mess", current_object.specificCategoryValue)
                    }
                }).show()
    }

    //load family members initials to pick family members
    fun LoadFamilyMembersNames() {
        doAsync {
            val client: OkHttpClient = OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build()

            val urlBuilder = HttpUrl.parse(ServerConfig().getFamilyMemberNamesAndPhoneNumber())!!.newBuilder()
            urlBuilder.addQueryParameter("familyId", familyId)

            val url = urlBuilder.build().toString()

            val request = Request.Builder()
                    .url(url)
                    .build()

            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()

            uiThread {
                Log.d("mess", responce_string.toString())

                if (responce_string != "dataMissing") {
                    if (responce_string != "serverError") {
                        if (responce_string.length < 450) {
                            loadFamilyMemberNamesInLayout(responce_string)
                        } else {
                            showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                        }
                    } else {
                        showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                    }
                } else {
                    showMessageAfterSignup(getString(R.string.something_goes_wrong), Color.RED)
                }
            }
        }
    }

    fun loadFamilyMemberNamesInLayout(familyMemberNamesString: String) {
        val arr_of_namesAndPhoneNumber = familyMemberNamesString.split("___")
        var counter = 1
        val size = arr_of_namesAndPhoneNumber.size
        while (counter < size) {
            val name = arr_of_namesAndPhoneNumber[counter].split("*")[0]
            val phoneNumber = arr_of_namesAndPhoneNumber[counter].split("*")[1]

            loadPickFamilyMembersToolBar(name, phoneNumber)

            counter++
        }
    }

    fun showMessageAfterSignup(msg_text: String, color: Int) {
        val st = StyleableToast.Builder(this)
                .text(msg_text)
                .textColor(Color.WHITE)
                .backgroundColor(color)
                .build()
        st.show()
    }

    fun loadPickFamilyMembersToolBar(fMN: String, fMPN: String) {
        val generator = ColorGenerator.MATERIAL // or use DEFAULT
        val color = generator.randomColor
        var t = TextDrawable.builder()
                .buildRoundRect(fMN[0].toString() + fMN[1].toString(), color, 300) // radius in px
        val inLay = findViewById<LinearLayout>(R.id.pickFamilyMembersScrollViewLayout) as LinearLayout
        val i = ImageView(this)
        i.setPadding(5, 5, 5, 5)
        i.setImageDrawable(t)
        i.setLayoutParams(android.view.ViewGroup.LayoutParams(140, 140))
        i.setMaxHeight(40)
        i.setMaxWidth(40)

        i.setOnClickListener {
            showMessageAfterSignup(fMN.toString() + R.string.is_selected, ContextCompat.getColor(this@previewImages, R.color.green))

            if (clickedItemCount == null && clickedFamilyMemberName == null) {
                clickedItemCount = inLay.indexOfChild(i)
                clickedFamilyMemberName = fMN
            } else {
                val t2 = TextDrawable.builder()
                        .buildRoundRect(clickedFamilyMemberName!![0].toString(), color, 300) // radius in px
                (inLay.getChildAt(clickedItemCount!!) as ImageView).setImageDrawable(t2)
                clickedItemCount = inLay.indexOfChild(i)
                clickedFamilyMemberName = fMN
            }

            t = TextDrawable.builder()
                    .beginConfig()
                    .withBorder(15) /* thickness in px */
                    .bold()
                    .endConfig()
                    .buildRoundRect(fMN[0].toString(), color, 300) // radius in px
            i.setImageDrawable(t)

            familyMemberName = fMN
            familyMemberPhoneNumber = fMPN
            Log.d("mess", familyMemberName + familyMemberPhoneNumber)
        }
        inLay.addView(i)
    }

    //open home activity
    fun openHome() {
        val i = Intent(this@previewImages, MainActivity::class.java)
        startActivity(i)
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        finish()
    }
}

