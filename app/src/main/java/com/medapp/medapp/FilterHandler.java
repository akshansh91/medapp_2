package com.medapp.medapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.medapp.medapp.utils.left_drawer;
import com.squareup.picasso.Picasso;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Calendar;

public class FilterHandler extends AppCompatActivity {

    SharedPreferences loginInfo;
    ImageView date_from, date_to, filter_category, calendar;
    String fromDate, fromday, frommonth, fromyear;
    String today, tomonth, toyear, toDate;
    String category_chosen;
    String familyId, phoneNumber;
    /*
        TextView label, dateViewer;
    */
    RadioGroup radioGroup;
    RadioButton radioButton;
    Button clear, done;
    /*LinearLayout picker;*/
    Toolbar toolbar;
    TextView fromDateView, toDateView, fromTitle, toTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.filter_layout);

        date_from = findViewById(R.id.dateFrom);
        date_to = findViewById(R.id.dateTo);
        filter_category = findViewById(R.id.category);
        /*calendar = findViewById(R.id.calendar);
        label = findViewById(R.id.label);
        dateViewer = findViewById(R.id.dateViewer);*/
        radioGroup = findViewById(R.id.radioCategory);
        clear = findViewById(R.id.clear);
        done = findViewById(R.id.done);
        /*picker = findViewById(R.id.picker);*/
        toolbar = findViewById(R.id.toolbarFilterLayout);
        fromDateView = findViewById(R.id.fromDate);
        toDateView = findViewById(R.id.toDate);
        fromTitle = findViewById(R.id.fromTitle);
        toTitle = findViewById(R.id.toTitle);

        new left_drawer(this, FilterHandler.this, toolbar).createNavigationDrawer();

        //get family id from persistent storage
        loginInfo = getSharedPreferences("loginInfo", Context.MODE_PRIVATE);
        familyId = loginInfo.getString("familyId", null);
        phoneNumber = loginInfo.getString("phoneNumber", "");

        try {
            Log.d("Check_FilterHandler", "Inside onCreate()");
            Picasso.with(this).load(R.drawable.calender_1).fit().into(date_from);
            Picasso.with(this).load(R.drawable.calender_1).fit().into(date_to);
            Picasso.with(this).load(R.drawable.category_1).fit().into(filter_category);
            //Picasso.with(this).load(R.drawable.calender_2).fit().into(calendar);

            date_from.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    /*if (calendar.getVisibility() == View.GONE) {
                        calendar.setVisibility(View.VISIBLE);
                        label.setVisibility(View.VISIBLE);
                        dateViewer.setVisibility(View.VISIBLE);
                        picker.setVisibility(View.GONE);
                    }*/
                    showFromDateDialog();
                }
            });

            date_to.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    /*dateViewer.setText("");*/
                    /*if (calendar.getVisibility() == View.GONE) {
                        calendar.setVisibility(View.VISIBLE);
                        label.setVisibility(View.VISIBLE);
                        dateViewer.setVisibility(View.VISIBLE);
                        picker.setVisibility(View.GONE);
                    }*/
                    if (!fromday.equals("0") && !frommonth.equals("0") && !fromyear.equals("0")) {
                        showToDateDialog();
                    } else {
                        Toast.makeText(FilterHandler.this, R.string.pick_from_date_first,
                                Toast.LENGTH_SHORT).show();
                    }
                }
            });

            /*filter_category.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (picker.getVisibility() == View.GONE) {
                        calendar.setVisibility(View.GONE);
                        *//*label.setVisibility(View.GONE);
                        dateViewer.setVisibility(View.GONE);*//*
                        picker.setVisibility(View.VISIBLE);
                    }
                }
            });*/

            //String fromDate,toDate,Category;
            done.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Log.d("Check_FilterHandler", "Inside done.setOnClickListener() ");
                        int selectedID = radioGroup.getCheckedRadioButtonId();
                        radioButton = findViewById(selectedID);

                        category_chosen = String.valueOf(radioButton.getText()).replace(" ", "");

                        Toast.makeText(FilterHandler.this, R.string.date_from + fromDate, Toast.LENGTH_SHORT).show();
                        Toast.makeText(FilterHandler.this, R.string.date_to + toDate, Toast.LENGTH_SHORT).show();
                        Toast.makeText(FilterHandler.this, R.string.category + category_chosen, Toast.LENGTH_SHORT).show();

                        Intent i = new Intent(FilterHandler.this, history_pickFamilyMember.class);
                        i.putExtra("startDate", fromDate);
                        i.putExtra("endDate", toDate);
                        i.putExtra("category", category_chosen);
                        startActivity(i);

                        Log.d("Values__", "FilterHandler === from: " + fromDate + " to: " + toDate + " category: " + category_chosen);
                    } catch (Exception e) {
                        Log.d("Check_FilterHandler", "Inside done.setOnClickListener() ... Exception: " + e);
                    }
                }
            });

            clear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //dateViewer.setText("");
                    //calendar.setVisibility(View.GONE);
                    //label.setVisibility(View.GONE);
                    fromDateView.setText("");
                    toDateView.setText("");
                }
            });
        } catch (Exception e) {
            Log.d("Check_FilterHandler", "Inside onCreate() ... Exception: " + e);
        }
    }

    private void showToDateDialog() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                        today = String.valueOf(dayOfMonth);
                        if (today.equals("0")) {
                            today.replaceFirst("0", "");
                        }
                        tomonth = String.valueOf(monthOfYear + 1);
                        if (tomonth.equals("0")) {
                            tomonth.replaceFirst("0", "");
                        }
                        toyear = String.valueOf(year);

                        toDate = toyear + "-" + tomonth + "-" + today;

                        Toast.makeText(FilterHandler.this, toDate,
                                Toast.LENGTH_SHORT).show();
                        toDateView.setText(toDate);
                    }
                }, now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH));
        dpd.setVersion(DatePickerDialog.Version.VERSION_1);
        dpd.show(getFragmentManager(), "Datepickerdialog");
    }

    private void showFromDateDialog() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                        fromday = String.valueOf(dayOfMonth);
                        if (fromday.equals("0")) {
                            fromday.replaceFirst("0", "");
                        }
                        frommonth = String.valueOf(monthOfYear + 1);
                        if (frommonth.equals("0")) {
                            frommonth.replaceFirst("0", "");
                        }
                        fromyear = String.valueOf(year);

                        fromDate = fromyear + "-" + frommonth + "-" + fromday;

                        Toast.makeText(FilterHandler.this, fromDate,
                                Toast.LENGTH_SHORT).show();
                        fromDateView.setText(fromDate);
                    }
                }, now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH));
        dpd.setVersion(DatePickerDialog.Version.VERSION_2);
        dpd.show(getFragmentManager(), "Datepickerdialog");
    }
}
