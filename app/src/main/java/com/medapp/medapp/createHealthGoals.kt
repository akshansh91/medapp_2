package com.medapp.medapp

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.Button
import android.widget.EditText
import android.widget.Spinner
import com.medapp.medapp.config.ServerConfig
import com.medapp.medapp.database.DB_HELPER
import com.muddzdev.styleabletoastlibrary.StyleableToast
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class createHealthGoals : AppCompatActivity() {
    private var phoneNumber: String? = null
    private var priority: String? = null
    private var fromday: String? = "0"
    private var frommonth: String? = "0"
    private var fromyear: String? = "0"
    private var UserSysTime: String? = "0:0"
    val TAG: String? = "Activity_Name"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_health_goals)
        Log.d(TAG, "Inside createHealthGoals")
        onClickCreateMyGoalButton()

        //get familyId
        val loginInfo = applicationContext.getSharedPreferences("loginInfo", Context.MODE_PRIVATE)
        phoneNumber = loginInfo.getString("phoneNumber", "")

        //date button function
        initDefaultSysDate()
        initDateButton()
        onClickDateButton()

    }

    fun getStringByLocal(context: Activity, id: Int, locale: String): Array<out String>? {
        var configuration: Configuration = Configuration(getResources().getConfiguration())
        configuration.setLocale(Locale(locale))
        return context.createConfigurationContext(configuration).getResources().getStringArray(id)
    }

    fun onClickCreateMyGoalButton() {
        findViewById<Button>(R.id.createMyGoalButton).setOnClickListener {

            val roozArrayValue = getStringByLocal(this@createHealthGoals,
                    R.array.healthGoalsPriorityList, "en")

            var goal = findViewById<EditText>(R.id.goalInput).text.toString()
            //var priority = findViewById<Spinner>(R.id.prioritySpinner).selectedItem.toString()
            var goalDay = fromday
            var goalMonth = frommonth
            var goalYear = fromyear


            findViewById<Spinner>(R.id.prioritySpinner).onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    val selectedItem = roozArrayValue!![position]
                    priority = selectedItem
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {
                    //To change body of created functions use File | Settings | File Templates.
                }
            }


            if (goal.length > 0 && priority!!.length > 0 && goalDay!!.length > 0 && goalMonth!!.length > 0 && goalYear!!.length > 0) {
                createHealthGoals(phoneNumber!!, goal, priority!!, goalDay, goalMonth, goalYear)
            } else {
                showMessageAfterSignup(getString(R.string.all_fields_are_compulsory), Color.RED)
            }
        }
    }

    fun createHealthGoals(phoneNumber: String, healthGoals: String, priority: String, goalDay: String, goalMonth: String, goalYear: String) {
        doAsync {
            val client: OkHttpClient = OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build()

            val urlBuilder = HttpUrl.parse(ServerConfig().createHealthGoalsApi())!!.newBuilder()
            urlBuilder.addQueryParameter("phoneNumber", phoneNumber)
            urlBuilder.addQueryParameter("healthGoals", healthGoals)
            urlBuilder.addQueryParameter("priority", priority)
            urlBuilder.addQueryParameter("goalDay", goalDay)
            urlBuilder.addQueryParameter("goalMonth", goalMonth)
            urlBuilder.addQueryParameter("goalYear", goalYear)

            val url = urlBuilder.build().toString()

            val request = Request.Builder()
                    .url(url)
                    .build()

            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()

            uiThread {
                Log.d("mess", responce_string.toString())

                if (responce_string != "dataMissing") {
                    if (responce_string != "serverError") {
                        if (responce_string == "ok") {
                            //save to local db
                            DB_HELPER(applicationContext).insertIntoHealthGoals(System.nanoTime().toInt(), healthGoals, priority, goalDay, goalMonth, goalYear, "0")

                            //layout response to user
                            showMessageAfterSignup(getString(R.string.you_have_successfully_created_your_health_goal), Color.GREEN)
                            openHealthGoalsList()
                            finish()
                        } else {
                            showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                        }
                    } else {
                        showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                    }
                } else {
                    showMessageAfterSignup(getString(R.string.something_goes_wrong), Color.RED)
                }
            }
        }
    }

    fun openHealthGoalsList() {
        val i = Intent(this, healthGoals::class.java)
        startActivity(i)
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
    }

    fun showMessageAfterSignup(msg_text: String, color: Int) {
        val st = StyleableToast.Builder(this)
                .text(msg_text)
                .textColor(Color.WHITE)
                .backgroundColor(color)
                .build()
        st.show()
    }


    //init date button
    fun initDateButton() {
        findViewById<Button>(R.id.dateButton).text = "$fromday/$frommonth/$fromyear"
    }

    //onclick date button
    fun onClickDateButton() {
        findViewById<Button>(R.id.dateButton).setOnClickListener {
            createDatePicker()
        }
    }

    //create date picker
    fun createDatePicker() {
        val now = Calendar.getInstance()
        val dpd = DatePickerDialog.newInstance(
                object : DatePickerDialog.OnDateSetListener {
                    override fun onDateSet(view: DatePickerDialog?, year_: Int, monthOfYear: Int, dayOfMonth: Int) {
                        fromday = dayOfMonth.toString()
                        if (fromday!![0].toString() == "0") {
                            fromday!!.replaceFirst("0", "")
                        }
                        frommonth = (monthOfYear + 1).toString()
                        if (frommonth!![0].toString() == "0") {
                            frommonth!!.replaceFirst("0", "")
                        }
                        fromyear = year_.toString()

                        initDateButton()
                    }
                },
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        )
        dpd.show(fragmentManager, "Datepickerdialog")
    }

    //initiate default date and time
    fun initDefaultSysDate() {
        fromday = getDay()
        frommonth = getMonth()
        fromyear = getYear()
        UserSysTime = getUserSysTime()
    }

    //system date and time
    fun getDay(): String {
        val c: Calendar = Calendar.getInstance()
        val df: SimpleDateFormat = SimpleDateFormat("dd")
        val formattedDate: String = df.format(c.time).toString()
        if (formattedDate[0].toString() == "0") {
            formattedDate.replaceFirst("0", "")
        }
        return formattedDate
    }

    fun getMonth(): String {
        val c: Calendar = Calendar.getInstance()
        val df: SimpleDateFormat = SimpleDateFormat("MM")
        val formattedDate: String = df.format(c.time).toString()
        if (formattedDate[0].toString() == "0") {
            formattedDate.replaceFirst("0", "")
        }
        return formattedDate
    }

    fun getYear(): String {
        val c: Calendar = Calendar.getInstance()
        val df: SimpleDateFormat = SimpleDateFormat("yyyy")
        val formattedDate: String = df.format(c.time).toString()
        return formattedDate
    }

    fun getUserSysTime(): String {
        val c: Calendar = Calendar.getInstance()
        val df: SimpleDateFormat = SimpleDateFormat("HH:mm")
        val formattedDate: String = df.format(c.time).toString()
        return formattedDate
    }

    //get time for arr data
    fun getTimeForArrDate(day: String, month: String, year: String): String {
        val c = Calendar.getInstance()
        c.set(year.toInt(), month.toInt(), day.toInt(), 0, 0)
        return c.timeInMillis.toString()
    }
}
