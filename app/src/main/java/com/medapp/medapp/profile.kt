package com.medapp.medapp

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Rect
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.StrictMode
import android.provider.MediaStore
import android.support.constraint.ConstraintLayout
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.InputType
import android.util.Log
import android.view.*
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.afollestad.materialdialogs.DialogAction
import com.afollestad.materialdialogs.MaterialDialog
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.drawee.view.SimpleDraweeView
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.medapp.medapp.config.ServerConfig
import com.medapp.medapp.utils.left_drawer
import com.muddzdev.styleabletoastlibrary.StyleableToast
import com.squareup.picasso.Picasso
import com.theartofdev.edmodo.cropper.CropImage
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import com.zhihu.matisse.Matisse
import com.zhihu.matisse.MimeType
import com.zhihu.matisse.engine.impl.PicassoEngine
import okhttp3.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.find
import org.jetbrains.anko.uiThread
import java.io.BufferedOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class profile : AppCompatActivity() {
    private var familyId: String? = null
    private var myPhoneNumber: String? = null
    private var phoneNumber: String? = null
    private var fullNameFromServer: String? = null
    private var emailFromServer: String? = null
    private var phoneNumberFromServer: String? = null
    private var dateOfBirthFromServer: String? = null
    private var sexFromServer: String? = null
    private var bloodGroupFromServer: String? = null
    private var ageFromServer: String? = null
    private var allergyFromServer: String? = null

    private var layout: ConstraintLayout? = null
    private var REQUEST_CODE_FOR_GALLERY_CAPTURE = 2

    private var profile_recyclerview: RecyclerView? = null
    private var result_list_for_profile: ArrayList<profile_store> = ArrayList<profile_store>()
    private var profile_adapter_holder: RecyclerView.Adapter<profile_adapter.NumberViewHolder>? = null

    val REQUEST_IMAGE_CAPTURE = 1
    var photoFile: File? = null

    var bdLayout: LinearLayout? = null

    var fromDate: String? = null
    var fromday: String? = null
    var frommonth: String? = null
    var fromyear: String? = null
    var gender: String? = null

    var bg_group: RadioGroup? = null

    var arrayList: HashMap<kotlin.Int, String>? = null
    var listImages: ArrayList<kotlin.Int>? = null
    var listValues: ArrayList<String>? = null
    var infoDialog: AlertDialog? = null
    var docGenderArray: Array<String>? = null

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_2)

        var alertD: AlertDialog = AlertDialog.Builder(this).create()

        infoDialog = AlertDialog.Builder(this).create()

        val isFirstRunHintProfile = getSharedPreferences("PREFERENCE_HINT_PROFILE", Context.MODE_PRIVATE)
                .getBoolean("isFirstRunHintProfile", true)

        if (isFirstRunHintProfile) {
            Log.d("isFirstRunHintProfile", "Loading for the first time. . .")
            showInfoDialog()
            getSharedPreferences("PREFERENCE_HINT_PROFILE", Context.MODE_PRIVATE).edit()
                    .putBoolean("isFirstRunHintProfile", false).apply()
        } else {
            Log.d("isFirstRunHintProfile", "Loading for the second time. . .")
        }

        Fresco.initialize(this@profile)

        docGenderArray = getStringByLocal(this@profile, R.array.gender, "en")

        bdLayout = findViewById(R.id.layout_bd)

        layout = findViewById(R.id.profileLayout)

        //left drawer
        left_drawer(this, this@profile, findViewById(R.id.toolbarProfile)).createNavigationDrawer()

        //get familyId
        val loginInfo = applicationContext.getSharedPreferences("loginInfo", Context.MODE_PRIVATE)
        familyId = loginInfo.getString("familyId", "")
        myPhoneNumber = loginInfo.getString("phoneNumber", "")

        //get phone number
        val p_i = intent
        phoneNumber = p_i.getStringExtra("phoneNumber")

        //load profile info
        loadProfileInfo()

        //function to edit icons
        makeEditIconsWork()

        //attach profile adapter to recyclerview
        //attach_profile_adapter_to_recyclerview()

        findViewById<LinearLayout>(R.id.layout_age).setOnClickListener {
            openDialog()
        }

        findViewById<LinearLayout>(R.id.layout_gender).setOnClickListener {
            openGenderSelectionDialog()
        }

        bdLayout!!.setOnClickListener {
            showBD_Dialog()
        }

        //this for choosing BG from a list of options...
        val layoutInflater: LayoutInflater = LayoutInflater.from(this@profile)
        val promptView: View = layoutInflater.inflate(R.layout.custom_bg_change_dialog, null)
        alertD.setView(promptView)

        bg_group = promptView.findViewById(R.id.rg_bg)

        bg_group!!.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.o1 -> {
                    val checkedRadioButton: RadioButton = group.findViewById(checkedId)
                    val text: String = checkedRadioButton.text.toString()
                    updateGivenField(getString(R.string.bolld_grp_), text)
                    alertD.dismiss()
                }
                R.id.o2 -> {
                    val checkedRadioButton: RadioButton = group.findViewById(checkedId)
                    val text: String = checkedRadioButton.text.toString()
                    updateGivenField(getString(R.string.bolld_grp_), text)
                    alertD.dismiss()
                }
                R.id.a1 -> {
                    val checkedRadioButton: RadioButton = group.findViewById(checkedId)
                    val text: String = checkedRadioButton.text.toString()
                    updateGivenField(getString(R.string.bolld_grp_), text)
                    alertD.dismiss()
                }
                R.id.a2 -> {
                    val checkedRadioButton: RadioButton = group.findViewById(checkedId)
                    val text: String = checkedRadioButton.text.toString()
                    updateGivenField(getString(R.string.bolld_grp_), text)
                    alertD.dismiss()
                }
                R.id.b1 -> {
                    val checkedRadioButton: RadioButton = group.findViewById(checkedId)
                    val text: String = checkedRadioButton.text.toString()
                    updateGivenField(getString(R.string.bolld_grp_), text)
                    alertD.dismiss()
                }
                R.id.b2 -> {
                    val checkedRadioButton: RadioButton = group.findViewById(checkedId)
                    val text: String = checkedRadioButton.text.toString()
                    updateGivenField(getString(R.string.bolld_grp_), text)
                    alertD.dismiss()
                }
                R.id.ab1 -> {
                    val checkedRadioButton: RadioButton = group.findViewById(checkedId)
                    val text: String = checkedRadioButton.text.toString()
                    updateGivenField(getString(R.string.bolld_grp_), text)
                    alertD.dismiss()
                }
                R.id.ab2 -> {
                    val checkedRadioButton: RadioButton = group.findViewById(checkedId)
                    val text: String = checkedRadioButton.text.toString()
                    updateGivenField(getString(R.string.bolld_grp_), text)
                    alertD.dismiss()
                }
            }
        }

        /*findViewById<ImageView>(R.id.editField_BG).setOnClickListener {
            alertD.show()
        }
        findViewById<ImageView>(R.id.editFieldAllergy).setOnClickListener {
            show_dialog(getString(R.string.allergy), getString(R.string.allergy_), R.string.allergy_, allergyFromServer!!)
        }
        findViewById<ImageView>(R.id.editFieldEmail).setOnClickListener {
            show_dialog(getString(R.string.e_mail), getString(R.string.email_), R.string.email_, emailFromServer!!)
        }*/

        findViewById<ImageView>(R.id.addFieldAllergy).setOnClickListener {
            show_dialog(getString(R.string.allergy__2), getString(R.string.allergy_2), R.string.allergy_2, "")
        }
        findViewById<ImageView>(R.id.addFieldEmail).setOnClickListener {
            show_dialog(getString(R.string.email__2), getString(R.string.email_2), R.string.email_2, "")
        }

        layout!!.setOnTouchListener { v, event ->
            if (event.action == MotionEvent.ACTION_DOWN) {
                if (findViewById<EditText>(R.id.type_value_allergy).isFocused) {
                    val outRect = Rect()
                    findViewById<EditText>(R.id.type_value_allergy).getGlobalVisibleRect(outRect)
                    if (!outRect.contains(event.rawX.toInt(), event.rawY.toInt())) {
                        findViewById<EditText>(R.id.type_value_allergy).clearFocus()
                        val imm = v.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                        imm.hideSoftInputFromWindow(v.windowToken, 0)
                    }
                } else if (findViewById<EditText>(R.id.type_value_bg).isFocused) {
                    val outRect = Rect()
                    findViewById<EditText>(R.id.type_value_bg).getGlobalVisibleRect(outRect)
                    if (!outRect.contains(event.rawX.toInt(), event.rawY.toInt())) {
                        findViewById<EditText>(R.id.type_value_bg).clearFocus()
                        val imm = v.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                        imm.hideSoftInputFromWindow(v.windowToken, 0)
                    }
                } else if (findViewById<EditText>(R.id.type_value_email).isFocused) {
                    val outRect = Rect()
                    findViewById<EditText>(R.id.type_value_email).getGlobalVisibleRect(outRect)
                    if (!outRect.contains(event.rawX.toInt(), event.rawY.toInt())) {
                        findViewById<EditText>(R.id.type_value_email).clearFocus()
                        val imm = v.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                        imm.hideSoftInputFromWindow(v.windowToken, 0)
                    }
                } else if (findViewById<EditText>(R.id.type_value_phone).isFocused) {
                    val outRect = Rect()
                    findViewById<EditText>(R.id.type_value_phone).getGlobalVisibleRect(outRect)
                    if (!outRect.contains(event.rawX.toInt(), event.rawY.toInt())) {
                        findViewById<EditText>(R.id.type_value_phone).clearFocus()
                        val imm = v.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                        imm.hideSoftInputFromWindow(v.windowToken, 0)
                    }
                }
            }
            false
        }

        findViewById<EditText>(R.id.type_value_phone).setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                val text = findViewById<EditText>(R.id.type_value_phone).text.toString()
                //we need to verify that number using otp too....
                //updateGivenField("familyMemberPhoneNumber", text)
                return@OnEditorActionListener true
            }
            false
        })

        findViewById<EditText>(R.id.type_value_email).setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                val text = findViewById<EditText>(R.id.type_value_email).text.toString()
                updateGivenField(getString(R.string.email_), text)
                return@OnEditorActionListener true
            }
            false
        })

        findViewById<EditText>(R.id.type_value_bg).setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                val text = findViewById<EditText>(R.id.type_value_bg).text.toString()
                updateGivenField(getString(R.string.bolld_grp_), text)
                return@OnEditorActionListener true
            }
            false
        })

        findViewById<EditText>(R.id.type_value_allergy).setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                val text = findViewById<EditText>(R.id.type_value_allergy).text.toString()
                updateGivenField(getString(R.string.allergy_), text)
                return@OnEditorActionListener true
            }
            false
        })
    }


    private fun showInfoDialog() {
        val promptView = layoutInflater.inflate(R.layout.info_dialog_layout, null)
        infoDialog!!.setView(promptView)
        infoDialog!!.setCancelable(true)
        infoDialog!!.show()

        val t = Timer()
        t.schedule(object : TimerTask() {
            override fun run() {
                infoDialog!!.dismiss()
                t.cancel()
            }
        }, 4000)
    }


    private fun openDialog() {
        var layout: ConstraintLayout = layoutInflater.inflate(R.layout.age_picker_layout, null) as ConstraintLayout
        var agePicker: NumberPicker = layout.findViewById(R.id.agePicker) as NumberPicker
        agePicker.maxValue = 100
        agePicker.minValue = 18
        agePicker.value = ageFromServer!!.toInt()

        var alertDialog: AlertDialog = AlertDialog.Builder(this).setPositiveButton("Submit", null)
                .setNegativeButton("Cancel", null)
                .setView(layout)
                .setCancelable(false)
                .create()

        alertDialog.show()

        alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener {
            var value: String = agePicker.value.toString()
            updateGivenField(getString(R.string.age_), value)
            alertDialog.dismiss()
            //Toast.makeText(this@profile, value, Toast.LENGTH_SHORT).show()
        }

        alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setOnClickListener {
            alertDialog.dismiss()
        }
    }

    private fun openGenderSelectionDialog() {
        var layout: ConstraintLayout = layoutInflater.inflate(R.layout.gender_picker_layout, null) as ConstraintLayout
        var genderPicker: NumberPicker = layout.findViewById(R.id.genderPicker)

        var length: Int = docGenderArray!!.size

        genderPicker.minValue = 0
        genderPicker.maxValue = length - 1
        genderPicker.setDisplayedValues(docGenderArray)
        genderPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS)

        var alertDialog: AlertDialog = AlertDialog.Builder(this).setPositiveButton("Submit", null)
                .setNegativeButton("Cancel", null)
                .setView(layout)
                .setCancelable(false)
                .create()

        alertDialog.show()

        alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener {
            var value2: Int = genderPicker.value
            updateGivenField(getString(R.string.age_), docGenderArray!![value2])
            alertDialog.dismiss()
        }

        alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setOnClickListener {
            alertDialog.dismiss()
        }
    }


    private fun showBD_Dialog() {
        var now: Calendar = Calendar.getInstance();
        val dpd = DatePickerDialog.newInstance(
                { view, year, monthOfYear, dayOfMonth ->
                    fromday = dayOfMonth.toString()
                    if (fromday == "0") {
                        fromday!!.replaceFirst("0".toRegex(), "")
                    }
                    frommonth = (monthOfYear + 1).toString()
                    if (frommonth == "0") {
                        frommonth!!.replaceFirst("0".toRegex(), "")
                    }
                    fromyear = year.toString()

                    fromDate = fromyear + "/" + frommonth + "/" + fromday

                    /*Toast.makeText(this@profile, fromDate,
                            Toast.LENGTH_SHORT).show()*/

                    findViewById<TextView>(R.id.dob).text = fromDate
                    Log.d("profile_11", "DOB: $fromDate")

                    updateGivenField(getString(R.string.year_), fromyear!!)
                    updateGivenField(getString(R.string.month_), frommonth!!)
                    updateGivenField(getString(R.string.day_), fromday!!)

                }, now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH))
        dpd.version = DatePickerDialog.Version.VERSION_1
        dpd.show(fragmentManager, "Datepickerdialog")
    }

    //load profile
    fun loadProfileInfo() {
        if (myPhoneNumber != phoneNumber) {
            findViewById<ImageView>(R.id.editProfilePictureByGallery).visibility = View.INVISIBLE
            findViewById<ImageView>(R.id.editProfilePictureByCamera).visibility = View.INVISIBLE
        }

        doAsync {
            val client: OkHttpClient = OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build()

            val urlBuilder = HttpUrl.parse(ServerConfig().getProfileApi())!!.newBuilder()
            urlBuilder.addQueryParameter("familyId", familyId.toString())
            urlBuilder.addQueryParameter("phoneNumber", phoneNumber)

            val url = urlBuilder.build().toString()

            val request = Request.Builder()
                    .url(url)
                    .build()

            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()

            uiThread {
                Log.d("mess", "Load profile___" + responce_string.toString())
                if (responce_string != "dataMissing") {
                    if (responce_string != "nullData") {
                        loadDataInLayout(responce_string)
                    } else {
                        Toast.makeText(this@profile, R.string.server_error, Toast.LENGTH_SHORT).show()
                        var intent: Intent = Intent(this@profile, Main2Activity::class.java)
                        startActivity(intent)
                        finish()
                    }
                } else {
                    Toast.makeText(this@profile, R.string.server_error, Toast.LENGTH_SHORT).show()
                    var intent: Intent = Intent(this@profile, Main2Activity::class.java)
                    startActivity(intent)
                    finish()
                }
            }
        }
    }

    override fun onBackPressed() {
        val intent = Intent(this, Main2Activity::class.java)
        startActivity(intent)
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        finish()
    }

    fun getStringByLocal_(context: Activity, id: Int, locale: String): String {
        var configuration = Configuration(resources.configuration)
        configuration.setLocale(Locale(locale))
        return context.createConfigurationContext(configuration).resources.getString(id)
    }

    fun getStringByLocal(context: Activity, id: Int?, locale: String): Array<String> {
        val configuration = Configuration(resources.configuration)
        configuration.setLocale(Locale(locale))
        return context.createConfigurationContext(configuration).resources.getStringArray(id!!)
    }

    fun loadDataInLayout(response: String) {
        try {

            val data1 = response?.replace("___", ",")

            val data2 = data1?.replace("*", ",")

            val data3 = data2?.replace("_", ",")

            val data4 = data3?.replace("^^", ",")

            val data5 = data4?.replace("{", ",")

            val data6 = data5?.replace("#", ",")

            val data7 = data6?.replace("}", ",")

            val data8 = data7?.replace(":", ",")

            //val data = data8?.split(",".toRegex(), -1)?.dropLastWhile { it.isEmpty() }?.toTypedArray()
            val data = data8!!.trim().split(",")

            Log.d("pic_indi__", "data_____: $data")

            val fullName: String = data[0]
            val email: String = data[1]
            val phoneNumber: String = data[2]
            val dateOfBirth: String = data[3]
            val profilePicture: String = data[4]
            val sex: String = data[5]
            val bloodGroup: String = data[6]
            val age: String = data[7]
            val allergy: String = data[8]

            fullNameFromServer = fullName
            emailFromServer = email
            phoneNumberFromServer = phoneNumber
            dateOfBirthFromServer = dateOfBirth
            sexFromServer = sex
            bloodGroupFromServer = bloodGroup
            ageFromServer = age
            allergyFromServer = allergy

            findViewById<TextView>(R.id.name).text = fullNameFromServer
            findViewById<TextView>(R.id.age).text = ageFromServer
            findViewById<TextView>(R.id.gender).text = sexFromServer
            findViewById<TextView>(R.id.dob).text = dateOfBirthFromServer

            findViewById<EditText>(R.id.type_value_allergy).setText(allergyFromServer)
            findViewById<EditText>(R.id.type_value_email).setText(emailFromServer)
            findViewById<EditText>(R.id.type_value_bg).setText(bloodGroupFromServer)
            findViewById<EditText>(R.id.type_value_phone).setText(phoneNumberFromServer)

            Log.d("Inside__", "Profile_Pic:$profilePicture")

            loadProfilePicture(profilePicture)
        } catch (e: Exception) {
            Log.d("loadProfile__", "Exception: $e")
        }
    }

    private fun getIndex(spinner: Spinner, myString: String): Int {
        for (i in 0 until spinner.count) {
            if (spinner.getItemAtPosition(i).toString().equals(myString, ignoreCase = true)) {
                Log.d("position ", "getIndex: " + i.toString())
                return i
            }
        }
        return 0
    }

    fun loadProfilePicture(profilePictureName: String) {
        try {
            val uri: Uri = Uri.parse(ServerConfig().getRootAddress()
                    + "userProfilePictures/" + profilePictureName + "?notCache=" + profilePictureName)

            findViewById<SimpleDraweeView>(R.id.profilePicture).setImageURI(uri)
        } catch (e: Exception) {
            Log.d("Profile_", e.toString())
        }
    }

    //profile recyclerview
    fun attach_profile_adapter_to_recyclerview() {
        val profile_recyclerView: RecyclerView = findViewById(R.id.profileRecyclerView)
        profile_recyclerview = profile_recyclerView
        val layoutManager = LinearLayoutManager(applicationContext)
        profile_recyclerView.layoutManager = layoutManager
        profile_recyclerView.setHasFixedSize(false)
        profile_recyclerView.isNestedScrollingEnabled = false
        profile_adapter_holder = profile_adapter()
        profile_recyclerView.adapter = profile_adapter_holder
    }

    inner class profile_store(nameOfField: String, valueOfField: String, fieldSymbol: Int) {
        private var inner_nameOfField: String = nameOfField
        private var inner_valueOfField: String = valueOfField
        private var inner_fieldSymbol: Int = fieldSymbol

        fun get_nameOfField(): String {
            return inner_nameOfField
        }

        fun get_valueOfField(): String {
            return inner_valueOfField
        }

        fun get_fieldSymbol(): Int {
            return inner_fieldSymbol
        }
    }

    private inner class profile_adapter : RecyclerView.Adapter<profile_adapter.NumberViewHolder>() {

        override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): NumberViewHolder {
            val context = viewGroup.context
            val layoutIdForListItem = R.layout.profile_blueprint
            val inflater = LayoutInflater.from(applicationContext)
            val shouldAttachToParentImmediately = false

            val view = inflater.inflate(layoutIdForListItem, viewGroup, shouldAttachToParentImmediately)
            val viewHolder = NumberViewHolder(view)

            return viewHolder
        }

        override fun onBindViewHolder(holder: NumberViewHolder, position: Int) {
            var current_object = result_list_for_profile.get(position)
            holder.bind(current_object)
        }

        override fun getItemCount(): Int {
            return result_list_for_profile.size
        }

        inner class NumberViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            private var nameTextView: TextView? = null
            private var valueOfFieldTextView: TextView? = null
            private var fieldSymbolImageView: ImageView? = null
            private var editIconImageView: ImageView? = null

            init {
                nameTextView = itemView.findViewById(R.id.nameOfField)
                valueOfFieldTextView = itemView.findViewById(R.id.valueOfField)
                fieldSymbolImageView = itemView.findViewById(R.id.fieldSymbol)
                editIconImageView = itemView.findViewById(R.id.editIcon)
            }

            fun bind(current_object: profile_store) {
                nameTextView!!.text = current_object.get_nameOfField()
                valueOfFieldTextView!!.text = current_object.get_valueOfField()
                fieldSymbolImageView!!.setImageResource(current_object.get_fieldSymbol())

                if (myPhoneNumber == phoneNumber) {
                    /*if (current_object.get_nameOfField() == "Email" ||
                            current_object.get_nameOfField() == "Birth Date" ||
                            current_object.get_nameOfField() == "Sex" ||
                            current_object.get_nameOfField() == "Blood Group" ||
                            current_object.get_nameOfField() == "Age" ||
                            current_object.get_nameOfField() == "Allergy") {
                        editIconImageView!!.visibility = View.VISIBLE
                    } else {
                        editIconImageView!!.visibility = View.GONE
                    }*/
                    if (current_object.get_nameOfField() == getStringByLocal_(this@profile, R.string.email_field, "en") ||
                            current_object.get_nameOfField() == getStringByLocal_(this@profile, R.string.bd_field, "en") ||
                            current_object.get_nameOfField() == getStringByLocal_(this@profile, R.string.sex_field, "en") ||
                            current_object.get_nameOfField() == getStringByLocal_(this@profile, R.string.bg_field, "en") ||
                            current_object.get_nameOfField() == getStringByLocal_(this@profile, R.string.age_field, "en") ||
                            current_object.get_nameOfField() == getStringByLocal_(this@profile, R.string.allergy_field, "en")) {
                        editIconImageView!!.visibility = View.VISIBLE
                    } else {
                        editIconImageView!!.visibility = View.GONE
                    }

                    editIconImageView!!.setOnClickListener {
                        if (current_object.get_nameOfField() == getStringByLocal_(this@profile, R.string.email_field, "en")) {
                            show_dialog(getString(R.string.e_mail), getString(R.string.e_mail), R.string.e_mail, emailFromServer!!)
                        }
                        if (current_object.get_nameOfField() == getStringByLocal_(this@profile, R.string.sex_field, "en")) {
                            show_dialog(getString(R.string.sex), getString(R.string.sex_), R.string.sex_, sexFromServer!!)
                        }
                        if (current_object.get_nameOfField() == getStringByLocal_(this@profile, R.string.bg_field, "en")) {
                            show_dialog(getString(R.string.blood_group), getString(R.string.bolld_grp_), R.string.bolld_grp_, bloodGroupFromServer!!)
                        }
                        if (current_object.get_nameOfField() == getStringByLocal_(this@profile, R.string.age_field, "en")) {
                            show_dialog(getString(R.string.age), getString(R.string.age_), R.string.age_, ageFromServer!!)
                        }
                        if (current_object.get_nameOfField() == getStringByLocal_(this@profile, R.string.allergy_field, "en")) {
                            show_dialog(getString(R.string.allergy), getString(R.string.allergy_), R.string.allergy_, allergyFromServer!!)
                        }
                        if (current_object.get_nameOfField() == getStringByLocal_(this@profile, R.string.bd_field, "en")) {
                            show_dialog(getString(R.string.year), getString(R.string.year_), R.string.year_, dateOfBirthFromServer!!.split("/")[2])
                            show_dialog(getString(R.string.month), getString(R.string.month_), R.string.month_, dateOfBirthFromServer!!.split("/")[1])
                            show_dialog(getString(R.string.day), getString(R.string.day_), R.string.day_, dateOfBirthFromServer!!.split("/")[0])
                        }
                    }
                } else {
                    editIconImageView!!.visibility = View.GONE
                }
            }
        }

    }

    //edit profile
    fun makeEditIconsWork() {
        findViewById<ImageView>(R.id.editProfilePictureByCamera).setOnClickListener {
            get_permissions_camera()
        }
        findViewById<ImageView>(R.id.editProfilePictureByGallery).setOnClickListener {
            get_permissions_gallery()
        }
    }

    fun show_dialog(fieldNameToShow: String, fieldName: String, fieldName_id: Int, previousValue: String) {

        var field_Name: String = getStringByLocal_(this@profile, fieldName_id, "en")

        if (fieldNameToShow != getStringByLocal_(this@profile, R.string.day, "en")
                && fieldNameToShow != getStringByLocal_(this@profile, R.string.month, "en")
                && fieldNameToShow != getStringByLocal_(this@profile, R.string.year, "en")) {


            MaterialDialog.Builder(this)
                    .title(getString(R.string.update))
                    .content(getString(R.string.update) + " " + fieldNameToShow)
                    .inputType(InputType.TYPE_CLASS_TEXT)
                    .input(fieldNameToShow, previousValue, MaterialDialog.InputCallback { dialog, input ->
                        updateGivenField(field_Name, input.toString())
                    })
                    .positiveText(R.string.update)
                    .show()
        } else {
            MaterialDialog.Builder(this)
                    .title(fieldNameToShow)
                    .content(getString(R.string.update_dob_here))
                    .inputType(InputType.TYPE_CLASS_TEXT)
                    .input(fieldNameToShow, previousValue, MaterialDialog.InputCallback { dialog, input ->
                        updateGivenField(field_Name, input.toString())
                    })
                    .positiveText(R.string.update)
                    .show()
        }
    }

    fun updateGivenField(fieldName: String, fieldValue: String) {
        doAsync {
            val client: OkHttpClient = OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build()

            val urlBuilder = HttpUrl.parse(ServerConfig().updateProfileFieldsApi())!!.newBuilder()
            urlBuilder.addQueryParameter("familyId", familyId)
            urlBuilder.addQueryParameter("phoneNumber", phoneNumber)
            urlBuilder.addQueryParameter("fieldName", fieldName)
            urlBuilder.addQueryParameter("fieldValue", fieldValue)

            val url = urlBuilder.build().toString()

            val request = Request.Builder()
                    .url(url)
                    .build()

            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()

            uiThread {
                Log.d("update__", responce_string.toString())

                if (responce_string != "dataMissing") {
                    if (responce_string != "serverError") {
                        if (responce_string == "ok") {
                            loadProfileInfo()
                        } else {
                            showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                        }
                    } else {
                        showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                    }
                } else {
                    showMessageAfterSignup(getString(R.string.something_goes_wrong), Color.RED)
                }
            }
        }
    }

    fun open_gallery() {
        Matisse.from(this)
                .choose(MimeType.allOf())
                .countable(true)
                .maxSelectable(1)
                .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
                .thumbnailScale(0.85f)
                .imageEngine(PicassoEngine())
                .forResult(REQUEST_CODE_FOR_GALLERY_CAPTURE)
    }

    fun makeRequestToUpdateProfilePictureApi(list: MutableList<Uri>) {
        if (list.size != 0) {
            val MEDIA_TYPE_PNG: MediaType = MediaType.parse("file/*")!!
            doAsync {
                //convert image to bitmap
                val bitmapImg = Picasso
                        .with(applicationContext)
                        .load(Uri.parse(list[0].toString()))
                        .get()

                //create a file to write bitmap data
                var f: File = File(applicationContext.cacheDir, System.currentTimeMillis().toString())
                var os: OutputStream = BufferedOutputStream(FileOutputStream(f))
                bitmapImg.compress(Bitmap.CompressFormat.JPEG, 100, os)
                os.close()
                var imageFile = f
                var imageName = "_" + phoneNumber.toString() + ".jpg"

                //make request to api
                var response: Response? = null
                try {
                    val client: OkHttpClient = OkHttpClient.Builder()
                            .connectTimeout(60, TimeUnit.SECONDS)
                            .writeTimeout(60, TimeUnit.SECONDS)
                            .readTimeout(60, TimeUnit.SECONDS)
                            .build()

                    val requestBody: RequestBody = MultipartBody.Builder().setType(MultipartBody.FORM)
                            .addFormDataPart("file", imageName, RequestBody.create(MEDIA_TYPE_PNG, imageFile))
                            .addFormDataPart("familyId", familyId!!)
                            .addFormDataPart("phoneNumber", phoneNumber!!)
                            .build()
                    val request: Request = Request.Builder().url(ServerConfig().getUpdateProfilePictureApi())
                            .post(requestBody).build()
                    response = client.newCall(request).execute()

                    Log.d("Image_Upload_Response", response.toString())
                } catch (e: UnknownHostException) {
                    uiThread {
                        val toast: Toast = Toast.makeText(applicationContext, e.toString(), Toast.LENGTH_LONG)
                        toast.show()
                    }
                } catch (e: SocketTimeoutException) {
                    uiThread {
                        val toast: Toast = Toast.makeText(applicationContext, e.toString(), Toast.LENGTH_LONG)
                        toast.show()
                    }
                }
                val responce_string = response!!.body()!!.string()

                //process response from server
                uiThread {
                    loadProfileInfo()
                    Log.d("mess", "photo upload: $responce_string")
                }
            }
        }
    }

    fun showMessageAfterSignup(msg_text: String, color: Int) {
        val st = StyleableToast.Builder(this)
                .text(msg_text)
                .textColor(Color.WHITE)
                .backgroundColor(color)
                .build()
        st.show()
    }

    fun _openCamera() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (takePictureIntent.resolveActivity(packageManager) != null) {
            try {
                photoFile = createImageFile()
            } catch (ex: Exception) {
                Log.d("Errors__", ex.toString())
            }
            if (photoFile != null) {
                val builder: StrictMode.VmPolicy.Builder = StrictMode.VmPolicy.Builder()
                StrictMode.setVmPolicy(builder.build())
                val photoURI: Uri = Uri.fromFile(photoFile!!)
                Log.d("URI__Profile", photoURI.toString())
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
            }
        }
    }

    fun createImageFile(): File {
        val mCurrentPhotoPath: String
        val imageFileName: String = "camera"
        val storageDir: File = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        )

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.absolutePath
        return image
    }

    //on Activity result
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
            var uriOfImage = Uri.fromFile(photoFile)
            Log.e("Main", "uri profile : " + uriOfImage.toString())
            //start croper
            CropImage.activity(uriOfImage)
                    .start(this)
        }

        if (requestCode == REQUEST_CODE_FOR_GALLERY_CAPTURE && resultCode == Activity.RESULT_OK) {
            var selected_images = Matisse.obtainResult(data)

            //start croper
            CropImage.activity(selected_images[0])
                    .start(this)

        }

        //for croper
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result: CropImage.ActivityResult = CropImage.getActivityResult(data)
            if (resultCode == RESULT_OK) {
                doAsync {
                    val builder: StrictMode.VmPolicy.Builder = StrictMode.VmPolicy.Builder()
                    StrictMode.setVmPolicy(builder.build())
                    var resultUri: Uri = result.uri

                    //save cropped image for persisitance
                    val croppedImage = createImageFile() //empty
                    val outputStream: FileOutputStream = FileOutputStream(croppedImage)
                    // a bit long running
                    (Picasso.with(this@profile)
                            .load(resultUri)
                            .get()
                            ).compress(Bitmap.CompressFormat.JPEG, 100, outputStream)

                    resultUri = Uri.fromFile(croppedImage)
                    outputStream.close()
                    uiThread {
                        //add to mu list
                        val mu_list = ArrayList<Uri>(1)
                        mu_list.add(resultUri)
                        makeRequestToUpdateProfilePictureApi(mu_list)
                    }
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                val error: Exception = result.error
                Log.d("Error__", error.toString())
            }
        }

    }

    //get permissions
    //Camera
    fun get_permissions_camera() {
        if (ContextCompat.checkSelfPermission(this@profile, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

            MaterialDialog.Builder(this@profile)
                    .title(R.string.camera_perm)
                    .content(getString(R.string.camera_permission_req))
                    .negativeText(R.string.cancel)
                    .onNegative(object : MaterialDialog.SingleButtonCallback {
                        override fun onClick(dialog: MaterialDialog, which: DialogAction) {

                        }
                    })
                    .positiveText(R.string.give_permission)
                    .onPositive(object : MaterialDialog.SingleButtonCallback {
                        override fun onClick(dialog: MaterialDialog, which: DialogAction) {
                            getPermissionsUsingDexter_camera(
                                    android.Manifest.permission.CAMERA
                            )
                        }
                    })
                    .show()

        } else {
            _openCamera()
        }

    }

    fun getPermissionsUsingDexter_camera(permissionString: String) {
        Dexter.withActivity(this)
                .withPermissions(
                        permissionString
                ).withListener(object : MultiplePermissionsListener {
                    override fun onPermissionRationaleShouldBeShown(permissions: MutableList<PermissionRequest>?, token: PermissionToken?) {

                    }

                    override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                        if (report!!.areAllPermissionsGranted() == true) {
                            _openCamera()

                            Log.d("mess", "permission given")
                        } else {
                            Log.d("mess", "permission not granted")
                        }
                    }
                })
                .check()
    }

    //gallery
    fun get_permissions_gallery() {
        if (ContextCompat.checkSelfPermission(this@profile, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            MaterialDialog.Builder(this@profile)
                    .title(R.string.storage_permission)
                    .content(getString(R.string.storage_perm_req))
                    .negativeText(R.string.cancel)
                    .onNegative(object : MaterialDialog.SingleButtonCallback {
                        override fun onClick(dialog: MaterialDialog, which: DialogAction) {

                        }
                    })
                    .positiveText(R.string.give_permission)
                    .onPositive(object : MaterialDialog.SingleButtonCallback {
                        override fun onClick(dialog: MaterialDialog, which: DialogAction) {
                            getPermissionsUsingDexter_gallery(
                                    android.Manifest.permission.READ_EXTERNAL_STORAGE
                            )
                        }
                    })
                    .show()

        } else {
            open_gallery()
        }

    }

    fun getPermissionsUsingDexter_gallery(permissionString: String) {
        Dexter.withActivity(this)
                .withPermissions(
                        permissionString
                ).withListener(object : MultiplePermissionsListener {
                    override fun onPermissionRationaleShouldBeShown(permissions: MutableList<PermissionRequest>?, token: PermissionToken?) {

                    }

                    override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                        if (report!!.areAllPermissionsGranted() == true) {
                            open_gallery()

                            Log.d("mess", "permission given")
                        } else {
                            Log.d("mess", "permission not granted")
                        }
                    }
                })
                .check()
    }
}
