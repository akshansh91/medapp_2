package com.medapp.medapp

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.AsyncTask
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import com.medapp.medapp.config.ServerConfig
import com.muddzdev.styleabletoastlibrary.StyleableToast
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import java.util.*
import java.util.concurrent.TimeUnit

class history_pickFamilyMember : AppCompatActivity() {
    //we won't be needing recycler view now . . .
    private var c_layout: ViewGroup? = null
    var familyId: String? = null
    var indiNumber: String? = null
    //private var familyNames_recyclerview: RecyclerView? = null
    //private var result_list_for_familyNames: ArrayList<familyNames_store> = ArrayList()
    var loaderImageView: ImageView? = null
    var family_member_names: ArrayList<String> = ArrayList()
    var family_member_number: ArrayList<String> = ArrayList()
    var family_member_pic: ArrayList<String> = ArrayList()
    //private var familyNames_adapter_holder: RecyclerView.Adapter<familyNames_adapter.NumberViewHolder>? = null
    val TAG: String? = "Activity_Name"
    val test: String? = "pickFamilyMember"
    var fromDate: String? = "none"
    var toDate: String? = "none"
    var category: String? = "none"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history_pick_family_member)

        try {
            Log.d("Check_pickFamilyMember", "Inside onCreate()")

            val intent: Intent = getIntent()

            if (intent.getExtras() != null) {
                fromDate = intent.getStringExtra("startDate")
                toDate = intent.getStringExtra("endDate")
                category = intent.getStringExtra("category")
            } else {
                fromDate = "none"
                toDate = "none"
                category = "none"
            }

            //get subpart_c_layout
            c_layout = findViewById(R.id.pickFamilyMember)

            loaderImageView = findViewById(R.id.loadingGIF)

            //get family id from persistent storage
            val loginInfo = getSharedPreferences("loginInfo", Context.MODE_PRIVATE)
            familyId = loginInfo.getString("familyId", null)
            indiNumber = loginInfo.getString("phoneNumber", "")

            //if individual user is logged in
            if (familyId == "0") {
                Log.d(test, "Individual user logged in")

                val obj = loadProfilePicIndividual()
                obj.execute(indiNumber, familyId)
                /*proceedToNextStep("All", "all")
                this@history_pickFamilyMember.finish()*/
            } else {
                //attach adapter to recycler view
                // we don't want to show the recycler view . . .
                //attach_familyName_adapter_to_recyclerview()
                //load family member names
                Log.d(test, "Family user logged in")
                LoadFamilyMembersNames()
                Log.d(test, "LoadFamilyMembersNames() executed successfully")
            }
        } catch (e: Exception) {
            Log.d("Check_pickFamilyMember", "Inside onCreate()....Exception: $e")
        }
    }

    fun LoadFamilyMembersNames() {

        val obj = loadFamilyMembers()
        obj.execute(familyId)

        /*Log.d(test, "Inside LoadFamilyMembersNames()")
        doAsync {
            val client: OkHttpClient = OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build()

            //val urlBuilder = HttpUrl.parse(ServerConfig().getFamilyMemberNamesAndPhoneNumber())!!.newBuilder()
            val urlBuilder = HttpUrl.parse(ServerConfig().getFamilyMemberNamesPhoneNumberProfilePic())!!.newBuilder()
            urlBuilder.addQueryParameter("familyId", familyId)

            val url = urlBuilder.build().toString()

            val request = Request.Builder()
                    .url(url)
                    .build()

            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()

            Log.d(test, "Inside LoadFamilyMembersNames() response String:" + responce_string.toString())

            uiThread {
                if (responce_string != "dataMissing") {
                    if (responce_string != "serverError") {
                        if (responce_string.length < 450) {
                            Log.d(test, "now going to loadFamilyMemberNamesInLayout")
                            //loadFamilyMemberNamesInLayout(responce_string)
                            //result_list_for_familyNames.add(familyNames_store("All Documents", "all"))
                            Log.d(test, "Inside loadFamilyMemberNamesInLayout")
                            val arr_of_namesAndPhoneNumber = responce_string.split("___")
                            var counter = 1
                            val size = arr_of_namesAndPhoneNumber.size
                            while (counter < size) {
                                val name = arr_of_namesAndPhoneNumber[counter].split("*")[0]
                                val phoneNumber = arr_of_namesAndPhoneNumber[counter].split("*")[1]
                                val profilePic: String = arr_of_namesAndPhoneNumber[counter].split("*")[2]
                                //adding names and phone number of family members to the arrayList of names and numbers . . .
                                family_member_names.add(name)
                                family_member_number.add(phoneNumber)
                                family_member_pic.add(profilePic)
                                //

                                //  result_list_for_familyNames.add(familyNames_store(name, phoneNumber))
                                //familyNames_adapter_holder!!.notifyDataSetChanged()

                                counter++
                            }
                            Log.d(test, "loadFamilyMemberNamesInLayout() executed successfully")
                            Log.d(test, "family_names" + family_member_names)
                            Log.d(test, "family_numbers" + family_member_number)
                            Log.d(test, "family_pics" + family_member_pic)
                            Log.d(test, "Proceeding to the next step . .")
                            proceedToNextStep_Family(family_member_names, family_member_number, family_member_pic)
                            this@history_pickFamilyMember.finish()
                        } else {
                            showMessageAfterSignup("Server Error", Color.RED)
                        }
                    } else {
                        showMessageAfterSignup("Server Error", Color.RED)
                    }
                } else {
                    showMessageAfterSignup("Something went wrong", Color.RED)
                }
            }
        }*/
    }

    fun loadFamilyMemberNamesInLayout(familyMemberNamesString: String) {
        //result_list_for_familyNames.add(familyNames_store("All Documents", "all"))
        Log.d(test, "Inside loadFamilyMemberNamesInLayout")
        val arr_of_namesAndPhoneNumber = familyMemberNamesString.split("___")
        var counter = 1
        val size = arr_of_namesAndPhoneNumber.size
        while (counter < size) {
            val name = arr_of_namesAndPhoneNumber[counter].split("*")[0]
            val phoneNumber = arr_of_namesAndPhoneNumber[counter].split("*")[1]
            val profilePic: String = arr_of_namesAndPhoneNumber[counter].split("*")[2]
            //adding names and phone number of family members to the arrayList of names and numbers . . .
            family_member_names.add(name)
            family_member_number.add(phoneNumber)
            family_member_pic.add(profilePic)
            //

            //  result_list_for_familyNames.add(familyNames_store(name, phoneNumber))
            //familyNames_adapter_holder!!.notifyDataSetChanged()

            counter++
        }
        Log.d(test, "loadFamilyMemberNamesInLayout() executed successfully")
        Log.d(test, "family_names" + family_member_names)
        Log.d(test, "family_numbers" + family_member_number)
        Log.d(test, "family_pics" + family_member_pic)
        Log.d(test, "Proceeding to the next step . .")
        proceedToNextStep_Family(family_member_names, family_member_number, family_member_pic)
        this@history_pickFamilyMember.finish()
    }

    fun showMessageAfterSignup(msg_text: String, color: Int) {
        val st = StyleableToast.Builder(this)
                .text(msg_text)
                .textColor(Color.WHITE)
                .backgroundColor(color)
                .build()
        st.show()
    }

    //folder recyclerview
    fun attach_familyName_adapter_to_recyclerview() {
        val folder_recyclerView: RecyclerView = findViewById(R.id.familyNameRecyclerView)
        //familyNames_recyclerview = folder_recyclerView
        val layoutManager = LinearLayoutManager(applicationContext)
        folder_recyclerView.layoutManager = layoutManager
        folder_recyclerView.setHasFixedSize(false)
        folder_recyclerView.isNestedScrollingEnabled = false
        //familyNames_adapter_holder = familyNames_adapter()
        //folder_recyclerView.adapter = familyNames_adapter_holder
    }

    inner class familyNames_store(familyMembername: String, familyMemberNumber: String) {
        private var inner_familyMemberName: String = familyMembername
        private var inner_familyMemberPhoneNumber: String = familyMemberNumber

        fun get_familyMemberName(): String {
            return inner_familyMemberName
        }

        fun get_familyMemberPhoneNumber(): String {
            return inner_familyMemberPhoneNumber
        }
    }

    private inner class familyNames_adapter : RecyclerView.Adapter<familyNames_adapter.NumberViewHolder>() {

        override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): NumberViewHolder {
            val context = viewGroup.context
            val layoutIdForListItem = R.layout.pick_family_member_blueprint
            val inflater = LayoutInflater.from(applicationContext)
            val shouldAttachToParentImmediately = false

            val view = inflater.inflate(layoutIdForListItem, viewGroup, shouldAttachToParentImmediately)
            val viewHolder = NumberViewHolder(view)

            return viewHolder
        }

        override fun onBindViewHolder(holder: NumberViewHolder, position: Int) {
            //var current_object = result_list_for_familyNames.get(position)
            //holder.bind(current_object)
        }

        override fun getItemCount(): Int {
            //   return result_list_for_familyNames.size
            return 0
        }

        inner class NumberViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            private var familyMemberNameButton: Button? = null

            init {
                familyMemberNameButton = itemView.findViewById(R.id.name)
            }

            fun bind(current_object: familyNames_store) {
                val family_member_name = current_object.get_familyMemberName()
                val family_member_phoneNumber = current_object.get_familyMemberPhoneNumber()

                familyMemberNameButton!!.text = family_member_name

                //onclick listener
                familyMemberNameButton!!.setOnClickListener {
                    //proceedToNextStep(family_member_name, family_member_phoneNumber)
                }
            }
        }
    }

    fun proceedToNextStep(name: String, familyMemberPhoneNumber: String) {
        val i = Intent(this, historyTabs::class.java)
        i.putExtra("Selected_familyMemberPhoneNumber", familyMemberPhoneNumber)
        i.putExtra("Selected_familyMemberName", name)
        i.putExtra("startDate", fromDate)
        i.putExtra("endDate", toDate)
        i.putExtra("category", category)
        startActivity(i)
        finish()
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
    }

    fun proceedToNextStep_Family(familyNames: ArrayList<String>, familyNumber: ArrayList<String>, familyPics: ArrayList<String>) {

        Log.d(test, "inside proceedToNextStep_Family()")
        Log.d(test, "familyNames<ArrayList>$familyNames")
        Log.d(test, "familyNumbers<ArrayList>$familyNumber")
        Log.d(test, "familyPics<ArrayList>$familyPics")

        val i = Intent(this, historyTabs::class.java)
        i.putStringArrayListExtra("familyNames", familyNames)
        i.putStringArrayListExtra("familyNumber", familyNumber)
        i.putStringArrayListExtra("familyProfilePic", familyPics)
        startActivity(i)
        finish()
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
    }

    @SuppressLint("StaticFieldLeak")
    inner class loadFamilyMembers : AsyncTask<String, String, String>() {

        val dialog = ProgressDialog(this@history_pickFamilyMember)

        override fun onPreExecute() {
            super.onPreExecute()

            dialog.setMessage(getString(R.string.please_wait))
            dialog.setTitle(getString(R.string.loading))
            dialog.setCancelable(false)
            dialog.isIndeterminate = true
            dialog.show()
        }

        override fun doInBackground(vararg params: String?): String {

            val familyId: String = params[0].toString()

            val client: OkHttpClient = OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build()

            //val urlBuilder = HttpUrl.parse(ServerConfig().getFamilyMemberNamesAndPhoneNumber())!!.newBuilder()
            val urlBuilder = HttpUrl.parse(ServerConfig().getFamilyMemberNamesPhoneNumberProfilePic())!!.newBuilder()
            urlBuilder.addQueryParameter("familyId", familyId)

            val url = urlBuilder.build().toString()

            val request = Request.Builder()
                    .url(url)
                    .build()

            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()

            Log.d("Calls_API", "Inside LoadFamilyMembersNames() response String:" + responce_string.toString())

            return responce_string
        }

        override fun onPostExecute(result: String?) {

            if (result != "dataMissing") {
                if (result != "serverError") {
                    if (result != null) {
                        if (result.length < 450) {
                            Log.d("Calls_API", "now going to loadFamilyMemberNamesInLayout")
                            //loadFamilyMemberNamesInLayout(responce_string)
                            //result_list_for_familyNames.add(familyNames_store("All Documents", "all"))
                            Log.d("Calls_API", "Inside loadFamilyMemberNamesInLayout")
                            val arr_of_namesAndPhoneNumber = result.split("___")
                            var counter = 1
                            val size = arr_of_namesAndPhoneNumber.size
                            while (counter < size) {
                                val name = arr_of_namesAndPhoneNumber[counter].split("*")[0]
                                val phoneNumber = arr_of_namesAndPhoneNumber[counter].split("*")[1]
                                val profilePic: String = arr_of_namesAndPhoneNumber[counter].split("*")[2]
                                //adding names and phone number of family members to the arrayList of names and numbers . . .
                                family_member_names.add(name)
                                family_member_number.add(phoneNumber)
                                family_member_pic.add(profilePic)
                                //

                                //  result_list_for_familyNames.add(familyNames_store(name, phoneNumber))
                                //familyNames_adapter_holder!!.notifyDataSetChanged()

                                counter++
                            }
                            Log.d("Calls_API", "loadFamilyMemberNamesInLayout() executed successfully")
                            Log.d("Calls_API", "family_names" + family_member_names)
                            Log.d("Calls_API", "family_numbers" + family_member_number)
                            Log.d("Calls_API", "family_pics" + family_member_pic)
                            Log.d("Calls_API", "Proceeding to the next step . .")
                            //proceedToNextStep_Family(family_member_names, family_member_number, family_member_pic)

                            val i = Intent(this@history_pickFamilyMember, historyTabs::class.java)
                            i.putStringArrayListExtra("familyNames", family_member_names)
                            i.putStringArrayListExtra("familyNumber", family_member_number)
                            i.putStringArrayListExtra("familyProfilePic", family_member_pic)
                            i.putExtra("startDate", fromDate)
                            i.putExtra("endDate", toDate)
                            i.putExtra("category", category)
                            dialog.dismiss()
                            startActivity(i)
                            finish()
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                            //this@history_pickFamilyMember.finish()
                        } else {
                            showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                        }
                    }
                } else {
                    showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                }
            } else {
                showMessageAfterSignup(getString(R.string.server_error), Color.RED)
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    inner class loadProfilePicIndividual : AsyncTask<String, String, String>() {

        val dialog = ProgressDialog(this@history_pickFamilyMember)

        override fun onPreExecute() {
            super.onPreExecute()

            dialog.setMessage(getString(R.string.please_wait))
            dialog.setTitle(getString(R.string.loading))
            dialog.setCancelable(false)
            dialog.isIndeterminate = true
            dialog.show()
        }

        override fun doInBackground(vararg strings: String): String {
            Log.d(test, "inside profileIndividual class::::doInBackground")

            val phoneNumber = strings[0]
            val familyId = strings[1]

            val client: OkHttpClient = OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build()

            //val urlBuilder = HttpUrl.parse(ServerConfig().getFamilyMemberNamesAndPhoneNumber())!!.newBuilder()
            val urlBuilder = HttpUrl.parse(ServerConfig().getProfileApi())!!.newBuilder()
            urlBuilder.addQueryParameter("familyId", familyId)
            urlBuilder.addQueryParameter("phoneNumber", phoneNumber)

            val url = urlBuilder.build().toString()

            val request = Request.Builder()
                    .url(url)
                    .build()

            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()

            Log.d("Calls_API", "Inside LoadFamilyMembersNames() response String:" + responce_string.toString())

            return responce_string
        }

        override fun onPostExecute(s: String?) {
            Log.d("pic_indi__", "Response String: " + s)

            if (s != "noImageUploadedYet" && s != "noMoreImages") {
                Log.d(test, "inside onPostExecute(): profileIndividual")

                try {
                    val data1 = s?.replace("___", ",")
                    Log.d("pic_indi__", "data1: " + data1)
                    val data2 = data1?.replace("*", ",")
                    Log.d("pic_indi__", "data2: " + data2)
                    val data3 = data2?.replace("_", ",")
                    Log.d("pic_indi__", "data3: " + data3)
                    val data4 = data3?.replace("^^", ",")
                    Log.d("pic_indi__", "data4: " + data4)
                    val data5 = data4?.replace("{", ",")
                    Log.d("pic_indi__", "data5: " + data5)
                    val data6 = data5?.replace("#", ",")
                    Log.d("pic_indi__", "data6: " + data6)
                    val data7 = data6?.replace("}", ",")
                    Log.d("pic_indi__", "data7: " + data7)
                    val data8 = data7?.replace(":", ",")
                    Log.d("pic_indi__", "data8: " + data8)
                    //val data = data8?.split("\\+".toRegex(), -1)?.dropLastWhile({ it.isEmpty() })?.toTypedArray()
                    val data = data8!!.trim().split(',')

                    Log.d("pic_indi__", "data_____: $data")

                    val profilePic = data.get(4)

                    Log.d("pic_indi__", "Profile_Pic: $profilePic")
                    val i = Intent(this@history_pickFamilyMember, historyTabs::class.java)
                    i.putExtra("Selected_familyMemberPhoneNumber", "All")
                    i.putExtra("Selected_familyMemberName", "all")
                    i.putExtra("profilePic", profilePic)
                    i.putExtra("startDate", fromDate)
                    i.putExtra("endDate", toDate)
                    i.putExtra("category", category)
                    dialog.dismiss()
                    startActivity(i)
                    finish()
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                } catch (e: Exception) {
                    Log.d("pic_indi__", "Exception: " + e)
                }
            }
        }
    }
}