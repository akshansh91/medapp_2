package com.medapp.medapp;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.medapp.medapp.config.ServerConfig;

import java.util.ArrayList;

public class Adapter_showSharedImages extends RecyclerView.Adapter<Adapter_showSharedImages.MyViewHolder> {

    private ArrayList<ArrayList<String>> mData;
    private LayoutInflater mInflater;
    private Context myContext;
    String by, familyId, phoneNumber, type, specific_category, description, category;

    Adapter_showSharedImages(Context context, ArrayList<ArrayList<String>> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.myContext = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Fresco.initialize(myContext);
        Log.d("Adapter List", String.valueOf(mData));
        View view = mInflater.inflate(R.layout.activity_show_shared_docs, parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        final String imgName = mData.get(position).get(0);

        if (imgName.equals("noDocsShared.jpg")) {
            holder.imageView.setImageResource(R.drawable.no_docs_shared_with_you_1);

            holder.description.setVisibility(View.GONE);
            holder.category.setVisibility(View.GONE);
            holder.by.setVisibility(View.GONE);

            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            params.addRule(Gravity.CENTER);

            holder.imageView.setLayoutParams(params);
        } else {
            Uri uri = Uri.parse(new ServerConfig().getRootAddress()
                    + "upload/" + imgName + "?notCache=" + imgName);

            by = mData.get(position).get(3);

            holder.imageView.setImageURI(uri);
            holder.description.setText(mData.get(position).get(1));
            holder.category.setText(mData.get(position).get(2));
            holder.by.setText(mData.get(position).get(3));

            description = mData.get(position).get(1);
            type = mData.get(position).get(4);
            category = mData.get(position).get(2);
            specific_category = mData.get(position).get(5);

            holder.imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(myContext, showPicFull.class);
                    intent.putExtra("ImageName", imgName);
                    intent.putExtra("description", description);
                    intent.putExtra("type", type);
                    intent.putExtra("category", category);
                    intent.putExtra("specific_category", specific_category);
                    intent.putExtra("docSharedBy", by);
                    myContext.startActivity(intent);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        com.facebook.drawee.view.SimpleDraweeView imageView;
        TextView description, category, by;

        MyViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.docs);
            description = itemView.findViewById(R.id.description);
            category = itemView.findViewById(R.id.category);
            by = itemView.findViewById(R.id.by);
        }
    }
}
