package com.medapp.medapp

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.facebook.drawee.backends.pipeline.Fresco
import com.medapp.medapp.config.ServerConfig
import com.muddzdev.styleabletoastlibrary.StyleableToast
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.util.*
import java.util.concurrent.TimeUnit

class profileChooser : AppCompatActivity() {
    private var familyId: String? = null
    private var phoneNumber: String? = null
    private var c_layout: ViewGroup? = null
    private var familyNames_recyclerview: RecyclerView? = null
    private var result_list_for_familyNames: ArrayList<familyNames_store> = ArrayList<familyNames_store>()
    private var familyNames_adapter_holder: RecyclerView.Adapter<familyNames_adapter.NumberViewHolder>? = null
    val TAG: String? = "Activity_Name"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_chooser)
        Log.d(TAG, "Inside profile chooser")

        Fresco.initialize(this)

        //get familyId
        val loginInfo = applicationContext.getSharedPreferences("loginInfo", Context.MODE_PRIVATE)
        familyId = loginInfo.getString("familyId", "")

        //set title
        if (familyId != "0") {
            setTitle(R.string.family_profile)
        } else {
            setTitle(R.string.my_profile)
        }

        if (familyId == "0") {
            phoneNumber = loginInfo.getString("phoneNumber", "")

            val i = Intent(this, profile::class.java)
            i.putExtra("phoneNumber", phoneNumber)
            startActivity(i)
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        } else {
            attach_familyName_adapter_to_recyclerview()
            LoadFamilyMembersNames()
        }
    }

    override fun onBackPressed() {
        val intent = Intent(this, Main2Activity::class.java)
        startActivity(intent)
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        finish()
    }

    fun LoadFamilyMembersNames() {
        doAsync {
            val client: OkHttpClient = OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build()

            val urlBuilder = HttpUrl.parse(ServerConfig().getFamilyMemberNamesAndPhoneNumber())!!.newBuilder()
            urlBuilder.addQueryParameter("familyId", familyId)

            val url = urlBuilder.build().toString()

            val request = Request.Builder()
                    .url(url)
                    .build()

            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()

            uiThread {
                Log.d("mess", responce_string.toString())

                if (responce_string != "dataMissing") {
                    if (responce_string != "serverError") {
                        loadFamilyMemberNamesInLayout(responce_string)
                    } else {
                        showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                    }
                } else {
                    showMessageAfterSignup(getString(R.string.something_goes_wrong), Color.RED)
                }
            }
        }
    }

    fun loadFamilyMemberNamesInLayout(familyMemberNamesString: String) {
        val arr_of_namesAndPhoneNumber = familyMemberNamesString.split("___")
        var counter = 1
        val size = arr_of_namesAndPhoneNumber.size
        while (counter < size) {
            Log.d("mess", arr_of_namesAndPhoneNumber[counter])

            val name = arr_of_namesAndPhoneNumber[counter].split("*")[0]
            val phoneNumber = arr_of_namesAndPhoneNumber[counter].split("*")[1]
            val profilePic = arr_of_namesAndPhoneNumber[counter].split("*")[2]
            val relation = arr_of_namesAndPhoneNumber[counter].split("*")[3]

            result_list_for_familyNames.add(familyNames_store(name, phoneNumber, profilePic, relation))
            familyNames_adapter_holder!!.notifyDataSetChanged()

            counter++
        }
    }

    fun showMessageAfterSignup(msg_text: String, color: Int) {
        val st = StyleableToast.Builder(this)
                .text(msg_text)
                .textColor(Color.WHITE)
                .backgroundColor(color)
                .build()
        st.show()
    }

    fun proceedToNextStep(name: String, phoneNumber: String) {
        val phoneNumber_v = phoneNumber

        val i = Intent(this, profile::class.java)
        i.putExtra("phoneNumber", phoneNumber_v)
        startActivity(i)
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        finish()
    }

    //folder recyclerview
    fun attach_familyName_adapter_to_recyclerview() {
        val folder_recyclerView: RecyclerView = findViewById(R.id.familyNameRecyclerView)
        familyNames_recyclerview = folder_recyclerView
        folder_recyclerView.addItemDecoration(ItemDecor(this, R.dimen.item_offset))
        val layoutManager = LinearLayoutManager(applicationContext)
        folder_recyclerView.layoutManager = layoutManager
        folder_recyclerView.setHasFixedSize(false)
        folder_recyclerView.isNestedScrollingEnabled = false
        familyNames_adapter_holder = familyNames_adapter()
        folder_recyclerView.adapter = familyNames_adapter_holder
    }

    inner class familyNames_store(familyMembername: String, phoneNumber: String, profilePic: String, relation: String) {
        private var inner_familyMemberName: String = familyMembername
        private var inner_phoneNumber: String = phoneNumber
        private var inner_relation: String = relation
        private var inner_profilePic: String = profilePic


        fun get_familyMemberName(): String {
            return inner_familyMemberName
        }

        fun get_familyMemberPhoneNumber(): String {
            return inner_phoneNumber
        }

        fun get_familyMemberRelation(): String {
            return inner_relation
        }

        fun get_familyMemberProfilePic(): String {
            return inner_profilePic
        }
    }

    private inner class familyNames_adapter() : RecyclerView.Adapter<familyNames_adapter.NumberViewHolder>() {

        override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): NumberViewHolder {
            val context = viewGroup.context
            val layoutIdForListItem = R.layout.pick_family_member_blueprint
            val inflater = LayoutInflater.from(applicationContext)
            val shouldAttachToParentImmediately = false

            val view = inflater.inflate(layoutIdForListItem, viewGroup, shouldAttachToParentImmediately)
            val viewHolder = NumberViewHolder(view)

            return viewHolder
        }

        override fun onBindViewHolder(holder: NumberViewHolder, position: Int) {
            var current_object = result_list_for_familyNames[position]
            holder.bind(current_object)
        }

        override fun getItemCount(): Int {
            return result_list_for_familyNames.size
        }

        inner class NumberViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            private var familyMemberNameButton: Button? = null
            private var familyMemberProfilePic: ImageView? = null
            private var familyMemberRelation: TextView? = null

            init {
                familyMemberNameButton = itemView.findViewById(R.id.name)
                familyMemberProfilePic = itemView.findViewById(R.id.profile_pic)
                familyMemberRelation = itemView.findViewById(R.id.relation)
            }

            @SuppressLint("SetTextI18n")
            fun bind(current_object: familyNames_store) {
                val family_member_name = current_object.get_familyMemberName()
                val family_member_phoneNumber = current_object.get_familyMemberPhoneNumber()
                val family_member_relation = current_object.get_familyMemberRelation()
                val profile_ = current_object.get_familyMemberProfilePic()

                familyMemberNameButton!!.text = family_member_name

                if (family_member_relation == "" && position == 0) {
                    //familyMemberRelation!!.text = "Admin"
                    familyMemberRelation!!.text = getString(R.string.admin)
                } else if (family_member_relation == "") {
                    familyMemberRelation!!.text = getString(R.string.family_member)
                } else {
                    familyMemberRelation!!.text = family_member_relation
                }
                //familyMemberRelation!!.setText(family_member_relation)

                if (profile_ == "") {
                    familyMemberProfilePic!!.setImageResource(R.drawable.icon_man)
                } else {
                    val uri: Uri = Uri.parse(ServerConfig()
                            .getRootAddress() + "upload/" + profile_ +
                            "?notCache=" + profile_)

                    familyMemberProfilePic!!.setImageURI(uri)
                }

                //onclick listener
                familyMemberNameButton!!.setOnClickListener {
                    proceedToNextStep(family_member_name, family_member_phoneNumber)
                }
            }
        }
    }
}
