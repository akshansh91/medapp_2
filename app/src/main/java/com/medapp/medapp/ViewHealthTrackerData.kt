package com.medapp.medapp

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.jjoe64.graphview.GraphView
import com.jjoe64.graphview.series.DataPoint
import com.jjoe64.graphview.series.LineGraphSeries
import com.medapp.medapp.config.ServerConfig
import com.medapp.medapp.utils.left_drawer
import com.muddzdev.styleabletoastlibrary.StyleableToast
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.util.concurrent.TimeUnit

class ViewHealthTrackerData : AppCompatActivity() {
    private var optionName: String? = null
    private var phoneNumber: String? = null
    private var series2: LineGraphSeries<DataPoint>? = null
    private var graph: GraphView? = null
    val tag: String? = "Activity_Name"
    private var leftEye: Button? = null
    private var rightEye: Button? = null
    private var systolic: Button? = null
    private var diastolic: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_health_tracker_data)
        //setTitle("Health Graph")
        Log.d(tag, "Inside view_health_tracker_data")

        left_drawer(this, this@ViewHealthTrackerData, findViewById(R.id.healthTrackerToolBar)!!).createNavigationDrawer()

        //get phone number
        val loginInfo = applicationContext.getSharedPreferences("loginInfo", Context.MODE_PRIVATE)
        phoneNumber = loginInfo.getString("phoneNumber", "")

        //option name
        val pI = intent
        optionName = pI.getStringExtra("optionName")

        leftEye = findViewById(R.id.leftEye)
        rightEye = findViewById(R.id.rightEye)
        graph = findViewById(R.id.optionGraph)

        systolic = findViewById(R.id.systolicPressure)
        diastolic = findViewById(R.id.diastolicPressure)

        leftEye!!.setOnClickListener {
            rightEye!!.setBackgroundColor(resources.getColor(R.color.white))
            rightEye!!.setTextColor(resources.getColor(R.color.colorAccent))
            leftEye!!.setBackgroundColor(resources.getColor(R.color.colorAccent))
            leftEye!!.setTextColor(resources.getColor(R.color.white))
            graph!!.visibility = View.VISIBLE
            loadOptionGraph("powerL")
        }

        rightEye!!.setOnClickListener {
            leftEye!!.setBackgroundColor(resources.getColor(R.color.white))
            leftEye!!.setTextColor(resources.getColor(R.color.colorAccent))
            rightEye!!.setBackgroundColor(resources.getColor(R.color.colorAccent))
            rightEye!!.setTextColor(resources.getColor(R.color.white))
            graph!!.visibility = View.VISIBLE
            loadOptionGraph("powerR")
        }

        systolic!!.setOnClickListener {
            diastolic!!.setBackgroundColor(resources.getColor(R.color.white))
            diastolic!!.setTextColor(resources.getColor(R.color.colorAccent))
            systolic!!.setBackgroundColor(resources.getColor(R.color.colorAccent))
            systolic!!.setTextColor(resources.getColor(R.color.white))
            graph!!.visibility = View.VISIBLE
            loadOptionGraph("systolic")
        }

        diastolic!!.setOnClickListener {
            systolic!!.setBackgroundColor(resources.getColor(R.color.white))
            systolic!!.setTextColor(resources.getColor(R.color.colorAccent))
            diastolic!!.setBackgroundColor(resources.getColor(R.color.colorAccent))
            diastolic!!.setTextColor(resources.getColor(R.color.white))
            graph!!.visibility = View.VISIBLE
            loadOptionGraph("diastolic")
        }

        when {
            optionName.equals(getString(R.string.bp)) -> {
                systolic!!.visibility = View.VISIBLE
                diastolic!!.visibility = View.VISIBLE
                graph!!.visibility = View.GONE
            }
            optionName.equals(getString(R.string.power_of_glasses)) -> {
                leftEye!!.visibility = View.VISIBLE
                rightEye!!.visibility = View.VISIBLE
                graph!!.visibility = View.GONE
            }
            else -> {
                graph!!.visibility = View.VISIBLE
                loadOptionGraph(optionName!!)
            }
        }
        //loadOptionGraph()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this@ViewHealthTrackerData, healthTracker::class.java)
        startActivity(intent)
        finish()
    }

    private fun loadOptionGraph(option: String) {
        Log.d("ViewHealthTrackerData", "inside loadOptionGraph()")

        graph = findViewById<View>(R.id.optionGraph) as GraphView
        graph!!.title = optionName!!.toUpperCase() + "-" + getString(R.string.graph)
        graph!!.viewport.isXAxisBoundsManual = true
        graph!!.viewport.isYAxisBoundsManual = true

        series2 = LineGraphSeries(
                arrayOf(
                        DataPoint(0.0, 0.0)
                )
        )
        Log.d("ViewHealthTrackerData", "calling loadDataPointsToGraph()")
        loadDataPointsToGraph(option)
    }

    private fun loadDataPointsToGraph(option: String) {
        doAsync {
            val client: OkHttpClient = OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build()

            val urlBuilder = HttpUrl.parse(ServerConfig().getHealthTrackerDatapoints())!!.newBuilder()
            urlBuilder.addQueryParameter("phoneNumber", phoneNumber)
            urlBuilder.addQueryParameter("optionName", option)


            val url = urlBuilder.build().toString()

            val request = Request.Builder()
                    .url(url)
                    .build()

            val response: Response = client.newCall(request).execute()
            val responseString = response.body()!!.string()

            Log.d("ViewHealthTrackerData", "Response String: " + responseString.toString())

            uiThread {
                Log.d("ViewHealthTrackerData", responseString)

                if (responseString != "dataMissing") {
                    if (responseString != "serverError") {
                        handleResponse(responseString)
                    } else {
                        showMessageAfterSignUp("You have not entered information yet", Color.DKGRAY)
                        val intent = Intent(this@ViewHealthTrackerData, addHealthTrackerData::class.java)
                        intent.putExtra("optionName", optionName)
                        startActivity(intent)
                        finish()
                    }
                } else {
                    showMessageAfterSignUp(getString(R.string.something_goes_wrong), Color.RED)
                    val intent = Intent(this@ViewHealthTrackerData, Main2Activity::class.java)
                    startActivity(intent)
                    finish()
                }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun handleResponse(responseString: String) {

        Log.d("ViewHealthTrackerData", "inside handleResponse()")

        try {
            val chunkOfData = responseString.split("___")
            val size = chunkOfData.size
            var counter = 1

            Log.d("datum", "chunkOfData: $chunkOfData")
            Log.d("datum", "size: $size")

            while (counter < size) {
                val singleDataStream = chunkOfData[counter]
                val id = singleDataStream.split("*")[0]
                val optionValue = singleDataStream.split("*")[1]
                val day = singleDataStream.split("*")[2]
                val month = singleDataStream.split("*")[3]
                val year = singleDataStream.split("*")[4]

                if (counter == 1) {
                    findViewById<TextView>(R.id.startDate).text = day + "/" + month + "/" + year
                }
                if (counter == size - 1) {
                    findViewById<TextView>(R.id.endDate).text = day + "/" + month + "/" + year
                }

                Log.d("datum", "counter: $counter\n")
                Log.d("datum", "singleDataStream: $singleDataStream")
                Log.d("datum", "id: $id")
                Log.d("datum", "optionValue: $optionValue")
                Log.d("datum", "day: $day")
                Log.d("datum", "month: $month")
                Log.d("datum", "year: $year")

                //series_2!!.appendData(DataPoint(counter.toDouble(), optionValue.toDouble()), false, 100000)
                series2!!.appendData(DataPoint(counter.toDouble(), optionValue.toDouble()), true, 10000)
                counter++
            }

            //val sizeTemp: Double = size.toDouble()
            series2!!.thickness = 10
            series2!!.color = Color.WHITE
            series2!!.isDrawDataPoints = true
            series2!!.dataPointsRadius = 15F
            series2!!.isDrawBackground = true

            graph!!.addSeries(series2)
        } catch (e: Exception) {
            Log.d("ViewHealthTrackerData", "Exception: $e")
        }
    }

    private fun showMessageAfterSignUp(msg_text: String, color: Int) {
        val st = StyleableToast.Builder(this)
                .text(msg_text)
                .textColor(Color.WHITE)
                .backgroundColor(color)
                .build()
        st.show()
    }
}
