package com.medapp.medapp

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.medapp.medapp.config.ServerConfig
import com.medapp.medapp.database.DB_HELPER
import com.medapp.medapp.utils.left_drawer
import com.muddzdev.styleabletoastlibrary.StyleableToast
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.util.concurrent.TimeUnit

class healthGoals : AppCompatActivity() {
    private var phoneNumber: String? = null

    private var healthGoals_recyclerview: RecyclerView? = null
    private var result_list_for_healthGoals: ArrayList<healthGoal_store> = ArrayList<healthGoal_store>()
    private var healthGoal_adapter_holder: RecyclerView.Adapter<healthGoal_adapter.NumberViewHolder>? = null
    val TAG: String? = "Activity_Name"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_health_goals)
        findViewById<Toolbar>(R.id.healthGoalsToolbar).setTitle("Health Goals")

        Log.d(TAG, "Inside healthGoals")

        //navigation drawer
        left_drawer(this, this@healthGoals, findViewById(R.id.healthGoalsToolbar)!!).createNavigationDrawer()

        //on add button clicked
        onAddButtonClicked()

        //get phone Number
        val loginInfo = applicationContext.getSharedPreferences("loginInfo", Context.MODE_PRIVATE)
        phoneNumber = loginInfo.getString("phoneNumber", "")

        //attch adapter to recyclerView
        attach_healthGoals_adapter_to_recyclerview()

        //get my health goals
        getMyHealthGoals()
    }

    //on add button clicked
    fun onAddButtonClicked() {
        findViewById<Button>(R.id.addHealthGoalsButton).setOnClickListener {
            openAddHealthGoals()
        }
    }

    fun openAddHealthGoals() {
        val i = Intent(this, createHealthGoals::class.java)
        startActivity(i)
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        finish()
    }

    //health Goals recyclerview
    fun attach_healthGoals_adapter_to_recyclerview() {
        val healthG_recyclerView: RecyclerView = findViewById<RecyclerView>(R.id.healthGoalsRecyclerView)
        healthGoals_recyclerview = healthG_recyclerView
        val layoutManager = LinearLayoutManager(applicationContext)
        healthG_recyclerView.layoutManager = layoutManager
        healthG_recyclerView.setHasFixedSize(false)
        healthG_recyclerView.isNestedScrollingEnabled = false
        healthGoal_adapter_holder = healthGoal_adapter()
        healthG_recyclerView.adapter = healthGoal_adapter_holder
    }

    inner class healthGoal_store(goal: String, priority: String, day: String, month: String, year: String) {
        private var inner_goal: String = goal
        private var inner_priority: String = priority
        private var inner_day: String = day
        private var inner_month: String = month
        private var inner_year: String = year

        fun get_goal(): String {
            return inner_goal
        }

        fun get_priority(): String {
            return inner_priority
        }

        fun get_date(): String {
            return inner_day + "/" + inner_month + "/" + inner_year
        }
    }

    private inner class healthGoal_adapter() : RecyclerView.Adapter<healthGoal_adapter.NumberViewHolder>() {

        override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): NumberViewHolder {
            val context = viewGroup.context
            val layoutIdForListItem = R.layout.health_goals_blueprint
            val inflater = LayoutInflater.from(applicationContext)
            val shouldAttachToParentImmediately = false

            val view = inflater.inflate(layoutIdForListItem, viewGroup, shouldAttachToParentImmediately)
            val viewHolder = NumberViewHolder(view)

            return viewHolder
        }

        override fun onBindViewHolder(holder: NumberViewHolder, position: Int) {
            val current_object = result_list_for_healthGoals.get(position)
            holder.bind(current_object)
        }

        override fun getItemCount(): Int {
            return result_list_for_healthGoals.size
        }

        inner class NumberViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            private var healthGoalsTextView: TextView? = null
            private var healthGoalsPriorityTextView: TextView? = null
            private var healthGoalsDateTextView: TextView? = null
            private var healthGoalIcon: ImageView? = null
            private var cancelHealthGoalButton: Button? = null

            init {
                healthGoalsTextView = itemView.findViewById(R.id.healthGoalsTextView)
                healthGoalsPriorityTextView = itemView.findViewById(R.id.healthGoalsPriorityTextView)
                healthGoalsDateTextView = itemView.findViewById(R.id.healthGoalsDate)
                healthGoalIcon = itemView.findViewById(R.id.healthGoalIcon)
                cancelHealthGoalButton = itemView.findViewById(R.id.cancelHealthGoalButton)
            }

            fun bind(current_object: healthGoal_store) {
                healthGoalsTextView!!.setText(current_object.get_goal())
                healthGoalsPriorityTextView!!.setText(current_object.get_priority())
                healthGoalsDateTextView!!.setText(current_object.get_date())
                healthGoalIcon!!.setImageResource(R.drawable.health_goals_icon)
                cancelHealthGoalButton!!.setOnClickListener {
                    onCancelHealthGoalClicked(phoneNumber!!, current_object.get_goal())
                }
            }

        }

    }

    //get health goals
    fun getMyHealthGoals() {
        doAsync {
            val client: OkHttpClient = OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build()

            val urlBuilder = HttpUrl.parse(ServerConfig().getHealthGoalsApi())!!.newBuilder()
            urlBuilder.addQueryParameter("phoneNumber", phoneNumber)

            val url = urlBuilder.build().toString()

            val request = Request.Builder()
                    .url(url)
                    .build()

            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()

            uiThread {
                Log.d("mess", responce_string.toString())

                if (responce_string != "dataMissing") {
                    if (responce_string != "serverError") {
                        processResponse(responce_string)
                    } else {
                        showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                    }
                } else {
                    showMessageAfterSignup(getString(R.string.something_goes_wrong), Color.RED)
                }
            }
        }
    }

    fun processResponse(responseString: String) {
        result_list_for_healthGoals.clear()
        val chunkOfData = responseString.split("___")
        val size = chunkOfData.size
        var counter = 1

        while (counter < size) {
            val dataString = chunkOfData[counter]
            val healthGoal = dataString.split("*")[0]
            val priority = (dataString.split("*")[1]).split("^")[0]
            val goalDay = ((dataString.split("*")[1]).split("^")[1]).split("??")[0]
            val goalMonth = (((dataString.split("*")[1]).split("^")[1]).split("??")[1]).split("$$")[0]
            val goalYear = (((dataString.split("*")[1]).split("^")[1]).split("??")[1]).split("$$")[1]

            result_list_for_healthGoals.add(healthGoal_store(healthGoal, priority, goalDay, goalMonth, goalYear))
            healthGoal_adapter_holder!!.notifyDataSetChanged()

            counter++
        }

    }

    fun showMessageAfterSignup(msg_text: String, color: Int) {
        val st = StyleableToast.Builder(this)
                .text(msg_text)
                .textColor(Color.WHITE)
                .backgroundColor(color)
                .build()
        st.show()
    }

    // cancel health goal
    fun onCancelHealthGoalClicked(phoneNumber: String, healthGoals: String) {
        doAsync {
            val client: OkHttpClient = OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build()

            val urlBuilder = HttpUrl.parse(ServerConfig().cancelHealthGoals())!!.newBuilder()
            urlBuilder.addQueryParameter("phoneNumber", phoneNumber)
            urlBuilder.addQueryParameter("healthGoal", healthGoals)

            val url = urlBuilder.build().toString()

            val request = Request.Builder()
                    .url(url)
                    .build()

            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()

            uiThread {
                Log.d("mess", responce_string.toString())

                if (responce_string != "dataMissing") {
                    if (responce_string != "serverError") {
                        if (responce_string == "ok") {
                            //update local database
                            DB_HELPER(this@healthGoals).ActivateExpiryFlagInHealthGoals(healthGoals)

                            getMyHealthGoals()
                        } else {
                            showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                        }
                    } else {
                        showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                    }
                } else {
                    showMessageAfterSignup(getString(R.string.something_goes_wrong), Color.RED)
                }
            }
        }
    }
}
