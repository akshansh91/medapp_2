package com.medapp.medapp;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ImageView;

import com.medapp.medapp.config.ServerConfig;
import com.medapp.medapp.utils.left_drawer;

import net.glxn.qrgen.android.QRCode;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class scanQR extends AppCompatActivity {

    ImageView viewQR;
    SharedPreferences loginInfo;
    String phoneNumber, familyId, type, pass;
    StringBuilder IdString;

    OkHttpClient okHttpClient;
    HttpUrl.Builder builder;

    private Toolbar toolbar;

    Response response;
    Request request;

    String responseString, uIdString;

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_qr);

        toolbar = findViewById(R.id.toolbar_tabs4);

        new left_drawer(this, scanQR.this, toolbar).createNavigationDrawer();

        progressDialog = new ProgressDialog(scanQR.this);
        progressDialog.setMessage(getString(R.string.please_wait));
        progressDialog.setTitle(R.string.loading);
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);

        try {
            loginInfo = getSharedPreferences("loginInfo", Context.MODE_PRIVATE);
            phoneNumber = loginInfo.getString("phoneNumber", "");
            familyId = loginInfo.getString("familyId", "");
            pass = loginInfo.getString("password", "");

            viewQR = findViewById(R.id.qr);

            Log.d("scanQR__", "onCreate()....phoneNumber: " + phoneNumber + " familyId: " + familyId + " pass: " + pass);

            if (!familyId.equals("0")) {
                //family
                type = "f";
            } else {
                //individual
                type = "i";
            }

            Log.d("scanQR__", "onCreate()....type: " + type);

            IdString = new StringBuilder();

            IdString = IdString.append(phoneNumber);
            IdString.append(type);

            uIdString = String.valueOf(IdString.reverse());

            Log.d("scanQR__", "onCreate()....Uid: " + uIdString);

            CheckIfUIDExists ifUIDExists = new CheckIfUIDExists();
            ifUIDExists.execute(phoneNumber, type, uIdString);

        } catch (Exception e) {
            Log.d("scanQR__", "onCreate()...Exception:..." + e);
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(scanQR.this, Main2Activity.class);
        startActivity(intent);
        finish();
    }

    @SuppressLint("StaticFieldLeak")
    private class CheckIfUIDExists extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String phoneNumber = strings[0];
            String type = strings[1];
            String uid_code = strings[2];

            okHttpClient = new OkHttpClient.Builder().connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build();

            builder = Objects.requireNonNull(HttpUrl.parse(new ServerConfig().checkIfUidExists())).newBuilder();
            builder.addQueryParameter("phoneNumber", phoneNumber);
            builder.addQueryParameter("type", type);
            builder.addQueryParameter("uid_code", uid_code);

            String url = builder.build().toString();

            request = new Request.Builder().url(url).build();

            try {
                response = okHttpClient.newCall(request).execute();
            } catch (Exception e) {
                Log.d("scanQR__", "Exception_1: " + e);
            }

            if (response.body() != null) {
                try {
                    responseString = response.body().string();
                } catch (Exception e) {
                    Log.d("scanQR__", "Exception_2: " + e);
                }
            }
            Log.d("scanQR__", "responseString: " + responseString);
            return responseString;
        }

        @Override
        protected void onPostExecute(String s) {

            Log.d("scanQR__", "onPostExecute()...token: " + s);

            ServerConfig serverConfig = new ServerConfig();
            String server = serverConfig.getRootAddress();

            progressDialog.dismiss();

            String url = server + "API/getInfoFromCode.php?code=" + s + "&type=" + type;

            Log.d("scanQR__", "onPostExecute()......url: " + url);

            Bitmap myBitmap = QRCode.from(url).withSize(275, 275).bitmap();
            viewQR.setImageBitmap(myBitmap);
        }
    }
}
