package com.medapp.medapp

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.medapp.medapp.utils.left_drawer

class about : AppCompatActivity() {
    val TAG: String? = "Activity_Name"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)
        setTitle("About")

        Log.d(TAG, "Inside aboutActivity")

        //navigation drawer
        left_drawer(this, this@about, findViewById(R.id.aboutToolBar)!!).createNavigationDrawer()

    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this, Main2Activity::class.java)
        startActivity(intent)
        overridePendingTransition(R.anim.fade_out, R.anim.fade_in)
    }
}
