package com.medapp.medapp;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.Toast;

import com.medapp.medapp.config.ServerConfig;
import com.medapp.medapp.utils.left_drawer;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class searchDocs extends AppCompatActivity {

    SearchView searchView;
    //RecyclerView list;

    String TAG = "searchDocs";

    private Toolbar toolbar;

    SharedPreferences loginInfo;

    OkHttpClient client;
    HttpUrl.Builder urlBuilder;

    Response response;
    Request request;

    String phoneNumber, responseString, gotImage, responseString2, responseString3, responseString4, sharableImgLink;

    ImageView imageView;

    Button delete, share;

    //int adminFlag = 0;

    String u_type, adminCheck, familyId;

    ProgressDialog dialog;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(searchDocs.this, history_pickFamilyMember.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_docs);

        toolbar = findViewById(R.id.toolbar_tabs5);

        new left_drawer(this, searchDocs.this, toolbar).createNavigationDrawer();

        Log.d(TAG, "Service Started . . . ");

        dialog = new ProgressDialog(searchDocs.this);
        dialog.setMessage(getString(R.string.please_wait));
        dialog.setTitle(getString(R.string.searching));
        dialog.setCancelable(false);
        dialog.setIndeterminate(true);


        //get family id from persistent storage
        try {
            loginInfo = getApplicationContext().getSharedPreferences("loginInfo", Context.MODE_PRIVATE);
            //familyId = loginInfo.getString("familyId", null);
            phoneNumber = loginInfo.getString("phoneNumber", "");
            familyId = loginInfo.getString("familyId", "");
            u_type = loginInfo.getString("familyOrIndividual", "");

            Log.d(TAG, "Phone number from prefs . . " + phoneNumber);
            Log.d("searchDocs_", "familyID: " + familyId + " u_type: " + u_type);

            searchView = findViewById(R.id.searchBar);
            imageView = findViewById(R.id.img);
            delete = findViewById(R.id.delete);
            share = findViewById(R.id.share);

            if (loginInfo.contains("familyOrIndividual")) {
                if (loginInfo.getString("familyOrIndividual", "").equals("f")) {
                    //for family individual . . . .
                    //keep a check if he's the admin or not..
                    //the admin is allowed to search across all the docs of the family
                    //while others only the docs uploaded by that particular person...
                    u_type = "f"; //f== family
                    checkIfAdmin ifAdmin = new checkIfAdmin();
                    ifAdmin.execute(phoneNumber);
                }
                if (loginInfo.getString("familyOrIndividual", "").equals("i")) {
                    //directly search for all docs he uploaded . . .
                    u_type = "i"; //i== individual
                    adminCheck = "I"; //I == individual
                }
            }

            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //call to delete API
                    deleteDoc doc = new deleteDoc();
                    doc.execute(gotImage);
                }
            });

            share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //call to share API
                    shareDoc doc = new shareDoc();
                    doc.execute(gotImage);
                }
            });

            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    //Toast.makeText(searchDocs.this, "You searched for: " + query, Toast.LENGTH_SHORT).show();

                    String textToSearch = query.trim().replaceAll(" ", "").toLowerCase();

                    Log.d("search__", "textToSearch: " + textToSearch);

                    dialog.show();

                    Search search = new Search();
                    //search.execute(phoneNumber, query, u_type, adminCheck);
                    search.execute(phoneNumber, textToSearch, u_type, adminCheck);
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    imageView.setVisibility(View.GONE);
                    return false;
                }
            });
        } catch (Exception e) {
            Log.d("searchDocs_", "Exception: " + e);
        }
    }

    @SuppressLint("StaticFieldLeak")
    class Search extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            String contact = strings[0];
            String docName = strings[1];
            String uType = strings[2];
            String adminCheck = strings[3];

            client = new OkHttpClient.Builder().readTimeout(60, TimeUnit.SECONDS).writeTimeout(60, TimeUnit.SECONDS)
                    .connectTimeout(60, TimeUnit.SECONDS).build();

            urlBuilder = Objects.requireNonNull(HttpUrl.parse(new ServerConfig().searchDocs())).newBuilder();
            urlBuilder.addQueryParameter("contact", contact);
            urlBuilder.addQueryParameter("docName", docName);
            urlBuilder.addQueryParameter("uType", uType);
            urlBuilder.addQueryParameter("adminCheck", adminCheck);
            urlBuilder.addQueryParameter("familyID", familyId);

            String url = urlBuilder.build().toString();

            request = new Request.Builder().url(url).build();

            try {
                response = client.newCall(request).execute();
            } catch (Exception e) {
                Log.d("searchDocs_", "Exception: " + e);
            }

            if (response != null) {
                try {
                    if (response.body() != null) {
                        responseString = response.body().string();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return responseString;
        }

        @Override
        protected void onPostExecute(String s) {
            dialog.dismiss();

            imageView.setVisibility(View.VISIBLE);
            if (!s.equals("noDocs")) {
                gotImage = s;
                delete.setVisibility(View.VISIBLE);
                share.setVisibility(View.VISIBLE);

                Toast.makeText(searchDocs.this, s, Toast.LENGTH_SHORT).show();
                Picasso.with(searchDocs.this)
                        .load(new ServerConfig().getRootAddress() + "upload/" + s + "?notCache=" + s)
                        .fit()
                        .into(imageView);
            } else {
                /*String imgName = "noFile.jpg";*/

                /*Picasso.with(searchDocs.this)
                        .load(new ServerConfig().getRootAddress() + "upload/" + imgName + "?notCache=" + imgName)
                        .fit()
                        .into(imageView);*/

                Picasso.with(searchDocs.this)
                        .load(R.drawable.no_search_doc_exists)
                        .fit()
                        .into(imageView);
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    class deleteDoc extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            String imgName = strings[0];

            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build();

            HttpUrl.Builder urlBuilder = Objects.requireNonNull(HttpUrl.parse(new ServerConfig().getDeleteDocApi())).newBuilder();
            urlBuilder.addQueryParameter("docName", imgName);

            String url = urlBuilder.build().toString();

            Request request = new Request.Builder().url(url).build();

            try {
                Response response = client.newCall(request).execute();
                if (response.body() != null) {
                    responseString2 = response.body().string();
                }
                Log.d("API_Call_Del_Img", "Image Name: " + imgName);
                Log.d("API_Call_Del_Img", "responseString: " + responseString2);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return responseString2;
        }

        @Override
        protected void onPostExecute(String s) {
            if (!s.equals("dataMissing")) {
                if (!s.equals("serverError")) {
                    if (s.equals("ok")) {
                        Toast.makeText(searchDocs.this, R.string.document_deleted_successfully, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(searchDocs.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(searchDocs.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(searchDocs.this, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    class shareDoc extends AsyncTask<String, String, String> {

        String imageName;

        @Override
        protected String doInBackground(String... strings) {

            imageName = strings[0];

            Log.d("API_Call_Token", "inside generateTokenforImage");

            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build();

            HttpUrl.Builder urlBuilder = Objects.requireNonNull(HttpUrl.parse(new ServerConfig().generateToken_n_TimeStamp())).newBuilder();
            urlBuilder.addQueryParameter("imgName", imageName);

            String url = urlBuilder.build().toString();

            Request request = new Request.Builder().url(url).build();

            try {
                Response response = client.newCall(request).execute();
                if (response.body() != null) {
                    String res = response.body().string().replace("'", "");
                    responseString3 = res;
                    sharableImgLink = res;
                }
                //Log.d("API_Call_Token", "Image Name:" + imageName);
                Log.d("API_Call_Token", "responseString: " + responseString3);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return responseString3;
        }

        @Override
        protected void onPostExecute(String s) {
            if (!s.equals("dataMissing")) {
                if (!s.equals("serverError")) {
                    Log.d("API_Call_Token", "URL:" + s);
                    Intent i = new Intent(searchDocs.this, shareImage_Menu.class);
                    i.putExtra("imageName", imageName);
                    i.putExtra("imageLink", sharableImgLink);
                    searchDocs.this.startActivity(i);
                    finish();
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                } else {
                    Toast.makeText(searchDocs.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(searchDocs.this, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class checkIfAdmin extends AsyncTask<String, String, String> {

        String numberToCheck;

        @Override
        protected String doInBackground(String... strings) {
            numberToCheck = strings[0];

            OkHttpClient okHttpClient = new OkHttpClient.Builder().connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build();

            //The order matters

            HttpUrl.Builder url = Objects.requireNonNull(HttpUrl.parse(new ServerConfig().checkIfAdmin())).newBuilder();
            url.addQueryParameter("phoneNumber", numberToCheck);

            String f_url = url.build().toString();

            Request request = new Request.Builder().url(f_url).build();

            try {
                Response response = okHttpClient.newCall(request).execute();

                if (response.body() != null) {
                    responseString4 = response.body().string();
                }
            } catch (IOException e) {
                Log.d("searchDocs_", "Background:.. Exception: " + e);
            }
            Log.d("searchDocs_", "Response String: " + responseString4);
            return responseString4;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                Log.d("searchDocs_", "Post Execute....Response String: " + s);
                if (s != null && s.equals("YES")) {
                    //adminFlag = 1;
                    adminCheck = "A";
                } else {
                    //adminFlag = 0;
                    adminCheck = "NA";
                }
                Log.d("searchDocs_", "Post Execute....AdminCheck: " + adminCheck);
            } catch (Exception e) {
                Log.d("searchDocs_", "Post Execute: " + e);
            }
        }
    }
}
