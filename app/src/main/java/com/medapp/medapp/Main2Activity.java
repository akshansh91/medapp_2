package com.medapp.medapp;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.job.JobScheduler;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.RectF;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import org.vudroid.pdfdroid.codec.PdfContext;

import android.widget.Button;
import android.widget.ImageView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.github.angads25.filepicker.controller.DialogSelectionListener;
import com.github.angads25.filepicker.model.DialogConfigs;
import com.github.angads25.filepicker.model.DialogProperties;
import com.github.angads25.filepicker.view.FilePickerDialog;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.medapp.medapp.database.DB_HELPER;

import com.medapp.medapp.services.checkAddedToFamily;
import com.medapp.medapp.services.checkDocsToBeVerified;
import com.medapp.medapp.utils.left_drawer;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.zhihu.matisse.Matisse;
import com.medapp.medapp.utils.individual_family_login;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.engine.impl.PicassoEngine;

import org.vudroid.core.DecodeServiceBase;
import org.vudroid.pdfdroid.codec.PdfPage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main2Activity extends AppCompatActivity {

    //private Drawer drawerResult;
    JobScheduler jobschedular;
    int jobschedularCode = 1, REQUEST_IMAGE_CAPTURE = 1, REQUEST_CODE_FOR_GALLERY_CAPTURE = 2;
    String phoneNumber, familyId, password;
    private Toolbar toolbar;
    private File photoFile;
    Dialog progressDialog;
    SharedPreferences loginInfo;
    FilePickerDialog dialog;

    @Override
    public void onBackPressed() {
        finishAffinity();
        System.exit(0);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //onclick listener for open camera
        onclickListenerForOpenCamera();

        //starting the services here  . .
        Intent service_checkAddedtoFamily = new Intent(Main2Activity.this, checkAddedToFamily.class);
        startService(service_checkAddedtoFamily);

        Intent service_checkDocsToBeVerified = new Intent(Main2Activity.this, checkDocsToBeVerified.class);
        startService(service_checkDocsToBeVerified);

        //onclick listener for select image button
        attach_onclick_listener_to_add_photos_from_gallery();

        //onclick listener for select pdf files
        onclickListenerForSelectPdfFile();
        //get toolbar for drawer
        toolbar = findViewById(R.id.toolbar_tabs);

        loginInfo = getSharedPreferences("loginInfo", Context.MODE_PRIVATE);
        phoneNumber = loginInfo.getString("phoneNumber", "");
        familyId = loginInfo.getString("familyId", "");

        //onclick listener for upload button
        //onclickListenerForUploadButton()

        //onclick listener for retrieve button
        onclickListenerForRetrieveButton();

        //on click permanent diseases button
        //onclickPermanentDiseasesButtton()

        //navigation drawer
        new left_drawer(this, Main2Activity.this, toolbar).createNavigationDrawer();

        //verify auto upload
        verifyAutoLoginInformation();

        //create Sqlite database
        new DB_HELPER(this).getWritableDatabase();

        //get job schedular service
        jobschedular = (JobScheduler) this.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        /*schedulTheJobForHealthGoals();
        schedulTheJobForHealthInsurance();
        setPreferencesForNutrition();
        schedulTheJobForNutrition();
        schedulTheJobForSyncNutritionOnline();*/
    }

    private void verifyAutoLoginInformation() {
        loginInfo = getSharedPreferences("loginInfo", Context.MODE_PRIVATE);

        if (loginInfo.contains("familyOrIndividual")) {
            //for family
            if (loginInfo.getString("familyOrIndividual", "").equals("f")) {
                if (loginInfo.contains("phoneNumber") && loginInfo.contains("password")) {
                    phoneNumber = loginInfo.getString("phoneNumber", "");
                    password = loginInfo.getString("password", "");
                    new individual_family_login(this).
                            makeFamilyLoginApiRequest(phoneNumber, password);
                } else {
                    new left_drawer(this, this, toolbar).makeUserLogOut();
                }
            }

            //for individual
            if (loginInfo.getString("familyOrIndividual", "").equals("i")) {
                if (loginInfo.contains("phoneNumber") && loginInfo.contains("password")) {
                    phoneNumber = loginInfo.getString("phoneNumber", "");
                    password = loginInfo.getString("password", "");
                    new individual_family_login(this).
                            makeLoginApiRequest(phoneNumber, password);
                } else {
                    new left_drawer(this, this, toolbar).makeUserLogOut();
                }
            }

            //for security
            if (!loginInfo.getString("familyOrIndividual", "").equals("i")
                    && !loginInfo.getString("familyOrIndividual", "").equals("f")) {
                new left_drawer(this, this, toolbar).makeUserLogOut();
            }

        } else {
            new left_drawer(this, this, toolbar).makeUserLogOut();
        }
    }

    private void onclickListenerForRetrieveButton() {
        Button button = findViewById(R.id.retrieveButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Main2Activity.this, history_pickFamilyMember.class);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });
    }

    private void onclickListenerForSelectPdfFile() {
        ImageView imageView = findViewById(R.id.selectPdfFile);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                get_permissions_fileExplorer();
            }
        });
    }

    private void get_permissions_fileExplorer() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            new MaterialDialog.Builder(this)
                    .title(R.string.storage_permission)
                    .content(R.string.storage_permissions_required)
                    .negativeText(R.string.cancel)
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                        }
                    })
                    .positiveText(R.string.give_permission)
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            getPermissionsUsingDexter_fileExplores(
                            );
                        }
                    })
                    .show();

        } else {
            openFileSelector();
        }
    }

    private void getPermissionsUsingDexter_fileExplores() {
        Dexter.withActivity(this)
                .withPermissions(
                        android.Manifest.permission.READ_EXTERNAL_STORAGE
                ).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {
                if (report.areAllPermissionsGranted()) {
                    openFileSelector();
                    Log.d("mess", "permission given");
                } else {
                    Log.d("mess", "permission not granted");
                }
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {

            }
        })
                .check();
    }

    private void openFileSelector() {
        try {
            Log.d("Picking_Docs", "Inside OpenFileSelector ");
            DialogProperties properties = new DialogProperties();

            properties.selection_mode = DialogConfigs.SINGLE_MODE;
            properties.selection_type = DialogConfigs.FILE_SELECT;
            properties.root = new File(DialogConfigs.DEFAULT_DIR);
            properties.error_dir = new File(DialogConfigs.DEFAULT_DIR);
            properties.offset = new File(DialogConfigs.DEFAULT_DIR);
            properties.extensions = null;

            dialog = new FilePickerDialog(Main2Activity.this, properties);
            dialog.setTitle(getString(R.string.select_file));

            dialog.setDialogSelectionListener(new DialogSelectionListener() {
                @Override
                public void onSelectedFilePaths(String[] files) {
                    ArrayList<String> arrayList = new ArrayList<>(Arrays.asList(files));
                    Log.d("File_Paths:", String.valueOf(arrayList));
                    convertPdfToImages(arrayList);
                }
            });
            dialog.show();
        } catch (Exception e) {
            Log.d("Picking_Docs", "Inside OpenFileSelector ... Exception: " + e);
        }
    }

    private void convertPdfToImages(ArrayList<String> files) {
        try {
            Log.d("Picking_Docs", "Inside convertPdfToImages ");
            convertPDF convertPDF = new convertPDF();
            convertPDF.execute(files.get(0));
        } catch (Exception e) {
            Log.d("Picking_Docs", "Inside convertPdfToImages ... Exception: " + e);
        }
    }


    private void onclickListenerForOpenCamera() {
        ImageView imageView = findViewById(R.id.openCamera);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                get_permissions_camera();
            }
        });
    }

    private void get_permissions_camera() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            new MaterialDialog.Builder(Main2Activity.this)
                    .title(R.string.camera_perm)
                    .content(getString(R.string.camera_permission_req))
                    .negativeText(R.string.cancel)
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                        }
                    })
                    .positiveText(R.string.give_permission)
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            getPermissionsUsingDexter_camera();
                        }
                    })
                    .show();

        } else {
            _openCamera();
        }
    }

    private void getPermissionsUsingDexter_camera() {
        Dexter.withActivity(this)
                .withPermissions(
                        android.Manifest.permission.CAMERA
                ).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {
                if (report.areAllPermissionsGranted()) {
                    _openCamera();
                    Log.d("mess", "permission given");
                } else {
                    Log.d("mess", "permission not granted");
                }
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {

            }
        })
                .check();
    }

    private void _openCamera() {
        try {
            Log.d("Picking_Images", "Inside _openCamera");
            //old
            // Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                try {
                    photoFile = createImageFile();
                } catch (Exception ex) {
                    Log.d("Picking_Images", ex.toString());
                }
                if (photoFile != null) {
                    StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                    StrictMode.setVmPolicy(builder.build());
                    Uri photoURI = Uri.fromFile(photoFile);
                    Log.d("URI__Profile", photoURI.toString());
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                }
            }
        } catch (Exception e) {
            Log.d("Picking_Images", "Inside _openCamera . . ..Exception" + e);
        }
    }

    private File createImageFile() {
        File image = null;
        String mCurrentPhotoPath;
        String imageFileName = "camera";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        //File image = new File.createTempFile(imageFileName, ".jpg", storageDir);
        try {
            image = File.createTempFile(imageFileName, ".jpg", storageDir);
        } catch (IOException e) {
            Log.d("Image", String.valueOf(e));
        }
        // Save a file: path for use with ACTION_VIEW intents
        assert image != null;
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void attach_onclick_listener_to_add_photos_from_gallery() {
        ImageView imageView = findViewById(R.id.selectImage);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                get_permissions_gallery();
            }
        });
    }

    private void get_permissions_gallery() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            new MaterialDialog.Builder(Main2Activity.this)
                    .title(R.string.storage_permission)
                    .content(getString(R.string.storage_perm_req))
                    .negativeText(R.string.cancel)
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                        }
                    }).positiveText(R.string.give_permission)
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            getPermissionsUsingDexter_gallery();
                        }
                    })
                    .show();

        } else {
            open_selector();
        }
    }

    private void getPermissionsUsingDexter_gallery() {
        Dexter.withActivity(this)
                .withPermissions(
                        android.Manifest.permission.READ_EXTERNAL_STORAGE
                ).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {
                if (report.areAllPermissionsGranted()) {
                    open_selector();
                    Log.d("mess", "permission given");
                } else {
                    Log.d("mess", "permission not granted");
                }
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {

            }
        })
                .check();
    }

    //old open_selector()

    private void open_selector() {
        try {
            Log.d("imageGallery__", "inside open_selector");
            Matisse.from(Main2Activity.this)
                    .choose(MimeType.allOf())
                    .countable(true)
                    .maxSelectable(1)
                    .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
                    .thumbnailScale(0.85f)
                    .imageEngine(new PicassoEngine())
                    .forResult(REQUEST_CODE_FOR_GALLERY_CAPTURE);
        } catch (Exception e) {
            Log.d("imageGallery__", "Exception....open_selector: " + e);
        }
    }

    /*private void open_selector() {
        try {
            Log.d("imageGallery__", "Inside....open_selector: ");
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Image"), REQUEST_CODE_FOR_GALLERY_CAPTURE);
        } catch (Exception e) {
            Log.d("imageGallery__", "Exception....open_selector: " + e);

        }
    }*/

    private void showProcessProgress() {
        progressDialog = new MaterialDialog.Builder(this)
                .title(R.string.please_wait)
                .content(getString(R.string.convert_pdf_to_image))
                .progress(true, 0)
                .show();
    }

    @SuppressLint("StaticFieldLeak")
    class convertPDF extends AsyncTask<String, String, ArrayList<Uri>> {

        ArrayList<Uri> uriList;

        @Override
        protected final ArrayList<Uri> doInBackground(String... files) {
            Log.d("Picking_Docs", "Inside convertPdfClass doInBackground");

            try {
                uriList = new ArrayList<>();
                int no_of_files = 1;
                int counter = 0;

                while (counter < no_of_files) {
                    File pdfFile = new File(files[0]);

                    DecodeServiceBase decodeService = new DecodeServiceBase(new PdfContext());
                    decodeService.setContentResolver(getContentResolver());
                    decodeService.open(Uri.fromFile(pdfFile));
                    int pageCount = decodeService.getPageCount();
                    int i = 0;

                    while (i < pageCount) {
                        PdfPage page = (PdfPage) decodeService.getPage(i);
                        RectF rectF = new RectF((float) 0, (float) 0, (float) 1, (float) 1);

                        // do a fit center to 1920x1080
                        int scaleBy = 1;
                        int with = (page.getWidth() * scaleBy);
                        int height = (page.getHeight() * scaleBy);

                        Bitmap bitmap = page.renderBitmap(with, height, rectF);

                        File outputFile = new File(getExternalCacheDir(), String.valueOf(System.currentTimeMillis())
                                + ".jpg");

                        FileOutputStream outputStream = null;
                        try {
                            outputStream = new FileOutputStream(outputFile);
                        } catch (FileNotFoundException e) {
                            Log.d("Exceptions", String.valueOf(e));
                        }

                        // a bit long running
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);

                        uriList.add(Uri.fromFile(outputFile));

                        try {
                            if (outputStream != null) {
                                outputStream.close();
                            }
                        } catch (IOException e) {
                            Log.d("Exceptions", String.valueOf(e));
                        }
                        i++;
                    }
                    counter++;
                }
            } catch (Exception e) {
                Log.d("Picking_Docs", "Inside convertPdfClass Background... Exception: " + e);
            }
            return uriList;
        }

        @Override
        protected void onPostExecute(ArrayList<Uri> uriList) {
            //progressDialog.dismiss();
            try {
                Log.d("Picking_Docs", "Inside convertPdfClass onPostExecute");
                dialog.dismiss();
                openPreview(uriList);
                Log.d("mess", "size: " + uriList.size() + " " + uriList.toString());
            } catch (Exception e) {
                Log.d("Picking_Docs", "Inside convertPdfClass onPostExecute... Exception: " + e);
            }
        }
    }

   /* @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }*/


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
                if (photoFile != null) {
                    Uri uriOfImage = Uri.fromFile(photoFile);
                    Log.e("Main", "uri Main : " + uriOfImage.toString());

                    //start croper
                    CropImage.activity(uriOfImage)
                            .start(this);
                } else {
                    Log.e("Main", "Uri == NULL");
                }
            } else if (requestCode == REQUEST_CODE_FOR_GALLERY_CAPTURE) {
                if (resultCode == RESULT_OK) {
                    try {
                        Log.d("imageGallery__", "inside ActivityResult...");

                        /*Uri imageURI = data.getData();
                        String picturePath = getPath(this.getApplicationContext(), imageURI);
*/
                       /* Log.d("imageGallery__", "ImageURI: " + imageURI);
                        Log.d("imageGallery__", "picturePath: " + picturePath);*/
                        Log.d("imageGallery__", "URI from Matisse: " + Matisse.obtainResult(data));

                        ArrayList<Uri> selected_images = (ArrayList<Uri>) Matisse.obtainResult(data);
                        openPreview(selected_images);
                    } catch (Exception e) {
                        Log.d("imageGallery__", "Exception....ActivityResult: " + e);
                    }
                }
            }

            //for croper
            else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                if (resultCode == RESULT_OK) {
                    //doing something in asyncTask
                    Cropper cropper = new Cropper();
                    cropper.execute(result);
                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    Exception error = result.getError();
                    Log.d("Exception", String.valueOf(error));
                }
            }
        } catch (Exception e) {
            Log.d("Picking_Docs", "Cropper onActivityResult Exception: " + e.toString());
        }
    }

    public static String getPath(Context context, Uri uri) {
        String result = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(uri, proj, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                int column_index = cursor.getColumnIndexOrThrow(proj[0]);
                result = cursor.getString(column_index);
            }
            cursor.close();
        }
        if (result == null) {
            result = "Not found";
        }
        return result;
    }

    private void openPreview(ArrayList<Uri> list) {
        try {
            Log.d("imageGallery__", "inside openPreview");
            Intent i = new Intent(Main2Activity.this, typeOfDocument.class);
            Bundle args = new Bundle();
            args.putSerializable("ARRAYLIST", list);
            i.putExtra("BUNDLE", args);
            startActivity(i);
            finish();
        } catch (Exception e) {
            Log.d("imageGallery__", "Exception....openPreview: " + e);
        }
    }

    @SuppressLint("StaticFieldLeak")
    class Cropper extends AsyncTask<CropImage.ActivityResult, String, Uri> {

        @Override
        protected Uri doInBackground(CropImage.ActivityResult... activityResults) {
            CropImage.ActivityResult result = activityResults[0];

            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
            Uri resultUri = result.getUri();

            //save cropped image for persisitance
            File croppedImage = createImageFile(); //empty
            FileOutputStream outputStream = null;
            try {
                outputStream = new FileOutputStream(croppedImage);
            } catch (FileNotFoundException e) {
                Log.d("Exception", String.valueOf(e));
            }
            // a bit long running
            try {
                (Picasso.with(Main2Activity.this)
                        .load(resultUri)
                        .get()
                ).compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
            } catch (IOException e) {
                Log.d("Exception", String.valueOf(e));
            }

            resultUri = Uri.fromFile(croppedImage);
            try {
                if (outputStream != null) {
                    outputStream.close();
                }
            } catch (IOException e) {
                Log.d("Exception", String.valueOf(e));
            }
            return resultUri;
        }

        @Override
        protected void onPostExecute(Uri resultUri) {
            ArrayList<Uri> mu_list = new ArrayList<>(1);
            mu_list.add(resultUri);
            Log.d("Main", "camera uri" + resultUri.toString());
            openPreview(mu_list);
        }
    }
}
