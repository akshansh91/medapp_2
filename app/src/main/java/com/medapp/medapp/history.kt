package com.medapp.medapp

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Bundle
import android.provider.ContactsContract
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.text.InputType
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.afollestad.materialdialogs.DialogAction
import com.afollestad.materialdialogs.MaterialDialog
import com.amulyakhare.textdrawable.TextDrawable
import com.amulyakhare.textdrawable.util.ColorGenerator
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.medapp.medapp.config.ServerConfig
import com.medapp.medapp.utils.left_drawer
import com.muddzdev.styleabletoastlibrary.StyleableToast
import com.onegravity.contactpicker.contact.Contact
import com.onegravity.contactpicker.contact.ContactDescription
import com.onegravity.contactpicker.contact.ContactSortOrder
import com.onegravity.contactpicker.core.ContactPickerActivity
import com.onegravity.contactpicker.picture.ContactPictureType
import com.squareup.picasso.Picasso
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import kotlinx.android.synthetic.main.activity_history.*
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.util.*
import java.util.concurrent.TimeUnit

class history : AppCompatActivity() {
    val TAG: String? = "Activity_Name"
    var result_list_for_history: java.util.ArrayList<history_store> = java.util.ArrayList()
    private var history_holder: RecyclerView.Adapter<history_adapter.NumberViewHolder>? = null
    private var g_recyclerView: RecyclerView? = null

    var result_list_for_familyMembers: java.util.ArrayList<familyMembers_store> = java.util.ArrayList()

    private var imageId: Int? = 0
    private var familyId: String? = "0"
    private var phoneNumber: String? = null

    private var fromday: String? = "0"
    private var frommonth: String? = "0"
    private var fromyear: String? = "0"
    private var today: String? = "0"
    private var tomonth: String? = "0"
    private var toyear: String? = "0"
    private var sortTerm: String? = "sort"
    private var sortApiRequestIsDoneOnce: Int? = 0
    private var sortByFamilyMembersTerm: String? = "none"

    private var clickedItemCount: Int? = null
    private var clickedFamilyMemberName: String? = null

    private var PICK_CONTACT_REQUEST = 1

    private var forContactChoose_textView: TextView? = null
    private var forContactChoose_imgName: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history)
        title = "History"
        Log.d(TAG, "Inside history")
        //drawer
        //navigation drawer
        try {
            Log.d("Check_history", "Inside onCreate()")
            left_drawer(this, this@history, findViewById(R.id.sortByFamilyMembersToolBar)!!).createNavigationDrawer()

            //attach history adapter to recycler view
            attach_history_adapter_to_recyclerview()

            //get familyId
            val loginInfo = applicationContext.getSharedPreferences("loginInfo", Context.MODE_PRIVATE)
            familyId = loginInfo.getString("familyId", "")
            phoneNumber = loginInfo.getString("phoneNumber", "")

            //data in previous intent
            val i = intent
            val selectedFamilyMemberPhoneNumber = i.getStringExtra("Selected_familyMemberPhoneNumber")
            val selectedFamilyMemberName = i.getStringExtra("Selected_familyMemberName")

            //create family member list
            createFamilyMembersList()

            if (selectedFamilyMemberPhoneNumber != "all") {
                //for family
                //check data from previous intent
                checkDataFromPreviousIntent(selectedFamilyMemberPhoneNumber)
            } else {
                //make history api request

                makeHistoryApiRequest(imageId!!, familyId!!, phoneNumber!!)
            }

            //on click load more button
            onClickLoadMoreButton()

            //onclick from and to buttons and sort Button
            onClickFromAndToButtonsAndSortButton()

            //load sort by family member if admin is logged in
            //loadSortByFamilymembers_IfAdminIsLoggedIn()
            //called in createFamilyMembersList() for proper execution

            //show user the link of unverified docs which are uploaded by family admin
            makeVerifyButtonWorks()
            isThereAnyDocRemaningToVerify()
        } catch (e: Exception) {
            Log.d("Check_history", "Inside onCreate()....Exception: $e")
        }
    }

    //check data from previous intent
    //this will be used if family admin is logged in
    fun checkDataFromPreviousIntent(selectedFamilyMemberPhoneNumber: String) {
        try {
            Log.d("Check_history", "Inside checkDataFromPreviousIntent()")
            if (selectedFamilyMemberPhoneNumber != "all") {
                LoadLayoutForIndividualUser()

                sortByFamilyMembersTerm = selectedFamilyMemberPhoneNumber

                imageId = 0
                clearHistoryRecyclerView()
                makeRequestToSortApiRequest(fromday!!, frommonth!!, fromyear!!, today!!, tomonth!!, toyear!!, sortTerm!!, sortByFamilyMembersTerm!!)
            }
        } catch (e: Exception) {
            Log.d("Check_history", "Inside checkDataFromPreviousIntent()....Exception: $e")
        }
    }

    //recyclerview
    fun attach_history_adapter_to_recyclerview() {
        try {
            Log.d("Check_history", "Inside attach_history_adapter_to_recyclerview()")
            val off_study_show_recyclerView: RecyclerView = findViewById<RecyclerView>(R.id.historyRecyclerView)
            g_recyclerView = off_study_show_recyclerView
            val layoutManager = LinearLayoutManager(applicationContext)
            off_study_show_recyclerView.layoutManager = layoutManager
            off_study_show_recyclerView.setHasFixedSize(false)
            off_study_show_recyclerView.isNestedScrollingEnabled = false
            history_holder = history_adapter()
            off_study_show_recyclerView.adapter = history_holder
        } catch (e: Exception) {
            Log.d("Check_history", "Inside attach_history_adapter_to_recyclerview()....Exception: $e")
        }
    }

    inner class history_store(
            familyMemberName: String,
            date: String,
            type: String,
            category: String,
            description: String,
            img_name: String,
            docIsVisibleTo: String
    ) {
        private var inner_familyMemberName: String = familyMemberName
        private var inner_date: String = date
        private var inner_type: String = type
        private var inner_category: String = category
        private var inner_description: String = description
        private var inner_img_name: String = img_name
        private var inner_docIsVisibleTO: String = docIsVisibleTo

        fun get_familyMemberName(): String {
            return inner_familyMemberName
        }

        fun get_date(): String {
            return inner_date
        }

        fun get_type(): String {
            return inner_type
        }

        fun get_category(): String {
            return inner_category
        }

        fun get_description(): String {
            return inner_description
        }

        fun get_img_name(): String {
            return inner_img_name
        }

        fun get_docIsVisibleToPhoneNumbers(): String {
            return inner_docIsVisibleTO
        }

        fun get_docIsVisibleTO(): String {
            val arrayOfVisibleToPhonenumber = inner_docIsVisibleTO.split("-")
            var counter = 1
            val size = arrayOfVisibleToPhonenumber.size
            var membersNamesStringToshow: String? = " "

            while (counter < size) {
                val onePhoneNumber = arrayOfVisibleToPhonenumber.get(counter)

                val w_size = result_list_for_familyMembers.size
                var w_counter = 0
                var status = 0
                while (w_counter < w_size) {
                    val singlePersonData = result_list_for_familyMembers.get(w_counter)
                    if (onePhoneNumber == singlePersonData.get_phoneNumber()) {
                        //if number is of one of the family member
                        if (membersNamesStringToshow != " ") {
                            membersNamesStringToshow = membersNamesStringToshow + "," + singlePersonData.get_name()
                        } else {
                            membersNamesStringToshow = singlePersonData.get_name()
                        }
                        status = 1
                    }

                    w_counter++
                }

                if (status == 0) {
                    //if number is selected by user from his contact list
                    if (membersNamesStringToshow != " ") {
                        membersNamesStringToshow = membersNamesStringToshow + "," + onePhoneNumber
                    } else {
                        membersNamesStringToshow = onePhoneNumber
                    }
                }

                counter++
            }

            if (membersNamesStringToshow != " ") {
                return membersNamesStringToshow!!
            } else {
                return "None"
            }

        }

    }

    private inner class history_adapter : RecyclerView.Adapter<history_adapter.NumberViewHolder>() {

        override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): NumberViewHolder {
            val context = viewGroup.context
            val layoutIdForListItem = R.layout.history_blueprint_family
            val inflater = LayoutInflater.from(context)
            val shouldAttachToParentImmediately = false

            val view = inflater.inflate(layoutIdForListItem, viewGroup, shouldAttachToParentImmediately)
            val viewHolder = NumberViewHolder(view)

            return viewHolder
        }

        override fun onBindViewHolder(holder: NumberViewHolder, position: Int) {
            val current_object = result_list_for_history.get(position)
            holder.bind(current_object)
        }

        override fun getItemCount(): Int {
            return result_list_for_history.size
        }

        inner class NumberViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            private var image_preview: ImageView? = null
            private var familyMemberNameTextView: TextView? = null
            private var dateTextView: TextView? = null
            private var typeTextView: TextView? = null
            private var categoryTextView: TextView? = null
            private var descriptionTextView: TextView? = null
            private var docIsVisibleToTextView: TextView? = null
            private var docIsVisibleToHeading: TextView? = null
            private var changeVisibilityButton: Button? = null
            private var changeVisibilityIcon: ImageView? = null
            private var deleteDocButton: Button? = null

            init {
                familyMemberNameTextView = itemView.findViewById(R.id.familyMemberName)
                image_preview = itemView.findViewById(R.id.img_preview)
                typeTextView = itemView.findViewById(R.id.type)
                categoryTextView = itemView.findViewById(R.id.category)
                dateTextView = itemView.findViewById(R.id.date)
                descriptionTextView = itemView.findViewById(R.id.description)
                docIsVisibleToHeading = itemView.findViewById(R.id.docVisibilityHeading)
                docIsVisibleToTextView = itemView.findViewById(R.id.docVisibilityContent)
                changeVisibilityButton = itemView.findViewById(R.id.changeVisibilityButton)
                deleteDocButton = itemView.findViewById(R.id.deleteDocButton)
                changeVisibilityIcon = itemView.findViewById(R.id.change_visibility_icon)
            }

            fun bind(current_object: history_store) {
                familyMemberNameTextView!!.text = current_object.get_familyMemberName()
                dateTextView!!.text = current_object.get_date()
                typeTextView!!.text = current_object.get_type()
                categoryTextView!!.text = current_object.get_category()
                descriptionTextView!!.text = current_object.get_description()
                docIsVisibleToTextView!!.text = current_object.get_docIsVisibleTO()

                changeVisibilityButton!!.setOnClickListener {
                    createFamilyMemberCheckBoxes(current_object.get_img_name(), docIsVisibleToTextView!!)
                }
                if (familyId!!.toInt() != 0 || (current_object.get_docIsVisibleToPhoneNumbers()).contains(phoneNumber.toString())) {
                    changeVisibilityButton!!.visibility = View.GONE
                    changeVisibilityIcon!!.visibility = View.GONE
                    docIsVisibleToTextView!!.visibility = View.GONE
                    docIsVisibleToHeading!!.visibility = View.GONE
                }

                deleteDocButton!!.setOnClickListener {
                    makeDocDeleteApiRequest(current_object.get_img_name(), deleteDocButton!!)
                }

                Picasso.with(this@history)
                        .load(ServerConfig().getRootAddress() + "upload/" + current_object.get_img_name() + "?notCache=" + current_object.get_img_name())
                        .fit()
                        .into(image_preview)
            }
        }
    }

    //delete doc
    fun makeDocDeleteApiRequest(docName: String, b: Button) {
        doAsync {
            try {
                Log.d("Check_history", "Inside makeDocDeleteApiRequest()")

                val client: OkHttpClient = OkHttpClient.Builder()
                        .connectTimeout(60, TimeUnit.SECONDS)
                        .writeTimeout(60, TimeUnit.SECONDS)
                        .readTimeout(60, TimeUnit.SECONDS)
                        .build()

                val urlBuilder = HttpUrl.parse(ServerConfig().getDeleteDocApi())!!.newBuilder()
                urlBuilder.addQueryParameter("docName", docName)

                val url = urlBuilder.build().toString()

                val request = Request.Builder()
                        .url(url)
                        .build()

                val response: Response = client.newCall(request).execute()
                val responce_string = response.body()!!.string()

                uiThread {
                    Log.d("mess", responce_string.toString())

                    if (responce_string != "dataMissing") {
                        if (responce_string != "serverError") {
                            if (responce_string == "ok") {
                                b.setText(R.string.this_document_is_deleted)
                            } else {
                                showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                            }
                        } else {
                            showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                        }
                    } else {
                        showMessageAfterSignup(getString(R.string.something_goes_wrong), Color.RED)
                    }
                }
            } catch (e: Exception) {
                Log.d("Check_history", "Inside makeDocDeleteApiRequest()....Exception: $e")
            }
        }
    }

    //make history api
    fun makeHistoryApiRequest(imgId: Int, f_id: String, p_n: String) {
        doAsync {
            try {
                Log.d("Check_history", "Inside makeHistoryApiRequest()")
                val client: OkHttpClient = OkHttpClient.Builder()
                        .connectTimeout(60, TimeUnit.SECONDS)
                        .writeTimeout(60, TimeUnit.SECONDS)
                        .readTimeout(60, TimeUnit.SECONDS)
                        .build()

                val urlBuilder = HttpUrl.parse(ServerConfig().getHistoryApi())!!.newBuilder()
                urlBuilder.addQueryParameter("imageId", imgId.toString())
                urlBuilder.addQueryParameter("familyId", f_id.toString())
                urlBuilder.addQueryParameter("phoneNumber", p_n)

                val url = urlBuilder.build().toString()

                val request = Request.Builder()
                        .url(url)
                        .build()

                val response: Response = client.newCall(request).execute()
                val responce_string = response.body()!!.string()

                uiThread {
                    Log.d("mess", responce_string.toString())
                    if (responce_string != "noImageUploadedYet" && responce_string != "noMoreImages") {
                        if (f_id == "0") {
                            //individial account
                            processIndividualHistoryResponse(responce_string)
                        } else {
                            //family account
                            processFamilyHistoryResponse(responce_string)
                        }
                    } else {
                        if (responce_string == "noImageUploadedYet") {
                            findViewById<Button>(R.id.loadMoreButton).visibility = View.GONE
                        }
                        if (responce_string == "noMoreImages") {
                            findViewById<Button>(R.id.loadMoreButton).visibility = View.GONE
                        }
                    }
                }
            } catch (e: Exception) {
                Log.d("Check_history", "Inside makeHistoryApiRequest()..Exception: $e")
            }
        }
    }

    fun processFamilyHistoryResponse(response_string: String) {
        val chunkOfData = response_string.split("___")
        imageId = chunkOfData[0].toInt()

        var counter = 1
        val size = chunkOfData.size
        while (counter < size) {
            val blockOfData = chunkOfData[counter]
            val type = (blockOfData.split("*"))[0]
            val category = (blockOfData.split("*"))[1]
            val description = (blockOfData.split("*"))[2]
            val familyMemberName = (blockOfData.split("*"))[3]
            val documentImageName = (blockOfData.split("*"))[4]
            val documentIsVisibleTo = (blockOfData.split("*"))[5]
            val dateAsText = (blockOfData.split("*"))[6] + "-" + (blockOfData.split("*"))[7] + "-" + (blockOfData.split("*"))[8]
            //SimpleDateFormat("dd-MM-yyyy").format(Date(((documentImageName.split(".")[0]).toInt() * 1000L)))

            result_list_for_history.add(history_store(familyMemberName, dateAsText, type, category, description, documentImageName, documentIsVisibleTo))
            history_holder!!.notifyDataSetChanged()

            counter++
        }
    }

    fun processIndividualHistoryResponse(response_string: String) {
        val chunkOfData = response_string.split("___")
        imageId = chunkOfData[0].toInt()

        var counter = 1
        val size = chunkOfData.size
        while (counter < size) {
            val blockOfData = chunkOfData[counter]
            val type = (blockOfData.split("*"))[0]
            val category = (blockOfData.split("*"))[1]
            val description = (blockOfData.split("*"))[2]
            val familyMemberName = "You"
            val documentImageName = (blockOfData.split("*"))[4]
            val documentIsVisibleTo = (blockOfData.split("*"))[5]
            val dateAsText = (blockOfData.split("*"))[6] + "-" + (blockOfData.split("*"))[7] + "-" + (blockOfData.split("*"))[8]
            //SimpleDateFormat("dd-MM-yyyy").format(Date(((documentImageName.split(".")[0]).toInt() * 1000L)))

            result_list_for_history.add(history_store(familyMemberName, dateAsText, type, category, description, documentImageName, documentIsVisibleTo))
            history_holder!!.notifyDataSetChanged()

            counter++
        }
    }

    //get family members lists
    inner class familyMembers_store(
            name: String,
            phoneNumber: String
    ) {
        private var inner_name: String = name
        private var inner_phoneNumber: String = phoneNumber

        fun get_name(): String {
            return inner_name
        }

        fun get_phoneNumber(): String {
            return inner_phoneNumber
        }

    }

    fun createFamilyMembersList() {
        doAsync {
            try {
                Log.d("Check_history", "Inside createFamilyMembersList()")
                val client: OkHttpClient = OkHttpClient.Builder()
                        .connectTimeout(60, TimeUnit.SECONDS)
                        .writeTimeout(60, TimeUnit.SECONDS)
                        .readTimeout(60, TimeUnit.SECONDS)
                        .build()

                val urlBuilder = HttpUrl.parse(ServerConfig().getFamilyMembersNamesAndPhoneNumberUsingUsersPhoneNumber())!!.newBuilder()
                urlBuilder.addQueryParameter("phoneNumber", phoneNumber)

                val url = urlBuilder.build().toString()

                val request = Request.Builder()
                        .url(url)
                        .build()

                val response: Response = client.newCall(request).execute()
                val responce_string = response.body()!!.string()

                uiThread {
                    Log.d("mess", responce_string.toString())

                    if (responce_string != "noFamilyAssoc") {
                        if (responce_string != "dataMissing") {
                            if (responce_string != "serverError") {
                                Log.d("response_body", responce_string.toString())
                                handleResponse(responce_string)
                            } else {
                                showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                            }
                        } else {
                            showMessageAfterSignup(getString(R.string.something_goes_wrong), Color.RED)
                        }
                    } else {
                        //don't do anything
                        Log.d("testing", responce_string)
                    }
                }
            } catch (e: Exception) {
                Log.d("Check_history", "Inside createFamilyMembersList().... Exception: $e")
            }
        }
    }

    fun showMessageAfterSignup(msg_text: String, color: Int) {
        val st = StyleableToast.Builder(this)
                .text(msg_text)
                .textColor(Color.WHITE)
                .backgroundColor(color)
                .build()
        st.show()
    }

    fun handleResponse(responseString: String) {
        result_list_for_familyMembers.clear()

        val chunkOfDate = responseString.split("___")
        val size = chunkOfDate.size
        var counter = 1

        while (counter < size) {
            val singleDataStream = chunkOfDate[counter]
            val name = singleDataStream.split("*")[0]
            val familyMemberPhoneNumber = singleDataStream.split("*")[1]

            result_list_for_familyMembers.add(
                    familyMembers_store(
                            name,
                            familyMemberPhoneNumber
                    )
            )

            counter++
        }
        loadSortByFamilymembers_IfAdminIsLoggedIn()
    }

    //create check box for selecting family members for documents visibility
    fun createFamilyMemberCheckBoxes(img_name: String, docIsVisibleToTextView: TextView) {
        doAsync {
            val size = result_list_for_familyMembers.size
            var counter = 0
            val arrayOfNamesOfFamilyMembers: ArrayList<String>? = arrayListOf()

            while (counter < size) {
                val dataBlock = result_list_for_familyMembers.get(counter)
                val familyMemberName = dataBlock.get_name()
                val familyMemberPhoneNumber = dataBlock.get_phoneNumber()

                arrayOfNamesOfFamilyMembers!!.add(familyMemberName)

                counter++
            }

            //pick from contact
            val PFC = "Pick From Contacts"
            arrayOfNamesOfFamilyMembers!!.add(PFC)

            uiThread {
                MaterialDialog.Builder(this@history)
                        .title("Document will be visible to")
                        .items(arrayOfNamesOfFamilyMembers)
                        .itemsCallbackMultiChoice(null, object : MaterialDialog.ListCallbackMultiChoice {
                            override fun onSelection(dialog: MaterialDialog?, which: Array<out Int>?, text: Array<out CharSequence>?): Boolean {
                                if (text!![0].toString() == PFC) {
                                    createContactChooser(docIsVisibleToTextView, img_name)
                                } else {
                                    handleVisibilityOfDocument(docIsVisibleToTextView, img_name, text, text.size)
                                }

                                return true
                            }
                        })
                        .positiveText("OK")
                        .show()
            }
        }
    }

    fun handleVisibilityOfDocument(docIsVisibleToTextView: TextView, img_name: String, arrayOfMembersNames: Array<out CharSequence>?, sizeOfList: Int) {
        if (sizeOfList != 0) {
            var MembersPhoneNumberString: String? = "none"
            var MembersNamesString: String? = " "
            var counter = 0

            while (counter < sizeOfList) {
                val singleName = arrayOfMembersNames!!.get(counter)

                val w_size = result_list_for_familyMembers.size
                var w_counter = 0
                while (w_counter < w_size) {
                    val singlePersonData = result_list_for_familyMembers.get(w_counter)
                    if (singleName == singlePersonData.get_name()) {
                        MembersPhoneNumberString = MembersPhoneNumberString + "-" + singlePersonData.get_phoneNumber()

                        if (MembersNamesString != " ") {
                            MembersNamesString = MembersNamesString + "," + singlePersonData.get_name()
                        } else {
                            MembersNamesString = singlePersonData.get_name()
                        }

                    } else {
                        MembersPhoneNumberString = MembersPhoneNumberString + "-" + singleName

                        if (MembersNamesString != " ") {
                            MembersNamesString = MembersNamesString + "," + singleName
                        } else {
                            MembersNamesString = singleName.toString()
                        }

                        break
                    }

                    w_counter++
                }

                counter++
            }

            makeRequestToUpdateDocVisibilityApi(MembersNamesString!!, docIsVisibleToTextView, img_name, MembersPhoneNumberString!!)
        } else {
            Log.d("mess", "empty")
        }
    }

    fun handleVisibilityOfDocument_whenContactSelectedFromContactList(docIsVisibleToTextView: TextView, img_name: String, arrayOfMembersPhoneNumber: Array<out CharSequence>?, sizeOfList: Int) {
        makeRequestToUpdateDocVisibilityApi_whenContactSelected(docIsVisibleToTextView, img_name, arrayOfMembersPhoneNumber!![0].toString())
    }

    fun makeRequestToUpdateDocVisibilityApi(memberNamesString: String, docIsVisibleToTextView: TextView, img_name: String, MembersPhoneNumberString: String) {
        doAsync {
            val client: OkHttpClient = OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build()

            val urlBuilder = HttpUrl.parse(ServerConfig().updateDocVisibilityInHistory())!!.newBuilder()
            urlBuilder.addQueryParameter("imgName", img_name)
            urlBuilder.addQueryParameter("membersPhoneNumber", MembersPhoneNumberString)

            val url = urlBuilder.build().toString()

            val request = Request.Builder()
                    .url(url)
                    .build()

            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()

            uiThread {
                Log.d("mess", responce_string.toString())

                if (responce_string != "dataMissing") {
                    if (responce_string != "serverError") {
                        if (responce_string == "ok") {
                            showMessageAfterSignup(getString(R.string.visibility_updated), Color.GREEN)
                            docIsVisibleToTextView.text = memberNamesString
                        } else {
                            showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                        }
                    } else {
                        showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                    }
                } else {
                    showMessageAfterSignup(getString(R.string.something_goes_wrong), Color.RED)
                }
            }
        }
    }

    fun makeRequestToUpdateDocVisibilityApi_whenContactSelected(docIsVisibleToTextView: TextView, img_name: String, MemberPhoneNumber: String) {
        doAsync {
            val client: OkHttpClient = OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build()

            val urlBuilder = HttpUrl.parse(ServerConfig().updateDocVisibilityInHistory_whenContactSelected())!!.newBuilder()
            urlBuilder.addQueryParameter("imgName", img_name)
            urlBuilder.addQueryParameter("membersPhoneNumber", MemberPhoneNumber)

            val url = urlBuilder.build().toString()

            val request = Request.Builder()
                    .url(url)
                    .build()

            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()

            uiThread {
                Log.d("mess", responce_string.toString())

                if (responce_string != "dataMissing") {
                    if (responce_string != "serverError") {
                        if (responce_string == "ok") {
                            showMessageAfterSignup(getString(R.string.visibility_updated), Color.GREEN)
                            docIsVisibleToTextView.text = docIsVisibleToTextView.text.toString() + "," + MemberPhoneNumber
                        } else {
                            showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                        }
                    } else {
                        showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                    }
                } else {
                    showMessageAfterSignup(getString(R.string.something_goes_wrong), Color.RED)
                }
            }
        }
    }

    //contact chooser
    fun createContactChooser(docIsVisibleToTextView: TextView, img_name: String) {
        get_permissions_Read_Contacts(docIsVisibleToTextView, img_name)
    }

    //get permissions
    //Read Contacts
    fun get_permissions_Read_Contacts(docIsVisibleToTextView: TextView, img_name: String) {
        if (ContextCompat.checkSelfPermission(this@history, android.Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {

            MaterialDialog.Builder(this@history)
                    .title(getString(R.string.contact_permission))
                    .content(getString(R.string.read_contacts_permission_is_required))
                    .negativeText(getString(R.string.cancel))
                    .onNegative(object : MaterialDialog.SingleButtonCallback {
                        override fun onClick(dialog: MaterialDialog, which: DialogAction) {

                        }
                    })
                    .positiveText(getString(R.string.give_permission))
                    .onPositive(object : MaterialDialog.SingleButtonCallback {
                        override fun onClick(dialog: MaterialDialog, which: DialogAction) {
                            getPermissionsUsingDexter_readContacts(
                                    android.Manifest.permission.READ_CONTACTS,
                                    docIsVisibleToTextView,
                                    img_name
                            )
                        }
                    })
                    .show()

        } else {
            launchContactChooser(docIsVisibleToTextView, img_name)
        }

    }

    fun getPermissionsUsingDexter_readContacts(permissionString: String, docIsVisibleToTextView: TextView, img_name: String) {
        Dexter.withActivity(this)
                .withPermissions(
                        permissionString
                ).withListener(object : MultiplePermissionsListener {
                    override fun onPermissionRationaleShouldBeShown(permissions: MutableList<PermissionRequest>?, token: PermissionToken?) {

                    }

                    override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                        if (report!!.areAllPermissionsGranted() == true) {
                            launchContactChooser(docIsVisibleToTextView, img_name)

                            Log.d("mess", "permission given")
                        } else {
                            Log.d("mess", "permission not granted")
                        }
                    }
                })
                .check()
    }

    fun launchContactChooser(docIsVisibleToTextView: TextView, img_name: String) {
        forContactChoose_textView = docIsVisibleToTextView
        forContactChoose_imgName = img_name

        val intent: Intent = Intent(this, ContactPickerActivity::class.java)
                .putExtra(ContactPickerActivity.EXTRA_CONTACT_BADGE_TYPE, ContactPictureType.ROUND.name)
                .putExtra(ContactPickerActivity.EXTRA_SHOW_CHECK_ALL, false)
                .putExtra(ContactPickerActivity.EXTRA_CONTACT_DESCRIPTION, ContactDescription.ADDRESS.name)
                .putExtra(ContactPickerActivity.EXTRA_CONTACT_DESCRIPTION_TYPE, ContactsContract.CommonDataKinds.Email.TYPE_WORK)
                .putExtra(ContactPickerActivity.EXTRA_CONTACT_SORT_ORDER, ContactSortOrder.AUTOMATIC.name)
        startActivityForResult(intent, PICK_CONTACT_REQUEST)
    }

    //Activity Results
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        //handle contact choose result
        if (requestCode == PICK_CONTACT_REQUEST) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    @SuppressWarnings("unchecked")
                    val contacts: List<Contact> = (data.getSerializableExtra(ContactPickerActivity.RESULT_CONTACT_DATA)) as List<Contact>

                    if (contacts.size > 0) {
                        val contact = contacts[0]
                        val phoneNumber = (contact.getPhone(1).toString()).substring((contact.getPhone(1).toString()).length - 10)
                        val arrayOfPhone: Array<out CharSequence>? = arrayOf(phoneNumber)

                        handleVisibilityOfDocument_whenContactSelectedFromContactList(
                                forContactChoose_textView!!,
                                forContactChoose_imgName!!,
                                arrayOfPhone,
                                1
                        )

                        Log.d("mess", phoneNumber)
                    }
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    //onclick load more button
    fun onClickLoadMoreButton() {
        findViewById<Button>(R.id.loadMoreButton).setOnClickListener {
            if (sortApiRequestIsDoneOnce == 0) {
                makeHistoryApiRequest(imageId!!, familyId!!, phoneNumber!!)
            } else {
                makeRequestToSortApiRequest(fromday!!, frommonth!!, fromyear!!, today!!, tomonth!!, toyear!!, sortTerm!!, sortByFamilyMembersTerm!!)
            }

        }
    }

    //onclick from and to buttons
    fun onClickFromAndToButtonsAndSortButton() {
        findViewById<Button>(R.id.fromButton).setOnClickListener {
            createDatePickerDialogForFrom()
        }
        findViewById<Button>(R.id.toButton).setOnClickListener {
            if (fromday != "0" && frommonth != "0" && fromyear != "0") {
                createDatePickerDialogForTo()
            } else {
                showMessageAfterSignup(getString(R.string.first_enter_your_from_date), Color.RED)
            }
        }
        findViewById<Button>(R.id.sortButton).setOnClickListener {
            createSinglePickDialogForPickingSpecificCategory()
        }
    }

    fun createDatePickerDialogForFrom() {
        val now = Calendar.getInstance()
        val dpd = DatePickerDialog.newInstance(
                object : DatePickerDialog.OnDateSetListener {
                    override fun onDateSet(view: DatePickerDialog?, year_: Int, monthOfYear: Int, dayOfMonth: Int) {
                        fromday = dayOfMonth.toString()
                        if (fromday!![0].toString() == "0") {
                            fromday!!.replaceFirst("0", "")
                        }
                        frommonth = (monthOfYear + 1).toString()
                        if (frommonth!![0].toString() == "0") {
                            frommonth!!.replaceFirst("0", "")
                        }
                        fromyear = year_.toString()

                        loadDateIntPreViewForFrom(fromday + "/" + frommonth + "/" + fromyear)
                    }
                },
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        )
        dpd.show(fragmentManager, "Datepickerdialog")
    }

    fun loadDateIntPreViewForFrom(date: String) {
        findViewById<Button>(R.id.fromButton).text = date
    }

    fun createDatePickerDialogForTo() {
        val now = Calendar.getInstance()
        val dpd = DatePickerDialog.newInstance(
                object : DatePickerDialog.OnDateSetListener {
                    override fun onDateSet(view: DatePickerDialog?, year_: Int, monthOfYear: Int, dayOfMonth: Int) {
                        today = dayOfMonth.toString()
                        if (today!![0].toString() == "0") {
                            today!!.replaceFirst("0", "")
                        }
                        tomonth = (monthOfYear + 1).toString()
                        if (tomonth!![0].toString() == "0") {
                            tomonth!!.replaceFirst("0", "")
                        }
                        toyear = year_.toString()

                        loadDateIntPreViewForTo(today + "/" + tomonth + "/" + toyear)
                    }
                },
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        )
        dpd.show(fragmentManager, "Datepickerdialog")
    }

    fun loadDateIntPreViewForTo(date: String) {
        imageId = 0

        clearHistoryRecyclerView()

        makeRequestToSortApiRequest(fromday!!, frommonth!!, fromyear!!, today!!, tomonth!!, toyear!!, sortTerm!!, sortByFamilyMembersTerm!!)

        findViewById<Button>(R.id.toButton).text = date
    }

    fun createSinglePickDialogForPickingSpecificCategory() {
        MaterialDialog.Builder(this)
                .title("Sort By")
                .items(R.array.specificSortCategoryItems)
                .itemsCallbackSingleChoice(-1, object : MaterialDialog.ListCallbackSingleChoice {
                    override fun onSelection(dialog: MaterialDialog?, itemView: View?, which: Int, text: CharSequence?): Boolean {
                        if (text == "Sort By Custom Category") {
                            createInputDialogForCustomCategory()
                        } else {
                            sortTerm = text.toString()
                            imageId = 0

                            clearHistoryRecyclerView()

                            makeRequestToSortApiRequest(fromday!!, frommonth!!, fromyear!!, today!!, tomonth!!, toyear!!, sortTerm!!, sortByFamilyMembersTerm!!)
                        }
                        return true
                    }
                })
                .positiveText(R.string.sort)
                .show()
    }

    fun createInputDialogForCustomCategory() {
        MaterialDialog.Builder(this)
                .title(getString(R.string.enter_custom_category))
                .content(getString(R.string.enter_your_custom_category))
                .inputType(InputType.TYPE_CLASS_TEXT)
                .positiveText(R.string.sort)
                .input(getString(R.string.enter_custom_category), "", object : MaterialDialog.InputCallback {
                    override fun onInput(dialog: MaterialDialog, input: CharSequence?) {
                        if (input.toString().length != 0) {
                            sortTerm = input.toString()
                            imageId = 0

                            clearHistoryRecyclerView()

                            makeRequestToSortApiRequest(fromday!!, frommonth!!, fromyear!!, today!!, tomonth!!, toyear!!, sortTerm!!, sortByFamilyMembersTerm!!)

                        } else {
                            showMessageAfterSignup(getString(R.string.custom_category_cannot_be_empty), Color.RED)
                        }
                    }
                }).show()
    }

    //sort api
    fun makeRequestToSortApiRequest(fromday: String, frommonth: String, fromyear: String, today: String, tomonth: String, toyear: String, sortTerm: String, sortByFamilyMemberTerm: String) {
        sortApiRequestIsDoneOnce = 1

        doAsync {
            val client: OkHttpClient = OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build()

            val urlBuilder = HttpUrl.parse(ServerConfig().sortHistoryApi())!!.newBuilder()
            urlBuilder.addQueryParameter("imageId", imageId!!.toString())
            urlBuilder.addQueryParameter("familyId", familyId.toString())
            urlBuilder.addQueryParameter("phoneNumber", phoneNumber.toString())
            urlBuilder.addQueryParameter("fromDay", fromday)
            urlBuilder.addQueryParameter("fromMonth", frommonth)
            urlBuilder.addQueryParameter("fromYear", fromyear)
            urlBuilder.addQueryParameter("toDay", today)
            urlBuilder.addQueryParameter("toMonth", tomonth)
            urlBuilder.addQueryParameter("toYear", toyear)
            urlBuilder.addQueryParameter("sortTerm", sortTerm)
            urlBuilder.addQueryParameter("sortByFamilyMemberTerm", sortByFamilyMemberTerm)

            val url = urlBuilder.build().toString()

            val request = Request.Builder()
                    .url(url)
                    .build()

            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()

            uiThread {
                Log.d("mess", responce_string.toString())
                if (responce_string != "noImageUploadedYet" && responce_string != "noMoreImages") {
                    if (familyId.toString() == "0") {
                        //individial account
                        processIndividualHistoryResponse(responce_string)
                    } else {
                        //family account
                        processFamilyHistoryResponse(responce_string)
                    }
                } else {
                    if (responce_string == "noImageUploadedYet") {
                        findViewById<Button>(R.id.loadMoreButton).visibility = View.GONE
                    }
                    if (responce_string == "noMoreImages") {
                        findViewById<Button>(R.id.loadMoreButton).visibility = View.GONE
                    }
                }
            }
        }
    }

    //clear history recyclerview
    fun clearHistoryRecyclerView() {
        result_list_for_history.clear()
        history_holder!!.notifyDataSetChanged()
        historyRecyclerView!!.adapter = null
        attach_history_adapter_to_recyclerview()
    }


    //create sort by family members tool bar
    fun loadSortByFamilymembers_IfAdminIsLoggedIn() {
        if (familyId != "0") {
            val size = result_list_for_familyMembers.size
            var counter = 0
            while (counter < size) {
                val currentObject = result_list_for_familyMembers.get(counter)

                load(this@history).loadSortByFamilyMembersToolBar(
                        currentObject.get_name(),
                        currentObject.get_phoneNumber()
                )

                counter++
            }
        } else {
            LoadLayoutForIndividualUser()
        }
    }

    inner class load(inner_context: Context) {
        private var context: Context = inner_context

        fun loadSortByFamilyMembersToolBar(familyMemberName: String, familyMemberPhoneNumber: String) {
            val generator = ColorGenerator.MATERIAL // or use DEFAULT
            val color = generator.randomColor
            var t = TextDrawable.builder()
                    .buildRoundRect(familyMemberName[0].toString(), color, 40) // radius in px
            val i = ImageView(context)
            i.setImageDrawable(t)
            i.layoutParams = android.view.ViewGroup.LayoutParams(90, 90)
            i.maxHeight = 20
            i.maxWidth = 20
            val inLay = findViewById<LinearLayout>(R.id.sortByFamilyMembersScrollViewLayout) as LinearLayout

            //onclick listener
            i.setOnClickListener {
                showMessageAfterSignup(familyMemberName.toString() + getString(R.string.is_selected), ContextCompat.getColor(this@history, R.color.green))

                imageId = 0
                clearHistoryRecyclerView()

                if (clickedItemCount == null && clickedFamilyMemberName == null) {
                    clickedItemCount = inLay.indexOfChild(i)
                    clickedFamilyMemberName = familyMemberName
                } else {
                    val t2 = TextDrawable.builder()
                            .buildRoundRect(clickedFamilyMemberName!![0].toString(), color, 40) // radius in px
                    (inLay.getChildAt(clickedItemCount!!) as ImageView).setImageDrawable(t2)
                    clickedItemCount = inLay.indexOfChild(i)
                    clickedFamilyMemberName = familyMemberName
                }

                t = TextDrawable.builder()
                        .beginConfig()
                        .withBorder(25) /* thickness in px */
                        .bold()
                        .endConfig()
                        .buildRoundRect(familyMemberName[0].toString(), color, 40) // radius in px
                i.setImageDrawable(t)

                sortByFamilyMembersTerm = familyMemberPhoneNumber
                makeRequestToSortApiRequest(fromday!!, frommonth!!, fromyear!!, today!!, tomonth!!, toyear!!, sortTerm!!, sortByFamilyMembersTerm!!)
            }

            inLay.addView(i)
        }
    }

    //show user the link of unverified docs which are uploaded by family admin
    fun showUserLinkTowardsUnverifiedDoc() {
        findViewById<Button>(R.id.verify).visibility = View.VISIBLE
        findViewById<TextView>(R.id.documentToBeVerifiedNote).visibility = View.VISIBLE
    }

    fun makeVerifyButtonWorks() {
        findViewById<Button>(R.id.verify).setOnClickListener {
            val i = Intent(this@history, documentToBeVerified::class.java)
            startActivity(i)
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        }
    }

    fun isThereAnyDocRemaningToVerify() {
        doAsync {
            val client: OkHttpClient = OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build()

            val urlBuilder = HttpUrl.parse(ServerConfig().IsThereAnyDocRemainingToVerify())!!.newBuilder()
            urlBuilder.addQueryParameter("phoneNumber", phoneNumber)

            val url = urlBuilder.build().toString()

            val request = Request.Builder()
                    .url(url)
                    .build()

            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()

            uiThread {
                if (responce_string == "YES") {
                    showUserLinkTowardsUnverifiedDoc()
                }
            }
        }
    }

    //load layout for individual user
    fun LoadLayoutForIndividualUser() {
        findViewById<Toolbar>(R.id.sortByFamilyMembersToolBar).title = getString(R.string.retrieve_documents)
        findViewById<android.widget.HorizontalScrollView>(R.id.sortByFamilyMembersScrollView).visibility = View.GONE
    }
}
