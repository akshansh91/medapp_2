package com.medapp.medapp;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.medapp.medapp.config.ServerConfig;
import com.medapp.medapp.utils.left_drawer;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class showPicFull extends AppCompatActivity {

    SimpleDraweeView view;
    String imgName, docSharedBy, type, category, specific_category, description;
    Button save, notShow;
    SharedPreferences loginInfo;

    String responseString;

    String familyId, phoneNumber, flagFamilyOrIndividual;
    String uploadDay, uploadMonth, uploadYear;

    OkHttpClient client;
    HttpUrl.Builder urlBuilder;

    Toolbar toolbar;

    Response response;
    Request request;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_pic_full);

        loginInfo = getSharedPreferences("loginInfo", Context.MODE_PRIVATE);
        familyId = loginInfo.getString("familyId", null);
        phoneNumber = loginInfo.getString("phoneNumber", "");
        flagFamilyOrIndividual = loginInfo.getString("familyOrIndividual", "");

        view = findViewById(R.id.pic);
        save = findViewById(R.id.save);
        notShow = findViewById(R.id.dontShow);
        toolbar = findViewById(R.id.toolBar_showFullPic);

        new left_drawer(this, showPicFull.this, toolbar).createNavigationDrawer();

        Intent intent = getIntent();
        imgName = intent.getStringExtra("ImageName");
        docSharedBy = intent.getStringExtra("docSharedBy");
        description = intent.getStringExtra("description");
        type = intent.getStringExtra("type");
        category = intent.getStringExtra("category");
        specific_category = intent.getStringExtra("specific_category");

        if (type.isEmpty()) {
            type = "";
        }
        if (specific_category.isEmpty()) {
            specific_category = "";
        }

        Calendar c = Calendar.getInstance();
        SimpleDateFormat df1 = new SimpleDateFormat("dd", Locale.US);
        SimpleDateFormat df2 = new SimpleDateFormat("MM", Locale.US);
        SimpleDateFormat df3 = new SimpleDateFormat("yyyy", Locale.US);

        uploadDay = df1.format(c.getTime());

        if (uploadDay.equals("0")) {
            uploadDay = (uploadDay.replaceFirst("0", ""));
        }

        uploadMonth = df2.format(c.getTime());

        if (uploadMonth.equals("0")) {
            uploadMonth = (uploadMonth.replaceFirst("0", ""));
        }

        uploadYear = df3.format(c.getTime());


        Uri uri = Uri.parse(new ServerConfig()
                .getRootAddress() + "upload/" + imgName + "?notCache=" + imgName);

        view.setImageURI(uri);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SavePic savePic = new SavePic();
                savePic.execute();
            }
        });

        notShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Remove remove = new Remove();
                remove.execute(imgName, docSharedBy);
            }
        });
    }

    @SuppressLint("StaticFieldLeak")
    private class Remove extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... strings) {
            String img = strings[0];
            String docSharedBy = strings[1];

            client = new OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS).build();

            urlBuilder = Objects.requireNonNull(HttpUrl.parse(new ServerConfig().removeSharedPic())).newBuilder();
            urlBuilder.addQueryParameter("imgName", img);
            urlBuilder.addQueryParameter("by", docSharedBy);

            String url = urlBuilder.build().toString();

            request = new Request.Builder().url(url).build();

            try {
                response = client.newCall(request).execute();
                if (response.body() != null) {
                    responseString = response.body().string();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return responseString;
        }

        @Override
        protected void onPostExecute(String s) {
            if (s.equals("Success")) {
                Toast.makeText(showPicFull.this, "Shared document removed", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(showPicFull.this, Main2Activity.class);
                startActivity(intent);
            } else {
                Toast.makeText(showPicFull.this, R.string.error, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class SavePic extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... strings) {

            client = new OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build();

            RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM)
                    .addFormDataPart("docName", imgName)
                    .addFormDataPart("phoneNumber", phoneNumber)
                    .addFormDataPart("familyId", familyId)
                    .addFormDataPart("familyMemberPhoneNumber", phoneNumber)
                    .addFormDataPart("description", description)
                    .addFormDataPart("typeOfDocument", type)
                    .addFormDataPart("categoryOfDocument", category)
                    .addFormDataPart("specificCategory", specific_category)
                    .addFormDataPart("uploadDay", uploadDay)
                    .addFormDataPart("uploadMonth", uploadMonth)
                    .addFormDataPart("uploadYear", uploadYear)
                    .addFormDataPart("memberFlag", "1")
                    .build();

           /* urlBuilder = Objects.requireNonNull(HttpUrl.parse(new ServerConfig().saveSharedPic())).newBuilder();

            urlBuilder.addQueryParameter("docName", imgName);
            urlBuilder.addQueryParameter("phoneNumber", phoneNumber);
            urlBuilder.addQueryParameter("familyId", familyId);

            //urlBuilder.addQueryParameter("familyMemberName", );
            //we need to get the admin's name too in case of family account. . .

            urlBuilder.addQueryParameter("familyMemberPhoneNumber", phoneNumber);
            //family member phone number will be 0 in case of individual . . .

            urlBuilder.addQueryParameter("description", description);
            urlBuilder.addQueryParameter("typeOfDocument", type);
            urlBuilder.addQueryParameter("categoryOfDocument", category);
            urlBuilder.addQueryParameter("specificCategory", specific_category);
            urlBuilder.addQueryParameter("uploadDay", uploadDay);
            urlBuilder.addQueryParameter("uploadMonth", uploadMonth);
            urlBuilder.addQueryParameter("uploadYear", uploadYear);
            urlBuilder.addQueryParameter("memberFlag", "1");
*/
            //          String url = urlBuilder.build().toString();

            //        Log.d("showPicFull", "save....URL: " + url);

            /*val request:
            Request = new Request.Builder().url(new ServerConfig().getStorageServerAddress())
                    .post(requestBody).build()*/

            request = new Request.Builder().url(new ServerConfig().saveSharedPic()).post(body).build();

            try {
                response = client.newCall(request).execute();
                if (response.body() != null) {
                    responseString = response.body().string();
                    Log.d("showPicFull", "save....response String: " + responseString);
                }
            } catch (Exception e) {
                Log.d("showPicFull", "save....Exception: " + e);
            }

            return responseString;
        }

        @Override
        protected void onPostExecute(String s) {
            if (s.equals("Success")) {
                Toast.makeText(showPicFull.this, R.string.doc_saved_successfully, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(showPicFull.this, Main2Activity.class);
                startActivity(intent);
            } else {
                Toast.makeText(showPicFull.this, R.string.error_while_saving, Toast.LENGTH_SHORT).show();
            }
        }
    }
}
