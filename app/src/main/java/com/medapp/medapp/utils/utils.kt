package com.medapp.medapp.utils

import android.content.Context
import android.graphics.Color
import android.support.v4.content.ContextCompat
import com.makeramen.roundedimageview.RoundedTransformationBuilder
import com.medapp.medapp.R
import com.squareup.picasso.Transformation

class utils {

    fun getCircularTransformation(context: Context): Transformation {
        val transformation = RoundedTransformationBuilder()
                .borderColor(ContextCompat.getColor(context, R.color.colorAccent))
                .borderWidthDp(0.3.toFloat())
                .cornerRadiusDp(300.toFloat())
                .oval(false)
                .build()

        return transformation
    }
}