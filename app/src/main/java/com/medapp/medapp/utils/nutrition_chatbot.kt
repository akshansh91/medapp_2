package com.medapp.medapp.utils

import android.content.Context

class nutrition_chatbot(type: String, context: Context) {
    val inner_type = type
    val inner_context = context

    //start chat
    fun init(): ArrayList<String> {
        return getStartMessages(setPreferences())
    }

    fun setPreferences(): Int {
        val nutritionChatbot = inner_context.getSharedPreferences("nutritionChatbot", Context.MODE_PRIVATE)
        var previous_value = "0"
        if (nutritionChatbot.contains(inner_type)) {
            previous_value = nutritionChatbot.getString(inner_type, null)
        }

        val editor = nutritionChatbot.edit()
        if (previous_value.toInt() < 2) {
            editor.putString(inner_type, (previous_value.toInt() + 1).toString())
        } else {
            editor.putString(inner_type, "0")
        }

        editor.commit()

        return previous_value.toInt()
    }

    fun getStartMessages(msgCode: Int): ArrayList<String> {
        var arrayOfMsg: ArrayList<String>? = null
        if (inner_type == "B") {
            arrayOfMsg = B_start_msgBlock(msgCode)
        }
        if (inner_type == "L") {
            arrayOfMsg = L_start_msgBlock(msgCode)
        }
        if (inner_type == "D") {
            arrayOfMsg = D_start_msgBlock(msgCode)
        }
        if (inner_type == "NOTYPE") {
            arrayOfMsg = NOTYPE_start_msgBlock(msgCode)
        }

        return arrayOfMsg!!
    }

    fun B_start_msgBlock(msgCode: Int): ArrayList<String> {
        val msgArray: ArrayList<String>? = arrayListOf<String>()
        if (msgCode == 0) {
            msgArray!!.add("hi")
            msgArray.add("Hello")
            msgArray.add("Bye")
        }
        if (msgCode == 1) {
            msgArray!!.add("hi")
            msgArray.add("Hello")
            msgArray.add("Bye")
        }
        if (msgCode == 2) {
            msgArray!!.add("hi")
            msgArray.add("Hello")
            msgArray.add("Bye")
        }

        return msgArray!!
    }

    fun L_start_msgBlock(msgCode: Int): ArrayList<String> {
        val msgArray: ArrayList<String>? = arrayListOf<String>()
        if (msgCode == 0) {
            msgArray!!.add("hi")
            msgArray.add("Hello")
            msgArray.add("Bye")
        }
        if (msgCode == 1) {
            msgArray!!.add("hi")
            msgArray.add("Hello")
            msgArray.add("Bye")
        }
        if (msgCode == 2) {
            msgArray!!.add("hi")
            msgArray.add("Hello")
            msgArray.add("Bye")
        }

        return msgArray!!
    }

    fun D_start_msgBlock(msgCode: Int): ArrayList<String> {
        val msgArray: ArrayList<String>? = arrayListOf<String>()
        if (msgCode == 0) {
            msgArray!!.add("hi")
            msgArray.add("Hello")
            msgArray.add("Bye")
        }
        if (msgCode == 1) {
            msgArray!!.add("hi")
            msgArray.add("Hello")
            msgArray.add("Bye")
        }
        if (msgCode == 2) {
            msgArray!!.add("hi")
            msgArray.add("Hello")
            msgArray.add("Bye")
        }

        return msgArray!!
    }

    fun NOTYPE_start_msgBlock(msgCode: Int): ArrayList<String> {
        val msgArray: ArrayList<String>? = arrayListOf<String>()
        if (msgCode == 0) {
            msgArray!!.add("hi")
            msgArray.add("Hello")
            msgArray.add("Bye")
        }
        if (msgCode == 1) {
            msgArray!!.add("hi")
            msgArray.add("Hello")
            msgArray.add("Bye")
        }
        if (msgCode == 2) {
            msgArray!!.add("hi")
            msgArray.add("Hello")
            msgArray.add("Bye")
        }

        return msgArray!!
    }

    //end chat
    fun end(): ArrayList<String> {
        return getEndMessages(getPreferences())
    }

    fun getPreferences(): Int {
        val nutritionChatbot = inner_context.getSharedPreferences("nutritionChatbot", Context.MODE_PRIVATE)
        val previous_value = nutritionChatbot.getString(inner_type, null)

        return previous_value.toInt()
    }

    fun getEndMessages(msgCode: Int): ArrayList<String> {
        var arrayOfMsg: ArrayList<String>? = null
        if (inner_type == "B") {
            arrayOfMsg = B_END_msgBlock(msgCode)
        }
        if (inner_type == "L") {
            arrayOfMsg = L_END_msgBlock(msgCode)
        }
        if (inner_type == "D") {
            arrayOfMsg = D_END_msgBlock(msgCode)
        }
        if (inner_type == "NOTYPE") {
            arrayOfMsg = NOTYPE_END_msgBlock(msgCode)
        }

        return arrayOfMsg!!
    }

    fun B_END_msgBlock(msgCode: Int): ArrayList<String> {
        val msgArray: ArrayList<String>? = arrayListOf<String>()
        if (msgCode == 0) {
            msgArray!!.add("hi")
            msgArray.add("Hello")
            msgArray.add("Bye")
        }
        if (msgCode == 1) {
            msgArray!!.add("hi")
            msgArray.add("Hello")
            msgArray.add("Bye")
        }
        if (msgCode == 2) {
            msgArray!!.add("hi")
            msgArray.add("Hello")
            msgArray.add("Bye")
        }

        return msgArray!!
    }

    fun L_END_msgBlock(msgCode: Int): ArrayList<String> {
        val msgArray: ArrayList<String>? = arrayListOf<String>()
        if (msgCode == 0) {
            msgArray!!.add("hi")
            msgArray.add("Hello")
            msgArray.add("Bye")
        }
        if (msgCode == 1) {
            msgArray!!.add("hi")
            msgArray.add("Hello")
            msgArray.add("Bye")
        }
        if (msgCode == 2) {
            msgArray!!.add("hi")
            msgArray.add("Hello")
            msgArray.add("Bye")
        }

        return msgArray!!
    }

    fun D_END_msgBlock(msgCode: Int): ArrayList<String> {
        val msgArray: ArrayList<String>? = arrayListOf<String>()
        if (msgCode == 0) {
            msgArray!!.add("hi")
            msgArray.add("Hello")
            msgArray.add("Bye")
        }
        if (msgCode == 1) {
            msgArray!!.add("hi")
            msgArray.add("Hello")
            msgArray.add("Bye")
        }
        if (msgCode == 2) {
            msgArray!!.add("hi")
            msgArray.add("Hello")
            msgArray.add("Bye")
        }

        return msgArray!!
    }

    fun NOTYPE_END_msgBlock(msgCode: Int): ArrayList<String> {
        val msgArray: ArrayList<String>? = arrayListOf<String>()
        if (msgCode == 0) {
            msgArray!!.add("hi")
            msgArray.add("Hello")
            msgArray.add("Bye")
        }
        if (msgCode == 1) {
            msgArray!!.add("hi")
            msgArray.add("Hello")
            msgArray.add("Bye")
        }
        if (msgCode == 2) {
            msgArray!!.add("hi")
            msgArray.add("Hello")
            msgArray.add("Bye")
        }

        return msgArray!!
    }

}