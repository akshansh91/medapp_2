package com.medapp.medapp.utils

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.support.v4.content.ContextCompat
import android.util.Log
import com.medapp.medapp.R
import com.medapp.medapp.config.ServerConfig
import com.muddzdev.styleabletoastlibrary.StyleableToast
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.util.concurrent.TimeUnit

class individual_family_login(con: Context) {
    private var context: Context = con
    private var familyId: String? = null
    private var phoneNumber: String? = null
    private var password: String? = null

    //individual
    fun showMessageAfterLogin(msg_text: String, color: Int) {
        /* val st = StyleableToast.Builder(context)
                 .text(msg_text)
                 .textColor(Color.WHITE)
                 .backgroundColor(color)
                 .build()
         st.show()
         */
        Log.d("mess", msg_text.toString())
    }

    fun makeLoginApiRequest(phoneNu: String, password: String) {
        doAsync {
            val client = OkHttpClient()

            val urlBuilder = HttpUrl.parse(ServerConfig().getLoginApi())!!.newBuilder()
            urlBuilder.addQueryParameter("phoneNumber", phoneNu)
            urlBuilder.addQueryParameter("password", password)

            val url = urlBuilder.build().toString()

            val request = Request.Builder()
                    .url(url)
                    .build()

            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()

            uiThread {
                if (responce_string == "login") {
                    showMessageAfterLogin("Successfully Logged In", ContextCompat.getColor(context, R.color.green))
                    storeInPersistentData(phoneNu, password)
                } else {
                    if (responce_string == "dataMissing") {
                        showMessageAfterLogin(R.string.something_goes_wrong.toString(), Color.RED)
                    }
                    if (responce_string == "noSuchAccount") {
                        showMessageAfterLogin(R.string.did_you_have_dinner.toString(), Color.RED)
                    }
                    if (responce_string == "serverError") {
                        showMessageAfterLogin(R.string.server_error.toString(), Color.RED)
                    }
                    if (responce_string == "invalidPhoneNumber") {
                        showMessageAfterLogin(R.string.invalid_phone_number.toString(), Color.RED)
                    }
                    if (responce_string != "login"
                            && responce_string != "dataMissing"
                            && responce_string != "invalidPhoneNumber"
                            && responce_string != "noSuchAccount"
                            && responce_string != "serverError") {
                        showMessageAfterLogin(R.string.server_error.toString(), Color.RED)
                    }

                    clearLoginInfo()
                    (context as Activity).finish()
                }
            }
        }
    }

    fun storeInPersistentData(phoneNu: String, password: String) {
        val loginInfo = context.getSharedPreferences("loginInfo", Context.MODE_PRIVATE)
        val editor = loginInfo.edit()
        editor.putString("password", password)
        editor.putString("phoneNumber", phoneNu)
        editor.putString("familyId", "0")
        editor.putString("familyOrIndividual", "i")
        editor.commit()
    }

    //family
    fun makeFamilyLoginApiRequest(l_phoneNumber: String, l_password: String) {
        doAsync {
            val client: OkHttpClient = OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build()

            val urlBuilder = HttpUrl.parse(ServerConfig().getFamilyLoginApi())!!.newBuilder()
            urlBuilder.addQueryParameter("l_phoneNumber", l_phoneNumber)
            urlBuilder.addQueryParameter("l_password", l_password)

            val url = urlBuilder.build().toString()

            val request = Request.Builder()
                    .url(url)
                    .build()

            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()

            uiThread {
                Log.d("mess", responce_string.toString())

                if (responce_string == "login") {
                    showErrorMsg(R.string.successfully_logged_in.toString(), ContextCompat.getColor(context, R.color.green))
                    phoneNumber = l_phoneNumber
                    password = l_password
                    getFamilyId()
                } else {
                    if (responce_string == "dataMissing") {
                        showErrorMsg(R.string.something_goes_wrong.toString(), Color.RED)
                    }
                    if (responce_string == "noSuchAccount") {
                        showErrorMsg(R.string.no_such_account_exist.toString(), Color.RED)
                    }
                    if (responce_string == "serverError") {
                        showErrorMsg(R.string.server_error.toString(), Color.RED)
                    }
                    if (responce_string == "invalidPhoneNumber") {
                        showErrorMsg(R.string.invalid_phone_number.toString(), Color.RED)
                    }
                    if (responce_string != "login"
                            && responce_string != "dataMissing"
                            && responce_string != "invalidPhoneNumber"
                            && responce_string != "noSuchAccount"
                            && responce_string != "serverError") {
                        showErrorMsg(R.string.server_error.toString(), Color.RED)
                    }

                    clearLoginInfo()
                    (context as Activity).finish()
                }
            }
        }
    }

    fun storeInPersistentData() {
        val loginInfo = context.getSharedPreferences("loginInfo", Context.MODE_PRIVATE)
        val editor = loginInfo.edit()
        editor.putString("password", password)
        editor.putString("phoneNumber", phoneNumber)
        editor.putString("familyId", familyId!!)
        editor.putString("familyOrIndividual", "f")
        editor.commit()
    }

    fun getFamilyId() {
        doAsync {
            val client: OkHttpClient = OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build()

            val urlBuilder = HttpUrl.parse(ServerConfig().getFamilyIdApi())!!.newBuilder()
            urlBuilder.addQueryParameter("phoneNumber", phoneNumber)

            val url = urlBuilder.build().toString()

            val request = Request.Builder()
                    .url(url)
                    .build()

            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()

            uiThread {
                Log.d("mess", responce_string.toString())

                familyId = responce_string
                storeInPersistentData()
            }
        }
    }

    fun showErrorMsg(msg_text: String, color: Int) {
        val st = StyleableToast.Builder(context)
                .text(msg_text)
                .textColor(Color.WHITE)
                .backgroundColor(color)
                .build()
        st.show()
    }

    //both
    fun clearLoginInfo() {
        val loginInfo = context.applicationContext.getSharedPreferences("loginInfo", Context.MODE_PRIVATE)
        loginInfo.edit().clear().commit()
    }

}