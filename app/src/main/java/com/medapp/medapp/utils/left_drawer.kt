package com.medapp.medapp.utils

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.app.SharedElementCallback
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.res.Configuration
import android.content.res.Resources
import android.graphics.Bitmap
import android.net.Uri
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.widget.Toolbar
import android.util.DisplayMetrics
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.facebook.drawee.backends.pipeline.Fresco
import com.google.android.gms.flags.impl.SharedPreferencesFactory.getSharedPreferences
import com.medapp.medapp.*
import com.medapp.medapp.config.ServerConfig
import com.medapp.medapp.loginSignup.individualOrFamily
import com.mikepenz.materialdrawer.AccountHeader
import com.mikepenz.materialdrawer.AccountHeaderBuilder
import com.mikepenz.materialdrawer.Drawer
import com.mikepenz.materialdrawer.DrawerBuilder
import com.mikepenz.materialdrawer.model.ProfileDrawerItem
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem
import com.mikepenz.materialdrawer.model.interfaces.IProfile
import com.squareup.picasso.Picasso
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.util.*
import java.util.concurrent.TimeUnit

class left_drawer(t_context: Context, t_activity: Activity, t_toolbar: Toolbar) {
    private var context: Context = t_context
    private var context_activity = t_activity
    private var phoneNumber: String? = null
    private var familyId: String? = null
    private var drawerResult: Drawer? = null
    private var myToolBar: Toolbar = t_toolbar
    private var p_d_i: ProfileDrawerItem? = null
    private var accountView: AccountHeader? = null
    private var english: Button? = null
    private var hindi: Button? = null
    var alertD: AlertDialog = AlertDialog.Builder(context).create()
    private var titleHindi: TextView? = null
    private var titleEnglish: TextView? = null

    fun createNavigationDrawer() {

        Fresco.initialize(context)
        //get phone number
        val loginInfo = context.getSharedPreferences("loginInfo", Context.MODE_PRIVATE)
        phoneNumber = loginInfo.getString("phoneNumber", "")
        familyId = loginInfo.getString("familyId", "")

        var currentLocale: Locale = context_activity.resources.configuration.locale
        val language = context_activity.getSharedPreferences("lang", Context.MODE_PRIVATE)

        var layoutInflater: LayoutInflater = LayoutInflater.from(context)
        var promptView: View = layoutInflater.inflate(R.layout.custom_lang_change_dialog_2, null)

        hindi = promptView.findViewById(R.id.hindi)
        english = promptView.findViewById(R.id.english)


        hindi!!.setOnClickListener {
            Toast.makeText(context, "Change to Hindi", Toast.LENGTH_SHORT).show()
            var myLocale = Locale("hi")
            var res: Resources = context_activity.resources
            var dm: DisplayMetrics = res.displayMetrics
            var conf: Configuration = res.configuration
            conf.locale = myLocale
            res.updateConfiguration(conf, dm)

            val editor = language.edit()
            editor.putString("language", "hi")
            editor.apply()
            context_activity.recreate()
        }

        english!!.setOnClickListener {
            Toast.makeText(context, "Change to English", Toast.LENGTH_SHORT).show()
            var myLocale = Locale("en")
            var res: Resources = context_activity.resources
            var dm: DisplayMetrics = res.displayMetrics
            var conf: Configuration = res.configuration
            conf.locale = myLocale
            res.updateConfiguration(conf, dm)
            context_activity.recreate()
            val editor = language.edit()
            editor.putString("language", "en")
            editor.apply()
        }

        alertD.setView(promptView)

        //account header
        //TODO: ADD Proper name , email and profile picture in accountview
        p_d_i = ProfileDrawerItem().withName("")
                .withEmail(phoneNumber).withIcon(ContextCompat.getDrawable(context, R.drawable.icon_man))

        accountView = AccountHeaderBuilder()
                .withActivity(context_activity)
                .withHeaderBackground(R.drawable.account_header_background_2)
                .addProfiles(
                        p_d_i
                )
                .withOnAccountHeaderListener(object : AccountHeader.OnAccountHeaderListener {
                    override fun onProfileChanged(view: View?, profile: IProfile<*>?, current: Boolean): Boolean {
                        openProfileChooser()
                        return true
                    }
                })
                .build()


        //if you want to update the items at a later time it is recommended to keep it in a variable
        val item1 = SecondaryDrawerItem().withIdentifier(1).withName(context.getString(R.string.log_out)).withIcon(R.drawable.ic_power_settings_new_black_24dp)
        //Showing the retrieve
        //val item2 = SecondaryDrawerItem().withIdentifier(2).withName(context.getString(R.string.retrieve)).withIcon(R.drawable.history)
        val item2 = SecondaryDrawerItem().withIdentifier(2).withName(context.getString(R.string.myReports)).withIcon(R.drawable.history)
        val item3 = SecondaryDrawerItem().withIdentifier(3).withName(context.getString(R.string.about)).withIcon(R.drawable.about)
        var myProfileHeading = ""
        if (familyId != "0") {
            myProfileHeading = context.getString(R.string.family_profile)
        } else {
            myProfileHeading = context.getString(R.string.my_profile)
        }
        val item4 = SecondaryDrawerItem().withIdentifier(4).withName(myProfileHeading).withIcon(R.drawable.ic_perm_identity_black_24dp)
        val item5 = SecondaryDrawerItem().withIdentifier(5).withName(context.getString(R.string.family_tree)).withIcon(R.drawable.family_tree_icon)
        //val item6 = SecondaryDrawerItem().withIdentifier(6).withName("Health Insurance").withIcon(R.drawable.health_insurance_icon)
        //val item7 = SecondaryDrawerItem().withIdentifier(7).withName("Health Goals").withIcon(R.drawable.health_goals_icon)

        //testing this item ...
        val item8 = SecondaryDrawerItem().withIdentifier(8).withName("Health Tracker").withIcon(R.drawable.health_tracker_icon)

        //val item9 = SecondaryDrawerItem().withIdentifier(9).withName("Habits").withIcon(R.drawable.habits_icon)
        val item10 = SecondaryDrawerItem().withIdentifier(10).withName(context.getString(R.string.documents_to_be_verified)).withIcon(R.drawable.document_to_be_verified_icon)
        //val item11 = SecondaryDrawerItem().withIdentifier(11).withName("Nutrition").withIcon(R.drawable.nutrition_popup_icon)
        val item12 = SecondaryDrawerItem().withIdentifier(12).withName(context.getString(R.string.home)).withIcon(R.drawable.home_icon)
        val item13 = SecondaryDrawerItem().withIdentifier(13).withName(context.getString(R.string.shared_with_me)).withIcon(R.drawable.sharedhand)
        val item14 = SecondaryDrawerItem().withIdentifier(14).withName(context.getString(R.string.scan_my_qr_code)).withIcon(R.drawable.qr_code)
        val item15 = SecondaryDrawerItem().withIdentifier(15).withName(context.getString(R.string.change_language)).withIcon(R.drawable.swap_language)

        //create the drawer and remember the `Drawer` result object
        if (familyId != "0") {
            drawerResult = DrawerBuilder()
                    .withActivity(context_activity)
                    .withTranslucentStatusBar(true)
                    .withActionBarDrawerToggle(true)
                    .withToolbar(myToolBar)
                    .withAccountHeader(accountView!!)
                    .addDrawerItems(
                            item12, // Home
                            item4, // my profile
                            item5, //family tree
                            item10, // documents to be verified
                            item2, // retrieve
                            item8, //health trackers
                            //item6, //health insurance
                            //item7, //health goals
                            //item11, //nutrition popup
                            item14,//scan my qr code
                            item13,//shared with me
                            //item9, //habits
                            item3, //about
                            item15,//language change
                            item1 //log out
                    )
                    .withOnDrawerItemClickListener(object : Drawer.OnDrawerItemClickListener {
                        override fun onItemClick(view: View, position: Int, drawerItem: IDrawerItem<*, *>): Boolean {
                            if (drawerItem.identifier.toInt() == 1) {
                                makeUserLogOut()
                            }
                            if (drawerItem.identifier.toInt() == 2) {
                                openHistory()
                            }
                            if (drawerItem.identifier.toInt() == 3) {
                                openAbout()
                            }
                            if (drawerItem.identifier.toInt() == 4) {
                                openProfileChooser()
                            }
                            if (drawerItem.identifier.toInt() == 5) {
                                openFamilyTree()
                            }
                            if (drawerItem.identifier.toInt() == 6) {
                                openHealthInsurance()
                            }
                            if (drawerItem.identifier.toInt() == 7) {
                                openHealthGoals()
                            }
                            if (drawerItem.identifier.toInt() == 8) {
                                openHealthTracker()
                            }
                            if (drawerItem.identifier.toInt() == 9) {
                                openHabits()
                            }
                            if (drawerItem.identifier.toInt() == 10) {
                                openDocumentToBeVerified()
                            }
                            if (drawerItem.identifier.toInt() == 11) {
                                openNutritionPopups()
                            }
                            if (drawerItem.identifier.toInt() == 12) {
                                openHome()
                            }
                            if (drawerItem.identifier.toInt() == 13) {
                                openSharedWithMe()
                            }
                            if (drawerItem.identifier.toInt() == 14) {
                                openQRCode()
                            }
                            if (drawerItem.identifier.toInt() == 15) {
                                openSwapLanguage()
                            }
                            return true
                        }
                    })
                    .withOnDrawerListener(object : Drawer.OnDrawerListener {
                        override fun onDrawerSlide(drawerView: View?, slideOffset: Float) {

                        }

                        override fun onDrawerClosed(drawerView: View?) {
                            //supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp)
                            //TODO: un comment
                        }

                        override fun onDrawerOpened(drawerView: View?) {
                            //supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_keyboard_backspace_white_24dp)
                            //TODO: un comment
                        }
                    })
                    .build()

            loadProfilePicture()
        } else {
            drawerResult = DrawerBuilder()
                    .withActivity(context_activity)
                    .withTranslucentStatusBar(true)
                    .withActionBarDrawerToggle(true)
                    .withToolbar(myToolBar)
                    .withAccountHeader(accountView!!)
                    .addDrawerItems(
                            item12, // Home
                            item4, // my profile
                            //item5, //family tree
                            item10, // documents to be verified
                            item2, // retrieve
                            item8, //health trackers
                            //item6, //health insurance
                            //item7, //health goals
                            //item11, //nutrition popup
                            item14,//scan my qr code
                            item13,//shared with me
                            //item9, //habits
                            item3, //about
                            item15, //change language
                            item1 //log out
                    )
                    .withOnDrawerItemClickListener(object : Drawer.OnDrawerItemClickListener {
                        override fun onItemClick(view: View, position: Int, drawerItem: IDrawerItem<*, *>): Boolean {
                            if (drawerItem.identifier.toInt() == 1) {
                                makeUserLogOut()
                            }
                            if (drawerItem.identifier.toInt() == 2) {
                                openHistory()
                            }
                            if (drawerItem.identifier.toInt() == 3) {
                                openAbout()
                            }
                            if (drawerItem.identifier.toInt() == 4) {
                                openProfileChooser()
                            }
                            //if (drawerItem.identifier.toInt() == 5) {
                            //    openFamilyTree()
                            //}
                            if (drawerItem.identifier.toInt() == 6) {
                                openHealthInsurance()
                            }
                            if (drawerItem.identifier.toInt() == 7) {
                                openHealthGoals()
                            }
                            if (drawerItem.identifier.toInt() == 8) {
                                openHealthTracker()
                            }
                            if (drawerItem.identifier.toInt() == 9) {
                                openHabits()
                            }
                            if (drawerItem.identifier.toInt() == 10) {
                                openDocumentToBeVerified()
                            }
                            if (drawerItem.identifier.toInt() == 11) {
                                openNutritionPopups()
                            }
                            if (drawerItem.identifier.toInt() == 12) {
                                openHome()
                            }
                            if (drawerItem.identifier.toInt() == 13) {
                                openSharedWithMe()
                            }
                            if (drawerItem.identifier.toInt() == 14) {
                                openQRCode()
                            }
                            if (drawerItem.identifier.toInt() == 15) {
                                openSwapLanguage()
                            }
                            return true
                        }
                    })
                    .withOnDrawerListener(object : Drawer.OnDrawerListener {
                        override fun onDrawerSlide(drawerView: View?, slideOffset: Float) {

                        }

                        override fun onDrawerClosed(drawerView: View?) {
                            //supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp)
                            //TODO: un comment
                        }

                        override fun onDrawerOpened(drawerView: View?) {
                            //supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_keyboard_backspace_white_24dp)
                            //TODO: un comment
                        }
                    })
                    .build()

            loadProfilePicture()
        }
    }

    private fun openSwapLanguage() {
        alertD.show()
        /*var currentLocale: Locale = context_activity.resources.configuration.locale

        val language = context_activity.getSharedPreferences("lang", Context.MODE_PRIVATE)

        Log.d("Language_drawer", "currentLocale: $currentLocale currentLocaleDisplayLang: ${currentLocale.displayLanguage}")

        if (currentLocale == Locale.ENGLISH) {
            //changing to hindi...
            var myLocale = Locale("hi")
            var res: Resources = context_activity.resources
            var dm: DisplayMetrics = res.displayMetrics
            var conf: Configuration = res.configuration
            conf.locale = myLocale
            res.updateConfiguration(conf, dm)

            val editor = language.edit()
            editor.putString("language", "hi")
            editor.apply()
            context_activity.recreate()
        } else {

            var myLocale = Locale("en")
            var res: Resources = context_activity.resources
            var dm: DisplayMetrics = res.displayMetrics
            var conf: Configuration = res.configuration
            conf.locale = myLocale
            res.updateConfiguration(conf, dm)
            context_activity.recreate()
            val editor = language.edit()
            editor.putString("language", "en")
            editor.apply()
        }*/
    }

    private fun openQRCode() {
        val i = Intent(context_activity, scanQR::class.java)
        context_activity.startActivity(i)
        handleFinishActivity()
    }

    @SuppressLint("ApplySharedPref")
    fun makeUserLogOut() {
        try {
            val loginInfo = context.getSharedPreferences("loginInfo", Context.MODE_PRIVATE)
            loginInfo.edit().clear().commit()
            drawerResult?.closeDrawer()
            //context_activity.finish()
            val i = Intent(context_activity, individualOrFamily::class.java)
            context_activity.startActivity(i)
            context_activity.finish()
            //handleFinishActivity()
        } catch (e: Exception) {
            Log.d("makeUserLogOut()", "error LogOut: $e")
        }
    }

    fun openHome() {
        val i = Intent(context_activity, MainActivity::class.java)
        context_activity.startActivity(i)
        handleFinishActivity()
    }

    fun openHistory() {
        val i = Intent(context_activity, history_pickFamilyMember::class.java)
        context_activity.startActivity(i)
        handleFinishActivity()
    }

    fun openAbout() {
        val i = Intent(context_activity, about::class.java)
        context_activity.startActivity(i)
        handleFinishActivity()
    }

    fun openSharedWithMe() {
        val i = Intent(context_activity, sharedWithMe::class.java)
        context_activity.startActivity(i)
        handleFinishActivity()
    }

    fun openProfileChooser() {
        val i = Intent(context_activity, profileChooser::class.java)
        context_activity.startActivity(i)
        handleFinishActivity()
    }

    fun openFamilyTree() {
        val i = Intent(context_activity, familyTree::class.java)
        context_activity.startActivity(i)
        handleFinishActivity()
    }

    fun openHealthInsurance() {
        val i = Intent(context_activity, HealthInsurance::class.java)
        context_activity.startActivity(i)
        handleFinishActivity()
    }

    fun openHealthGoals() {
        val i = Intent(context_activity, healthGoals::class.java)
        context_activity.startActivity(i)
        handleFinishActivity()
    }

    fun openHealthTracker() {
        val i = Intent(context_activity, healthTracker::class.java)
        context_activity.startActivity(i)
        handleFinishActivity()
    }

    fun openHabits() {
        val i = Intent(context_activity, habits::class.java)
        context_activity.startActivity(i)
        handleFinishActivity()
    }

    fun openDocumentToBeVerified() {
        val i = Intent(context_activity, documentToBeVerified::class.java)
        context_activity.startActivity(i)
        handleFinishActivity()
    }

    fun openNutritionPopups() {
        val i = Intent(context_activity, nutrition::class.java)
        context_activity.startActivity(i)
        handleFinishActivity()
    }


    fun handleFinishActivity() {
        if (context_activity.localClassName.toString() != "MainActivity") {
            context_activity.finish()
        }
    }

    //load profile
    fun loadProfilePicture() {
        doAsync {
            val client: OkHttpClient = OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build()

            val urlBuilder = HttpUrl.parse(ServerConfig().getProfileApi())!!.newBuilder()
            urlBuilder.addQueryParameter("familyId", familyId.toString())
            urlBuilder.addQueryParameter("phoneNumber", phoneNumber)

            val url = urlBuilder.build().toString()

            val request = Request.Builder()
                    .url(url)
                    .build()

            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()

            val data1 = responce_string?.replace("___", ",")

            val data2 = data1?.replace("*", ",")

            val data3 = data2?.replace("_", ",")

            val data4 = data3?.replace("^^", ",")

            val data5 = data4?.replace("{", ",")

            val data6 = data5?.replace("#", ",")

            val data7 = data6?.replace("}", ",")

            val data8 = data7?.replace(":", ",")

            val data = data8!!.trim().split(",")

            Log.d("mess_profile", "Pic: $data[4]")
            Log.d("mess_profile", "Name: $data[0]")

            val temp = data[0].toLowerCase()
            val profilePicture: String = data[4]

            val fullName = temp.substring(0, 1).toUpperCase() + temp.substring(1)

            val uri: Uri = Uri.parse(ServerConfig().getRootAddress()
                    + "userProfilePictures/" + profilePicture + "?notCache=" + profilePicture)

            val bitmap: Bitmap = Picasso.with(context_activity)
                    .load(uri)
                    .get()

            p_d_i = ProfileDrawerItem().withName(fullName)
                    .withEmail(phoneNumber)
                    .withIcon(bitmap)

            uiThread {
                Log.d("mess_profile", responce_string.toString())
                accountView!!.removeProfile(0)
                accountView!!.addProfile(p_d_i!!, 0)
            }
        }
    }
}