package com.medapp.medapp;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.nfc.Tag;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.medapp.medapp.PagerTransformers.DepthPageTransformer;
import com.medapp.medapp.config.ServerConfig;
import com.medapp.medapp.utils.left_drawer;
import com.squareup.picasso.Picasso;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class historyTabs extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    SharedPreferences loginInfo;
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;

    ImageButton searchBtn, menuBtn;

    int ImageId = 0;
    String familyId, phoneNumber;

    TabLayout tabLayout;

    Toolbar toolbar;

    String test = "history_Tabs";
    String names, numbers;
    String responseString;
    String indi_profilePic;

    Intent i, j;

    ArrayList<String> family_names;
    ArrayList<String> family_numbers;
    ArrayList<String> family_profile_pic;
    ArrayList<ArrayList<String>> historyList;
    ArrayList<ArrayList<ArrayList<String>>> historyList_family;

    ArrayList<String> list;

    OkHttpClient client;
    HttpUrl.Builder urlBuilder;

    Response response;
    Request request;

    String fromDate, toDate, category;

    int size;


    ProgressDialog dialog;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, Main2Activity.class);
        startActivity(intent);
        finish();
        //overridePendingTransition(R.anim.fade_out, R.anim.fade_in);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_tabs);

        try {
            Log.d("Check_historyTabs", "Inside onCreate()");
            Log.d(test, "Inside onCreate()");

            /*StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);*/

            dialog = new ProgressDialog(historyTabs.this);
            dialog.setMessage(getString(R.string.please_wait));
            dialog.setTitle(getString(R.string.loading));
            dialog.setIndeterminate(false);
            dialog.setCancelable(false);

            toolbar = findViewById(R.id.toolbar_tabs);

            searchBtn = findViewById(R.id.searchBtn);

            menuBtn = findViewById(R.id.menu);

            menuBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(historyTabs.this, FilterHandler.class);
                    startActivity(intent);
                }
            });

            searchBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Intent i = new Intent(historyTabs.this, searchDocs.class);
                        startActivity(i);
                        //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    } catch (Exception e) {
                        Log.d("searchBtn", "Exception: " + e);
                    }
                }
            });

            new left_drawer(this, historyTabs.this, toolbar).createNavigationDrawer();

            historyList = new ArrayList<>();
            historyList_family = new ArrayList<>();

            tabLayout = findViewById(R.id.tabs);
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            // Create the adapter that will return a fragment for each of the three
            // primary sections of the activity.

            // Set up the ViewPager with the sections adapter.
            mViewPager = findViewById(R.id.container);

            //mViewPager.setPageTransformer(true, new DepthPageTransformer());

            //get family id from persistent storage
            loginInfo = getSharedPreferences("loginInfo", Context.MODE_PRIVATE);
            familyId = loginInfo.getString("familyId", null);
            phoneNumber = loginInfo.getString("phoneNumber", "");

            //Log.d(test, "LogIn info:" + familyId);
            //callToAPI(sort_flag);

            if (!familyId.equals("0")) {
                //handling the family things . . .
                //getting the list of family members. . .
                try {
                    i = getIntent();

                    family_names = i.getStringArrayListExtra("familyNames");
                    family_numbers = i.getStringArrayListExtra("familyNumber");
                    family_profile_pic = i.getStringArrayListExtra("familyProfilePic");

                    fromDate = i.getStringExtra("startDate");
                    toDate = i.getStringExtra("endDate");
                    category = i.getStringExtra("category");

                    size = family_names.size();

                    Log.d(test, "historyTabs familyNames<ArrayList>:" + family_names);
                    Log.d(test, "historyTabs familyNumbers<ArrayList>:" + family_numbers);
                    Log.d(test, "historyTabs familyPics<ArrayList>:" + family_profile_pic);

                    Log.d(test, "HistoryTabs ====== from: " + fromDate + " to: " + toDate + " category: " + category);

                    for (int i = 0; i < size; i++) {
                        //ImageId = 0;
                        tabLayout.addTab(tabLayout.newTab()
                                .setCustomView(createTabItemView(family_profile_pic.get(i), family_names.get(i))));
                        //makeHistoryApiRequest(ImageId, familyId, family_numbers.get(i));
                    }
                    for (int i = 0; i < size; i++) {
                        ImageId = 0;
                        //tabLayout.addTab(tabLayout.newTab().setText(family_names.get(i).substring(0, 3)));
                        makeHistoryApiRequest(ImageId, familyId, family_numbers.get(i), fromDate, toDate, category);
                    }
                /*mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
                mViewPager.setAdapter(mSectionsPagerAdapter);
                mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
                tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));*/
                } catch (Exception e) {
                    Log.d(test, "Inside Family Thing . . . Exception: " + e);
                }
            } else {
                //we'll handle the individual things . . .
                //just create a single tab here and show his docs . . .

                //getProfilePicIndividual(phoneNumber);

                j = getIntent();
                names = j.getStringExtra("Selected_familyMemberName");
                numbers = j.getStringExtra("Selected_familyMemberPhoneNumber");
                indi_profilePic = j.getStringExtra("profilePic");
                fromDate = j.getStringExtra("startDate");
                toDate = j.getStringExtra("endDate");
                category = j.getStringExtra("category");

                Log.d(test, "HistoryTabs ====== from: " + fromDate + " to: " + toDate + " category: " + category);

                Log.d(test, "number from loginInfo" + phoneNumber);
                Log.d(test, "Image Name: " + indi_profilePic);

                //tabLayout.addTab(tabLayout.newTab().setCustomView(createTabItemView(null, phoneNumber)));
                //tabLayout.addTab(tabLayout.newTab().setText("You"));
                tabLayout.addTab(tabLayout.newTab().setCustomView(createTabItemView(indi_profilePic, "You")));
                //make request to API for the data . . .
                Log.d(test, "making call to makeHistoryApiRequest");

                makeHistoryApiRequest(ImageId, familyId, phoneNumber, fromDate, toDate, category);

                /*mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
                mViewPager.setAdapter(mSectionsPagerAdapter);
                mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
                tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));*/
            }
        } catch (Exception ex) {
            String err = (ex.getLocalizedMessage() == null) ? "failed" : ex.getMessage();
            Log.d(test, "Inside onCreate().... Exception: " + err);
        }
    }

    /*private void getProfilePicIndividual(String phoneNumber) {

        try {
            Log.d(test, "inside getProfilePicIndividual()....");
            profileIndividual individual = new profileIndividual();
            individual.execute(phoneNumber, familyId);
        } catch (Exception e) {
            Log.d(test, "inside getProfilePicIndividual()....Exception: " + e);
        }
        *//*Log.d(test, "inside getPicIndividual");
        client = new OkHttpClient.Builder().connectTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build();

        urlBuilder = Objects.requireNonNull(HttpUrl.parse(new ServerConfig().getProfileApi())).newBuilder();
        urlBuilder.addQueryParameter("familyId", familyId);
        urlBuilder.addQueryParameter("phoneNumber", phoneNumber);

        String url = urlBuilder.build().toString();

        request = new Request.Builder().url(url).build();

        try {
            response = client.newCall(request).execute();
            if (response.body() != null) {
                responseString = response.body().string();
            }
            Log.d(test, "Num: " + phoneNumber);
            Log.d(test, "responseString: " + responseString);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (!responseString.equals("noImageUploadedYet") && !responseString.equals("noMoreImages")) {
            Log.d(test, "We get image name from here . . .");
            //String[] data = responseString.split("___|\\*|-|^^|\\{");

            String data1 = responseString.replace("___", "+");
            String data2 = data1.replace("*", "+");
            String data3 = data2.replace("-", "+");
            String data4 = data3.replace("^^", "+");
            String data5 = data4.replace("{", "+");
            String data6 = data5.replace("#", "+");
            String data7 = data6.replace("}", "+");
            String data8 = data7.replace(":", "+");
            String data[] = data8.split("\\+");

            indi_profilePic = data[4];
        }*//*
    }*/

    private View createTabItemView(String imgUri, String text) {

        Log.d("ImageId: ", imgUri);
        de.hdodenhof.circleimageview.CircleImageView imageView;
        //SimpleDraweeView imageView;
        TextView textView;

        LayoutInflater factory = LayoutInflater.from(this);
        View view = factory.inflate(R.layout.custom_tab, null);

        imageView = view.findViewById(R.id.pic);
        textView = view.findViewById(R.id.text);

        textView.setText(text);
        if (imgUri.length() > 0) {
            Picasso.with(historyTabs.this)
                    .load(new ServerConfig().getRootAddress() + "upload/" + imgUri + "?notCache=" + imgUri)
                    .fit()
                    .into(imageView);
        } else {
            Picasso.with(historyTabs.this)
                    .load(R.drawable.icon_man)
                    .fit()
                    .into(imageView);
        }
        return view;
    }

    private void makeHistoryApiRequest(int imageId, String familyId, String phoneNumber, String fromDate, String toDate, String category) {

        getHistory history = new getHistory();
        history.execute(String.valueOf(imageId), familyId, phoneNumber, fromDate, toDate, category);

        /*//Inside background . . .
        Log.d(test, "inside makeHistoryApiRequest");
        client = new OkHttpClient.Builder().connectTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build();

        urlBuilder = Objects.requireNonNull(HttpUrl.parse(new ServerConfig().getHistoryApi())).newBuilder();
        urlBuilder.addQueryParameter("imageId", String.valueOf(imageId));
        urlBuilder.addQueryParameter("familyId", familyId);
        urlBuilder.addQueryParameter("phoneNumber", phoneNumber);
        urlBuilder.addQueryParameter("fromDate", fromDate);
        urlBuilder.addQueryParameter("toDate", toDate);
        urlBuilder.addQueryParameter("category", category);

        String url = urlBuilder.build().toString();

        Log.d(test, "URL: " + url);

        request = new Request.Builder().url(url).build();

        try {
            response = client.newCall(request).execute();
            if (response.body() != null) {
                responseString = response.body().string();
            }

            Log.d("API_Call", "imgID: " + imageId);
            Log.d("API_Call", "familyID: " + familyId);
            Log.d("API_Call", "Num: " + phoneNumber);
            Log.d("API_Call", "responseString: " + responseString);
        } catch (Exception e) {
            Log.d(test, "Exception: " + e);
        }

        //inside onPostExecute . .

        if (!responseString.equals("noImageUploadedYet") && !responseString.equals("noMoreImages")) {
            if (familyId.equals("0")) {
                //individial account
                Log.d("API_Call", "calling processIndividualHistoryResponse()");
                processIndividualHistoryResponse(responseString);
            } else {
                //family account
                Log.d("API_Call", "calling processFamilyHistoryResponse()");
                processFamilyHistoryResponse(responseString);
            }
        } else {
            if (familyId.equals("0")) {

                Log.d("API_Call", "No Images in this individual Account");

                list = new ArrayList<>();

                list.add("0");
                list.add("1");
                list.add("No Uploads");
                list.add("You");
                //Image Name Here
                list.add("noFile.jpg");
                list.add("5");
                list.add("6");
                list.add("7");
                list.add("8");

                historyList.add(list);

            } else {
                ArrayList<ArrayList<String>> historyList2 = new ArrayList<>();
                //family account
                Log.d("API_Call", "No Images in this family Account");

                list = new ArrayList<>();

                list.add("0");
                list.add("1");
                list.add("No Uploads");
                list.add("You");
                //Image Name Here
                list.add("noFile.jpg");
                list.add("5");
                list.add("6");
                list.add("7");
                list.add("8");

                historyList2.add(list);

                historyList_family.add(historyList2);
            }
        }*/
    }

    private void processFamilyHistoryResponse(String responseString) {
        Log.d(test, "Inside processFamilyHistoryResponse()");
        Log.d(test, "Inside processFamilyHistoryResponse()..ResponseString: " + responseString);

        try {
            String[] chunkOfData = responseString.split("___");
            ImageId = Integer.parseInt(chunkOfData[0]);
            Log.d("API_Call", "ImageId: " + ImageId);

            ArrayList<ArrayList<String>> historyList2;
            ArrayList<String> list;
            historyList2 = new ArrayList<>();

            int counter = 1;
            int size = chunkOfData.length;
            Log.d("API_Call", "size: " + size);
            while (counter < size) {
                //Log.d("API_Call", "Inside the loop");

                list = new ArrayList<>();

                String blockOfData = chunkOfData[counter];
                String[] array = blockOfData.trim().split("\\*");

                //Log.d("API_Call", "blockOfData: " + blockOfData);

                String type = array[0];
                list.add(type);
                //Log.d("API_Call", "type: " + type);
                String category = array[1];
                list.add(category);
                //Log.d("API_Call", "category: " + category);
                String description = array[2];
                list.add(description);
                //Log.d("API_Call", "description: " + description);
                String familyMemberName = "U";
                //Log.d("API_Call", "familyMemberName: " + familyMemberName);
                list.add(familyMemberName);
                String documentImageName = array[4];
                list.add(documentImageName);
                //Log.d("API_Call", "imageName: " + documentImageName);
                String documentIsVisibleTo = array[5];
                list.add(documentIsVisibleTo);
                //Log.d("API_Call", "documentVisible: " + documentIsVisibleTo);
                String dateAsText = array[6] + "-" + array[7] + "-" + array[8];
                list.add(dateAsText);
                //Log.d("API_Call", "date: " + dateAsText);

                //Log.d("API_Call", "We're creating the lists . . ");
                Log.d("API_Call", "Individual Lists:");
                Log.d("API_Call", "List" + counter + ":" + list);

                historyList2.add(list);
                counter++;
            }
            historyList_family.add(historyList2);

            Log.d("API_Call", "All data added inside lists");
            Log.d("API_Call", "Complete_historyList");
            Log.d("API_Call", String.valueOf(historyList_family));

            mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
            mViewPager.setAdapter(mSectionsPagerAdapter);
            mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
            tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));
            //dialog.dismiss();
        } catch (Exception e) {
            Log.d("API_Call", "Inside processFamilyHistoryResponse()..Exception: " + e);
        }
    }

    private void processIndividualHistoryResponse(String responseString) {
        Log.d("API_Call", "Inside processIndividualHistoryResponse()");
        Log.d("API_Call", "Inside processIndividualHistoryResponse()....responseString: " + responseString);

        try {
            String[] chunkOfData = responseString.split("___");
            ImageId = Integer.parseInt(chunkOfData[0]);
            Log.d("API_Call", "ImageId: " + ImageId);

            int counter = 1;
            int size = chunkOfData.length;
            Log.d("API_Call", "size: " + size);
            while (counter < size) {
                //Log.d("API_Call", "Inside the loop");

                list = new ArrayList<>();

                String blockOfData = chunkOfData[counter];
                String[] array = blockOfData.trim().split("\\*");

                Log.d("API_Call", "blockOfData: " + blockOfData);

                String type = array[0];
                list.add(type);
                //Log.d("API_Call", "type: " + type);
                String category = array[1];
                list.add(category);
                //Log.d("API_Call", "category: " + category);
                String description = array[2];
                list.add(description);
                //Log.d("API_Call", "description: " + description);
                String familyMemberName = getString(R.string.you);
                //Log.d("API_Call", "familyMemberName: " + familyMemberName);
                list.add(familyMemberName);
                String documentImageName = array[4];
                list.add(documentImageName);
                //Log.d("API_Call", "imageName: " + documentImageName);
                String documentIsVisibleTo = array[5];
                list.add(documentIsVisibleTo);
                //Log.d("API_Call", "documentVisible: " + documentIsVisibleTo);
                String dateAsText = array[6] + "-" + array[7] + "-" + array[8];
                list.add(dateAsText);
                //Log.d("API_Call", "date: " + dateAsText);

                //Log.d("API_Call", "We're creating the lists . . ");
                Log.d("API_Call", "Individual Lists:");
                Log.d("API_Call", "List" + counter + ":" + list);
                //Order
                //0-type,1-category,2-description,3-familyMemName,4-docImgName,5-isVisibleTo,6-date
                historyList.add(list);
                counter++;
            }
            Log.d("API_Call", "All data added inside lists");
            Log.d("API_Call", "Complete_historyList");
            Log.d("API_Call", String.valueOf(historyList));

            mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
            mViewPager.setAdapter(mSectionsPagerAdapter);
            mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
            tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));

            //dialog.dismiss();
        } catch (Exception e) {
            Log.d(test, "Inside processIndividualHistoryResponse()...Exception: " + e);
        }
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

    }


    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_history_tabs, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }*/


    public static class PlaceholderFragment extends Fragment {
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_history_tabs, container, false);
            TextView textView = rootView.findViewById(R.id.section_label);
            if (getArguments() != null) {
                textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
            }
            return rootView;
        }
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        int mNumOfTabs;
        Fragment fragment = null;

        public SectionsPagerAdapter(FragmentManager fm, int NumOfTabs) {
            super(fm);
            this.mNumOfTabs = NumOfTabs;
        }


        @Override

        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            //return PlaceholderFragment.newInstance(position + 1);
            for (int i = 0; i < mNumOfTabs; i++) {
                if (i == position) {
                    if (!familyId.equals("0")) {
                        Bundle bundle = new Bundle();
                        bundle.putString("familyID", familyId);
                        bundle.putSerializable("data", historyList_family.get(i));
                        fragment = new myFragment();
                        fragment.setArguments(bundle);
                        break;
                    } else {
                        Log.d("API_Call", "in Bundle ::::" + String.valueOf(historyList));
                        Bundle bundle = new Bundle();
                        bundle.putString("familyID", familyId);
                        bundle.putSerializable("data", historyList);
                        fragment = new myFragment();
                        fragment.setArguments(bundle);
                        break;
                    }
                }
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return mNumOfTabs;
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return super.getPageTitle(position);
        }
    }

    @SuppressLint("StaticFieldLeak")
    class getHistory extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //dialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {

            String imageId = strings[0];
            String familyId = strings[1];
            String phoneNumber = strings[2];
            String fromDate = strings[3];
            String toDate = strings[4];
            String category = strings[5];

            try {
                Log.d(test, "inside getHistory....doInBackground");
                client = new OkHttpClient.Builder().connectTimeout(60, TimeUnit.SECONDS)
                        .writeTimeout(60, TimeUnit.SECONDS)
                        .readTimeout(60, TimeUnit.SECONDS)
                        .build();

                urlBuilder = Objects.requireNonNull(HttpUrl.parse(new ServerConfig().getHistoryApi())).newBuilder();
                urlBuilder.addQueryParameter("imageId", String.valueOf(imageId));
                urlBuilder.addQueryParameter("familyId", familyId);
                urlBuilder.addQueryParameter("phoneNumber", phoneNumber);
                urlBuilder.addQueryParameter("fromDate", fromDate);
                urlBuilder.addQueryParameter("toDate", toDate);
                urlBuilder.addQueryParameter("category", category);

                String url = urlBuilder.build().toString();

                Log.d("URL_", url);

                request = new Request.Builder().url(url).build();
            } catch (Exception e) {
                Log.d(test, "inside getHistory....doInBackground.....Exception1: " + e);
            }
            try {
                response = client.newCall(request).execute();
                if (response.body() != null) {
                    responseString = response.body().string();
                }

                Log.d("API_Call", "responseString: " + responseString);
            } catch (Exception e) {
                Log.d(test, "inside getHistory....doInBackground.....Exception2: " + e);
            }
            Log.d(test, "inside getHistory....doInBackground.....Response string: " + responseString);
            return responseString;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                if (!s.equals("noImageUploadedYet") && !s.equals("noMoreImages")) {
                    if (familyId.equals("0")) {
                        //individial account
                        Log.d("API_Call", "calling processIndividualHistoryResponse()");
                        processIndividualHistoryResponse(s);
                        //dialog.dismiss();
                    } else {
                        //family account
                        Log.d("API_Call", "calling processFamilyHistoryResponse()");
                        processFamilyHistoryResponse(s);
                        //dialog.dismiss();
                    }
                } else {
                    if (familyId.equals("0")) {
                        Log.d("API_Call", "No Images in this individual Account");
                        list = new ArrayList<>();

                        list.add("0");
                        list.add("1");
                        list.add("No Uploads");
                        list.add("You");
                        //Image Name Here
                        list.add("noFile.jpg");
                        list.add("5");
                        list.add("6");
                        list.add("7");
                        list.add("8");

                        historyList.add(list);

                        Toast.makeText(historyTabs.this, R.string.no_docs_exists, Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(historyTabs.this, Main2Activity.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        finish();
                    } else {
                        ArrayList<ArrayList<String>> historyList2 = new ArrayList<>();
                        //family account
                        Log.d("API_Call", "No Images in this family Account");

                        list = new ArrayList<>();

                        list.add("0");
                        list.add("1");
                        list.add("No Uploads");
                        list.add("You");
                        //Image Name Here
                        list.add("noFile.jpg");
                        list.add("5");
                        list.add("6");
                        list.add("7");
                        list.add("8");

                        historyList2.add(list);

                        historyList_family.add(historyList2);

                        Toast.makeText(historyTabs.this, R.string.no_docs_exists, Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(historyTabs.this, Main2Activity.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        finish();
                    }
                }
            } catch (Exception e) {
                Log.d(test, "inside getHistory....postExecute().....Exception: " + e);
            }
        }
    }

    /*@SuppressLint("StaticFieldLeak")
    class profileIndividual extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... strings) {
            Log.d(test, "inside profileIndividual class::::doInBackground");

            String phoneNumber = strings[0];
            String familyId = strings[1];
            try {
                client = new OkHttpClient.Builder().connectTimeout(60, TimeUnit.SECONDS)
                        .writeTimeout(60, TimeUnit.SECONDS)
                        .readTimeout(60, TimeUnit.SECONDS)
                        .build();

                urlBuilder = Objects.requireNonNull(HttpUrl.parse(new ServerConfig().getProfileApi())).newBuilder();
                urlBuilder.addQueryParameter("familyId", familyId);
                urlBuilder.addQueryParameter("phoneNumber", phoneNumber);

                String url = urlBuilder.build().toString();

                request = new Request.Builder().url(url).build();
            } catch (Exception e) {
                Log.d(test, "Inside doInBackground...Exception1: " + e);
            }
            try {
                response = client.newCall(request).execute();
                if (response.body() != null) {
                    responseString = response.body().string();
                }
//                Log.d(test, "Num: " + phoneNumber);
                Log.d(test, "responseString: " + responseString);
            } catch (Exception e) {
                Log.d(test, "Inside doInBackground...Exception2: " + e);
            }
            return responseString;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                if (!s.equals("noImageUploadedYet") && !s.equals("noMoreImages")) {
                    Log.d(test, "inside onPostExecute(): profileIndividual");
                    //String[] data = responseString.split("___|\\*|-|^^|\\{");

                    String data1 = s.replace("___", "+");
                    String data2 = data1.replace("*", "+");
                    String data3 = data2.replace("-", "+");
                    String data4 = data3.replace("^^", "+");
                    String data5 = data4.replace("{", "+");
                    String data6 = data5.replace("#", "+");
                    String data7 = data6.replace("}", "+");
                    String data8 = data7.replace(":", "+");
                    String data[] = data8.split("\\+");

                    indi_profilePic = data[4];
                    Log.d(test, "Profile_Pic: " + indi_profilePic);
                }
            } catch (Exception e) {
                Log.d(test, "inside onPostExecute(): profileIndividual....Exception: " + e);
            }
        }
    }*/
}
