package com.medapp.medapp

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Button
import android.widget.EditText
import com.medapp.medapp.config.ServerConfig
import com.medapp.medapp.database.DB_HELPER
import com.muddzdev.styleabletoastlibrary.StyleableToast
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.util.*
import java.util.concurrent.TimeUnit

class addHealthInsuranceDetails : AppCompatActivity() {
    private var phoneNumber: String? = null
    private var day: String? = "0"
    private var month: String? = "0"
    private var year: String? = "0"

    var TAG: String? = "Activity_Name"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_health_insurance_details)
        setTitle("Health Insurance Details")

        Log.d(TAG, "Inside activity_add_health_insurance_details")

        val loginInfo = applicationContext.getSharedPreferences("loginInfo", Context.MODE_PRIVATE)
        phoneNumber = loginInfo.getString("phoneNumber", "")

        onNextClicked()
        onSelectDateButtonClicked()

    }

    fun onNextClicked() {
        findViewById<Button>(R.id.NextButton).setOnClickListener {
            var policyNumber = findViewById<EditText>(R.id.policyNumber).text.toString()

            if (policyNumber.length != 0 && day != "0" && month != "0" && year != "0") {
                addNewInsuranceRenewDateReminder(phoneNumber!!, policyNumber, day!!, month!!, year!!)
            } else {
                showMessageAfterSignup(getString(R.string.all_fields_are_compulsory), Color.RED)
            }
        }
    }

    fun onSelectDateButtonClicked() {
        findViewById<Button>(R.id.dateButton).setOnClickListener {
            createDatePicker()
        }
    }

    fun addNewInsuranceRenewDateReminder(PhoneNumber: String, PolicyNumber: String, renewDay: String, renewMonth: String, renewYear: String) {
        doAsync {
            val client: OkHttpClient = OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build()

            val urlBuilder = HttpUrl.parse(ServerConfig().insertHealthInsuranceRenewDate())!!.newBuilder()
            urlBuilder.addQueryParameter("phoneNumber", PhoneNumber)
            urlBuilder.addQueryParameter("policyNumber", PolicyNumber)
            urlBuilder.addQueryParameter("renewDay", renewDay)
            urlBuilder.addQueryParameter("renewMonth", renewMonth)
            urlBuilder.addQueryParameter("renewYear", renewYear)

            val url = urlBuilder.build().toString()

            val request = Request.Builder()
                    .url(url)
                    .build()

            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()

            uiThread {
                Log.d("mess", responce_string.toString())

                if (responce_string != "dataMissing") {
                    if (responce_string != "serverError") {
                        if (responce_string == "ok") {
                            //add data to local database
                            DB_HELPER(this@addHealthInsuranceDetails).insertIntoHealthInsurance(
                                    System.nanoTime().toInt(),
                                    PolicyNumber,
                                    renewDay,
                                    renewMonth,
                                    renewYear,
                                    "0"
                            )

                            val i = Intent(this@addHealthInsuranceDetails, HealthInsurance::class.java)
                            startActivity(i)
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                        } else {
                            showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                        }
                    } else {
                        showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                    }
                } else {
                    showMessageAfterSignup(getString(R.string.something_goes_wrong), Color.RED)
                }
            }
        }
    }

    fun showMessageAfterSignup(msg_text: String, color: Int) {
        val st = StyleableToast.Builder(this)
                .text(msg_text)
                .textColor(Color.WHITE)
                .backgroundColor(color)
                .build()
        st.show()
    }


    //create date picker
    fun createDatePicker() {
        val now = Calendar.getInstance()
        val dpd = DatePickerDialog.newInstance(
                object : DatePickerDialog.OnDateSetListener {
                    override fun onDateSet(view: DatePickerDialog?, year_: Int, monthOfYear: Int, dayOfMonth: Int) {
                        day = dayOfMonth.toString()
                        if (day!![0].toString() == "0") {
                            day!!.replaceFirst("0", "")
                        }
                        month = (monthOfYear + 1).toString()
                        if (month!![0].toString() == "0") {
                            month!!.replaceFirst("0", "")
                        }
                        year = year_.toString()

                        findViewById<Button>(R.id.dateButton).setText(day + "/" + month + "/" + year)
                    }
                },
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        )
        dpd.show(fragmentManager, "Datepickerdialog")
    }
}
