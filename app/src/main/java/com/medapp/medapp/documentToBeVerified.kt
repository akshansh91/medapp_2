package com.medapp.medapp

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.facebook.drawee.backends.pipeline.Fresco
import com.medapp.medapp.config.ServerConfig
import com.medapp.medapp.utils.left_drawer
import com.muddzdev.styleabletoastlibrary.StyleableToast
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.util.concurrent.TimeUnit

class documentToBeVerified : AppCompatActivity() {
    private var result_list_for_documentToBeVerified: java.util.ArrayList<documentToBeVerified_store> = java.util.ArrayList<documentToBeVerified_store>()
    private var documentToBeVerified_holder: RecyclerView.Adapter<documentToBeVerified_adapter.NumberViewHolder>? = null
    private var documentToBeVerified_recyclerView: RecyclerView? = null

    private var allowed_by_family_member_status: Int? = 1
    private var imageId: Int? = 0
    private var familyId: String? = "0"
    private var phoneNumber: String? = null
    val TAG: String? = "Activity_Name"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Fresco.initialize(this)
        setContentView(R.layout.activity_document_to_be_verified)

        try {
            Log.d(TAG, "Inside documentToBeVerified")
            Log.d("Check_docToBeVerified", "Inside onCreate()")
            findViewById<Toolbar>(R.id.docToBeVerifiedToolbar).title = getString(R.string.documents_to_be_verified)
            findViewById<Toolbar>(R.id.docToBeVerifiedToolbar).setTitleTextColor(ContextCompat.getColor(this@documentToBeVerified, R.color.white))

            //navigation drawer
            left_drawer(this, this@documentToBeVerified, findViewById(R.id.docToBeVerifiedToolbar)!!).createNavigationDrawer()

            //get familyId
            val loginInfo = applicationContext.getSharedPreferences("loginInfo", Context.MODE_PRIVATE)
            familyId = loginInfo.getString("familyId", "")
            phoneNumber = loginInfo.getString("phoneNumber", "")

            //make api request
            makeDocToBeVerifiedApiRequest(imageId!!, familyId!!, phoneNumber!!)

            //attach adapter to recyclerview
            attach_documentToBeVerified_adapter_to_recyclerview()
        } catch (e: Exception) {
            Log.d("Check_docToBeVerified", "Inside onCreate().. Exception: $e")
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
    }

    //recyclerview
    fun attach_documentToBeVerified_adapter_to_recyclerview() {
        try {
            Log.d("Check_docToBeVerified", "Inside attach_documentToBeVerified_adapter_to_recyclerview()")
            val documentToBeVerified_recyclerview: RecyclerView = findViewById(R.id.docToBeVerifiedRecyclerView)
            documentToBeVerified_recyclerView = documentToBeVerified_recyclerview
            val layoutManager = LinearLayoutManager(applicationContext)
            documentToBeVerified_recyclerview.layoutManager = layoutManager
            documentToBeVerified_recyclerview.setHasFixedSize(false)
            documentToBeVerified_recyclerview.isNestedScrollingEnabled = false
            documentToBeVerified_holder = documentToBeVerified_adapter()
            documentToBeVerified_recyclerview.adapter = documentToBeVerified_holder
        } catch (e: Exception) {
            Log.d("Check_docToBeVerified", "Inside attach_documentToBeVerified_adapter_to_recyclerview().. Exception: $e")
        }
    }

    inner class documentToBeVerified_store(
            familyMemberName: String,
            date: String,
            type: String,
            category: String,
            description: String,
            img_name: String
    ) {
        private var inner_familyMemberName: String = familyMemberName
        private var inner_date: String = date
        private var inner_type: String = type
        private var inner_category: String = category
        private var inner_description: String = description
        private var inner_img_name: String = img_name

        fun get_familyMemberName(): String {
            return inner_familyMemberName
        }

        fun get_date(): String {
            return inner_date
        }

        fun get_type(): String {
            return inner_type
        }

        fun get_category(): String {
            return inner_category
        }

        fun get_description(): String {
            return inner_description
        }

        fun get_img_name(): String {
            return inner_img_name
        }

    }

    private inner class documentToBeVerified_adapter() : RecyclerView.Adapter<documentToBeVerified_adapter.NumberViewHolder>() {

        override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): NumberViewHolder {
            val context = viewGroup.context
            val layoutIdForListItem = R.layout.document_to_be_verified_blueprint
            val inflater = LayoutInflater.from(context)
            val shouldAttachToParentImmediately = false

            val view = inflater.inflate(layoutIdForListItem, viewGroup, shouldAttachToParentImmediately)
            val viewHolder = NumberViewHolder(view)

            return viewHolder
        }

        override fun onBindViewHolder(holder: NumberViewHolder, position: Int) {
            val current_object = result_list_for_documentToBeVerified.get(position)
            holder.bind(current_object)
        }

        override fun getItemCount(): Int {
            return result_list_for_documentToBeVerified.size
        }

        inner class NumberViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            private var image_preview: ImageView? = null
            private var familyMemberNameTextView: TextView? = null
            private var dateTextView: TextView? = null
            private var typeTextView: TextView? = null
            private var categoryTextView: TextView? = null
            private var descriptionTextView: TextView? = null
            private var acceptButton: Button? = null
            private var rejectButton: Button? = null
            private var AORR: TextView? = null
            private var imgName: String? = null
            private var iconDate: ImageView? = null
            private var iconIdentity: ImageView? = null
            private var behalfOfYou: TextView? = null

            init {
                familyMemberNameTextView = itemView.findViewById<TextView>(R.id.familyMemberName)
                image_preview = itemView.findViewById<ImageView>(R.id.img_preview)
                typeTextView = itemView.findViewById<TextView>(R.id.type)
                categoryTextView = itemView.findViewById<TextView>(R.id.category)
                dateTextView = itemView.findViewById<TextView>(R.id.date)
                descriptionTextView = itemView.findViewById<TextView>(R.id.description)
                acceptButton = itemView.findViewById<Button>(R.id.acceptButton)
                rejectButton = itemView.findViewById<Button>(R.id.rejectButton)
                AORR = itemView.findViewById<TextView>(R.id.accOrRejTextView)
                iconDate = itemView.findViewById<ImageView>(R.id.date_Icon)
                iconIdentity = itemView.findViewById<ImageView>(R.id.identity_icon)
                behalfOfYou = itemView.findViewById<TextView>(R.id.onBehalfOfYouHeading)
            }

            fun bind(current_object: documentToBeVerified_store) {
                //changing this for a while . . .
                if (!current_object.get_img_name().equals("noDocsShared.jpg")) {
                    familyMemberNameTextView!!.setText(current_object.get_familyMemberName())
                    if (current_object.get_familyMemberName().length < 2) {
                        familyMemberNameTextView!!.setText("You")
                    }
                    dateTextView!!.setText(current_object.get_date())
                    typeTextView!!.setText(current_object.get_type())
                    categoryTextView!!.setText(current_object.get_category())
                    descriptionTextView!!.setText(current_object.get_description())
                    imgName = current_object.get_img_name()

                    acceptButton!!.setOnClickListener {
                        acceptButton!!.setVisibility(View.INVISIBLE)
                        rejectButton!!.setVisibility(View.INVISIBLE)
                        AORR!!.setVisibility(View.VISIBLE)
                        AORR!!.setText("Accepted")
                        AORR!!.setTextColor(ContextCompat.getColor(this@documentToBeVerified, R.color.green))

                        makeAcceptDocVerificationApiRequest(imgName!!, phoneNumber!!, "1")
                    }

                    rejectButton!!.setOnClickListener {
                        acceptButton!!.setVisibility(View.INVISIBLE)
                        rejectButton!!.setVisibility(View.INVISIBLE)
                        AORR!!.setVisibility(View.VISIBLE)
                        AORR!!.setText("Rejected")
                        AORR!!.setTextColor(ContextCompat.getColor(this@documentToBeVerified, R.color.red))

                        makeRejectDocVerificationApiRequest(imgName!!, phoneNumber!!, "2")
                    }
                    var uri: Uri = Uri.parse(ServerConfig().getRootAddress() + "upload/" + current_object.get_img_name() + "?notCache=" + current_object.get_img_name())

                    image_preview!!.setImageURI(uri)

                    /*Picasso.with(this@documentToBeVerified)
                            .load(ServerConfig().getRootAddress() + "upload/" + current_object.get_img_name() + "?notCache=" + current_object.get_img_name())
                            .resize(300, 400)
                            .into(image_preview)*/
                } else {
                    familyMemberNameTextView!!.visibility = View.INVISIBLE
                    dateTextView!!.visibility = View.INVISIBLE
                    typeTextView!!.visibility = View.INVISIBLE
                    categoryTextView!!.visibility = View.INVISIBLE
                    descriptionTextView!!.visibility = View.INVISIBLE
                    acceptButton!!.visibility = View.GONE
                    rejectButton!!.visibility = View.GONE
                    iconDate!!.visibility = View.INVISIBLE
                    iconIdentity!!.visibility = View.INVISIBLE
                    behalfOfYou!!.visibility = View.INVISIBLE

                    //val uri: Uri = Uri.parse(ServerConfig().getRootAddress() + "upload/" + current_object.get_img_name() + "?notCache=" + current_object.get_img_name())

                    image_preview!!.setImageResource(R.drawable.no_docs_to_verify_1)
                    /*Picasso.with(this@documentToBeVerified)
                            .load(ServerConfig().getRootAddress() + "upload/" + current_object.get_img_name() + "?notCache=" + current_object.get_img_name())
                            .resize(300, 400)
                            .into(image_preview)*/
                }
            }
        }
    }

    //make document to be verifiy Api
    fun makeDocToBeVerifiedApiRequest(imgId: Int, f_id: String, p_n: String) {
        doAsync {
            try {
                Log.d("Check_docToBeVerified", "Inside makeDocToBeVerifiedApiRequest()")
                val client: OkHttpClient = OkHttpClient.Builder()
                        .connectTimeout(60, TimeUnit.SECONDS)
                        .writeTimeout(60, TimeUnit.SECONDS)
                        .readTimeout(60, TimeUnit.SECONDS)
                        .build()

                val urlBuilder = HttpUrl.parse(ServerConfig().getDocToBeVerified())!!.newBuilder()
                urlBuilder.addQueryParameter("imageId", imgId.toString())
                urlBuilder.addQueryParameter("phoneNumber", p_n)

                val url = urlBuilder.build().toString()

                val request = Request.Builder()
                        .url(url)
                        .build()

                val response: Response = client.newCall(request).execute()
                val responce_string = response.body()!!.string()

                uiThread {
                    Log.d("mess", responce_string.toString())
                    if (responce_string != "noImageUploadedYet" && responce_string != "noMoreImages") {
                        processIndividualHistoryResponse(responce_string)
                    } else {
                        if (responce_string == "noImageUploadedYet") {
                            result_list_for_documentToBeVerified.add(documentToBeVerified_store("", "", "", "", "", "noDocsShared.jpg"))
                            documentToBeVerified_holder!!.notifyDataSetChanged()
                            findViewById<Button>(R.id.loadMoreButton).setVisibility(View.GONE)

                        }
                        if (responce_string == "noMoreImages") {
                            findViewById<Button>(R.id.loadMoreButton).setVisibility(View.GONE)
                        }
                    }
                }
            } catch (e: Exception) {
                Log.d("Check_docToBeVerified", "Inside makeDocToBeVerifiedApiRequest().. Exception: $e")
            }
        }
    }

    fun processIndividualHistoryResponse(response_string: String) {
        try {
            Log.d("Check_docToBeVerified", "Inside processIndividualHistoryResponse()")
            val chunkOfData = response_string.split("___")
            imageId = chunkOfData[0].toInt()

            var counter = 1
            val size = chunkOfData.size
            while (counter < size) {
                val blockOfData = chunkOfData[counter]
                val type = (blockOfData.split("*"))[0]
                val category = (blockOfData.split("*"))[1]
                val description = (blockOfData.split("*"))[2]
                val familyMemberName = ""
                val documentImageName = (blockOfData.split("*"))[4]
                val dateAsText = (blockOfData.split("*"))[5] + "-" + (blockOfData.split("*"))[6] + "-" + (blockOfData.split("*"))[7]
                //SimpleDateFormat("dd-MM-yyyy").format(Date(((documentImageName.split(".")[0]).toInt() * 1000L)))

                result_list_for_documentToBeVerified.add(documentToBeVerified_store(familyMemberName, dateAsText, type, category, description, documentImageName))
                documentToBeVerified_holder!!.notifyDataSetChanged()

                counter++
            }
        } catch (e: Exception) {
            Log.d("Check_docToBeVerified", "Inside processIndividualHistoryResponse().. Exception: $e")
        }
    }

    //accept or reject
    fun makeAcceptDocVerificationApiRequest(img_name: String, MembersPhoneNumberString: String, verificationStatus: String) {
        doAsync {
            try {
                Log.d("Check_docToBeVerified", "Inside makeAcceptDocVerificationApiRequest()")
                val client: OkHttpClient = OkHttpClient.Builder()
                        .connectTimeout(60, TimeUnit.SECONDS)
                        .writeTimeout(60, TimeUnit.SECONDS)
                        .readTimeout(60, TimeUnit.SECONDS)
                        .build()

                val urlBuilder = HttpUrl.parse(ServerConfig().accOrRejDocVerification())!!.newBuilder()
                urlBuilder.addQueryParameter("imgName", img_name)
                urlBuilder.addQueryParameter("membersPhoneNumber", MembersPhoneNumberString)
                urlBuilder.addQueryParameter("verificationStatus", verificationStatus)

                val url = urlBuilder.build().toString()

                val request = Request.Builder()
                        .url(url)
                        .build()

                val response: Response = client.newCall(request).execute()
                val responce_string = response.body()!!.string()

                uiThread {
                    Log.d("mess", responce_string.toString())

                    if (responce_string != "dataMissing") {
                        if (responce_string != "serverError") {
                            if (responce_string == "ok") {
                                showMessageAfterSignup(getString(R.string.accepted), Color.GREEN)
                            } else {
                                showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                            }
                        } else {
                            showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                        }
                    } else {
                        showMessageAfterSignup(getString(R.string.something_goes_wrong), Color.RED)
                    }
                }
            } catch (e: Exception) {
                Log.d("Check_docToBeVerified", "Inside makeAcceptDocVerificationApiRequest().. Exception: $e")
            }
        }
    }

    fun makeRejectDocVerificationApiRequest(img_name: String, MembersPhoneNumberString: String, verificationStatus: String) {
        doAsync {
            try {
                Log.d("Check_docToBeVerified", "Inside makeRejectDocVerificationApiRequest()")
                val client: OkHttpClient = OkHttpClient.Builder()
                        .connectTimeout(60, TimeUnit.SECONDS)
                        .writeTimeout(60, TimeUnit.SECONDS)
                        .readTimeout(60, TimeUnit.SECONDS)
                        .build()

                val urlBuilder = HttpUrl.parse(ServerConfig().accOrRejDocVerification())!!.newBuilder()
                urlBuilder.addQueryParameter("imgName", img_name)
                urlBuilder.addQueryParameter("membersPhoneNumber", MembersPhoneNumberString)
                urlBuilder.addQueryParameter("verificationStatus", verificationStatus)

                val url = urlBuilder.build().toString()

                val request = Request.Builder()
                        .url(url)
                        .build()

                val response: Response = client.newCall(request).execute()
                val responce_string = response.body()!!.string()

                uiThread {
                    Log.d("mess", responce_string.toString())

                    if (responce_string != "dataMissing") {
                        if (responce_string != "serverError") {
                            if (responce_string == "ok") {
                                showMessageAfterSignup(getString(R.string.rejected), Color.GREEN)
                            } else {
                                showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                            }
                        } else {
                            showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                        }
                    } else {
                        showMessageAfterSignup(getString(R.string.something_goes_wrong), Color.RED)
                    }
                }
            } catch (e: Exception) {
                Log.d("Check_docToBeVerified", "Inside makeRejectDocVerificationApiRequest().. Exception: $e")
            }
        }
    }

    fun showMessageAfterSignup(msg_text: String, color: Int) {
        val st = StyleableToast.Builder(this)
                .text(msg_text)
                .textColor(Color.WHITE)
                .backgroundColor(color)
                .build()
        st.show()
    }

    /*
    * allowed by family member status
    * 0-> not done anything
    * 1->accepted
    * 2->rejected
    * */


}
