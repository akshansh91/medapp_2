package com.medapp.medapp;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.medapp.medapp.config.ServerConfig;
import com.medapp.medapp.utils.left_drawer;

import java.io.IOException;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class shareImage_Menu extends AppCompatActivity {

    String imageName, imageLink, responseString;
    SimpleDraweeView imageHolder;
    ImageButton pickerContact;
    String pickedContactName, pickedContactNumber;
    TextView contactName, contactNumber;
    Button share;

    SharedPreferences loginInfo;
    String familyId, phoneNumber;

    Toolbar toolbar;

    OkHttpClient client;
    HttpUrl.Builder urlBuilder;

    Response response;
    Request request;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_image__menu);

        Fresco.initialize(this);

        loginInfo = getSharedPreferences("loginInfo", Context.MODE_PRIVATE);
        familyId = loginInfo.getString("familyId", null);
        phoneNumber = loginInfo.getString("phoneNumber", "");

        imageHolder = findViewById(R.id.myPic);
        pickerContact = findViewById(R.id.contactPicker);
        toolbar = findViewById(R.id.toolbar_tabs6);

        contactName = findViewById(R.id.name);
        contactNumber = findViewById(R.id.familyID);

        share = findViewById(R.id.startShare);

        new left_drawer(this, shareImage_Menu.this, toolbar).createNavigationDrawer();

        Intent i = getIntent();
        imageName = i.getStringExtra("imageName");
        imageLink = i.getStringExtra("imageLink");

        Log.d("shareImage_Menu", "name:" + imageName + "  Link:" + imageLink);

        if (imageName.length() > 0) {
            Uri uri = Uri.parse(new ServerConfig().getRootAddress() + "upload/" + imageName + "?notCache=" + imageName);
            imageHolder.setImageURI(uri);
        }

        pickerContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pickContactIntent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                //pickContactIntent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE) // Show user only contacts w/ phone numbers
                startActivityForResult(pickContactIntent, 1);
            }
        });

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkIfContactHasAccount(pickedContactNumber, imageName);
            }
        });
    }

    private void checkIfContactHasAccount(String pickedContactNumber, String imageName) {
        contactHasAccount hasAccount = new contactHasAccount();
        hasAccount.execute(pickedContactNumber, imageName);
    }

    private void shareDocuments(String pickedContactNumber, String imageName, String by) {
        shareDocs docs = new shareDocs();
        docs.execute(pickedContactNumber, imageName, by);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, history_pickFamilyMember.class);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_out, R.anim.fade_in);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {

                Uri contactData = data.getData();
                Cursor c = getContentResolver().query(contactData, null, null, null, null);
                if (c.moveToFirst()) {

                    pickedContactName = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                    String contactId = c.getString(c.getColumnIndex(ContactsContract.Contacts._ID));

                    String hasPhone = c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

                    if (hasPhone.equals("1"))
                        hasPhone = "true";
                    else
                        hasPhone = "false";

                    if (java.lang.Boolean.parseBoolean(hasPhone)) {
                        Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId, null, null);
                        while (phones.moveToNext()) {
                            String number = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                            if (number.length() == 10) {
                                pickedContactNumber = number;
                            } else if (number.length() == 11) {
                                pickedContactNumber = number.substring(1);
                            } else if (number.length() == 12) {
                                pickedContactNumber = number.substring(2);
                            } else if (number.length() == 13) {
                                pickedContactNumber = number.substring(3);
                            }
                        }
                        phones.close();
                    }

                    Log.d("NumberSelected", "name: " + pickedContactName + "  number:" + pickedContactNumber);
                }
                contactName.setText(pickedContactName);
                contactNumber.setText(pickedContactNumber);
                c.close();
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    class contactHasAccount extends AsyncTask<String, String, String> {

        String pickedContactNumber, imageName;

        @Override
        protected String doInBackground(String... strings) {

            pickedContactNumber = strings[0];
            imageName = strings[1];

            Log.d("API_Call_Account_Exists", "inside checkIfContactHasAccount");
            client = new OkHttpClient.Builder().connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build();

            urlBuilder = Objects.requireNonNull(HttpUrl.parse(new ServerConfig().accountExists())).newBuilder();
            urlBuilder.addQueryParameter("contactNumber", String.valueOf(pickedContactNumber));

            String url = urlBuilder.build().toString();

            request = new Request.Builder().url(url).build();

            try {
                response = client.newCall(request).execute();
                if (response.body() != null) {
                    responseString = response.body().string();
                }
                Log.d("API_Call_Account_Exists", "Num: " + pickedContactNumber);
                Log.d("API_Call_Account_Exists", "responseString: " + responseString);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return responseString;
        }

        @Override
        protected void onPostExecute(String s) {
            if (s.equals("Family") || s.equals("Individual") || s.equals("Both")) {
                Log.d("API_Call_Account_Exists", "Sharing pic into the account of that individual");
                shareDocuments(pickedContactNumber, imageName, phoneNumber);
            } else {
                Log.d("API_Call_Account_Exists", "Sharing pic externally using link ");
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = imageLink;
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, R.string.subject_here);
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, getString(R.string.share_via)));
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    class shareDocs extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            Log.d("API_Call_shareDocuments", "inside shareDocuments");

            String pickedContactNumber = strings[0];
            String imageName = strings[1];
            String by = strings[2];

            client = new OkHttpClient.Builder().connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build();

            urlBuilder = Objects.requireNonNull(HttpUrl.parse(new ServerConfig().shareDocuments())).newBuilder();
            urlBuilder.addQueryParameter("contactNumber", String.valueOf(pickedContactNumber));
            urlBuilder.addQueryParameter("documentName", String.valueOf(imageName));
            urlBuilder.addQueryParameter("sharedBy", String.valueOf(by));

            String url = urlBuilder.build().toString();

            request = new Request.Builder().url(url).build();

            try {
                response = client.newCall(request).execute();
                if (response.body() != null) {
                    responseString = response.body().string();
                }
                Log.d("API_Call_shareDocuments", "Num: " + pickedContactNumber);
                Log.d("API_Call_shareDocuments", "DocumentName: " + imageName);
                Log.d("API_Call_shareDocuments", "responseString: " + responseString);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return responseString;
        }

        @Override
        protected void onPostExecute(String s) {
            if (s.equals("Success")) {
                Toast.makeText(shareImage_Menu.this, R.string.doc_shared_successfully, Toast.LENGTH_SHORT).show();
                Intent i = new Intent(shareImage_Menu.this, history_pickFamilyMember.class);
                startActivity(i);
                overridePendingTransition(R.anim.fade_out, R.anim.fade_in);
            } else {
                Toast.makeText(shareImage_Menu.this, R.string.doc_shared_failed, Toast.LENGTH_SHORT).show();
            }
        }
    }

}
