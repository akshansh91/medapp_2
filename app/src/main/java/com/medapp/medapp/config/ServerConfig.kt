package com.medapp.medapp.config

class ServerConfig {
    private var serverName = "http://galleryapp.esy.es/"
    //private var serverName = "http://192.168.0.104:8080/"

    //root of server
    fun getRootAddress(): String {
        return serverName
    }

    //to generate url with token and timeStamp
    fun generateToken_n_TimeStamp(): String {
        return serverName + "API/setToken_n_timeStamp.php"
    }

    fun accountExists(): String {
        return serverName + "login/checkContactHasAccount.php"
    }

    fun shareDocuments(): String {
        return serverName + "API/shareDocument.php"
    }

    fun getAdminDetails(): String {
        return serverName + "API/getAdminDetails.php"
    }

    fun checkIfAdmin(): String {
        return serverName + "API/checkIfAdmin.php"
    }

    fun checkIfDocToBeVerified(): String {
        return serverName + "API/checkDocsToBeVerified.php"
    }

    fun getImageInfo(): String {
        return serverName + "API/getImageInfo.php"
    }

    fun checkAddedInFamily(): String {
        return serverName + "API/checkAddedInFamily.php"
    }


    fun getSharedDocs(): String {
        return serverName + "API/getSharedDocuments.php"
    }

    fun searchDocs(): String {
        return serverName + "API/getSearchedDocuments.php"
    }

    //storage
    fun getStorageServerAddress(): String {
        return serverName + "upload.php"
    }

    //vision api
    fun getVisionApiKey(): String {
        return "AIzaSyBCStA6e7V9c2uPOvmUGg542znn2E5Lbgw"
    }

    //signup
    fun getSignupApi(): String {
        return serverName + "Signup/signup.php"
    }

    // family signup
    fun getFamilySignupApi(): String {
        return serverName + "Signup/family_signup.php"
    }

    //login
    fun getLoginApi(): String {
        return serverName + "login/login.php"
    }

    //login
    fun getFamilyLoginApi(): String {
        return serverName + "login/family_login.php"
    }

    fun getFamilyIdApi(): String {
        return serverName + "login/getFamilyId.php"
    }

    fun checkIfUidExists(): String {
        return serverName + "API/checkIfUidExists.php"
    }

    //get profile
    fun getProfileApi(): String {
        return serverName + "API/getProfileApi.php"
    }

    //join family
    //detect family api
    fun getDetectFamilyApi(): String {
        return serverName + "joinFamily/detectFamilyUsingPhoneNumber.php"
    }

    fun getDetectFamilUsingFamilyIdyApi(): String {
        return serverName + "joinFamily/detectFamilyUsingFamilyId.php"
    }

    fun getJoinFamilyApi(): String {
        return serverName + "joinFamily/joinFamily.php"
    }

    //uploading documents
    fun getFamilyMemberNamesApi(): String {
        return serverName + "API/getFamilyMembersNamesUsingFamilyId.php"
    }

    fun getHistoryApi(): String {
        return serverName + "API/getHistory.php"
    }

    fun getFamilyMemberNamesAndPhoneNumber(): String {
        return serverName + "API/getFamilyMembersNamesAndPhoneNumberUsingFamilyId.php"
    }

    fun getFamilyMemberNamesPhoneNumberProfilePic(): String {
        return serverName + "API/getFamilyMembersNamesPhoneNumberProfiePictureUsingFamilyId.php"
    }

    fun updateProfileFieldsApi(): String {
        return serverName + "API/updateProfileFieldsApi.php"
    }

    fun updateDetailsOfDocument(): String {
        return serverName + "API/updateDetailsOfDocument.php"
    }

    fun getUpdateProfilePictureApi(): String {
        return serverName + "API/updateProfilePicture.php"
    }

    fun getUserProfilePictureDirectory(): String {
        return serverName + "userProfilePictures"
    }

    fun getFamilyTreeApi(): String {
        return serverName + "API/getFamilyMemberRelationWithAdmin.php"
    }

    fun updateRelationWithFamilyMember(): String {
        return serverName + "API/updateRelationWithFamilyMember.php"
    }

    fun insertHealthInsuranceRenewDate(): String {
        return serverName + "API/insertHealthInsuranceRenewDate.php"
    }

    fun getMyInsuranceReminderApi(): String {
        return serverName + "API/getMyInsuranceReminder.php"
    }

    fun cancelInsuranceReminderApi(): String {
        return serverName + "API/cancelInsuranceReminder.php"
    }

    fun createHealthGoalsApi(): String {
        return serverName + "API/createHealthGoals.php"
    }

    fun getHealthGoalsApi(): String {
        return serverName + "API/getHealthGoals.php"
    }

    fun cancelHealthGoals(): String {
        return serverName + "API/cancelHealthGoal.php"
    }

    fun trackHealth(): String {
        return serverName + "API/trackHealth.php"
    }

    fun getHealthTrackerDatapoints(): String {
        return serverName + "API/getHealthTrackerDataPoints.php"
    }

    fun insertPermanentDisease(): String {
        return serverName + "API/insertPermanentDisease.php"
    }

    fun getPermanentDisease(): String {
        return serverName + "API/getPermanentDisease.php"
    }

    fun deletePermanentDisease(): String {
        return serverName + "API/deletePermanentDisease.php"
    }

    fun insertDoctorAppointment(): String {
        return serverName + "API/insertDoctorAppointment.php"
    }

    fun getDoctorAppointments(): String {
        return serverName + "API/getDoctorAppointments.php"
    }

    fun deleteDoctorAppointments(): String {
        return serverName + "API/deleteDoctorAppointments.php"
    }

    fun acceptInvite(): String {
        return serverName + "API/acceptInvite.php"
    }

    fun rejectInvite(): String {
        return serverName + "API/rejectInvite.php"
    }

    fun removeSharedPic(): String {
        return serverName + "API/removeSharedPic.php"
    }

    fun saveSharedPic(): String {
        return serverName + "API/saveSharedPic.php"
    }

    fun insertTests(): String {
        return serverName + "API/insertTests.php"
    }

    fun getTests(): String {
        return serverName + "API/getTests.php"
    }

    fun deleteTests(): String {
        return serverName + "API/deleteTests.php"
    }

    fun insertMedicineSchedule(): String {
        return serverName + "API/insertMedcineSchedule.php"
    }

    fun getMedicineSchedule(): String {
        return serverName + "API/getMedcineSchedule.php"
    }

    fun deleteMedicineSchedule(): String {
        return serverName + "API/deleteMedcineSchedule.php"
    }

    fun insertHabits(): String {
        return serverName + "API/insertHabits.php"
    }

    fun deleteHabit(): String {
        return serverName + "API/deleteHabit.php"
    }

    fun getHabits(): String {
        return serverName + "API/getHabits.php"
    }

    fun getFamilyMembersNamesAndPhoneNumberUsingUsersPhoneNumber(): String {
        return serverName + "API/getFamilyMemberNamesAndPhoneNumberUsingUsersPhoneNumber.php"
    }

    fun updateDocVisibilityInHistory(): String {
        return serverName + "API/updateDocVisibilityInHistory.php"
    }

    fun updateDocVisibilityInHistory_whenContactSelected(): String {
        return serverName + "API/updateDocVisibilityInHistory_whenContactSelected.php"
    }

    fun sortHistoryApi(): String {
        return serverName + "API/sortHistoryApi.php"
    }

    fun getDocToBeVerified(): String {
        return serverName + "API/getDocToBeVerified.php"
    }

    fun accOrRejDocVerification(): String {
        return serverName + "API/accOrRejDocVerification.php"
    }

    fun getDeleteDocApi(): String {
        return serverName + "API/DeleteDocApi.php"
    }

    fun removeFamilyMemberFromFamily(): String {
        return serverName + "API/removeFamilyMemberFromFamily.php"
    }

    fun AddFamilyMemberViaFamilyTree(): String {
        return serverName + "API/AddFamilyMemberViaFamilyTree.php"
    }

    fun IsThereAnyDocRemainingToVerify(): String {
        return serverName + "API/IsThereAnyDocRemainingToVerify.php"
    }

    fun updatePassword(): String {
        return serverName + "API/updatePassword.php"
    }

    fun isAccountExist_resetPassword(): String {
        return serverName + "API/isAccountExist_resetPassword.php"
    }


    //offline
    fun getOfflineHealthGoals(): String {
        return serverName + "if_client_is_offline/getHealthGoals.php"
    }

    fun getOfflineHealthInsurance(): String {
        return serverName + "if_client_is_offline/getHealthInsurance.php"
    }

    fun syncNutritionOnline(): String {
        return serverName + "if_client_is_offline/syncNutritionOnline.php"
    }


    //MSG91
    fun getMsg91_securityCode_from_webServer(): String {
        return serverName + "API/getMsg91AuthKey.php"
    }

    fun getMsg91_securityCode_from_webServer_2(): String {
        return serverName + "API/getMsg91AuthKey_join.php"
    }
}