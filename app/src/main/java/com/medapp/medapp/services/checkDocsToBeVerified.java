package com.medapp.medapp.services;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.medapp.medapp.R;
import com.medapp.medapp.config.ServerConfig;
import com.medapp.medapp.documentToBeVerified;

import java.io.IOException;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class checkDocsToBeVerified extends Service {

    String TAG = "CheckDocsToBeVerified";

    SharedPreferences loginInfo;

    OkHttpClient client;
    HttpUrl.Builder urlBuilder;

    Response response;
    Request request;

    String phoneNumber, responseString;

    Handler handler;

    Timer timer;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "Service Started . . . ");
        //get family id from persistent storage
        loginInfo = getApplicationContext().getSharedPreferences("loginInfo", Context.MODE_PRIVATE);
        //familyId = loginInfo.getString("familyId", null);
        phoneNumber = loginInfo.getString("phoneNumber", "");
        Log.d(TAG, "Phone number from prefs . . " + phoneNumber);
        handler = new Handler();

        timer = new Timer();

        TimerTask doTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Log.d(TAG, "Call to checkIfAdded. . .");
                            //Call to the server . . . . .
                            checkIfAdded(phoneNumber);
                        } catch (Exception e) {
                            Log.d(TAG, "Error: " + String.valueOf(e));
                        }
                    }
                });
            }
        };
        timer.schedule(doTask, 0, 600000); //10 min in ms
    }

    private void checkIfAdded(String phoneNumber) {
        Log.d(TAG, "inside checkIfAdded");
        callToApi call = new callToApi();
        call.execute(phoneNumber);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "Service Started");
        return START_STICKY;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        startService(new Intent(this, checkDocsToBeVerified.class));
        super.onTaskRemoved(rootIntent);
    }

    @SuppressLint("StaticFieldLeak")
    public class callToApi extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            Log.d(TAG, "Inside doInBackground");
            String num = strings[0];

            Log.d(TAG, "Num: " + num);

            client = new OkHttpClient.Builder().connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build();

            urlBuilder = Objects.requireNonNull(HttpUrl.parse(new ServerConfig().checkIfDocToBeVerified())).newBuilder();
            urlBuilder.addQueryParameter("contactNumber", num);

            String url = urlBuilder.build().toString();

            request = new Request.Builder().url(url).build();

            try {
                response = client.newCall(request).execute();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response.body() != null) {
                try {
                    responseString = response.body().string();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return responseString;
        }

        @Override
        protected void onPostExecute(String s) {
            Log.d(TAG, "String after background execution . . " + s);

            Intent intent = new Intent(getBaseContext(), documentToBeVerified.class);
            PendingIntent pIntent = PendingIntent.getActivity(getBaseContext(), (int) System.currentTimeMillis(), intent, 0);

            if (s.equals("Yes")) {
                Notification notification = new Notification.Builder(getBaseContext()).setAutoCancel(true).
                        setContentTitle(getString(R.string.new_document)).setContentText(getString(R.string.you_have_some_documents_to_be_verified))
                        .setContentIntent(pIntent)
                        .setDefaults(Notification.DEFAULT_ALL)
                        .setSmallIcon(R.drawable.logo_notif).build();
                NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

                if (manager != null) {
                    manager.notify(0, notification);
                }
            } else {
                Log.d(TAG, "No Need to be notified. . .");
            }
        }
    }
}
