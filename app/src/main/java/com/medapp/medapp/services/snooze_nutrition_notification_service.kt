package com.medapp.medapp.services

import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.IBinder


class snooze_nutrition_notification_service : Service() {

    override fun onBind(intent: Intent): IBinder {
        TODO("Return the communication channel to the service.")
    }

    override fun onCreate() {

        super.onCreate()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val type = intent!!.getStringExtra("type")
        val notificationIdentifier = intent.getStringExtra("ni")

        if (type == "B") {
            snoozeBreakFast()
        }
        if (type == "L") {
            snoozeLunch()
        }
        if (type == "D") {
            snoozeDinner()
        }

        cancelNotification(applicationContext, notificationIdentifier.toInt())

        this.stopSelf()

        return super.onStartCommand(intent, flags, startId)
    }

    fun snoozeBreakFast() {
        val nutritionInfo = getSharedPreferences("nutrition", Context.MODE_PRIVATE)
        val previous_value = nutritionInfo.getString("breakFastTime_Hour", null).toInt()
        val editor = nutritionInfo.edit()
        editor.putString("breakFastTime_Hour", (previous_value + 1).toString())
        editor.commit()
    }

    fun snoozeLunch() {
        val nutritionInfo = getSharedPreferences("nutrition", Context.MODE_PRIVATE)
        val previous_value = nutritionInfo.getString("lunchTime_Hour", null).toInt()
        val editor = nutritionInfo.edit()
        editor.putString("lunchTime_Hour", (previous_value + 1).toString())
        editor.commit()
    }

    fun snoozeDinner() {
        val nutritionInfo = getSharedPreferences("nutrition", Context.MODE_PRIVATE)
        val previous_value = nutritionInfo.getString("DinnerTime_Hour", null).toInt()
        val editor = nutritionInfo.edit()
        editor.putString("DinnerTime_Hour", (previous_value + 1).toString())
        editor.commit()
    }

    fun cancelNotification(ctx: Context, notifyId: Int) {
        val ns = Context.NOTIFICATION_SERVICE
        val nMgr = ctx.getSystemService(ns) as NotificationManager
        nMgr.cancel(notifyId)
    }
}
