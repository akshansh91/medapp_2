package com.medapp.medapp.services

import android.app.Notification
import android.app.job.JobParameters
import android.app.job.JobService
import br.com.goncalves.pugnotification.notification.PugNotification
import com.medapp.medapp.MainActivity
import com.medapp.medapp.R
import com.medapp.medapp.database.DB_HELPER
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.ThreadLocalRandom

class health_goals_services : JobService() {

    override fun onStartJob(params: JobParameters?): Boolean {
        getAllHealthGoals()

        return true
    }

    override fun onStopJob(params: JobParameters?): Boolean {
        return true
    }

    fun createNotification(title: String, message: String, bigMessage: String) {

        PugNotification.with(applicationContext)
                .load()
                .title(title)
                .message(message)
                .click(MainActivity::class.java)
                .bigTextStyle(bigMessage)
                .smallIcon(R.drawable.pugnotification_ic_launcher)
                .largeIcon(R.drawable.pugnotification_ic_launcher)
                .flags(Notification.DEFAULT_ALL)
                .identifier(ThreadLocalRandom.current().nextInt(1, 100 + 1))
                .simple()
                .build()
    }

    fun getAllHealthGoals() {
        val res = DB_HELPER(applicationContext).getAllDataFromHealthGoals()
        if (res.moveToFirst()) {
            do {
                val id = res.getString(res.getColumnIndex(DB_HELPER(applicationContext).TABLE_NAME_for_health_goals_id))
                val healthGoals = res.getString(res.getColumnIndex(DB_HELPER(applicationContext).TABLE_NAME_for_health_goals_healthGoals))
                val priority = res.getString(res.getColumnIndex(DB_HELPER(applicationContext).TABLE_NAME_for_health_goals_priority))
                val goalDay = res.getString(res.getColumnIndex(DB_HELPER(applicationContext).TABLE_NAME_for_health_goals_goalDay))
                val goalMonth = res.getString(res.getColumnIndex(DB_HELPER(applicationContext).TABLE_NAME_for_health_goals_goalMonth))
                val goalYear = res.getString(res.getColumnIndex(DB_HELPER(applicationContext).TABLE_NAME_for_health_goals_goalYear))
                val expiryFlag = res.getString(res.getColumnIndex(DB_HELPER(applicationContext).TABLE_NAME_for_health_goals_expiryFlag))

                val cal1 = Calendar.getInstance()
                val cal2 = Calendar.getInstance()
                cal1.time = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(getTodaysDate())
                cal2.time = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse((goalYear + "-" + goalMonth + "-" + goalDay))

                if ((cal1.before(cal2) || cal1 == cal2) && expiryFlag == "0") {
                    createNotification(healthGoals, pickMessage(), pickMessage())
                }

            } while (res.moveToNext())
        }
        res.close()
    }

    fun getTodaysDate(): String {
        val c: Calendar = Calendar.getInstance()
        val df: SimpleDateFormat = SimpleDateFormat("dd-MM-yyyy")
        val formattedDate: String = df.format(c.getTime()).toString()
        return formattedDate
    }

    fun pickMessage(): String {
        //TODO:add list of messages to pick random
        return "Message is not seeted"
    }
}
