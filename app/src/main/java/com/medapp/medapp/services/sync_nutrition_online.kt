package com.medapp.medapp.services

import android.app.job.JobParameters
import android.app.job.JobService
import android.util.Log
import com.medapp.medapp.config.ServerConfig
import com.medapp.medapp.database.DB_HELPER
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.util.concurrent.TimeUnit

class sync_nutrition_online : JobService() {
    override fun onStopJob(params: JobParameters?): Boolean {
        return true
    }

    override fun onStartJob(params: JobParameters?): Boolean {
        loadUnSyncData()
        return true
    }

    fun loadUnSyncData() {
        val res = DB_HELPER(applicationContext).getAllUnsyncedDataFromNutrition()
        if (res.moveToFirst()) {
            do {
                val id = res.getString(res.getColumnIndex(DB_HELPER(applicationContext).TABLE_NAME_for_nutrition_id))
                val myPhoneNumber = res.getString(res.getColumnIndex(DB_HELPER(applicationContext).TABLE_NAME_for_nutrition_myPhoneNumber))
                val foodItems = res.getString(res.getColumnIndex(DB_HELPER(applicationContext).TABLE_NAME_for_nutrition_foodItems))
                val day = res.getString(res.getColumnIndex(DB_HELPER(applicationContext).TABLE_NAME_for_nutrition_day))
                val month = res.getString(res.getColumnIndex(DB_HELPER(applicationContext).TABLE_NAME_for_nutrition_month))
                val year = res.getString(res.getColumnIndex(DB_HELPER(applicationContext).TABLE_NAME_for_nutrition_year))
                val time = res.getString(res.getColumnIndex(DB_HELPER(applicationContext).TABLE_NAME_for_nutrition_time))
                val type = res.getString(res.getColumnIndex(DB_HELPER(applicationContext).TABLE_NAME_for_nutrition_type))

                syncOnline(id, myPhoneNumber, foodItems, day, month, year, time, type)

            } while (res.moveToNext())
        }
        res.close()
    }

    fun syncOnline(
            id: String, //only for updating local database
            myPhoneNumber: String,
            foodItem: String,
            day: String,
            month: String,
            year: String,
            time: String,
            type: String
    ) {
        doAsync {
            val client: OkHttpClient = OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build()

            val urlBuilder = HttpUrl.parse(ServerConfig().syncNutritionOnline())!!.newBuilder()
            urlBuilder.addQueryParameter("myPhoneNumber", myPhoneNumber)
            urlBuilder.addQueryParameter("foodItem", foodItem)
            urlBuilder.addQueryParameter("day", day)
            urlBuilder.addQueryParameter("month", month)
            urlBuilder.addQueryParameter("year", year)
            urlBuilder.addQueryParameter("time", time)
            urlBuilder.addQueryParameter("type", type)

            val url = urlBuilder.build().toString()

            val request = Request.Builder()
                    .url(url)
                    .build()

            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()

            uiThread {
                Log.d("mess", "response from php: " + responce_string.toString())

                if (responce_string != "dataMissing") {
                    if (responce_string != "serverError") {
                        if (responce_string == "ok") {
                            //Successful
                            DB_HELPER(this@sync_nutrition_online).ActivateSyncFlagInNutrition(id)
                        } else {
                            //server error
                        }
                    } else {
                        //Server Error
                    }
                } else {
                    //Something goes wrong
                }
            }
        }
    }
}
