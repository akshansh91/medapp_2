package com.medapp.medapp.services

import android.app.Notification
import android.app.PendingIntent
import android.app.job.JobParameters
import android.app.job.JobService
import android.content.Context
import android.content.Intent
import br.com.goncalves.pugnotification.notification.PugNotification
import com.medapp.medapp.R
import com.medapp.medapp.nutrition
import java.text.SimpleDateFormat
import java.util.*

class nutrition_service : JobService() {

    override fun onStartJob(params: JobParameters?): Boolean {
        checkTime()

        return true
    }

    override fun onStopJob(params: JobParameters?): Boolean {
        return true
    }

    //create nutrition notification
    fun createNutritionNotification(title: String, message: String, bigMessage: String, type: String) {
        val ran = Random()
        val ni = (ran.nextInt(1000) + 20).toInt()

        val Intent = Intent(this@nutrition_service, nutrition::class.java)
        Intent.putExtra("type", type)
        val p_in = PendingIntent.getActivity(this@nutrition_service, 0, Intent, PendingIntent.FLAG_UPDATE_CURRENT)

        val Intent2 = Intent(this@nutrition_service, snooze_nutrition_notification_service::class.java)
        Intent2.putExtra("type", type)
        Intent2.putExtra("ni", ni.toString())
        val p_in2 = PendingIntent.getService(this@nutrition_service, 0, Intent2, PendingIntent.FLAG_UPDATE_CURRENT)

        PugNotification.with(this@nutrition_service)
                .load()
                .title(title)
                .message(message)
                .button(R.drawable.yes_icon, "Yes", p_in)
                .button(R.drawable.snooze_icon, "Snooze", p_in2)
                .bigTextStyle(bigMessage)
                .smallIcon(R.drawable.nutrition_icon)
                .largeIcon(R.drawable.nutrition_icon)
                .flags(Notification.DEFAULT_ALL)
                .identifier(ni)
                .simple()
                .build()

    }

    //check time
    fun checkTime() {
        if (getBreakFastTime_Hours() == getCurrentTimeHours()) {
            createNutritionNotification(getString(R.string.nutrition), getString(R.string.did_you_have_breakfast), getString(R.string.did_you_have_breakfast), "B")
        }
        if (getLunchTime_Hours() == getCurrentTimeHours()) {
            createNutritionNotification(getString(R.string.nutrition), getString(R.string.did_you_have_lunch), getString(R.string.did_you_have_lunch), "L")
        }
        if (getDinnerTime_Hours() == getCurrentTimeHours()) {
            createNutritionNotification(getString(R.string.nutrition), getString(R.string.did_you_have_dinner), getString(R.string.did_you_have_dinner), "D")
        }
    }

    //get current time
    fun getCurrentTimeHours(): Int {
        val c: Calendar = Calendar.getInstance()
        val df: SimpleDateFormat = SimpleDateFormat("HH")
        var formattedDate: String = df.format(c.getTime()).toString()
        if (formattedDate[0].toString() == "0") {
            formattedDate = formattedDate.replaceFirst("0", "")
        }
        return formattedDate.toInt()
    }

    //get stored preferences
    fun getBreakFastTime_Hours(): Int {
        val nutritionInfo = applicationContext.getSharedPreferences("nutrition", Context.MODE_PRIVATE)
        if (nutritionInfo.contains("breakFastTime_Hour") == true) {
            var t = nutritionInfo.getString("breakFastTime_Hour", "").toString()
            if (t[0].toString() == "0") {
                t = t.replaceFirst("0", "")
            }
            return t.toInt()
        } else {
            return 7
        }
    }

    fun getLunchTime_Hours(): Int {
        val nutritionInfo = applicationContext.getSharedPreferences("nutrition", Context.MODE_PRIVATE)
        if (nutritionInfo.contains("lunchTime_Hour") == true) {
            var t = nutritionInfo.getString("lunchTime_Hour", "").toString()
            if (t[0].toString() == "0") {
                t = t.replaceFirst("0", "")
            }
            return t.toInt()
        } else {
            return 13
        }
    }

    fun getDinnerTime_Hours(): Int {
        val nutritionInfo = applicationContext.getSharedPreferences("nutrition", Context.MODE_PRIVATE)
        if (nutritionInfo.contains("DinnerTime_Hour") == true) {
            var t = nutritionInfo.getString("DinnerTime_Hour", "").toString()
            if (t[0].toString() == "0") {
                t = t.replaceFirst("0", "")
            }
            return t.toInt()
        } else {
            return 20
        }
    }
}
