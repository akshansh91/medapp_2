package com.medapp.medapp.services

import android.app.Notification
import android.app.job.JobParameters
import android.app.job.JobService
import android.util.Log
import br.com.goncalves.pugnotification.notification.PugNotification
import com.medapp.medapp.MainActivity
import com.medapp.medapp.R
import com.medapp.medapp.database.DB_HELPER
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.ThreadLocalRandom

class health_insurance_service : JobService() {

    override fun onStartJob(params: JobParameters?): Boolean {
        getAllHealthInsurance()

        return true
    }

    override fun onStopJob(params: JobParameters?): Boolean {
        return true
    }

    fun createNotification(title: String, message: String, bigMessage: String) {
        PugNotification.with(applicationContext)
                .load()
                .title(title)
                .message(message)
                .click(MainActivity::class.java)
                .bigTextStyle(bigMessage)
                .smallIcon(R.drawable.pugnotification_ic_launcher)
                .largeIcon(R.drawable.pugnotification_ic_launcher)
                .flags(Notification.DEFAULT_ALL)
                .identifier(ThreadLocalRandom.current().nextInt(1, 100 + 1))
                .simple()
                .build()
    }

    fun getAllHealthInsurance() {
        val res = DB_HELPER(applicationContext).getAllDataFromHealthInsurance()
        if (res.moveToFirst()) {
            do {
                val id = res.getString(res.getColumnIndex(DB_HELPER(applicationContext).TABLE_NAME_for_health_insurance_id))
                val policyNumber = res.getString(res.getColumnIndex(DB_HELPER(applicationContext).TABLE_NAME_for_health_insurance_policyNumber))
                val renew_day = res.getString(res.getColumnIndex(DB_HELPER(applicationContext).TABLE_NAME_for_health_insurance_Day))
                val renew_month = res.getString(res.getColumnIndex(DB_HELPER(applicationContext).TABLE_NAME_for_health_insurance_Month))
                val renew_year = res.getString(res.getColumnIndex(DB_HELPER(applicationContext).TABLE_NAME_for_health_insurance_Year))
                val expiryFlag = res.getString(res.getColumnIndex(DB_HELPER(applicationContext).TABLE_NAME_for_health_insurance_expiryFlag))

                Log.d("mess", getTodaysDateDay() + getTodaysDateMonth() + getTodaysDateYear())

                if (
                        (getTodaysDateYear() == renew_year) &&
                        (expiryFlag == "0") &&
                        ((getTodaysDateMonth() == renew_month && renew_day.toInt() >= getTodaysDateDay().toInt()) || (getTodaysDateMonth().toInt() + 1 == renew_month.toInt() && renew_day.toInt() <= 10))
                ) {
                    createNotification(policyNumber, pickMessage(), pickMessage())
                }

            } while (res.moveToNext())
        }
        res.close()
    }

    fun getTodaysDateDay(): String {
        val c: Calendar = Calendar.getInstance()
        val df: SimpleDateFormat = SimpleDateFormat("dd")
        var formattedDate: String = df.format(c.getTime()).toString()
        if (formattedDate[0].toString() == "0") {
            formattedDate = formattedDate.replaceFirst("0", "")
        }
        return formattedDate
    }

    fun getTodaysDateMonth(): String {
        val c: Calendar = Calendar.getInstance()
        val df: SimpleDateFormat = SimpleDateFormat("MM")
        var formattedDate: String = df.format(c.getTime()).toString()
        if (formattedDate[0].toString() == "0") {
            formattedDate = formattedDate.replaceFirst("0", "")
        }
        return formattedDate
    }

    fun getTodaysDateYear(): String {
        val c: Calendar = Calendar.getInstance()
        val df: SimpleDateFormat = SimpleDateFormat("yyyy")
        val formattedDate: String = df.format(c.getTime()).toString()
        return formattedDate
    }

    fun pickMessage(): String {
        //TODO:add list of messages to pick random
        return "insurance message is not setted"
    }

}
