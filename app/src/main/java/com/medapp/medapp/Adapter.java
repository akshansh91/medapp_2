package com.medapp.medapp;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.medapp.medapp.config.ServerConfig;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class Adapter extends RecyclerView.Adapter<Adapter.MyViewHolder> {

    //Interface for click listener in side recycler View. . .
    public interface clickListener {
        void onItemClicked(String image);
    }

    private clickListener anInterface;

    private ArrayList<ArrayList<String>> individual;
    private ArrayList<ArrayList<String>> family;
    private LayoutInflater mInflater;
    private Context myContext;
    private String FamilyID = "0";
    private String responseString;
    private String sharableImgLink;
    int size;
    final boolean[] visible = {false};
    ItemDecor decor;

    Adapter(Context context, ArrayList<ArrayList<String>> data, clickListener anInterface) {
        this.mInflater = LayoutInflater.from(context);
        this.individual = data;
        this.myContext = context;
        this.anInterface = anInterface;
    }

    Adapter(Context context, ArrayList<ArrayList<String>> data, String familyId, clickListener anInterface) {
        this.mInflater = LayoutInflater.from(context);
        this.family = data;
        this.myContext = context;
        this.anInterface = anInterface;
        FamilyID = familyId;
    }

    @NonNull
    @Override
    public Adapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.holder_blueprint, parent, false);
        /*StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);*/
        return new MyViewHolder(view);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(@NonNull final Adapter.MyViewHolder holder, int position) {
        if (FamilyID.equals("0")) {

            size = individual.size();

            String description = individual.get(position).get(2);
            String itemNum = String.valueOf(position + 1);
            final String imgName = individual.get(position).get(4);
            String date = individual.get(position).get(6);

            if (imgName.equals("noFile.jpg")) {
                holder.cardView.setBackgroundColor(Color.TRANSPARENT);
                //holder.sideMenu.setVisibility(View.GONE);
                //Uri uri = Uri.parse(new ServerConfig().getRootAddress() + "upload/" + imgName + "?notCache=" + imgName);
                holder.imageView.setImageResource(R.drawable.no_docs_added_1);
                /*Picasso.with(myContext)
                        .load(R.drawable.no_docs)
                        .fit()
                        .into(holder.imageView);*/
                //holder.imageView.setImageURI(uri);

                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                params.addRule(Gravity.CENTER);

                holder.imageView.setLayoutParams(params);
            } else {
                holder.bind(individual.get(position).get(4), anInterface);
                holder.imageNum.setText(itemNum);
                holder.description.setText(description);
                //holder.date.setText(date);
                //Uri uri = Uri.parse(new ServerConfig().getRootAddress() + "upload/" + imgName + "?notCache=" + imgName);
                Picasso.with(myContext)
                        .load(new ServerConfig().getRootAddress() + "upload/" + imgName + "?notCache=" + imgName)
                        .fit()
                        .into(holder.imageView);
                //holder.imageView.setImageURI(uri);

                /*holder.imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(myContext, ImageViewer.class);
                        intent.putExtra("imageName", imgName);
                        myContext.startActivity(intent);
                        ((Activity) myContext).overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    }
                });*/

                /*holder.imageView.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        if (holder.sideMenu.getVisibility() == View.VISIBLE) {
                            holder.sideMenu.setVisibility(View.GONE);
                        } else {
                            holder.sideMenu.setVisibility(View.VISIBLE);
                        }

                        holder.deleteBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //Toast.makeText(myContext, "We'll delete:" + imgName, Toast.LENGTH_SHORT).show();
                                if (imgName.equals("noFile.jpg")) {
                                    Toast.makeText(myContext, "This image can't be deleted", Toast.LENGTH_SHORT).show();
                                } else {
                                    deleteImageAPIRequest(individual.get(position).get(4));
                                }
                            }
                        });
                        holder.shareBtns.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //Toast.makeText(myContext, "We'll share:" + imgName, Toast.LENGTH_SHORT).show();
                                //String urlImg = sharableImgLink + individual.get(position).get(4);

                                if (imgName.equals("noFile.jpg")) {
                                    Toast.makeText(myContext, "This image can't be shared", Toast.LENGTH_SHORT).show();
                                } else {
                                    generateTokenforImage(individual.get(position).get(4));

                                    Intent i = new Intent(myContext, shareImage_Menu.class);
                                    i.putExtra("imageName", individual.get(position).get(4));
                                    i.putExtra("imageLink", sharableImgLink);
                                    myContext.startActivity(i);
                                }
                            }
                        });
                    }
                });*/
            }
        } else {
            size = family.size();

            String description = family.get(position).get(2);
            String itemNum = String.valueOf(position + 1);
            String imgName = family.get(position).get(4);
            String date = family.get(position).get(6);

            if (imgName.equals("noFile.jpg")) {
                holder.cardView.setBackgroundColor(Color.TRANSPARENT);
                //holder.sideMenu.setLayoutParams(new RelativeLayout.LayoutParams(0, 0));
                //holder.sideMenu.setVisibility(View.GONE);
                //holder.descTitle.setVisibility(View.GONE);
                holder.description.setVisibility(View.GONE);
                //holder.linearLayout.setVisibility(View.INVISIBLE);
                //Uri uri = Uri.parse(new ServerConfig().getRootAddress() + "upload/" + imgName + "?notCache=" + imgName);
                holder.imageView.setImageResource(R.drawable.no_docs_added_1);
                /*Picasso.with(myContext)
                        .load(R.drawable.no_docs)
                        .fit()
                        .into(holder.imageView);*/
                //holder.imageView.setImageURI(uri);

                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                params.addRule(Gravity.CENTER);

                holder.imageView.setLayoutParams(params);
            } else {
                holder.bind(family.get(position).get(4), anInterface);
                holder.imageNum.setText(itemNum);
                holder.description.setText(description);
                //holder.date.setText(date);

                //Uri uri = Uri.parse(new ServerConfig().getRootAddress() + "upload/" + imgName + "?notCache=" + imgName);
                Picasso.with(myContext)
                        .load(new ServerConfig().getRootAddress() + "upload/" + imgName + "?notCache=" + imgName)
                        .fit()
                        .into(holder.imageView);
                //holder.imageView.setImageURI(uri);
            }

            /*holder.imageNum.setText(itemNum);
            holder.imageTitle.setText(data);

            Picasso.with(myContext)
                    .load(new ServerConfig().getRootAddress() + "upload/" + family.get(position).get(4) + "?notCache=" + family.get(position).get(4))
                    .fit()
                    .into(holder.imageView);
            */

            /*holder.imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (holder.sideMenu.getVisibility() == View.VISIBLE) {
                        holder.sideMenu.setVisibility(View.GONE);
                    } else {
                        holder.sideMenu.setVisibility(View.VISIBLE);
                    }

                    holder.deleteBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //Toast.makeText(myContext, "We'll delete:" + family.get(position).get(4), Toast.LENGTH_SHORT).show();
                            if (family.get(position).get(4).equals("noFile.jpg")) {
                                Toast.makeText(myContext, "This image can't be deleted", Toast.LENGTH_SHORT).show();
                            } else {
                                deleteImageAPIRequest(family.get(position).get(4));
                            }
                        }
                    });
                    holder.shareBtns.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //Toast.makeText(myContext, "We'll share:" + family.get(position).get(4), Toast.LENGTH_SHORT).show();
                            //String urlImg = new ServerConfig().getSharableImageLink() + family.get(position).get(4);

                            if (family.get(position).get(4).equals("noFile.jpg")) {
                                Toast.makeText(myContext, "This image can't be deleted", Toast.LENGTH_SHORT).show();
                            } else {
                                generateTokenforImage(family.get(position).get(4));
                                Intent i = new Intent(myContext, shareImage_Menu.class);
                                i.putExtra("imageName", family.get(position).get(4));
                                i.putExtra("imageLink", sharableImgLink);
                                myContext.startActivity(i);
                            }
                        }
                    });
                }
            });*/
        }
    }

    private void generateTokenforImage(String imageName) {

        //generateToken token = new generateToken();
        //token.execute(imageName);

        //inside background . .
        Log.d("API_Call_Token", "inside generateTokenforImage");

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build();

        HttpUrl.Builder urlBuilder = Objects.requireNonNull(HttpUrl.parse(new ServerConfig().generateToken_n_TimeStamp())).newBuilder();
        urlBuilder.addQueryParameter("imgName", imageName);

        String url = urlBuilder.build().toString();

        Request request = new Request.Builder().url(url).build();

        try {
            Response response = client.newCall(request).execute();
            if (response.body() != null) {
                String res = response.body().string().replace("'", "");
                responseString = res;
                sharableImgLink = res;
            }
            //Log.d("API_Call_Token", "Image Name:" + imageName);
            Log.d("API_Call_Token", "responseString: " + responseString);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //on post execute . . .
        if (!responseString.equals("dataMissing")) {
            if (!responseString.equals("serverError")) {
                Log.d("API_Call_Token", "URL:" + responseString);
            } else {
                Toast.makeText(myContext, myContext.getString(R.string.server_error), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(myContext, myContext.getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
        }
    }


    private void deleteImageAPIRequest(String s) {

        //delete delete = new delete();
        //delete.execute(s);
        //inside background
        Log.d("API_Call", "inside makeHistoryApiRequest");
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build();

        HttpUrl.Builder urlBuilder = Objects.requireNonNull(HttpUrl.parse(new ServerConfig().getDeleteDocApi())).newBuilder();
        urlBuilder.addQueryParameter("docName", s);

        String url = urlBuilder.build().toString();

        Request request = new Request.Builder().url(url).build();

        try {
            Response response = client.newCall(request).execute();
            if (response.body() != null) {
                responseString = response.body().string();
            }
            Log.d("API_Call_Del_Img", "Image Name: " + s);
            Log.d("API_Call_Del_Img", "responseString: " + responseString);
        } catch (IOException e) {
            e.printStackTrace();
        }
        // inside onPost
        if (!responseString.equals("dataMissing")) {
            if (!responseString.equals("serverError")) {
                if (responseString.equals("ok")) {
                    Toast.makeText(myContext, R.string.this_document_is_deleted, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(myContext, myContext.getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(myContext, myContext.getString(R.string.server_error), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(myContext, myContext.getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public int getItemCount() {
        int size;
        if (FamilyID.equals("0")) {
            size = individual.size();
        } else {
            size = family.size();
        }
        return size;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView description, imageNum;
        ImageView imageView;
        //LinearLayout sideMenu;
        CardView cardView;

        //de.hdodenhof.circleimageview.CircleImageView deleteBtn, shareBtns;

        MyViewHolder(View itemView) {
            super(itemView);

            //descTitle = itemView.findViewById(R.id.textView4);
            //date = itemView.findViewById(R.id.date);
            cardView = itemView.findViewById(R.id.myCards);
            description = itemView.findViewById(R.id.description);
            imageNum = itemView.findViewById(R.id.number);
            imageView = itemView.findViewById(R.id.myDocs);
            //sideMenu = itemView.findViewById(R.id.sideMenu);
            //deleteBtn = itemView.findViewById(R.id.delBtn);
            //shareBtns = itemView.findViewById(R.id.shareBtn)

        }

        public void bind(final String item, final clickListener listener) {
            //Picasso.with(itemView.getContext()).load(item.imageUrl).into(image);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClicked(item);
                }
            });
        }


    }

    @SuppressLint("StaticFieldLeak")
    class generateToken extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... strings) {

            String imageName = strings[0];

            Log.d("API_Call_Token", "inside generateTokenforImage");

            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build();

            HttpUrl.Builder urlBuilder = Objects.requireNonNull(HttpUrl.parse(new ServerConfig().generateToken_n_TimeStamp())).newBuilder();
            urlBuilder.addQueryParameter("imgName", imageName);

            String url = urlBuilder.build().toString();

            Request request = new Request.Builder().url(url).build();

            try {
                Response response = client.newCall(request).execute();
                if (response.body() != null) {
                    String res = response.body().string().replace("'", "");
                    responseString = res;
                    sharableImgLink = res;
                }
                //Log.d("API_Call_Token", "Image Name:" + imageName);
                Log.d("API_Call_Token", "responseString: " + responseString);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return responseString;
        }

        @Override
        protected void onPostExecute(String s) {
            Log.d("API_Call_Token", "Response String: " + s);
            if (!s.equals("dataMissing")) {
                if (!s.equals("serverError")) {
                    Log.d("API_Call_Token", "URL:" + s);
                } else {
                    Toast.makeText(myContext, myContext.getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(myContext, myContext.getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    class delete extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {

            String s = strings[0];

            Log.d("API_Call", "inside makeHistoryApiRequest");
            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build();

            HttpUrl.Builder urlBuilder = Objects.requireNonNull(HttpUrl.parse(new ServerConfig().getDeleteDocApi())).newBuilder();
            urlBuilder.addQueryParameter("docName", s);

            String url = urlBuilder.build().toString();

            Request request = new Request.Builder().url(url).build();

            try {
                Response response = client.newCall(request).execute();
                if (response.body() != null) {
                    responseString = response.body().string();
                }
                Log.d("API_Call_Del_Img", "Image Name: " + s);
                Log.d("API_Call_Del_Img", "responseString: " + responseString);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return responseString;
        }

        @Override
        protected void onPostExecute(String s) {
            if (!s.equals("dataMissing")) {
                if (!s.equals("serverError")) {
                    if (s.equals("ok")) {
                        Toast.makeText(myContext, myContext.getString(R.string.this_document_is_deleted), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(myContext, myContext.getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(myContext, myContext.getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(myContext, myContext.getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
            }
        }
    }
}

