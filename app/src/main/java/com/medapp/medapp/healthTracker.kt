package com.medapp.medapp

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.PopupMenu
import android.widget.TextView
import com.facebook.drawee.backends.pipeline.Fresco
import com.medapp.medapp.utils.left_drawer
import android.widget.Toast


class healthTracker : AppCompatActivity() {
    private var healthTracker_recyclerview: RecyclerView? = null
    private var result_list_for_healthTracker: ArrayList<healthTracker_store> = ArrayList<healthTracker_store>()
    private var healthTracker_adapter_holder: RecyclerView.Adapter<healthTracker_adapter.NumberViewHolder>? = null
    val TAG: String? = "Activity_Name"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_health_tracker)
        findViewById<Toolbar>(R.id.healthTrackerToolBar).title = getString(R.string.health_tracker)

        Log.d(TAG, "Inside healthTracker")

        //navigation drawer
        left_drawer(this, this@healthTracker, findViewById(R.id.healthTrackerToolBar)!!).createNavigationDrawer()

        attach_healthTracker_adapter_to_recyclerview()

        result_list_for_healthTracker.add(healthTracker_store(getString(R.string.bp)))
        result_list_for_healthTracker.add(healthTracker_store(getString(R.string.sugar)))
        result_list_for_healthTracker.add(healthTracker_store(getString(R.string.weight)))
        result_list_for_healthTracker.add(healthTracker_store(getString(R.string.sleep)))
        result_list_for_healthTracker.add(healthTracker_store(getString(R.string.power_of_glasses)))
        result_list_for_healthTracker.add(healthTracker_store(getString(R.string.pulse)))
        result_list_for_healthTracker.add(healthTracker_store(getString(R.string.calories)))
        result_list_for_healthTracker.add(healthTracker_store(getString(R.string.height)))

        healthTracker_adapter_holder!!.notifyDataSetChanged()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        var intent = Intent(this@healthTracker, Main2Activity::class.java)
        startActivity(intent)
        finish()
    }

    //profile recyclerview
    fun attach_healthTracker_adapter_to_recyclerview() {
        val healthTracker_recyclerView: RecyclerView = findViewById(R.id.healthTrackerRecyclerView)
        healthTracker_recyclerview = healthTracker_recyclerView
        val layoutManager = LinearLayoutManager(applicationContext)
        healthTracker_recyclerView.layoutManager = layoutManager
        healthTracker_recyclerView.setHasFixedSize(false)
        healthTracker_recyclerView.addItemDecoration(DividerItemDecoration(this@healthTracker, DividerItemDecoration.VERTICAL))
        healthTracker_recyclerView.isNestedScrollingEnabled = false
        healthTracker_adapter_holder = healthTracker_adapter()
        healthTracker_recyclerView.adapter = healthTracker_adapter_holder
    }

    inner class healthTracker_store(optionName: String) {
        private var inner_optionName: String = optionName

        fun get_optionName(): String {
            return inner_optionName
        }
    }

    private inner class healthTracker_adapter : RecyclerView.Adapter<healthTracker_adapter.NumberViewHolder>() {

        override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): NumberViewHolder {
            val context = viewGroup.context
            val layoutIdForListItem = R.layout.health_tracker_blueprint
            val inflater = LayoutInflater.from(applicationContext)
            val shouldAttachToParentImmediately = false

            val view = inflater.inflate(layoutIdForListItem, viewGroup, shouldAttachToParentImmediately)
            val viewHolder = NumberViewHolder(view)

            return viewHolder
        }

        override fun onBindViewHolder(holder: NumberViewHolder, position: Int) {
            val current_object = result_list_for_healthTracker.get(position)
            holder.bind(current_object)
        }

        override fun getItemCount(): Int {
            return result_list_for_healthTracker.size
        }

        inner class NumberViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            private var optionTextView: TextView? = null
            private var optionIcon: ImageView? = null
            private var viewButton: Button? = null
            private var recordButton: Button? = null
            private var menuBtn: ImageView? = null

            init {
                optionTextView = itemView.findViewById(R.id.healthTrackerOptionTextView)
                optionIcon = itemView.findViewById(R.id.optionIcon)
                viewButton = itemView.findViewById(R.id.viewButton)
                recordButton = itemView.findViewById(R.id.recordButton)
                menuBtn = itemView.findViewById(R.id.menuBtn)
            }

            @SuppressLint("SetTextI18n")
            fun bind(current_object: healthTracker_store) {
                var temp: String = current_object.get_optionName()
                //optionTextView!!.text = current_object.get_optionName()
                optionTextView!!.text = temp.substring(0, 1).toUpperCase() + temp.substring(1).toLowerCase()
                optionIcon!!.setImageResource(R.drawable.health_tracker_icon)

                viewButton!!.setOnClickListener {
                    val i = Intent(this@healthTracker, ViewHealthTrackerData::class.java)
                    i.putExtra("optionName", current_object.get_optionName())
                    startActivity(i)
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                }
                recordButton!!.setOnClickListener {
                    val i = Intent(this@healthTracker, addHealthTrackerData::class.java)
                    i.putExtra("optionName", current_object.get_optionName())
                    startActivity(i)
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                }
                menuBtn!!.setOnClickListener {
                    var popupMenu = PopupMenu(this@healthTracker, menuBtn)
                    popupMenu.menuInflater.inflate(R.menu.menu_health_tracker, popupMenu.menu)

                    popupMenu.setOnMenuItemClickListener { item ->
                        if (item.itemId == R.id.recordBtn) {
                            val i = Intent(this@healthTracker, addHealthTrackerData::class.java)
                            i.putExtra("optionName", current_object.get_optionName())
                            startActivity(i)
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                        } else if (item.itemId == R.id.viewBtn) {
                            val i = Intent(this@healthTracker, ViewHealthTrackerData::class.java)
                            i.putExtra("optionName", current_object.get_optionName())
                            startActivity(i)
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                        }
                        true
                    }
                    popupMenu.show()
                }
            }
        }
    }
}
