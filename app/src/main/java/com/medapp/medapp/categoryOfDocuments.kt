package com.medapp.medapp

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Button
import android.widget.Spinner
import java.util.*

class categoryOfDocuments : AppCompatActivity() {
    val TAG: String? = "Activity_Name"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_category_of_documents)

        try {
            Log.d("Check_categoryOfDocs", "Inside onCreate()")
            Log.d("Picking_Docs", "Inside categoryOfDocuments activity . . ")

            Log.d(TAG, "Inside categoryOfDocuments")
            setTitle("Step 3")
            //overridePendingTransition(R.xml.slide_in, R.xml.slide_out)

            //onclickNextButton()

            openImagePreview("Medical")

        } catch (e: Exception) {
            Log.d("Check_categoryOfDocs", "Inside onCreate()..Exception: $e")
        }
    }

    fun getStringByLocal(context: Activity, id: Int, locale: String): Array<out String>? {
        var configuration: Configuration = Configuration(getResources().getConfiguration())
        configuration.setLocale(Locale(locale))
        return context.createConfigurationContext(configuration).getResources().getStringArray(id)
    }

    fun onclickNextButton() {
        findViewById<Button>(R.id.nextButton).setOnClickListener {

            val roozArrayValue = getStringByLocal(this@categoryOfDocuments,
                    R.array.documentCategory, "en")

            //val selectedItem = roozArrayValue!![position]

            val mySpinner = findViewById(R.id.categorySpinner) as Spinner
            val category = mySpinner.selectedItem.toString()
            openImagePreview(category)
        }
    }

    fun openImagePreview(category: String) {
        try {
            Log.d("Check_categoryOfDocs", "Inside openImagePreview()")
            Log.d("imageGallery__", "Inside categoryOfDocument.....openImagePreview ")
            val intent = intent
            val b_args = intent.getBundleExtra("BUNDLE")
            val _object = (b_args.getSerializable("ARRAYLIST")) as java.io.Serializable
            val typeOfDocument = intent.getStringExtra("typeOfDocument")

            val loginInfo = getSharedPreferences("loginInfo", Context.MODE_PRIVATE)

            /*
            if(loginInfo.contains("familyId") == true && loginInfo.getString("familyId",null) != "0") {
                val i = Intent(this, pickFamilyMember::class.java)
                val args = Bundle()
                args.putSerializable("ARRAYLIST", _object)
                i.putExtra("BUNDLE", args)
                i.putExtra("typeOfDocument", typeOfDocument)
                i.putExtra("categoryOfDocument", category)
                startActivity(i)
            }else{
                val i = Intent(this, previewImages::class.java)
                val args = Bundle()
                args.putSerializable("ARRAYLIST", _object)
                i.putExtra("BUNDLE", args)
                i.putExtra("typeOfDocument", typeOfDocument)
                i.putExtra("categoryOfDocument", category)
                startActivity(i)
            }
            */

            val i = Intent(this, previewImages::class.java)
            val args = Bundle()
            args.putSerializable("ARRAYLIST", _object)
            i.putExtra("BUNDLE", args)
            i.putExtra("typeOfDocument", typeOfDocument)
            i.putExtra("categoryOfDocument", category)
            startActivity(i)
            //overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            finish()
        } catch (e: Exception) {
            Log.d("imageGallery__", "Inside categoryOfDocument.....openImagePreview")
        }
    }

    override fun onBackPressed() {
        val intent = Intent(this@categoryOfDocuments, Main2Activity::class.java)
        startActivity(intent)
        finish()
    }
}
