package com.medapp.medapp;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.medapp.medapp.config.ServerConfig;
import com.medapp.medapp.utils.left_drawer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class sharedWithMe extends AppCompatActivity {

    SharedPreferences loginInfo;
    RecyclerView recyclerView;
    RecyclerView.LayoutManager manager;
    String familyId, phoneNumber, responseString;

    ArrayList<ArrayList<String>> sharedDocs;
    Adapter_showSharedImages adapter_showSharedImages;
    OkHttpClient client;
    HttpUrl.Builder urlBuilder;

    Response response;
    Request request;

    Toolbar toolbar;

    ItemDecor decor;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared_with_me);

        toolbar = findViewById(R.id.toolbar_sharedWithMe);
        new left_drawer(this, sharedWithMe.this, toolbar).createNavigationDrawer();

        try {
            decor = new ItemDecor(sharedWithMe.this, R.dimen.item_offset);

            loginInfo = getSharedPreferences("loginInfo", Context.MODE_PRIVATE);
            familyId = loginInfo.getString("familyId", null);
            phoneNumber = loginInfo.getString("phoneNumber", "");

            sharedDocs = new ArrayList<>();

            Log.d("API_Call_getSharedDocs", "ID: " + familyId);
            Log.d("API_Call_getSharedDocs", "Phone number: " + phoneNumber);

            recyclerView = findViewById(R.id.showSharedDocs);
            recyclerView.addItemDecoration(decor);
            manager = new LinearLayoutManager(sharedWithMe.this);
            recyclerView.setLayoutManager(manager);

            getSharedDocumentList(phoneNumber);
            Log.d("API_Call_getSharedDocs", String.valueOf(sharedDocs));
        } catch (Exception e) {
            Log.d("sharedWithMe__", "onCreate()....Exception: " + e);
        }
    }

    private void getSharedDocumentList(String phoneNumber) {

        getShared shared = new getShared();
        shared.execute(phoneNumber);
    }

    private void processResult(String responseString) {

        Log.d("sharedWithMe__", "Inside processResult....");

        String[] chunkOfData = responseString.split("___");

        ArrayList<String> list;
        int counter = 1;
        int size = chunkOfData.length;
        Log.d("API_Call", "size: " + size);

        try {
            while (counter < size) {

                list = new ArrayList<>();

                String blockOfData = chunkOfData[counter];
                Log.d("sharedWithMe__", "blockOfData: " + blockOfData);
                String[] array = blockOfData.trim().split("\\*", -1);
                Log.d("sharedWithMe__", "array: " + Arrays.toString(array));

                String documentName = array[0];
                list.add(documentName);

                String description = array[1];
                list.add(description);

                String category = array[2];
                list.add(category);

                String docSharedBy = array[3];
                list.add(docSharedBy);

                String type = array[4];
                list.add(type);

                String specific_category = array[5];
                list.add(specific_category);

                Log.d("sharedWithMe__", "Individual Lists:");
                Log.d("sharedWithMe__", "List" + counter + ":" + list);

                sharedDocs.add(list);
                counter++;
            }
        } catch (Exception e) {
            Log.d("sharedWithMe__", "Loop error: " + e);
        }
        adapter_showSharedImages = new Adapter_showSharedImages(sharedWithMe.this, sharedDocs);
        recyclerView.setAdapter(adapter_showSharedImages);
    }
    //0-imgName,1-description,2-category,3-sharedBy,4-type,5-specific_category

    @SuppressLint("StaticFieldLeak")
    class getShared extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            String phoneNumber = strings[0];

            Log.d("API_Call_getSharedDocs", "inside getSharedDocumentList");
            client = new OkHttpClient.Builder().connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build();

            urlBuilder = Objects.requireNonNull(HttpUrl.parse(new ServerConfig().getSharedDocs())).newBuilder();
            urlBuilder.addQueryParameter("contactNumber", phoneNumber);

            String url = urlBuilder.build().toString();

            request = new Request.Builder().url(url).build();

            try {
                response = client.newCall(request).execute();
                if (response.body() != null) {
                    responseString = response.body().string();
                    Log.d("sharedWithMe__", "onBackGr.... responseString: " + responseString);
                }
            } catch (Exception e) {
                Log.d("sharedWithMe__", "onBackGround: error: " + e);
            }
            return responseString;
        }

        @Override
        protected void onPostExecute(String s) {
            if (!s.equals("no_images")) {
                processResult(s);
            } else {
                ArrayList<String> l = new ArrayList<>();
                l.add("noDocsShared.jpg");
                l.add("");
                l.add("");
                l.add("");
                l.add("");
                l.add("");
                sharedDocs.add(l);
            }
        }
    }
}
