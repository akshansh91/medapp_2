package com.medapp.medapp

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Button

class typeOfDocument : AppCompatActivity() {
    val TAG: String? = "Activity_Name"

    override fun onCreate(savedInstanceState: Bundle?) {
        try {
            Log.d("Picking_Docs", "Inside typeOfDocument activity . . ")
            super.onCreate(savedInstanceState)
            setContentView(R.layout.activity_type_of_document)
            setTitle("Step 2")
            Log.d(TAG, "Inside typeOfDocument")
            //overridePendingTransition(R.xml.slide_in, R.xml.slide_out)

            skipThisStep()

            onclickPrinted()
            onclickHandWritten()
            onclickBoth()
        } catch (e: Exception) {
            Log.d("Picking_Docs", "Inside typeOfDocument activity Exception: $e")
        }
    }

    fun skipThisStep() {
        openSelectCategory("notSelected")
    }

    fun onclickPrinted() {
        findViewById<Button>(R.id.printedButton).setOnClickListener {
            openSelectCategory("printed")
        }
    }

    fun onclickHandWritten() {
        findViewById<Button>(R.id.HandwrittenButton).setOnClickListener {
            openSelectCategory("handwritten")
        }
    }

    fun onclickBoth() {
        findViewById<Button>(R.id.bothButton).setOnClickListener {
            openSelectCategory("both")
        }
    }

    fun openSelectCategory(typeOfDocument: String) {
        try {
            Log.d("imageGallery__", "Inside typeOfDocument.....openSelectCategory ")
            val intent = intent
            val b_args = intent.getBundleExtra("BUNDLE")
            val _object = (b_args.getSerializable("ARRAYLIST")) as java.io.Serializable

            val i = Intent(this, categoryOfDocuments::class.java)
            val args = Bundle()
            args.putSerializable("ARRAYLIST", _object)
            i.putExtra("BUNDLE", args)
            i.putExtra("typeOfDocument", typeOfDocument)
            startActivity(i)
            finish()
        } catch (e: Exception) {
            Log.d("imageGallery__", "Inside typeOfDocument.....openSelectCategory Exception: $e")
        }
    }

}
