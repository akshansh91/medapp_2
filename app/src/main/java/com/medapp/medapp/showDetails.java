package com.medapp.medapp;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.medapp.medapp.config.ServerConfig;
import com.medapp.medapp.utils.left_drawer;
import com.muddzdev.styleabletoastlibrary.StyleableToast;

import java.io.IOException;
import java.util.Locale;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class showDetails extends AppCompatActivity {

    SharedPreferences loginInfo;

    OkHttpClient client;
    HttpUrl.Builder urlBuilder;

    Response response;
    Request request;

    String phoneNumber, familyId, responseString, uploadedBy, uploadedFor, type, category,
            specific_category, description, familyMemberName, imageName, responseString_delImg, sharableImageLink,
            responseString_Token, pickedContactName, pickedContactNumber;

    ImageView pic, fullPic;
    TextView desc, by_, categ, spec_categ, type_;
    EditText for__;

    ConstraintLayout layout;

    ImageView deleteBtn, shareBtn, pickFromContact;

    RelativeLayout showFullPic;

    Toolbar toolbar;

    Spinner typeSpinner, categorySpinner, specificCategorySpinner;

    String[] docTypeArray, docCategoryArray, docSCategoryArray;

    LinearLayout categoryLayout, typeLayout;

    ConstraintLayout specificCategoryLayout;

    AlertDialog infoDialog;

    LayoutInflater layoutInflater;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.bottom_down, R.anim.stay);
    }

    public String[] getStringByLocal(Activity context, Integer id, String locale) {
        Configuration configuration = new Configuration(getResources().getConfiguration());
        configuration.setLocale(new Locale(locale));
        return context.createConfigurationContext(configuration).getResources().getStringArray(id);
    }

    public String getStringByLocal_(Activity context, Integer id, String locale) {
        Configuration configuration = new Configuration(getResources().getConfiguration());
        configuration.setLocale(new Locale(locale));
        return context.createConfigurationContext(configuration).getResources().getString(id);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_details);

        Fresco.initialize(showDetails.this);

        pic = findViewById(R.id.pic);
        fullPic = findViewById(R.id.fullImage);
        desc = findViewById(R.id.description);
        by_ = findViewById(R.id.uploadedBy);
        for__ = findViewById(R.id.uploadedFor);
        categ = findViewById(R.id.category);
        spec_categ = findViewById(R.id.categorySpecific);
        type_ = findViewById(R.id.type);
        deleteBtn = findViewById(R.id.delBtn);
        shareBtn = findViewById(R.id.shareBtn);
        showFullPic = findViewById(R.id.reveal_view);
        toolbar = findViewById(R.id.showDetailsToolbar);
        typeSpinner = findViewById(R.id.spinnerType);
        categorySpinner = findViewById(R.id.spinnerCategory);
        specificCategorySpinner = findViewById(R.id.spinnerSpecificCategory);
        pickFromContact = findViewById(R.id.pickFromContact);
        layout = findViewById(R.id.dataView);

        categoryLayout = findViewById(R.id.layout_category);
        typeLayout = findViewById(R.id.layout_type);
        specificCategoryLayout = findViewById(R.id.layout_categorySpecific);

        layoutInflater = LayoutInflater.from(showDetails.this);

        infoDialog = new AlertDialog.Builder(this).create();

        boolean isFirstRunHint = getSharedPreferences("PREFERENCE_HINT", MODE_PRIVATE)
                .getBoolean("isFirstRunHint", true);

        if (isFirstRunHint) {
            Log.d("isFirstRunHint", "Loading for the first time. . .");
            //show Dialog for just few secs...
            showInfoDialog();
            getSharedPreferences("PREFERENCE_HINT", MODE_PRIVATE).edit()
                    .putBoolean("isFirstRunHint", false).apply();
        } else {
            Log.d("isFirstRunHint", "Loading for the second time. . .");
        }

        new left_drawer(this, showDetails.this, toolbar).createNavigationDrawer();

        docTypeArray = getStringByLocal(showDetails.this, R.array.typeOfDocument, "en");
        docCategoryArray = getStringByLocal(showDetails.this, R.array.documentCategory_2, "en");
        docSCategoryArray = getStringByLocal(showDetails.this, R.array.specificCategoryItems_2, "en");

        loginInfo = getSharedPreferences("loginInfo", Context.MODE_PRIVATE);
        familyId = loginInfo.getString("familyId", null);
        phoneNumber = loginInfo.getString("phoneNumber", "");

        Intent i = getIntent();

        imageName = i.getStringExtra("imageName");

        DisplayImage displayImage = new DisplayImage();
        displayImage.execute(imageName);

        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imageName.equals("noFile.jpg")) {
                    Toast.makeText(showDetails.this, R.string.this_image_cant_be_deleted, Toast.LENGTH_SHORT).show();
                } else {
                    deleteImage deleteImage = new deleteImage();
                    deleteImage.execute(imageName);
                }
            }
        });

        fullPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (showFullPic.getVisibility() == View.GONE) {
                    showFullPic.setVisibility(View.VISIBLE);
                    fullPic.setVisibility(View.VISIBLE);
                } else {
                    showFullPic.setVisibility(View.GONE);
                    fullPic.setVisibility(View.GONE);
                }
            }
        });

        pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Uri uri = Uri.parse(new ServerConfig().getRootAddress() + "upload/" + imageName + "?notCache=" + imageName);
                fullPic.setImageURI(uri);

                if (showFullPic.getVisibility() == View.GONE) {
                    showFullPic.setVisibility(View.VISIBLE);
                    fullPic.setVisibility(View.VISIBLE);
                } else {
                    showFullPic.setVisibility(View.GONE);
                    fullPic.setVisibility(View.GONE);
                }
            }
        });

        shareBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imageName.equals("noFile.jpg")) {
                    Toast.makeText(showDetails.this, R.string.this_image_cant_be_shared, Toast.LENGTH_SHORT).show();
                } else {
                    shareToken token = new shareToken();
                    token.execute(imageName);
                }
            }
        });

        layout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (for__.isFocused()) {
                        Rect outRect = new Rect();
                        for__.getGlobalVisibleRect(outRect);
                        if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                            for__.clearFocus();
                            InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                        }
                    }
                }
                return false;
            }
        });

        for__.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    String text = String.valueOf(for__.getText());
                    updateGivenField("familyMemberPhoneNumber", text);
                    return true;
                }
                return false;
            }
        });

        //spinners n stuff
        typeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTypeSelectionDialog();
            }
        });

        categoryLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCategorySelectionDialog();
            }
        });

        specificCategoryLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openSpecificCategorySelectionDialog();
            }
        });

        pickFromContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pickContactIntent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(pickContactIntent, 1);
            }
        });
    }

    private void showInfoDialog() {
        View promptView = layoutInflater.inflate(R.layout.info_dialog_layout, null);
        infoDialog.setView(promptView);
        infoDialog.setCancelable(true);
        infoDialog.show();

        final Timer t = new Timer();
        t.schedule(new TimerTask() {
            @Override
            public void run() {
                infoDialog.dismiss();
                t.cancel();
            }
        }, 4000);
    }

    private void openTypeSelectionDialog() {

        ConstraintLayout layout = (ConstraintLayout) layoutInflater.inflate(R.layout.type_picker_layout, null);
        final NumberPicker typePicker = layout.findViewById(R.id.typePicker);

        int length = docTypeArray.length;

        typePicker.setMinValue(0);
        typePicker.setMaxValue(length - 1);
        typePicker.setDisplayedValues(docTypeArray);
        typePicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);

        final AlertDialog alertDialog = new AlertDialog.Builder(this).setPositiveButton("Submit", null)
                .setNegativeButton("Cancel", null)
                .setView(layout)
                .setCancelable(false)
                .create();

        alertDialog.show();

        alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int value2 = typePicker.getValue();
                updateGivenField("type", docTypeArray[value2]);
                alertDialog.dismiss();
            }
        });


        alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
    }


    private void openCategorySelectionDialog() {

        ConstraintLayout layout = (ConstraintLayout) layoutInflater.inflate(R.layout.category_picker_layout, null);
        final NumberPicker categoryPicker = layout.findViewById(R.id.categoryPicker);

        int length = docCategoryArray.length;

        categoryPicker.setMinValue(0);
        categoryPicker.setMaxValue(length - 1);
        categoryPicker.setDisplayedValues(docCategoryArray);
        categoryPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);

        final AlertDialog alertDialog = new AlertDialog.Builder(this).setPositiveButton("Submit", null)
                .setNegativeButton("Cancel", null)
                .setView(layout)
                .setCancelable(false)
                .create();

        alertDialog.show();

        alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int value2 = categoryPicker.getValue();
                updateGivenField("category", docCategoryArray[value2]);
                alertDialog.dismiss();
            }
        });


        alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
    }

    private void openSpecificCategorySelectionDialog() {

        ConstraintLayout layout = (ConstraintLayout) layoutInflater.inflate(R.layout.s_category_picker_layout, null);
        final NumberPicker s_categoryPicker = layout.findViewById(R.id.s_categoryPicker);

        int length = docSCategoryArray.length;

        s_categoryPicker.setMinValue(0);
        s_categoryPicker.setMaxValue(length - 1);
        s_categoryPicker.setDisplayedValues(docSCategoryArray);
        s_categoryPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);

        final AlertDialog alertDialog = new AlertDialog.Builder(this).setPositiveButton("Submit", null)
                .setNegativeButton("Cancel", null)
                .setView(layout)
                .setCancelable(false)
                .create();

        alertDialog.show();

        alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int value2 = s_categoryPicker.getValue();
                updateGivenField("specific_category", docSCategoryArray[value2]);
                alertDialog.dismiss();
            }
        });


        alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {

                Uri contactData = data.getData();
                Cursor c = getContentResolver().query(contactData, null, null, null, null);
                if (c.moveToFirst()) {

                    pickedContactName = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                    String contactId = c.getString(c.getColumnIndex(ContactsContract.Contacts._ID));

                    String hasPhone = c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

                    if (hasPhone.equals("1"))
                        hasPhone = "true";
                    else
                        hasPhone = "false";

                    if (java.lang.Boolean.parseBoolean(hasPhone)) {
                        Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId, null, null);
                        while (phones.moveToNext()) {
                            String number = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                            if (number.length() == 10) {
                                pickedContactNumber = number;
                            } else if (number.length() == 11) {
                                pickedContactNumber = number.substring(1);
                            } else if (number.length() == 12) {
                                pickedContactNumber = number.substring(2);
                            } else if (number.length() == 13) {
                                pickedContactNumber = number.substring(3);
                            }
                        }
                        phones.close();
                    }
                    Log.d("NumberSelected", "name: " + pickedContactName + "  number:" + pickedContactNumber);
                }
                //contactName.setText(pickedContactName);
                for__.setText(pickedContactNumber);
                c.close();
            }
        }
    }

    private void show_dialog(String fieldNameToShow, final String fieldName, int fieldName_id, String previousValue) {
        final String field_Name = getStringByLocal_(showDetails.this, fieldName_id, "en");

        if (!fieldNameToShow.equals(getStringByLocal_(showDetails.this, R.string.day, "en"))
                && !fieldNameToShow.equals(getStringByLocal_(showDetails.this, R.string.month, "en"))
                && !fieldNameToShow.equals(getStringByLocal_(showDetails.this, R.string.year, "en"))) {


            new MaterialDialog.Builder(this)
                    .title(getString(R.string.update))
                    .content(getString(R.string.update) + fieldNameToShow)
                    .inputType(InputType.TYPE_CLASS_TEXT)
                    .input(fieldNameToShow, previousValue, new MaterialDialog.InputCallback() {
                        @Override
                        public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
                            updateGivenField(fieldName, input.toString());
                        }
                    })
                    .positiveText(R.string.update)
                    .show();
        } else {
            new MaterialDialog.Builder(this)
                    .title(fieldNameToShow)
                    .content(getString(R.string.update_dob_here))
                    .inputType(InputType.TYPE_CLASS_TEXT)
                    .input(fieldNameToShow, previousValue, new MaterialDialog.InputCallback() {
                        @Override
                        public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
                            updateGivenField(field_Name, input.toString());
                        }
                    })
                    .positiveText(R.string.update)
                    .show();
        }

    }

    private void show_dialog_2(String fieldNameToShow, final String fieldName, int fieldName_id, String previousValue) {
        final String field_Name = getStringByLocal_(showDetails.this, fieldName_id, "en");

        if (!fieldNameToShow.equals(getStringByLocal_(showDetails.this, R.string.day, "en"))
                && !fieldNameToShow.equals(getStringByLocal_(showDetails.this, R.string.month, "en"))
                && !fieldNameToShow.equals(getStringByLocal_(showDetails.this, R.string.year, "en"))) {


            new MaterialDialog.Builder(this)
                    .title(getString(R.string.update))
                    .content(getString(R.string.update) + fieldNameToShow)
                    .inputType(InputType.TYPE_CLASS_TEXT)
                    .input(fieldNameToShow, previousValue, new MaterialDialog.InputCallback() {
                        @Override
                        public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
                            //updateGivenField_Number_Name(fieldName, input.toString());
                        }
                    })
                    .positiveText(R.string.update)
                    .show();
        } else {
            new MaterialDialog.Builder(this)
                    .title(fieldNameToShow)
                    .content(getString(R.string.update_dob_here))
                    .inputType(InputType.TYPE_CLASS_TEXT)
                    .input(fieldNameToShow, previousValue, new MaterialDialog.InputCallback() {
                        @Override
                        public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
                            //updateGivenField(field_Name, input.toString());
                        }
                    })
                    .positiveText(R.string.update)
                    .show();
        }

    }

    private void updateGivenField(String field_name, String toString) {
        UpdateFields updateFields = new UpdateFields();
        updateFields.execute(field_name, toString, imageName);
    }

    private void showMessageAfterSignUp(String msg_text) {
        StyleableToast st = new StyleableToast.Builder(showDetails.this)
                .text(msg_text)
                .textColor(Color.WHITE)
                .backgroundColor(Color.RED)
                .build();
        st.show();
    }

    @SuppressLint("StaticFieldLeak")
    class deleteImage extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            String imgName = strings[0];

            Log.d("API_Call", "inside makeHistoryApiRequest");
            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build();

            HttpUrl.Builder urlBuilder = Objects.requireNonNull(HttpUrl.parse(new ServerConfig().getDeleteDocApi())).newBuilder();
            urlBuilder.addQueryParameter("docName", imgName);

            String url = urlBuilder.build().toString();

            Request request = new Request.Builder().url(url).build();

            try {
                Response response = client.newCall(request).execute();
                if (response.body() != null) {
                    responseString_delImg = response.body().string();
                }
                Log.d("API_Call_Del_Img", "Image Name: " + imgName);
                Log.d("API_Call_Del_Img", "responseString: " + responseString_delImg);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return responseString_delImg;
        }

        @Override
        protected void onPostExecute(String s) {
            if (!s.equals("dataMissing")) {
                if (!s.equals("serverError")) {
                    if (s.equals("ok")) {
                        Toast.makeText(showDetails.this, R.string.document_deleted_successfully, Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(showDetails.this, history_pickFamilyMember.class);
                        startActivity(intent);
                        //overridePendingTransition(R.anim.fade_out, R.anim.fade_in);
                    } else {
                        Toast.makeText(showDetails.this, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(showDetails.this, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(showDetails.this, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    class shareToken extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            String imgName = strings[0];

            Log.d("API_Call_Token", "inside generateTokenforImage");

            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build();

            HttpUrl.Builder urlBuilder = Objects.requireNonNull(HttpUrl.parse(new ServerConfig().generateToken_n_TimeStamp())).newBuilder();
            urlBuilder.addQueryParameter("imgName", imgName);
            urlBuilder.addQueryParameter("sharedBy", phoneNumber);

            String url = urlBuilder.build().toString();

            Request request = new Request.Builder().url(url).build();

            try {
                Response response = client.newCall(request).execute();
                if (response.body() != null) {
                    String res = response.body().string().replace("'", "");
                    responseString_Token = res;
                    sharableImageLink = res;
                }
                //Log.d("API_Call_Token", "Image Name:" + imageName);
                Log.d("API_Call_Token", "responseString: " + responseString);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return responseString_Token;
        }

        @Override
        protected void onPostExecute(String s) {
            if (!s.equals("dataMissing")) {
                if (!s.equals("serverError")) {
                    Log.d("API_Call_Token", "URL:" + responseString_Token);
                    Intent i = new Intent(showDetails.this, shareImage_Menu.class);
                    i.putExtra("imageName", imageName);
                    i.putExtra("imageLink", s);
                    startActivity(i);
                    //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                } else {
                    Toast.makeText(showDetails.this, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(showDetails.this, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    class DisplayImage extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            String imgName = strings[0];

            client = new OkHttpClient.Builder().connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS).build();

            urlBuilder = Objects.requireNonNull(HttpUrl.parse(new ServerConfig().getImageInfo())).newBuilder();
            urlBuilder.addQueryParameter("imgName", imgName);

            String url = urlBuilder.build().toString();

            request = new Request.Builder().url(url).build();

            try {
                response = client.newCall(request).execute();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (response.body() != null) {
                try {
                    responseString = response.body().string();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return responseString;
        }

        @Override
        protected void onPostExecute(String s) {
            if (s.equals("dataMissing")) {
                Toast.makeText(showDetails.this, R.string.data_missing, Toast.LENGTH_SHORT).show();
            } else {
                String[] arr = s.split(",");
                uploadedBy = arr[0];
                uploadedFor = arr[1];
                type = arr[2];
                category = arr[3];
                specific_category = arr[4];
                description = arr[5];
                familyMemberName = arr[6];
            }

            Uri uri = Uri.parse(new ServerConfig().getRootAddress() + "upload/" + imageName + "?notCache=" + imageName);

            pic.setImageURI(uri);

            desc.setText(description);

            by_.setText(uploadedBy);

            if (uploadedFor.equals("0")) {
                uploadedFor = getString(R.string.self);
            }
            for__.setText(uploadedFor);

            if (category.isEmpty()) {
                category = getString(R.string.none);
                /*category = "Select Category";
                categorySpinner.setSelection(getIndex(categorySpinner, category));*/
            }
            categ.setText(category);


            if (specific_category.equals("None")) {
                specific_category = getString(R.string.none);
                /*specific_category = "Select Specific Category";
                specificCategorySpinner.setSelection(getIndex_2(specificCategorySpinner, specific_category));*/
            } else {
                //specificCategorySpinner.setSelection(getIndex_2(specificCategorySpinner, specific_category));
            }
            spec_categ.setText(specific_category);

            if (type.equals("notSelected")) {
                type = getString(R.string.none);
                /*type = "Select Type";
                typeSpinner.setSelection(getIndex(typeSpinner, type));*/
            } else {
                //typeSpinner.setSelection(getIndex(typeSpinner, type));
            }
            type_.setText(type);
        }

        private int getIndex(Spinner spinner, String myString) {
            Log.d("position: ", "inside getIndex()");
            for (int i = 0; i < spinner.getCount(); i++) {
                if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)) {
                    Log.d("position ", "getIndex: " + String.valueOf(i));
                    return i;
                }
            }
            return 0;
        }

        private int getIndex_2(Spinner spinner, String myString) {
            Log.d("position: ", "inside getIndex_2()");
            for (int i = 0; i < spinner.getCount(); i++) {
                if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)) {
                    Log.d("position ", "getIndex_2:" + String.valueOf(i));
                    return i;
                }
            }
            return 0;
        }
    }

    @SuppressLint("StaticFieldLeak")
    class UpdateFields extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            String fieldName = strings[0];
            String fieldValue = strings[1];
            String imageName = strings[2];

            client = new OkHttpClient.Builder().connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS).build();

            urlBuilder = Objects.requireNonNull(HttpUrl.parse(new ServerConfig().updateDetailsOfDocument())).newBuilder();
            urlBuilder.addQueryParameter("familyId", familyId);
            urlBuilder.addQueryParameter("phoneNumber", phoneNumber);
            urlBuilder.addQueryParameter("imageName", imageName);
            urlBuilder.addQueryParameter("fieldName", fieldName);
            urlBuilder.addQueryParameter("fieldValue", fieldValue);

            String url = urlBuilder.build().toString();

            request = new Request.Builder().url(url).build();

            try {
                response = client.newCall(request).execute();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (response.body() != null) {
                try {
                    responseString = response.body().string();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return responseString;
        }

        @Override
        protected void onPostExecute(String responce_string) {
            if (!responce_string.equals("dataMissing")) {
                if (!responce_string.equals("serverError")) {
                    if (responce_string.equals("ok")) {
                        recreate();
                    } else {
                        showMessageAfterSignUp(getString(R.string.server_error));
                    }
                } else {
                    showMessageAfterSignUp(getString(R.string.server_error));
                }
            } else {
                showMessageAfterSignUp(getString(R.string.something_goes_wrong));
            }
        }
    }
}
