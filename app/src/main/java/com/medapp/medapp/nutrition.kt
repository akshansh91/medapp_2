package com.medapp.medapp

import android.content.Context
import android.media.MediaPlayer
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.medapp.medapp.database.DB_HELPER
import com.medapp.medapp.utils.left_drawer
import com.medapp.medapp.utils.nutrition_chatbot
import java.text.SimpleDateFormat
import java.util.*

class nutrition : AppCompatActivity() {
    private var result_list_for_nutrition: java.util.ArrayList<nutrition_store> = java.util.ArrayList<nutrition_store>()
    private var nutrition_holder: RecyclerView.Adapter<nutrition_adapter.NumberViewHolder>? = null
    private var nutrition_recyclerView: RecyclerView? = null
    var chatBot: nutrition_chatbot? = null
    private var phoneNumber: String? = null
    private var type: String? = null
    val TAG: String? = "Activity_Name"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nutrition)

        Log.d(TAG, "Inside nutrition")

        //navigation drawer
        left_drawer(this, this@nutrition, findViewById(R.id.nutritionToolbar)!!).createNavigationDrawer()

        //get phone number
        val loginInfo = applicationContext.getSharedPreferences("loginInfo", Context.MODE_PRIVATE)
        phoneNumber = loginInfo.getString("phoneNumber", "")

        //attahc nutrition adapter to recyclerview
        attach_nutrition_adapter_to_recyclerview()

        //getting data from intent
        val p_i = getIntent()
        if (p_i.hasExtra("type") == true) {
            type = p_i.getStringExtra("type")
            chatBot = nutrition_chatbot(type!!, this@nutrition)
        } else {
            type = "NOTYPE"
            chatBot = nutrition_chatbot(type!!, this@nutrition)
        }

        //init chat
        handleNutritionChat(chatBot!!)

        //when user interact
        whenUserInteract()

    }

    //when user enters food items
    fun whenUserInteract() {
        findViewById<ImageView>(R.id.sendIcon).setOnClickListener {
            val foodItemsByUser = findViewById<EditText>(R.id.inputMessage).text.toString()
            loadChat("SENT", foodItemsByUser, getCurrentTime())

            addFoodToLocalDatabase(phoneNumber!!, foodItemsByUser, getDay(), getMonth(), getYear(), getCurrentTime())

            handleEndNutritionChat()
        }
    }

    //add fooditems to local databse
    fun addFoodToLocalDatabase(myPhoneNumber: String, foodItem: String, day: String, month: String, year: String, time: String) {
        DB_HELPER(this@nutrition)
                .insertIntoNutrition(
                        myPhoneNumber,
                        foodItem,
                        day,
                        month,
                        year,
                        time,
                        type!!,
                        "0"
                )
    }

    //recyclerview
    fun attach_nutrition_adapter_to_recyclerview() {
        val nutrition_show_recyclerView: RecyclerView = findViewById<RecyclerView>(R.id.nutritionRecyclerView)
        nutrition_recyclerView = nutrition_show_recyclerView
        val layoutManager = LinearLayoutManager(applicationContext)
        nutrition_show_recyclerView.layoutManager = layoutManager
        nutrition_show_recyclerView.setHasFixedSize(false)
        nutrition_show_recyclerView.isNestedScrollingEnabled = false
        nutrition_holder = nutrition_adapter()
        nutrition_show_recyclerView.adapter = nutrition_holder
    }

    inner class nutrition_store(
            rec_or_sent: String,
            message: String,
            time: String
    ) {
        private var inner_rec_or_sent: String = rec_or_sent
        private var inner_message: String = message
        private var inner_time: String = time

        fun get_rec_or_sent(): String {
            return inner_rec_or_sent
        }

        fun get_message(): String {
            return inner_message
        }

        fun get_time(): String {
            return inner_time
        }

    }

    private inner class nutrition_adapter() : RecyclerView.Adapter<nutrition_adapter.NumberViewHolder>() {

        override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): NumberViewHolder {
            val context = viewGroup.context
            val layoutIdForListItem = R.layout.nutrition_blueprint
            val inflater = LayoutInflater.from(context)
            val shouldAttachToParentImmediately = false

            val view = inflater.inflate(layoutIdForListItem, viewGroup, shouldAttachToParentImmediately)
            val viewHolder = NumberViewHolder(view)

            return viewHolder
        }

        override fun onBindViewHolder(holder: NumberViewHolder, position: Int) {
            val current_object = result_list_for_nutrition.get(position)
            holder.bind(current_object)
        }

        override fun getItemCount(): Int {
            return result_list_for_nutrition.size
        }

        inner class NumberViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            private var sent_textView_1: TextView? = null
            private var sent_time_textView_1: TextView? = null
            private var received_textView_2: TextView? = null
            private var received_time_textView_2: TextView? = null
            private var nutrition_bot_icon: ImageView? = null

            init {
                sent_textView_1 = itemView.findViewById<TextView>(R.id.sentMessage)
                sent_time_textView_1 = itemView.findViewById<TextView>(R.id.sentTime)
                received_time_textView_2 = itemView.findViewById<TextView>(R.id.receivedTime)
                received_textView_2 = itemView.findViewById<TextView>(R.id.receivedMessage)
                nutrition_bot_icon = itemView.findViewById<ImageView>(R.id.nutrition_bot_icon)
            }

            fun bind(current_object: nutrition_store) {
                if (current_object.get_rec_or_sent() == "SENT") {
                    sent_textView_1!!.setVisibility(View.VISIBLE)
                    sent_textView_1!!.setText(current_object.get_message())

                    sent_time_textView_1!!.setVisibility(View.VISIBLE)
                    sent_time_textView_1!!.setText(current_object.get_time())

                    received_time_textView_2!!.setVisibility(View.GONE)
                    received_textView_2!!.setVisibility(View.GONE)
                    nutrition_bot_icon!!.setVisibility(View.GONE)
                }

                if (current_object.get_rec_or_sent() == "RECEIVED") {
                    received_textView_2!!.setVisibility(View.VISIBLE)
                    received_textView_2!!.setText(current_object.get_message())

                    received_time_textView_2!!.setVisibility(View.VISIBLE)
                    received_time_textView_2!!.setText(current_object.get_time())

                    sent_time_textView_1!!.setVisibility(View.GONE)
                    sent_textView_1!!.setVisibility(View.GONE)
                }
            }
        }
    }

    //chatbot handlers
    fun handleNutritionChat(chatBot: nutrition_chatbot) {
        val startingMsg: ArrayList<String> = chatBot.init()
        val size = startingMsg.size
        var counter = 0
        while (counter < size) {
            loadChat("RECEIVED", startingMsg[counter], getCurrentTime())

            counter++
        }
    }

    //load chat in layout
    fun loadChat(rec_or_sent: String, message: String, time: String) {
        result_list_for_nutrition.add(nutrition_store(rec_or_sent, message, time))
        nutrition_holder!!.notifyDataSetChanged()
        val position = nutrition_recyclerView!!.getAdapter().getItemCount() - 1
        nutrition_recyclerView!!.smoothScrollToPosition(position)


        //make ring
        val mPlayer: MediaPlayer = MediaPlayer.create(this@nutrition, R.raw.nutrition_chat_sound)
        mPlayer.start()
    }

    //get current time
    fun getCurrentTime(): String {
        val c: Calendar = Calendar.getInstance()
        val df: SimpleDateFormat = SimpleDateFormat("HH:mm")
        var formattedDate: String = df.format(c.getTime()).toString()
        return formattedDate
    }

    //end nutrition chat
    fun handleEndNutritionChat() {
        val endingMsg: ArrayList<String> = chatBot!!.end()
        val size = endingMsg.size
        var counter = 0
        while (counter < size) {
            loadChat("RECEIVED", endingMsg[counter], getCurrentTime())

            counter++
        }
    }

    //get date
    fun getDay(): String {
        val c: Calendar = Calendar.getInstance()
        val df: SimpleDateFormat = SimpleDateFormat("dd")
        var formattedDate: String = df.format(c.getTime()).toString()
        if (formattedDate[0].toString() == "0") {
            formattedDate = formattedDate.replaceFirst("0", "")
        }
        return formattedDate
    }

    fun getMonth(): String {
        val c: Calendar = Calendar.getInstance()
        val df: SimpleDateFormat = SimpleDateFormat("MM")
        var formattedDate: String = df.format(c.getTime()).toString()
        if (formattedDate[0].toString() == "0") {
            formattedDate = formattedDate.replaceFirst("0", "")
        }
        return formattedDate
    }

    fun getYear(): String {
        val c: Calendar = Calendar.getInstance()
        val df: SimpleDateFormat = SimpleDateFormat("yyyy")
        var formattedDate: String = df.format(c.getTime()).toString()
        if (formattedDate[0].toString() == "0") {
            formattedDate = formattedDate.replaceFirst("0", "")
        }
        return formattedDate
    }

}
