package com.medapp.medapp;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.medapp.medapp.utils.GridSpacingItemDecorator;
import com.wdullaer.materialdatetimepicker.GravitySnapHelper;

import java.util.ArrayList;
import java.util.Objects;

public class myFragment extends Fragment {

    RecyclerView recyclerView;
    String familyID;
    ArrayList<ArrayList<String>> individualList;
    ArrayList<ArrayList<String>> familyList;
    Adapter adapter;
    View view;
    ItemDecor decor;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        if (getArguments() != null) {
            familyID = getArguments().getString("familyID");
            individualList = (ArrayList<ArrayList<String>>) getArguments().getSerializable("data");
            familyList = (ArrayList<ArrayList<String>>) getArguments().getSerializable("data");
        }

        Log.d("Datum__", "Individual List" + String.valueOf(individualList));
        Log.d("Datum__", "Family List" + String.valueOf(familyList));
        view = inflater.inflate(R.layout.activity_my_fragment, container, false);

        decor = new ItemDecor(Objects.requireNonNull(getContext()), R.dimen.item_offset);
        recyclerView = view.findViewById(R.id.userDocs);


        /*recyclerView.addItemDecoration(decor);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this.getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);*/

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity(), 2);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecorator(2, dpToPx(10), true));
        //recyclerView.setItemAnimator(new DefaultItemAnimator());
        //recyclerView.setItemAnimator(R.anim.item_fall_down);

        /*SnapHelper snapHelper = new GravitySnapHelper(Gravity.START);
        snapHelper.attachToRecyclerView(recyclerView);*/

        if (familyID.equals("0")) {
            adapter = new Adapter(this.getActivity(), individualList, new Adapter.clickListener() {
                @Override
                public void onItemClicked(String image) {
                    //Intent intent = new Intent(getActivity(), ImageViewer.class);
                    Intent intent = new Intent(getActivity(), showDetails.class);
                    intent.putExtra("imageName", image);
                    Objects.requireNonNull(getActivity()).startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });
        } else {
            adapter = new Adapter(this.getActivity(), familyList, familyID, new Adapter.clickListener() {
                @Override
                public void onItemClicked(String image) {
                    Intent intent = new Intent(getActivity(), showDetails.class);
                    intent.putExtra("imageName", image);
                    Objects.requireNonNull(getActivity()).startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });
        }

        recyclerView.setAdapter(adapter);

        return view;
    }

    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
}
