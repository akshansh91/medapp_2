package com.medapp.medapp

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.medapp.medapp.config.ServerConfig
import com.medapp.medapp.permanent_diseases.show_doctor_appointment
import com.medapp.medapp.permanent_diseases.show_medicine_schedule
import com.medapp.medapp.permanent_diseases.tests
import com.muddzdev.styleabletoastlibrary.StyleableToast
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.util.concurrent.TimeUnit

class permanentDiseaese : AppCompatActivity() {
    private var permanentDisease_recyclerview: RecyclerView? = null
    private var result_list_for_permanentDisease: ArrayList<permanentDisease_store> = ArrayList<permanentDisease_store>()
    private var permanentDisease_adapter_holder: RecyclerView.Adapter<permanentDisease_adapter.NumberViewHolder>? = null
    val TAG: String? = "Activity_Name"
    private var phoneNumber: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_permanent_diseaese)

        Log.d(TAG, "Inside permanentDiseases")

        val toolBar = findViewById<Toolbar>(R.id.premanentDiseasesToolBar)
        toolBar.title = getString(R.string.permanent_disease)
        toolBar.setTitleTextColor(ContextCompat.getColor(this@permanentDiseaese, R.color.white))

        //get phone number
        val loginInfo = applicationContext.getSharedPreferences("loginInfo", Context.MODE_PRIVATE)
        phoneNumber = loginInfo.getString("phoneNumber", "")

        //onclick add permanent diseases
        onclickAddDisease()

        //attach permanent disease adapter to recycler view
        attach_permanentDisease_adapter_to_recyclerview()

        //load data to recyclerview
        loadPermanentDisease(phoneNumber!!)
    }

    //on click add permanent disease button
    fun onclickAddDisease() {
        findViewById<Button>(R.id.addPermanentDiesasesButton).setOnClickListener {
            openAddPDiseases()
        }
    }

    fun openAddPDiseases() {
        val i = Intent(this@permanentDiseaese, addPermanentDiseases::class.java)
        startActivity(i)
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
    }

    //permanent disease recyclerview
    fun attach_permanentDisease_adapter_to_recyclerview() {
        val permanentDisease_recyclerView: RecyclerView = findViewById<RecyclerView>(R.id.permanentDiseaseRecyclerView)
        permanentDisease_recyclerview = permanentDisease_recyclerView
        val layoutManager = LinearLayoutManager(applicationContext)
        permanentDisease_recyclerView.layoutManager = layoutManager
        permanentDisease_recyclerView.setHasFixedSize(false)
        permanentDisease_recyclerView.isNestedScrollingEnabled = false
        permanentDisease_adapter_holder = permanentDisease_adapter()
        permanentDisease_recyclerView.adapter = permanentDisease_adapter_holder
    }

    inner class permanentDisease_store(diseaseName: String, diseaseDescription: String) {
        private var inner_diseaseName: String = diseaseName
        private var inner_diseaseDescription: String = diseaseDescription

        fun get_diseaseName(): String {
            return inner_diseaseName
        }

        fun get_diseaseDescription(): String {
            return inner_diseaseDescription
        }
    }

    private inner class permanentDisease_adapter : RecyclerView.Adapter<permanentDisease_adapter.NumberViewHolder>() {

        override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): NumberViewHolder {
            val context = viewGroup.context
            val layoutIdForListItem = R.layout.permanent_diseases_blueprint
            val inflater = LayoutInflater.from(applicationContext)
            val shouldAttachToParentImmediately = false

            val view = inflater.inflate(layoutIdForListItem, viewGroup, shouldAttachToParentImmediately)
            val viewHolder = NumberViewHolder(view)

            return viewHolder
        }

        override fun onBindViewHolder(holder: NumberViewHolder, position: Int) {
            val current_object = result_list_for_permanentDisease.get(position)
            holder.bind(current_object)
        }

        override fun getItemCount(): Int {
            return result_list_for_permanentDisease.size
        }

        inner class NumberViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            private var diseaseNameTextView: TextView? = null
            private var diseaseDescriptionTextView: TextView? = null
            private var deleteButton: Button? = null
            private var doctorAppointButton: Button? = null
            private var testButton: Button? = null
            private var medicineSchedule: Button? = null

            init {
                diseaseNameTextView = itemView.findViewById(R.id.diseaseName)
                diseaseDescriptionTextView = itemView.findViewById(R.id.diseaseDescription)
                deleteButton = itemView.findViewById(R.id.deletePermanentDiseaseButton)
                doctorAppointButton = itemView.findViewById(R.id.doctorAppointment)
                testButton = itemView.findViewById(R.id.tests)
                medicineSchedule = itemView.findViewById(R.id.medicineSchedule)
            }

            fun bind(current_object: permanentDisease_store) {
                diseaseNameTextView!!.text = current_object.get_diseaseName()
                diseaseDescriptionTextView!!.text = current_object.get_diseaseDescription()

                deleteButton!!.setOnClickListener {
                    itemView.visibility = View.GONE
                    deleteCurrentPermanentDisease(phoneNumber!!, current_object.get_diseaseName())
                }

                doctorAppointButton!!.setOnClickListener {
                    onClickDoctorAppointMent(current_object.get_diseaseName())
                }
                testButton!!.setOnClickListener {
                    onClickDoctorTests(current_object.get_diseaseName())
                }
                medicineSchedule!!.setOnClickListener {
                    onClickMedicineSchedule(current_object.get_diseaseName())
                }
            }
        }

    }

    //on click deleted permanent disease button
    fun deleteCurrentPermanentDisease(phoneNumber: String, diseaseName: String) {
        doAsync {
            val client: OkHttpClient = OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build()

            val urlBuilder = HttpUrl.parse(ServerConfig().deletePermanentDisease())!!.newBuilder()
            urlBuilder.addQueryParameter("phoneNumber", phoneNumber)
            urlBuilder.addQueryParameter("diseaseName", diseaseName)

            val url = urlBuilder.build().toString()

            val request = Request.Builder()
                    .url(url)
                    .build()

            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()

            uiThread {
                Log.d("mess", responce_string.toString())

                if (responce_string != "dataMissing") {
                    if (responce_string != "serverError") {
                        if (responce_string == "ok") {
                            permanentDiseaseDeletedSuccessfully()
                        } else {
                            showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                        }
                    } else {
                        showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                    }
                } else {
                    showMessageAfterSignup(getString(R.string.something_goes_wrong), Color.RED)
                }
            }
        }
    }

    fun permanentDiseaseDeletedSuccessfully() {
        showMessageAfterSignup(getString(R.string.deleted_successfully), Color.GREEN)
        loadPermanentDisease(phoneNumber!!)
    }

    //load permanent diseases
    fun loadPermanentDisease(phoneNumber: String) {
        doAsync {
            val client: OkHttpClient = OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build()

            val urlBuilder = HttpUrl.parse(ServerConfig().getPermanentDisease())!!.newBuilder()
            urlBuilder.addQueryParameter("phoneNumber", phoneNumber)

            val url = urlBuilder.build().toString()

            val request = Request.Builder()
                    .url(url)
                    .build()

            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()

            uiThread {
                Log.d("mess", responce_string.toString())

                if (responce_string != "dataMissing") {
                    if (responce_string != "NoDiseaseAdded") {
                        handleResponse(responce_string)
                    } else {
                        showMessageAfterSignup(getString(R.string.permanent_diseases_found), Color.RED)
                    }
                } else {
                    showMessageAfterSignup(getString(R.string.something_goes_wrong), Color.RED)
                }
            }
        }
    }

    fun handleResponse(responseString: String) {
        result_list_for_permanentDisease.clear()
        result_list_for_permanentDisease = ArrayList<permanentDisease_store>()
        permanentDisease_adapter_holder!!.notifyDataSetChanged()

        val chunkOfDate = responseString.split("___")
        val size = chunkOfDate.size
        var counter = 1

        while (counter < size) {
            val singleDataStream = chunkOfDate[counter]
            val name = singleDataStream.split("*")[0]
            val description = singleDataStream.split("*")[1]

            result_list_for_permanentDisease.add(permanentDisease_store(name, description))

            counter++
        }

        permanentDisease_adapter_holder!!.notifyDataSetChanged()
    }

    fun showMessageAfterSignup(msg_text: String, color: Int) {
        val st = StyleableToast.Builder(this)
                .text(msg_text)
                .textColor(Color.WHITE)
                .backgroundColor(color)
                .build()
        st.show()
    }

    //on doctore appointment,tests,medicine schedule button
    fun onClickDoctorAppointMent(diseaseName: String) {
        val i = Intent(this@permanentDiseaese, show_doctor_appointment::class.java)
        i.putExtra("diseaseName", diseaseName)
        startActivity(i)
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
    }

    fun onClickDoctorTests(diseaseName: String) {
        val i = Intent(this@permanentDiseaese, tests::class.java)
        i.putExtra("diseaseName", diseaseName)
        startActivity(i)
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
    }

    fun onClickMedicineSchedule(diseaseName: String) {
        val i = Intent(this@permanentDiseaese, show_medicine_schedule::class.java)
        i.putExtra("diseaseName", diseaseName)
        startActivity(i)
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
    }

}
