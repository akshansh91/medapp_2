package com.medapp.medapp

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.InputType
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.afollestad.materialdialogs.MaterialDialog
import com.medapp.medapp.config.ServerConfig
import com.medapp.medapp.utils.left_drawer
import com.muddzdev.styleabletoastlibrary.StyleableToast
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.util.concurrent.TimeUnit

class habits : AppCompatActivity() {
    private var habits_recyclerview: RecyclerView? = null
    private var result_list_for_habits: ArrayList<habits_store> = ArrayList<habits_store>()
    private var habits_adapter_holder: RecyclerView.Adapter<habits_adapter.NumberViewHolder>? = null
    private var phoneNumber: String? = null
    val TAG: String? = "Activity_Name"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_habits)
        Log.d(TAG, "Inside habits")
        //navigation drawer
        left_drawer(this, this@habits, findViewById(R.id.habitsToolbar)!!).createNavigationDrawer()

        //get phone number
        val loginInfo = applicationContext.getSharedPreferences("loginInfo", Context.MODE_PRIVATE)
        phoneNumber = loginInfo.getString("phoneNumber", "")

        //attach habits adapter to recycleview
        attach_habits_adapter_to_recyclerview()

        //onclick add habits button
        onClickAddHabitsButton()

        //load habits
        loadHabits()
    }

    //load habits
    fun loadHabits() {
        doAsync {
            val client: OkHttpClient = OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build()

            val urlBuilder = HttpUrl.parse(ServerConfig().getHabits())!!.newBuilder()
            urlBuilder.addQueryParameter("phoneNumber", phoneNumber)

            val url = urlBuilder.build().toString()

            val request = Request.Builder()
                    .url(url)
                    .build()

            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()

            uiThread {
                Log.d("mess", responce_string.toString())

                if (responce_string != "dataMissing") {
                    if (responce_string != "serverError") {
                        handleResponse(responce_string)
                    } else {
                        showMessageAfterSignup(getString(R.string.no_habits_added), Color.RED)
                    }
                } else {
                    showMessageAfterSignup(getString(R.string.something_goes_wrong), Color.RED)
                }
            }
        }
    }

    fun handleResponse(responseString: String) {
        result_list_for_habits.clear()
        habits_adapter_holder!!.notifyDataSetChanged()
        habits_recyclerview!!.adapter = null
        attach_habits_adapter_to_recyclerview()

        val chunkOfDate = responseString.split("___")
        val size = chunkOfDate.size
        var counter = 1

        while (counter < size) {
            val singleDataStream = chunkOfDate[counter]
            val habit = singleDataStream.split("*")[0]

            result_list_for_habits.add(
                    habits_store(
                            habit
                    )
            )

            counter++
        }

        habits_adapter_holder!!.notifyDataSetChanged()
    }

    //habits appointments recyclerview
    fun attach_habits_adapter_to_recyclerview() {
        val habits_recyclerView: RecyclerView = findViewById<RecyclerView>(R.id.habits_recyclerView)
        habits_recyclerview = habits_recyclerView
        val layoutManager = LinearLayoutManager(applicationContext)
        habits_recyclerView.layoutManager = layoutManager
        habits_recyclerView.setHasFixedSize(false)
        habits_recyclerView.isNestedScrollingEnabled = false
        habits_adapter_holder = habits_adapter()
        habits_recyclerView.adapter = habits_adapter_holder
    }

    inner class habits_store(habit: String) {
        private var inner_habit: String = habit

        fun get_habit(): String {
            return inner_habit
        }

    }

    private inner class habits_adapter : RecyclerView.Adapter<habits_adapter.NumberViewHolder>() {

        override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): NumberViewHolder {
            val context = viewGroup.context
            val layoutIdForListItem = R.layout.habits_blueprint
            val inflater = LayoutInflater.from(applicationContext)
            val shouldAttachToParentImmediately = false

            val view = inflater.inflate(layoutIdForListItem, viewGroup, shouldAttachToParentImmediately)
            val viewHolder = NumberViewHolder(view)

            return viewHolder
        }

        override fun onBindViewHolder(holder: NumberViewHolder, position: Int) {
            val current_object = result_list_for_habits.get(position)
            holder.bind(current_object)
        }

        override fun getItemCount(): Int {
            return result_list_for_habits.size
        }

        inner class NumberViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            private var habitsTextView: TextView? = null
            private var removeHabitsButton: Button? = null

            init {
                habitsTextView = itemView.findViewById(R.id.habitsTextView)
                removeHabitsButton = itemView.findViewById(R.id.removeHabitsButton)
            }

            fun bind(current_object: habits_store) {
                habitsTextView!!.text = current_object.get_habit()

                removeHabitsButton!!.setOnClickListener {
                    deleteHabit(phoneNumber!!, current_object.get_habit(), removeHabitsButton!!)
                }
            }
        }

    }

    fun showMessageAfterSignup(msg_text: String, color: Int) {
        val st = StyleableToast.Builder(this)
                .text(msg_text)
                .textColor(Color.WHITE)
                .backgroundColor(color)
                .build()
        st.show()
    }

    //onclick add habits button
    fun onClickAddHabitsButton() {
        findViewById<Button>(R.id.addHabitsButton).setOnClickListener {
            createAddHabitDialog()
        }
    }

    //create dialog
    fun createAddHabitDialog() {
        MaterialDialog.Builder(this)
                .title(getString(R.string.habits))
                .content(getString(R.string.enter_habit_here))
                .inputType(InputType.TYPE_CLASS_TEXT)
                .input(getString(R.string.my_habit), "", MaterialDialog.InputCallback { dialog, input ->
                    handleInput(input.toString())
                }).show()
    }

    fun handleInput(input: String) {
        if (input.length != 0) {
            makeRequestToAddHabitsApi(input)
        } else {
            showMessageAfterSignup(getString(R.string.enter_habit_first), Color.RED)
        }
    }

    fun makeRequestToAddHabitsApi(myHabit: String) {
        doAsync {
            val client: OkHttpClient = OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build()

            val urlBuilder = HttpUrl.parse(ServerConfig().insertHabits())!!.newBuilder()
            urlBuilder.addQueryParameter("myHabit", myHabit)
            urlBuilder.addQueryParameter("phoneNumber", phoneNumber)

            val url = urlBuilder.build().toString()

            val request = Request.Builder()
                    .url(url)
                    .build()

            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()

            uiThread {
                Log.d("mess", responce_string.toString())

                if (responce_string != "dataMissing") {
                    if (responce_string != "serverError") {
                        if (responce_string == "ok") {
                            showMessageAfterSignup(getString(R.string.habit_added_successfully), Color.GREEN)
                            loadHabits()
                        } else {
                            showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                        }
                    } else {
                        showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                    }
                } else {
                    showMessageAfterSignup(getString(R.string.something_goes_wrong), Color.RED)
                }
            }
        }
    }

    //delete habit
    fun deleteHabit(phoneNumber: String, myHabit: String, removeHabitButton: Button) {
        doAsync {
            val client: OkHttpClient = OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build()

            val urlBuilder = HttpUrl.parse(ServerConfig().deleteHabit())!!.newBuilder()
            urlBuilder.addQueryParameter("habit", myHabit)
            urlBuilder.addQueryParameter("phoneNumber", phoneNumber)

            val url = urlBuilder.build().toString()

            val request = Request.Builder()
                    .url(url)
                    .build()

            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()

            uiThread {
                Log.d("mess", responce_string.toString())

                if (responce_string != "dataMissing") {
                    if (responce_string != "serverError") {
                        if (responce_string == "ok") {
                            showMessageAfterSignup(getString(R.string.habit_deleted_successfully), Color.GREEN)
                            removeHabitButton.text = getString(R.string.habit_deleted)

                        } else {
                            showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                        }
                    } else {
                        showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                    }
                } else {
                    showMessageAfterSignup(getString(R.string.something_goes_wrong), Color.RED)
                }
            }
        }
    }
}
