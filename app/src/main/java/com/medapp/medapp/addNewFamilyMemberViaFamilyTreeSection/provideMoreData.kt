package com.medapp.medapp.addNewFamilyMemberViaFamilyTreeSection

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import com.muddzdev.styleabletoastlibrary.StyleableToast
import com.medapp.medapp.R
import com.medapp.medapp.config.ServerConfig
import com.medapp.medapp.familyTree
import com.medapp.medapp.utils.left_drawer
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class provideMoreData : AppCompatActivity() {
    private var familyMemberPhoneNumber: String? = null
    private var familyId: String? = null
    private var password: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_provide_more_data)

        left_drawer(this, this@provideMoreData, findViewById(R.id.toolbar_tabs3)!!).createNavigationDrawer()

        //get family password
        val loginInfo = applicationContext.getSharedPreferences("loginInfo", Context.MODE_PRIVATE)
        familyId = loginInfo.getString("familyId", "")
        password = loginInfo.getString("password", "")

        //get family member phone number
        val p_i = getIntent()
        familyMemberPhoneNumber = p_i.getStringExtra("familyMemberPhoneNumber")

        //set family member phone number
        findViewById<EditText>(R.id.phoneNumber).setText(familyMemberPhoneNumber)

        // on click next
        onclickNext()
    }

    fun onclickNext() {
        findViewById<Button>(R.id.nextButton).setOnClickListener {
            val fullName = findViewById<EditText>(R.id.fullName).text.toString()
            var email = findViewById<EditText>(R.id.email).text.toString()
            var day = findViewById<EditText>(R.id.day).text.toString()
            var month = findViewById<EditText>(R.id.month).text.toString()
            var year = findViewById<EditText>(R.id.year).text.toString()

            if (fullName.length != 0) {

                if (email.length == 0) {
                    email = " "
                }
                if (day.length == 0) {
                    day = " "
                }
                if (month.length == 0) {
                    month = " "
                }
                if (year.length == 0) {
                    year = " "
                }
                makeAddFamilyMemberViaFamilyTreeApiRequest(
                        fullName,
                        familyMemberPhoneNumber!!,
                        day,
                        month,
                        year,
                        password!!,
                        email
                )
            } else {
                showMessageAfterSignup(getString(R.string.enter_full_name), Color.RED)
            }
        }
    }

    fun getTodaysDate(): String {
        val c: Calendar = Calendar.getInstance()
        val df: SimpleDateFormat = SimpleDateFormat("dd-MM-yyyy")
        val formattedDate: String = df.format(c.getTime()).toString()
        return formattedDate
    }

    fun showMessageAfterSignup(msg_text: String, color: Int) {
        val st = StyleableToast.Builder(this)
                .text(msg_text)
                .textColor(Color.WHITE)
                .backgroundColor(color)
                .build()
        st.show()
    }

    //add family member api
    fun makeAddFamilyMemberViaFamilyTreeApiRequest(
            fullName: String,
            familyMemberPhoneNumber: String,
            day: String,
            month: String,
            year: String,
            password: String,
            email: String) {
        doAsync {
            val client: OkHttpClient = OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build()

            val urlBuilder = HttpUrl.parse(ServerConfig().AddFamilyMemberViaFamilyTree())!!.newBuilder()
            urlBuilder.addQueryParameter("familyId", familyId)
            urlBuilder.addQueryParameter("fullName", fullName)
            urlBuilder.addQueryParameter("phoneNumber", familyMemberPhoneNumber)
            urlBuilder.addQueryParameter("email", email)
            urlBuilder.addQueryParameter("password", password)
            urlBuilder.addQueryParameter("day", day)
            urlBuilder.addQueryParameter("month", month)
            urlBuilder.addQueryParameter("year", year)
            urlBuilder.addQueryParameter("dateOfRegister", getTodaysDate())


            val url = urlBuilder.build().toString()

            val request = Request.Builder()
                    .url(url)
                    .build()

            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()

            uiThread {
                Log.d("mess", responce_string.toString())

                if (responce_string != "alreadyInFamily") {
                    if (responce_string != "dataMissing") {
                        if (responce_string != "serverError") {
                            if (responce_string == "ok") {
                                showMessageAfterSignup(getString(R.string.family_member_added), Color.GREEN)
                                val i = Intent(this@provideMoreData, familyTree::class.java)
                                startActivity(i)
                                finish()
                            } else {
                                showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                            }
                        } else {
                            showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                        }
                    } else {
                        showMessageAfterSignup(getString(R.string.something_goes_wrong), Color.RED)
                    }
                } else {
                    showMessageAfterSignup(getString(R.string.person_already_in_family), Color.GREEN)
                    val i = Intent(this@provideMoreData, familyTree::class.java)
                    startActivity(i)
                    finish()
                }
            }
        }
    }

    override fun onBackPressed() {
        val i = Intent(this@provideMoreData, familyTree::class.java)
        startActivity(i)
        finish()
    }
}
