package com.medapp.medapp

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.AsyncTask
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.InputType
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.afollestad.materialdialogs.MaterialDialog
import com.medapp.medapp.addNewFamilyMemberViaFamilyTreeSection.phoneNumberOfNewFamilyMember
import com.medapp.medapp.config.ServerConfig
import com.medapp.medapp.utils.utils
import com.muddzdev.styleabletoastlibrary.StyleableToast
import com.squareup.picasso.Picasso
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.util.concurrent.TimeUnit

class familyTree : AppCompatActivity() {

    private var familyTree_recyclerview: RecyclerView? = null
    private var result_list_for_familyTree: ArrayList<familyTree_store> = ArrayList()
    private var familyTree_adapter_holder: RecyclerView.Adapter<familyTree_adapter.NumberViewHolder>? = null
    private var familyId: String? = null
    private var my_phoneNumber: String? = null
    private var family_member_names: ArrayList<String> = ArrayList()
    private var family_member_number: ArrayList<String> = ArrayList()
    private var family_member_pic: ArrayList<String> = ArrayList()
    private var family_member_relation: ArrayList<String> = ArrayList()
    val TAG: String? = "Activity_Name"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_family_tree)
        Log.d(TAG, "Inside familyTree")
        //left drawer
        //left_drawer(this, this@familyTree, findViewById(R.id.familyTreeToolBar)).createNavigationDrawer()

        //get familyId
        try {
            Log.d("Check_familyTree", "Inside onCreate()")
            val loginInfo = applicationContext.getSharedPreferences("loginInfo", Context.MODE_PRIVATE)
            familyId = loginInfo.getString("familyId", "")
            my_phoneNumber = loginInfo.getString("phoneNumber", "")

            //show add new family members is admin logged in
            if (familyId != "0") {
                //findViewById<Button>(R.id.addNewFamilyMemberButton).setVisibility(View.VISIBLE)
            }

            findViewById<Button>(R.id.showTree).setOnClickListener {
                val i = Intent(this@familyTree, familyTree_2::class.java)
                i.putStringArrayListExtra("familyNames", family_member_names)
                i.putStringArrayListExtra("familyRelation", family_member_relation)
                i.putStringArrayListExtra("familyProfilePic", family_member_pic)
                startActivity(i)
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            }
            //attach family tree adapter to recyclerView
            // we don't want the results in recycler view . . .
            //attach_familyTree_adapter_to_recyclerview()

            //load family tree
            loadFamilyTreeInfo()

            //attach onclick listener to add new family member button
            //attachOnClickListenerToAddNewFamilyMemberButton()
        } catch (e: Exception) {
            Log.d("Check_familyTree", "Inside onCreate()... Exception: $e")
        }
    }

    //load admin of family
    fun loadFamilyTreeInfo() {
        //result_list_for_familyTree.clear()
        try {
            Log.d("Check_familyTree", "Inside loadFamilyTreeInfo()")
            val obj = loadFamilyTree()
            obj.execute()
        } catch (e: Exception) {
            Log.d("Check_familyTree", "Inside loadFamilyTreeInfo()... Exception: $e")
        }
        /*doAsync {
            val client: OkHttpClient = OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build()


            val urlBuilder = HttpUrl.parse(ServerConfig().getFamilyTreeApi())!!.newBuilder()
            urlBuilder.addQueryParameter("familyId", familyId.toString())
            urlBuilder.addQueryParameter("phoneNumber", my_phoneNumber)

            val url = urlBuilder.build().toString()

            val request = Request.Builder()
                    .url(url)
                    .build()

            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()

            Log.d("familyTree__s", responce_string)

            uiThread {
                if (responce_string != "dataMissing") {
                    if (responce_string != "nullData") {
                        loadDataInLayout(responce_string)
                    } else {
                        //null data
                    }
                } else {
                    //datamissing
                }
            }
        }*/
    }

    @SuppressLint("StaticFieldLeak")
    inner class loadFamilyTree() : AsyncTask<String, String, String>() {

        val dialog = ProgressDialog(this@familyTree)

        override fun onPreExecute() {
            super.onPreExecute()

            dialog.setMessage("Please wait")
            dialog.setTitle("Loading")
            dialog.setCancelable(false)
            dialog.isIndeterminate = true
            dialog.show()
        }

        override fun doInBackground(vararg params: String?): String {

            val client: OkHttpClient = OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build()

            val urlBuilder = HttpUrl.parse(ServerConfig().getFamilyTreeApi())!!.newBuilder()
            urlBuilder.addQueryParameter("familyId", familyId.toString())
            urlBuilder.addQueryParameter("phoneNumber", my_phoneNumber)

            val url = urlBuilder.build().toString()

            val request = Request.Builder()
                    .url(url)
                    .build()

            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()

            Log.d("familyTree__s", responce_string)

            return responce_string
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            if (result != "dataMissing") {
                if (result != "nullData") {
                    if (result != null) {
                        loadDataInLayout(result)
                        dialog.dismiss()
                    }
                } else {
                    val st = StyleableToast.Builder(this@familyTree)
                            .text(getString(R.string.no_family_data))
                            .textColor(Color.WHITE)
                            .backgroundColor(Color.BLACK)
                            .build()
                    st.show()
                }
            } else {
                val st = StyleableToast.Builder(this@familyTree)
                        .text(getString(R.string.no_family_data))
                        .textColor(Color.WHITE)
                        .backgroundColor(Color.BLACK)
                        .build()
                st.show()
            }
        }
    }

    fun loadDataInLayout(responseString: String) {
        val chunkOfData = responseString.split("___")
        val size = chunkOfData.size
        var counter = 1

        while (counter < size) {
            val familyMemberData = chunkOfData[counter]

            val name = familyMemberData.split("*")[0]
            val profilePicture = (familyMemberData.split("*")[1]).split("#")[0]
            val relationWithAdmin = ((familyMemberData.split("*")[1]).split("#")[1]).split("^")[0]
            val phoneNumber = (((familyMemberData.split("*")[1]).split("#")[1]).split("^")[1]).split("??")[0]
            val adminFlag = ((((familyMemberData.split("*")[1]).split("#")[1]).split("^")[1]).split("??")[1])

            family_member_names.add(name)
            family_member_pic.add(profilePicture)
            family_member_relation.add(relationWithAdmin)
            /*if (relationWithAdmin == "") {
                family_member_relation.add("Admin")
            } else {
                family_member_relation.add(relationWithAdmin)
            }*/

            //We're changing this now . . .
            /*if (adminFlag.toInt() != 1) {
                //We're changing this now . . .
                //result_list_for_familyTree.add(familyTree_store(profilePicture, name, relationWithAdmin, phoneNumber, adminFlag))
                //familyTree_adapter_holder!!.notifyDataSetChanged()

                //adding elements to list . . .
                //family_member_names.add(name)
                //family_member_pic.add(profilePicture)
                //family_member_relation.add(relationWithAdmin)

            } else {
                findViewById<TextView>(R.id.adminName).setText(name)
                Picasso.with(this@familyTree)
                        .load(ServerConfig().getUserProfilePictureDirectory() + "/" + profilePicture)
                        .resize(200, 200)
                        .transform(utils().getCircularTransformation(this@familyTree))
                        .placeholder(R.drawable.ic_perm_identity_black_24dp)
                        .error(R.drawable.ic_perm_identity_black_24dp)
                        .into(findViewById<ImageView>(R.id.adminProfilePicture))
            }*/
            counter++
        }
        proceedToNextStep(family_member_names, family_member_pic, family_member_relation)
        this@familyTree.finish()
    }

    private fun proceedToNextStep(family_member_names: ArrayList<String>, family_member_pic: ArrayList<String>, family_member_relation: ArrayList<String>) {
        val i = Intent(this@familyTree, familyTree_2::class.java)
        i.putStringArrayListExtra("familyNames", family_member_names)
        i.putStringArrayListExtra("familyRelation", family_member_relation)
        i.putStringArrayListExtra("familyProfilePic", family_member_pic)
        startActivity(i)
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
    }

    //familyTree recyclerview
    fun attach_familyTree_adapter_to_recyclerview() {
        val familytree_recyclerView: RecyclerView = findViewById<RecyclerView>(R.id.familyTreeRecyclerView)
        familyTree_recyclerview = familytree_recyclerView
        val layoutManager = LinearLayoutManager(applicationContext)
        familytree_recyclerView.layoutManager = layoutManager
        familytree_recyclerView.setHasFixedSize(false)
        familytree_recyclerView.isNestedScrollingEnabled = false
        familyTree_adapter_holder = familyTree_adapter()
        familytree_recyclerView.adapter = familyTree_adapter_holder
    }

    inner class familyTree_store(
            familyMemberProfilePicture: String,
            familyMemberName: String,
            relationWithAdmin: String,
            familyMemberPhoneNumber: String,
            adminFlag: String) {
        private var inner_familyMemberProfilePicture: String = familyMemberProfilePicture
        private var inner_familyMemberName: String = familyMemberName
        private var inner_relationWithAdmin: String = relationWithAdmin
        private var inner_familyMemberPhoneNumber: String = familyMemberPhoneNumber
        private var inner_adminFlag: String = adminFlag

        fun get_familyMemberProfilePicture(): String {
            if (inner_familyMemberProfilePicture.length == 0) {
                inner_familyMemberProfilePicture = "NULL"
            }
            return inner_familyMemberProfilePicture
        }

        fun get_familyMemberName(): String {
            return inner_familyMemberName
        }

        fun get_relationWithAdmin(): String {
            return inner_relationWithAdmin
        }

        fun get_familyMemberPhoneNumber(): String {
            return inner_familyMemberPhoneNumber
        }

        fun get_adminFlag(): Int {
            return inner_adminFlag.toInt()
        }
    }

    private inner class familyTree_adapter() : RecyclerView.Adapter<familyTree_adapter.NumberViewHolder>() {

        override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): NumberViewHolder {
            val context = viewGroup.context
            val layoutIdForListItem = R.layout.family_tree_blueprint
            val inflater = LayoutInflater.from(applicationContext)
            val shouldAttachToParentImmediately = false

            val view = inflater.inflate(layoutIdForListItem, viewGroup, shouldAttachToParentImmediately)
            val viewHolder = NumberViewHolder(view)

            return viewHolder
        }

        override fun onBindViewHolder(holder: NumberViewHolder, position: Int) {
            var current_object = result_list_for_familyTree.get(position)
            holder.bind(current_object)
        }

        override fun getItemCount(): Int {
            return result_list_for_familyTree.size
        }

        inner class NumberViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            private var relationWithAdminTextView: TextView? = null
            private var familyMemberNameTextView: TextView? = null
            private var profilePictureImageView: ImageView? = null
            private var editIconImageView: ImageView? = null
            private var deleteFamilyMemberButton: Button? = null
            private var familyMemberPhoneNumber: String? = null

            init {
                profilePictureImageView = itemView.findViewById(R.id.familyMemberProfilePicture)
                familyMemberNameTextView = itemView.findViewById(R.id.familyMemberName)
                relationWithAdminTextView = itemView.findViewById(R.id.relationshipWithAdmin)
                editIconImageView = itemView.findViewById(R.id.editIcon)
                deleteFamilyMemberButton = itemView.findViewById(R.id.deleteButton)
            }

            fun bind(current_object: familyTree_store) {
                familyMemberPhoneNumber = current_object.get_familyMemberPhoneNumber()
                familyMemberNameTextView!!.setText(current_object.get_familyMemberName())
                relationWithAdminTextView!!.setText(current_object.get_relationWithAdmin())


                Picasso.with(this@familyTree)
                        .load(ServerConfig().getUserProfilePictureDirectory() + "/" + current_object.get_familyMemberProfilePicture())
                        .resize(200, 200)
                        .transform(utils().getCircularTransformation(this@familyTree))
                        .placeholder(R.drawable.ic_perm_identity_black_24dp)
                        .error(R.drawable.ic_perm_identity_black_24dp)
                        .into(profilePictureImageView)

                if (familyId != "0") {
                    editIconImageView!!.setVisibility(View.VISIBLE)
                    deleteFamilyMemberButton!!.setVisibility(View.VISIBLE)
                }
                editIconImageView!!.setOnClickListener {
                    handleUpdate(current_object.get_familyMemberPhoneNumber())
                }

                deleteFamilyMemberButton!!.setOnClickListener {
                    removeFamilyMemberFromFamily(familyMemberPhoneNumber!!, deleteFamilyMemberButton!!, editIconImageView!!)
                }

            }
        }

    }

    //update relation with admin
    fun handleUpdate(phoneNumber: String) {
        MaterialDialog.Builder(this)
                .title(getString(R.string.update))
                .content(getString(R.string.update_relation))
                .inputType(InputType.TYPE_CLASS_TEXT)
                .items(R.array.itemsForRelations)
                .itemsCallback(MaterialDialog.ListCallback { dialog, view, which, text ->
                    updateFamilyMemberRelation(phoneNumber, text.toString())
                })
                .positiveText("Update")
                .show()
    }

    fun updateFamilyMemberRelation(phoneNumber: String, newRelation: String) {
        doAsync {
            val client: OkHttpClient = OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build()

            val urlBuilder = HttpUrl.parse(ServerConfig().updateRelationWithFamilyMember())!!.newBuilder()
            urlBuilder.addQueryParameter("phoneNumber", phoneNumber)
            urlBuilder.addQueryParameter("newRelation", newRelation)

            val url = urlBuilder.build().toString()

            val request = Request.Builder()
                    .url(url)
                    .build()

            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()

            uiThread {
                Log.d("mess", responce_string.toString())

                if (responce_string != "dataMissing") {
                    if (responce_string != "serverError") {
                        if (responce_string == "ok") {
                            loadFamilyTreeInfo()
                        } else {
                            showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                        }
                    } else {
                        showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                    }
                } else {
                    showMessageAfterSignup(getString(R.string.something_goes_wrong), Color.RED)
                }
            }
        }
    }

    fun showMessageAfterSignup(msg_text: String, color: Int) {
        val st = StyleableToast.Builder(this)
                .text(msg_text)
                .textColor(Color.WHITE)
                .backgroundColor(color)
                .build()
        st.show()
    }

    //remove family member
    fun removeFamilyMemberFromFamily(
            familyMemberPhoneNumber: String,
            deleteButton: Button,
            editIcon: ImageView) {
        doAsync {
            val client: OkHttpClient = OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build()

            val urlBuilder = HttpUrl.parse(ServerConfig().removeFamilyMemberFromFamily())!!.newBuilder()
            urlBuilder.addQueryParameter("phoneNumber", familyMemberPhoneNumber)

            val url = urlBuilder.build().toString()

            val request = Request.Builder()
                    .url(url)
                    .build()

            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()

            uiThread {
                Log.d("mess", responce_string.toString())

                if (responce_string != "dataMissing") {
                    if (responce_string != "serverError") {
                        if (responce_string == "ok") {
                            editIcon.setVisibility(View.GONE)
                            deleteButton.isEnabled = false
                            deleteButton.setText(getString(R.string.family_member_removed))
                            showMessageAfterSignup(getString(R.string.family_member_removed), Color.GREEN)
                        } else {
                            showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                        }
                    } else {
                        showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                    }
                } else {
                    showMessageAfterSignup(getString(R.string.something_goes_wrong), Color.RED)
                }
            }
        }
    }

    //on click add new family member
    fun attachOnClickListenerToAddNewFamilyMemberButton() {
        findViewById<Button>(R.id.addNewFamilyMemberButton).setOnClickListener {
            val i = Intent(this@familyTree, phoneNumberOfNewFamilyMember::class.java)
            startActivity(i)
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        }
    }


}
