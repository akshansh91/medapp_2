package com.medapp.medapp

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.TextView
import com.jjoe64.graphview.GraphView
import com.jjoe64.graphview.series.DataPoint
import com.jjoe64.graphview.series.LineGraphSeries
import com.medapp.medapp.config.ServerConfig
import com.muddzdev.styleabletoastlibrary.StyleableToast
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.util.concurrent.TimeUnit

class viewHealthTrackerData : AppCompatActivity() {
    private var optionName: String? = null
    private var phoneNumber: String? = null
    private var series_2: LineGraphSeries<DataPoint>? = null
    private var graph: GraphView? = null
    val TAG: String? = "Activity_Name"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_health_tracker_data)
        setTitle("Health Graph")
        Log.d(TAG, "Inside view_health_tracker_data")
        //get phone number
        val loginInfo = applicationContext.getSharedPreferences("loginInfo", Context.MODE_PRIVATE)
        phoneNumber = loginInfo.getString("phoneNumber", "")

        //option name
        val p_i = getIntent()
        optionName = p_i.getStringExtra("optionName")

        //load graph
        loadOptionGraph()

    }

    fun loadOptionGraph() {
        graph = findViewById<View>(R.id.optionGraph) as GraphView
        graph!!.title = optionName + getString(R.string.graph)
        series_2 = LineGraphSeries(
                arrayOf(
                        DataPoint(0.0, 0.0)
                )
        )

        loadDataPointsToGraph()
    }

    fun loadDataPointsToGraph() {
        doAsync {
            val client: OkHttpClient = OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build()

            val urlBuilder = HttpUrl.parse(ServerConfig().getHealthTrackerDatapoints())!!.newBuilder()
            urlBuilder.addQueryParameter("optionName", optionName)
            urlBuilder.addQueryParameter("phoneNumber", phoneNumber)

            val url = urlBuilder.build().toString()

            val request = Request.Builder()
                    .url(url)
                    .build()

            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()

            uiThread {
                Log.d("mess", responce_string.toString())

                if (responce_string != "dataMissing") {
                    if (responce_string != "serverError") {
                        handleResponse(responce_string)
                    } else {
                        showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                    }
                } else {
                    showMessageAfterSignup(getString(R.string.something_goes_wrong), Color.RED)
                }
            }
        }
    }

    fun handleResponse(responseString: String) {
        val chunkOfData = responseString.split("___")
        val size = chunkOfData.size
        var counter = 1

        while (counter < size) {
            val signleDataStream = chunkOfData[counter]
            val id = signleDataStream.split("*")[0]
            val optionValue = signleDataStream.split("*")[1]
            val day = signleDataStream.split("*")[2]
            val month = signleDataStream.split("*")[3]
            val year = signleDataStream.split("*")[4]

            /*Log.d("mess","date "+day+month+year)

            val cal = Calendar.getInstance()
            cal.set(Calendar.YEAR, year.toInt())
            cal.set(Calendar.MONTH, month.toInt())
            cal.set(Calendar.DAY_OF_MONTH, day.toInt())
            val dateRepresentation = cal.time

            */

            series_2!!.appendData(DataPoint(counter.toDouble(), optionValue.toDouble()), false, 100000)

            if (counter == 1) {
                findViewById<TextView>(R.id.startDate).setText(day + "/" + month + "/" + year)
            }

            counter++

            if (counter < size) {
                findViewById<TextView>(R.id.endDate).setText(day + "/" + month + "/" + year)
            }
        }

        graph!!.addSeries(series_2)

    }

    fun showMessageAfterSignup(msg_text: String, color: Int) {
        val st = StyleableToast.Builder(this)
                .text(msg_text)
                .textColor(Color.WHITE)
                .backgroundColor(color)
                .build()
        st.show()
    }

}
