package com.medapp.medapp.IntroSlider;

import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TextView;

import com.medapp.medapp.R;

public class PagerAnim implements ViewPager.PageTransformer {

  de.hdodenhof.circleimageview.CircleImageView imageView;
  TextView head, descriptions;


  @Override
  public void transformPage(@NonNull View view, float position) {

    imageView = view.findViewById(R.id.profile_image);
    head = view.findViewById(R.id.heading);
    descriptions = view.findViewById(R.id.desc);
    //    skipBtn = view.findViewById(R.id.btn_skip);
    //  nextBtn = view.findViewById(R.id.btn_next);

    int pageWidth = view.getWidth();

    if (position < -1) { // [-Infinity,-1)
      // This page is way off-screen to the left.
      view.setAlpha(0);

    } else if (position <= 1) { // [-1,1]

      //imageView.setTranslationX((float) (-(1 - position) * 0.15 * pageWidth));
      imageView.setTranslationX((float) (-(1 - position) * 0.5 * pageWidth));
      //mBlurLabel.setTranslationX((float) (-(1 - position) * 0.5 * pageWidth));

      //head.setTranslationX((float) (-(1 - position) * 0.30 * pageWidth));
      head.setTranslationX(-(1 - position) * pageWidth);
      // mDimLabel.setTranslationX((float) (-(1 - position) * pageWidth));

      //descriptions.setTranslationX((float) (-(1 - position) * 0.5 * pageWidth));
      descriptions.setTranslationX((float) (-(1 - position) * 1.5 * pageWidth));

      //mDoneButton.setTranslationX((float) (-(1 - position) * 1.7 * pageWidth));
      // The 0.5, 1.5, 1.7 values you see here are what makes the view move in a different speed.
      // The bigger the number, the faster the view will translate.
      // The result float is preceded by a minus because the views travel in the opposite direction of the movement.

      //  skipBtn.setTranslationX((position) * (pageWidth / 4));

      //nextBtn.setTranslationX((position) * (pageWidth));

      //mTint.setTranslationX((position) * (pageWidth / 2));

      //mDesaturate.setTranslationX((position) * (pageWidth / 1));
      // This is another way to do it

      imageView.setTranslationX((position) * (pageWidth / 4));
      //imageView.setTranslationX((float) (-(1 - position) * 0.5 * pageWidth));
      //mBlurLabel.setTranslationX((float) (-(1 - position) * 0.5 * pageWidth));

      head.setTranslationX((position) * pageWidth);
      //head.setTranslationX(-(1 - position) * pageWidth);
      // mDimLabel.setTranslationX((float) (-(1 - position) * pageWidth));

      descriptions.setTranslationX((position) * (pageWidth / 2));


    } else { // (1,+Infinity]
      // This page is way off-screen to the right.
      view.setAlpha(0);
    }
  }
}
