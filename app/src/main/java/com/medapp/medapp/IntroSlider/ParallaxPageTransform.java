package com.medapp.medapp.IntroSlider;

import android.support.v4.view.ViewPager;
import android.view.View;

import com.medapp.medapp.R;

public class ParallaxPageTransform implements ViewPager.PageTransformer {

  de.hdodenhof.circleimageview.CircleImageView dummyImageView;

  @Override
  public void transformPage(View view, float position) {

    dummyImageView = view.findViewById(R.id.profile_image);

    int pageWidth = view.getWidth();


    if (position < -1) { // [-Infinity,-1)
      // This page is way off-screen to the left.
      view.setAlpha(1);

    } else if (position <= 1) { // [-1,1]

      dummyImageView.setTranslationX(-position * (pageWidth / 2)); //Half the normal speed

    } else { // (1,+Infinity]
      // This page is way off-screen to the right.
      view.setAlpha(1);
    }
  }
}
