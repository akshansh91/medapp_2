package com.medapp.medapp

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Button
import android.widget.EditText
import com.medapp.medapp.config.ServerConfig
import com.muddzdev.styleabletoastlibrary.StyleableToast
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.util.concurrent.TimeUnit

class addPermanentDiseases : AppCompatActivity() {
    private var phoneNumber: String? = null
    val TAG: String? = "Activity_Name"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_permanent_diseases)
        Log.d(TAG, "Inside addPermanentDiseases")
        //get phone number
        val loginInfo = applicationContext.getSharedPreferences("loginInfo", Context.MODE_PRIVATE)
        phoneNumber = loginInfo.getString("phoneNumber", "")

        //onclick next button
        onClickNextButton()

    }

    fun onClickNextButton() {
        findViewById<Button>(R.id.submitDiseaseName).setOnClickListener {
            val diseaseName = findViewById<EditText>(R.id.d_name).text.toString()
            val description = findViewById<EditText>(R.id.descriptionOfDisease).text.toString()
            if (diseaseName.length != 0) {
                if (description.length == 0) {
                    description == " "
                }
                insertPermanentDisease(phoneNumber!!, diseaseName, description)
            } else {
                showMessageAfterSignup(getString(R.string.enter_disease_name), Color.RED)
            }
        }
    }

    fun insertPermanentDisease(phoneNumber: String, diseaseName: String, description: String) {
        doAsync {
            val client: OkHttpClient = OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build()

            val urlBuilder = HttpUrl.parse(ServerConfig().insertPermanentDisease())!!.newBuilder()
            urlBuilder.addQueryParameter("diseaseName", diseaseName)
            urlBuilder.addQueryParameter("phoneNumber", phoneNumber)
            urlBuilder.addQueryParameter("description", description)

            val url = urlBuilder.build().toString()

            val request = Request.Builder()
                    .url(url)
                    .build()

            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()

            uiThread {
                Log.d("mess", responce_string.toString())

                if (responce_string != "dataMissing") {
                    if (responce_string != "serverError") {
                        if (responce_string == "ok") {
                            diseaseAddedSuccessfully()
                        } else {
                            showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                        }
                    } else {
                        showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                    }
                } else {
                    showMessageAfterSignup(getString(R.string.something_goes_wrong), Color.RED)
                }
            }
        }
    }

    fun showMessageAfterSignup(msg_text: String, color: Int) {
        val st = StyleableToast.Builder(this)
                .text(msg_text)
                .textColor(Color.WHITE)
                .backgroundColor(color)
                .build()
        st.show()
    }

    fun diseaseAddedSuccessfully() {
        showMessageAfterSignup(getString(R.string.successfully_added_the_disease), Color.GREEN)

        val i = Intent(this@addPermanentDiseases, permanentDiseaese::class.java)
        startActivity(i)
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        finish()
    }

}
