package com.medapp.medapp.permanent_diseases

import android.content.Context
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.muddzdev.styleabletoastlibrary.StyleableToast
import com.medapp.medapp.R
import com.medapp.medapp.config.ServerConfig
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.util.*
import java.util.concurrent.TimeUnit

class add_doctor_appointment : AppCompatActivity() {
    private var phoneNumber: String? = null
    private var diseaseName: String? = null
    private var flag: String = "0"
    private var day: String? = null
    private var month: String? = null
    private var year: String? = null
    private var hour: String? = null
    private var min: String? = null
    private var sec: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_doctor_appointment)

        //get phone number
        val loginInfo = applicationContext.getSharedPreferences("loginInfo", Context.MODE_PRIVATE)
        phoneNumber = loginInfo.getString("phoneNumber", "")

        //get disease Name
        val p_i = getIntent()
        diseaseName = p_i.getStringExtra("diseaseName")

        //onclick add date and time
        onclickDateAndTimePickerButtons()

        //on next clicked
        onNextClicked()

    }

    //on next button clicked
    fun onNextClicked() {
        findViewById<Button>(R.id.nextButton).setOnClickListener {
            var description = getDescription()
            var hospitalName = getHospitalName()
            if (description.length == 0) {
                description = " "
            }
            if (hospitalName.length == 0) {
                hospitalName = " "
            }
            if (day != null && month != null && year != null && hour != null && min != null && sec != null) {
                insertDoctorAppoinmentToDatabase(
                        phoneNumber!!,
                        diseaseName!!,
                        description,
                        day!!,
                        month!!,
                        year!!,
                        hour + ":" + min + ":" + sec,
                        hospitalName,
                        flag

                )
            } else {
                showMessageAfterSignup(getString(R.string.enter_date_n_time), Color.RED)
            }
        }
    }

    //date and time pickers
    fun onclickDateAndTimePickerButtons() {
        findViewById<Button>(R.id.PickDate).setOnClickListener {
            createDatePickerDialog()
        }
        findViewById<Button>(R.id.PickTime).setOnClickListener {
            createTimePickerDialog()
        }
    }

    fun createDatePickerDialog() {
        val now = Calendar.getInstance()
        val dpd = DatePickerDialog.newInstance(
                object : DatePickerDialog.OnDateSetListener {
                    override fun onDateSet(view: DatePickerDialog?, year_: Int, monthOfYear: Int, dayOfMonth: Int) {
                        day = dayOfMonth.toString()
                        month = (monthOfYear + 1).toString()
                        year = year_.toString()

                        loadDateIntPreView(day + "/" + month + "/" + year)
                    }
                },
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        )
        dpd.show(fragmentManager, "Datepickerdialog")
    }

    fun loadDateIntPreView(date: String) {
        findViewById<TextView>(R.id.datePreview).setText(date)
    }

    fun createTimePickerDialog() {
        val now = Calendar.getInstance()
        val dpd = TimePickerDialog.newInstance(
                object : TimePickerDialog.OnTimeSetListener {
                    override fun onTimeSet(view: TimePickerDialog?, hourOfDay: Int, minute: Int, second: Int) {
                        hour = hourOfDay.toString()
                        min = minute.toString()
                        sec = second.toString()

                        loadTImeIntPreView(hour + ":" + min + ":" + sec)
                    }

                },
                0,
                0,
                true
        )
        dpd.show(fragmentManager, "Timepickerdialog")
    }

    fun loadTImeIntPreView(time: String) {
        findViewById<TextView>(R.id.timePreview).setText(time)
    }

    //hospital name and description
    fun getHospitalName(): String {
        return findViewById<EditText>(R.id.hospitalName).text.toString()
    }

    fun getDescription(): String {
        return findViewById<EditText>(R.id.description).text.toString()
    }

    //next step
    fun insertDoctorAppoinmentToDatabase(
            phoneNumber: String,
            diseaseName: String,
            description: String,
            day: String,
            month: String,
            year: String,
            time: String,
            hospitalName: String,
            flag: String
    ) {
        doAsync {
            val client: OkHttpClient = OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build()

            val urlBuilder = HttpUrl.parse(ServerConfig().insertDoctorAppointment())!!.newBuilder()
            urlBuilder.addQueryParameter("phoneNumber", phoneNumber)
            urlBuilder.addQueryParameter("diseaseName", diseaseName)
            urlBuilder.addQueryParameter("description", description)
            urlBuilder.addQueryParameter("day", day)
            urlBuilder.addQueryParameter("month", month)
            urlBuilder.addQueryParameter("year", year)
            urlBuilder.addQueryParameter("time", time)
            urlBuilder.addQueryParameter("hospitalName", hospitalName)
            urlBuilder.addQueryParameter("flag", flag)


            val url = urlBuilder.build().toString()

            val request = Request.Builder()
                    .url(url)
                    .build()

            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()

            uiThread {
                Log.d("mess", responce_string.toString())

                if (responce_string != "dataMissing") {
                    if (responce_string != "serverError") {
                        if (responce_string == "ok") {
                            addedSuccessfully()
                        } else {
                            showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                        }
                    } else {
                        showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                    }
                } else {
                    showMessageAfterSignup(getString(R.string.something_goes_wrong), Color.RED)
                }
            }
        }
    }

    fun showMessageAfterSignup(msg_text: String, color: Int) {
        val st = StyleableToast.Builder(this)
                .text(msg_text)
                .textColor(Color.WHITE)
                .backgroundColor(color)
                .build()
        st.show()
    }

    fun addedSuccessfully() {
        showMessageAfterSignup(getString(R.string.successful_we_will_take_care_of_your_appointment_dates), Color.GREEN)
        finish()
    }


}
