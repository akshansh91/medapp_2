package com.medapp.medapp.permanent_diseases

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.medapp.medapp.R
import com.medapp.medapp.config.ServerConfig
import com.muddzdev.styleabletoastlibrary.StyleableToast
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.util.*
import java.util.concurrent.TimeUnit

class add_medicine_schedule : AppCompatActivity() {
    private var time: String? = null
    private var hour: String? = null
    private var min: String? = null
    private var sec: String? = null
    private var diseaseName: String? = null
    private var phoneNumber: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_medicine_schedule)

        //get phone number
        val loginInfo = applicationContext.getSharedPreferences("loginInfo", Context.MODE_PRIVATE)
        phoneNumber = loginInfo.getString("phoneNumber", "")

        //get disease Name
        val p_i = getIntent()
        diseaseName = p_i.getStringExtra("diseaseName")

        //time picker
        attachTimePicker()

        //onclick next
        onClickNextButton()
    }

    //time picker
    fun attachTimePicker() {
        findViewById<Button>(R.id.PickTime).setOnClickListener {
            createTimePickerDialog()
        }
    }

    fun createTimePickerDialog() {
        val now = Calendar.getInstance()
        val dpd = TimePickerDialog.newInstance(
                object : TimePickerDialog.OnTimeSetListener {
                    override fun onTimeSet(view: TimePickerDialog?, hourOfDay: Int, minute: Int, second: Int) {
                        hour = hourOfDay.toString()
                        min = minute.toString()
                        sec = second.toString()

                        time = hour + ":" + min + ":" + sec
                        loadTimeIntPreView(hour + ":" + min + ":" + sec)
                    }

                },
                0,
                0,
                true
        )
        dpd.show(fragmentManager, "Timepickerdialog")
    }

    fun loadTimeIntPreView(time: String) {
        findViewById<TextView>(R.id.timePreview).setText(time)
    }

    //on click next button
    fun getMedicineName(): String {
        return findViewById<EditText>(R.id.medicineNameEditText).text.toString()
    }

    fun onClickNextButton() {
        findViewById<Button>(R.id.nextButton).setOnClickListener {
            if (time != null) {
                createNewMedicineSchedule(phoneNumber!!, diseaseName!!, getMedicineName(), time!!)
            } else {
                showMessageAfterSignup(getString(R.string.enter_time), Color.RED)
            }
        }
    }

    fun showMessageAfterSignup(msg_text: String, color: Int) {
        val st = StyleableToast.Builder(this)
                .text(msg_text)
                .textColor(Color.WHITE)
                .backgroundColor(color)
                .build()
        st.show()
    }

    fun createNewMedicineSchedule(phoneNumber: String, diseaseName: String, medicineName: String, time: String) {
        doAsync {
            val client: OkHttpClient = OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build()

            val urlBuilder = HttpUrl.parse(ServerConfig().insertMedicineSchedule())!!.newBuilder()
            urlBuilder.addQueryParameter("phoneNumber", phoneNumber)
            urlBuilder.addQueryParameter("diseaseName", diseaseName)
            urlBuilder.addQueryParameter("medicineName", medicineName)
            urlBuilder.addQueryParameter("time", time)

            val url = urlBuilder.build().toString()

            val request = Request.Builder()
                    .url(url)
                    .build()

            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()

            uiThread {
                Log.d("mess", responce_string.toString())

                if (responce_string != "dataMissing") {
                    if (responce_string != "serverError") {
                        if (responce_string == "ok") {
                            addedSuccessfully()
                        } else {
                            showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                        }
                    } else {
                        showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                    }
                } else {
                    showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                }
            }
        }
    }

    fun addedSuccessfully() {
        showMessageAfterSignup(getString(R.string.successful_we_will_take_care_of_your_medicine_schedule), Color.GREEN)
        finish()
    }
}
