package com.medapp.medapp.permanent_diseases

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.medapp.medapp.R
import com.medapp.medapp.config.ServerConfig
import com.muddzdev.styleabletoastlibrary.StyleableToast
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.util.concurrent.TimeUnit

class show_medicine_schedule : AppCompatActivity() {
    private var diseaseName: String? = null
    private var medicine_schedule_recyclerview: RecyclerView? = null
    private var result_list_for_medicine_schedule: ArrayList<medicine_schedule_store> = ArrayList<medicine_schedule_store>()
    private var medicine_schedule_adapter_holder: RecyclerView.Adapter<medicine_schedule_adapter.NumberViewHolder>? = null
    private var phoneNumber: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_medicine_schedule)

        //get phone number
        val loginInfo = applicationContext.getSharedPreferences("loginInfo", Context.MODE_PRIVATE)
        phoneNumber = loginInfo.getString("phoneNumber", "")

        //get disease Name
        val p_i = getIntent()
        diseaseName = p_i.getStringExtra("diseaseName")

        //on click add button
        onAddClicked()

        //attach adapter to recyclerView
        attach_medicine_schedule_adapter_to_recyclerview()

        loadMedicineSchedule()

    }

    //load medicine schedule
    fun loadMedicineSchedule() {
        doAsync {
            val client: OkHttpClient = OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build()

            val urlBuilder = HttpUrl.parse(ServerConfig().getMedicineSchedule())!!.newBuilder()
            urlBuilder.addQueryParameter("phoneNumber", phoneNumber)
            urlBuilder.addQueryParameter("diseaseName", diseaseName)

            val url = urlBuilder.build().toString()

            val request = Request.Builder()
                    .url(url)
                    .build()

            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()

            uiThread {
                Log.d("mess", responce_string.toString())

                if (responce_string != "dataMissing") {
                    if (responce_string != "serverError") {
                        handleResponse(responce_string)
                    } else {
                        showMessageAfterSignup(getString(R.string.no_doctor_appointment_is_there), Color.RED)
                    }
                } else {
                    showMessageAfterSignup(getString(R.string.something_goes_wrong), Color.RED)
                }
            }
        }
    }

    fun handleResponse(responseString: String) {
        result_list_for_medicine_schedule.clear()
        medicine_schedule_adapter_holder!!.notifyDataSetChanged()
        medicine_schedule_recyclerview!!.adapter = null
        attach_medicine_schedule_adapter_to_recyclerview()

        val chunkOfDate = responseString.split("___")
        val size = chunkOfDate.size
        var counter = 1

        while (counter < size) {
            val singleDataStream = chunkOfDate[counter]
            val medicineName = singleDataStream.split("*")[0]
            val hours = (singleDataStream.split("*")[1]).split(":")[0]
            val min = (singleDataStream.split("*")[1]).split(":")[1]
            val sec = (singleDataStream.split("*")[1]).split(":")[2]

            result_list_for_medicine_schedule.add(
                    medicine_schedule_store(
                            medicineName,
                            hours,
                            min,
                            sec
                    )
            )
            counter++
        }

        medicine_schedule_adapter_holder!!.notifyDataSetChanged()
    }

    //recycler view adapter
    fun attach_medicine_schedule_adapter_to_recyclerview() {
        val medicine_schedule_recyclerView: RecyclerView = findViewById<RecyclerView>(R.id.medicine_schedule_recyclerView)
        medicine_schedule_recyclerview = medicine_schedule_recyclerView
        val layoutManager = LinearLayoutManager(applicationContext)
        medicine_schedule_recyclerView.layoutManager = layoutManager
        medicine_schedule_recyclerView.setHasFixedSize(false)
        medicine_schedule_recyclerView.isNestedScrollingEnabled = false
        medicine_schedule_adapter_holder = medicine_schedule_adapter()
        medicine_schedule_recyclerView.adapter = medicine_schedule_adapter_holder
    }

    inner class medicine_schedule_store(medicineName: String, hours: String, min: String, sec: String) {
        private var inner_medicineName: String = medicineName
        private var inner_hours: String = hours
        private var inner_min: String = min
        private var inner_sec: String = sec

        fun get_medicineName(): String {
            return inner_medicineName
        }

        fun get_hours(): String {
            return inner_hours
        }

        fun get_min(): String {
            return inner_min
        }

        fun get_sec(): String {
            return inner_sec
        }

    }

    private inner class medicine_schedule_adapter() : RecyclerView.Adapter<medicine_schedule_adapter.NumberViewHolder>() {

        override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): NumberViewHolder {
            val context = viewGroup.context
            val layoutIdForListItem = R.layout.show_medicine_blueprint
            val inflater = LayoutInflater.from(applicationContext)
            val shouldAttachToParentImmediately = false

            val view = inflater.inflate(layoutIdForListItem, viewGroup, shouldAttachToParentImmediately)
            val viewHolder = NumberViewHolder(view)

            return viewHolder
        }

        override fun onBindViewHolder(holder: NumberViewHolder, position: Int) {
            val current_object = result_list_for_medicine_schedule.get(position)
            holder.bind(current_object)
        }

        override fun getItemCount(): Int {
            return result_list_for_medicine_schedule.size
        }

        inner class NumberViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            private var medicineNameTextView: TextView? = null
            private var timeTextView: TextView? = null
            private var deleteButton: Button? = null
            private var m: String? = null
            private var time: String? = null

            init {
                medicineNameTextView = itemView.findViewById(R.id.medicineNameTextView)
                timeTextView = itemView.findViewById(R.id.timeTextView)
                deleteButton = itemView.findViewById(R.id.deleteButton)
            }

            fun bind(current_object: medicine_schedule_store) {
                m = current_object.get_medicineName()
                time = current_object.get_hours() + ":" + current_object.get_min() + ":" + current_object.get_sec()

                if (m!!.length > 0) {
                    medicineNameTextView!!.setText(current_object.get_medicineName())
                }
                timeTextView!!.setText(time)

                deleteButton!!.setOnClickListener {
                    onDeleteClicked(phoneNumber!!, diseaseName!!, time!!)
                }
            }
        }

    }

    fun showMessageAfterSignup(msg_text: String, color: Int) {
        val st = StyleableToast.Builder(this)
                .text(msg_text)
                .textColor(Color.WHITE)
                .backgroundColor(color)
                .build()
        st.show()
    }

    //on add clicked
    fun onAddClicked() {
        findViewById<Button>(R.id.addMedicineScheduleButton).setOnClickListener {
            val i = Intent(this@show_medicine_schedule, add_medicine_schedule::class.java)
            i.putExtra("diseaseName", diseaseName)
            startActivity(i)

            finish()
        }
    }

    //on delete button clicked
    fun onDeleteClicked(phoneNumber: String, diseaseName: String, time: String) {
        doAsync {
            val client: OkHttpClient = OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build()

            val urlBuilder = HttpUrl.parse(ServerConfig().deleteMedicineSchedule())!!.newBuilder()
            urlBuilder.addQueryParameter("phoneNumber", phoneNumber)
            urlBuilder.addQueryParameter("diseaseName", diseaseName)
            urlBuilder.addQueryParameter("time", time)

            val url = urlBuilder.build().toString()

            val request = Request.Builder()
                    .url(url)
                    .build()

            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()

            uiThread {
                Log.d("mess", responce_string.toString())

                if (responce_string != "dataMissing") {
                    if (responce_string != "serverError") {
                        if (responce_string == "ok") {
                            medicineScheduleDeletedSuccessfully()
                        } else {
                            showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                        }
                    } else {
                        showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                    }
                } else {
                    showMessageAfterSignup(getString(R.string.something_goes_wrong), Color.RED)
                }
            }
        }
    }

    fun medicineScheduleDeletedSuccessfully() {
        showMessageAfterSignup(getString(R.string.deleted_successfully), Color.GREEN)
        loadMedicineSchedule()
    }
}
