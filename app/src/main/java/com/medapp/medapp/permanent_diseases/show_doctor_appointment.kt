package com.medapp.medapp.permanent_diseases

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.medapp.medapp.R
import com.medapp.medapp.config.ServerConfig
import com.muddzdev.styleabletoastlibrary.StyleableToast
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.util.concurrent.TimeUnit

class show_doctor_appointment : AppCompatActivity() {
    private var diseaseName: String? = null
    private var doctorApponitment_recyclerview: RecyclerView? = null
    private var result_list_for_doctorApponitment: ArrayList<doctorApponitment_store> = ArrayList<doctorApponitment_store>()
    private var doctorApponitment_adapter_holder: RecyclerView.Adapter<doctorApponitment_adapter.NumberViewHolder>? = null
    private var phoneNumber: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_doctor_appointment)

        //get phone number
        val loginInfo = applicationContext.getSharedPreferences("loginInfo", Context.MODE_PRIVATE)
        phoneNumber = loginInfo.getString("phoneNumber", "")

        //get disease Name
        val p_i = getIntent()
        diseaseName = p_i.getStringExtra("diseaseName")

        //onclick add button
        onClickAddButton()

        //attach adapter to recyclerview
        attach_da_adapter_to_recyclerview()

        //load doctor appointments
        loadDoctorAppointments()

    }

    //load doctor appointment
    fun loadDoctorAppointments() {
        doAsync {
            val client: OkHttpClient = OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build()

            val urlBuilder = HttpUrl.parse(ServerConfig().getDoctorAppointments())!!.newBuilder()
            urlBuilder.addQueryParameter("phoneNumber", phoneNumber)
            urlBuilder.addQueryParameter("diseaseName", diseaseName)

            val url = urlBuilder.build().toString()

            val request = Request.Builder()
                    .url(url)
                    .build()

            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()

            uiThread {
                Log.d("mess", responce_string.toString())

                if (responce_string != "dataMissing") {
                    if (responce_string != "serverError") {
                        handleResponse(responce_string)
                    } else {
                        showMessageAfterSignup(getString(R.string.no_doctor_appointment_is_there), Color.RED)
                    }
                } else {
                    showMessageAfterSignup(getString(R.string.something_goes_wrong), Color.RED)
                }
            }
        }
    }

    fun handleResponse(responseString: String) {
        result_list_for_doctorApponitment.clear()
        doctorApponitment_adapter_holder!!.notifyDataSetChanged()
        doctorApponitment_recyclerview!!.adapter = null
        attach_da_adapter_to_recyclerview()

        val chunkOfDate = responseString.split("___")
        val size = chunkOfDate.size
        var counter = 1

        while (counter < size) {
            val singleDataStream = chunkOfDate[counter]
            val hospitalName = singleDataStream.split("*")[0]
            val description = singleDataStream.split("*")[1]
            val day = singleDataStream.split("*")[2]
            val month = singleDataStream.split("*")[3]
            val year = singleDataStream.split("*")[4]
            val time = singleDataStream.split("*")[5]

            result_list_for_doctorApponitment.add(
                    doctorApponitment_store(
                            hospitalName,
                            description,
                            day.toString() + "/" + month.toString() + "/" + year.toString(),
                            time
                    )
            )

            counter++
        }

        doctorApponitment_adapter_holder!!.notifyDataSetChanged()
    }

    //doctor appointments recyclerview
    fun attach_da_adapter_to_recyclerview() {
        val doctorApponitment_recyclerView: RecyclerView = findViewById<RecyclerView>(R.id.showDoctorAppointmentRecyclerview)
        doctorApponitment_recyclerview = doctorApponitment_recyclerView
        val layoutManager = LinearLayoutManager(applicationContext)
        doctorApponitment_recyclerView.layoutManager = layoutManager
        doctorApponitment_recyclerView.setHasFixedSize(false)
        doctorApponitment_recyclerView.isNestedScrollingEnabled = false
        doctorApponitment_adapter_holder = doctorApponitment_adapter()
        doctorApponitment_recyclerView.adapter = doctorApponitment_adapter_holder
    }

    inner class doctorApponitment_store(hospitalName: String, description: String, date: String, time: String) {
        private var inner_hospitalName: String = hospitalName
        private var inner_description: String = description
        private var inner_date: String = date
        private var inner_time: String = time

        fun get_hospitalName(): String {
            return inner_hospitalName
        }

        fun get_description(): String {
            return inner_description
        }

        fun get_date(): String {
            return inner_date
        }

        fun get_time(): String {
            return inner_time
        }

    }

    private inner class doctorApponitment_adapter() : RecyclerView.Adapter<doctorApponitment_adapter.NumberViewHolder>() {

        override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): NumberViewHolder {
            val context = viewGroup.context
            val layoutIdForListItem = R.layout.doctor_appointment_blueprint
            val inflater = LayoutInflater.from(applicationContext)
            val shouldAttachToParentImmediately = false

            val view = inflater.inflate(layoutIdForListItem, viewGroup, shouldAttachToParentImmediately)
            val viewHolder = NumberViewHolder(view)

            return viewHolder
        }

        override fun onBindViewHolder(holder: NumberViewHolder, position: Int) {
            val current_object = result_list_for_doctorApponitment.get(position)
            holder.bind(current_object)
        }

        override fun getItemCount(): Int {
            return result_list_for_doctorApponitment.size
        }

        inner class NumberViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            private var hospitalName: TextView? = null
            private var description: TextView? = null
            private var h: String? = null
            private var d: String? = null
            private var date: TextView? = null
            private var time: TextView? = null
            private var deleteButton: Button? = null

            init {
                hospitalName = itemView.findViewById(R.id.hospitalName)
                description = itemView.findViewById(R.id.description)
                date = itemView.findViewById(R.id.date)
                time = itemView.findViewById(R.id.time)
                deleteButton = itemView.findViewById(R.id.deleteButton)
            }

            fun bind(current_object: doctorApponitment_store) {
                h = current_object.get_hospitalName()
                d = current_object.get_description()

                if (h!!.length > 2) {
                    hospitalName!!.setText(current_object.get_hospitalName())
                }
                if (d!!.length > 2) {
                    description!!.setText(current_object.get_description())
                }
                time!!.setText(current_object.get_time())
                date!!.setText(current_object.get_date())

                deleteButton!!.setOnClickListener {
                    onDeleteClicked(phoneNumber!!, diseaseName!!, current_object.get_time())
                }
            }
        }

    }

    fun showMessageAfterSignup(msg_text: String, color: Int) {
        val st = StyleableToast.Builder(this)
                .text(msg_text)
                .textColor(Color.WHITE)
                .backgroundColor(color)
                .build()
        st.show()
    }

    //on click add button
    fun onClickAddButton() {
        findViewById<Button>(R.id.showDoctorAppointmentButton).setOnClickListener {
            finish()

            val i = Intent(this@show_doctor_appointment, add_doctor_appointment::class.java)
            i.putExtra("diseaseName", diseaseName)
            startActivity(i)

        }
    }

    //on delete button clicked
    fun onDeleteClicked(phoneNumber: String, diseaseName: String, time: String) {
        doAsync {
            val client: OkHttpClient = OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build()

            val urlBuilder = HttpUrl.parse(ServerConfig().deleteDoctorAppointments())!!.newBuilder()
            urlBuilder.addQueryParameter("phoneNumber", phoneNumber)
            urlBuilder.addQueryParameter("diseaseName", diseaseName)
            urlBuilder.addQueryParameter("time", time)

            val url = urlBuilder.build().toString()

            val request = Request.Builder()
                    .url(url)
                    .build()

            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()

            uiThread {
                Log.d("mess", responce_string.toString())

                if (responce_string != "dataMissing") {
                    if (responce_string != "serverError") {
                        if (responce_string == "ok") {
                            doctorAppointmentDeletedSuccessfully()
                        } else {
                            showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                        }
                    } else {
                        showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                    }
                } else {
                    showMessageAfterSignup(getString(R.string.something_goes_wrong), Color.RED)
                }
            }
        }
    }

    fun doctorAppointmentDeletedSuccessfully() {
        showMessageAfterSignup(getString(R.string.deleted_successfully), Color.GREEN)
        loadDoctorAppointments()
    }
}
