package com.medapp.medapp

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.medapp.medapp.config.ServerConfig
import com.muddzdev.styleabletoastlibrary.StyleableToast
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.util.concurrent.TimeUnit

class pickFamilyMember : AppCompatActivity() {
    private var c_layout: ViewGroup? = null
    private var familyId: String? = null
    private var familyNames_recyclerview: RecyclerView? = null
    private var result_list_for_familyNames: ArrayList<familyNames_store> = ArrayList()
    private var familyNames_adapter_holder: RecyclerView.Adapter<familyNames_adapter.NumberViewHolder>? = null
    val TAG: String? = "Activity_Name"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pick_family_member)
        setTitle(getString(R.string.pick_family_member))
        Log.d(TAG, "Inside pickFamilyMember")
        //get subpart_c_layout
        c_layout = findViewById(R.id.pickFamilyMember)

        //get family id from persistent storage
        val loginInfo = getSharedPreferences("loginInfo", Context.MODE_PRIVATE)
        familyId = loginInfo.getString("familyId", null)

        //attach adapter to recycler view
        attach_familyName_adapter_to_recyclerview()

        //load family member names
        LoadFamilyMembersNames()

    }

    fun LoadFamilyMembersNames() {
        doAsync {
            val client: OkHttpClient = OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build()

            val urlBuilder = HttpUrl.parse(ServerConfig().getFamilyMemberNamesAndPhoneNumber())!!.newBuilder()
            urlBuilder.addQueryParameter("familyId", familyId)

            val url = urlBuilder.build().toString()

            val request = Request.Builder()
                    .url(url)
                    .build()

            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()

            uiThread {
                Log.d("mess", responce_string.toString())

                if (responce_string != "dataMissing") {
                    if (responce_string != "serverError") {
                        if (responce_string.length < 450) {
                            loadFamilyMemberNamesInLayout(responce_string)
                        } else {
                            showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                        }
                    } else {
                        showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                    }
                } else {
                    showMessageAfterSignup(getString(R.string.something_went_wrong), Color.RED)
                }
            }
        }
    }

    fun loadFamilyMemberNamesInLayout(familyMemberNamesString: String) {
        val arr_of_namesAndPhoneNumber = familyMemberNamesString.split("___")
        var counter = 1
        val size = arr_of_namesAndPhoneNumber.size
        while (counter < size) {
            val name = arr_of_namesAndPhoneNumber[counter].split("*")[0]
            val phoneNumber = arr_of_namesAndPhoneNumber[counter].split("*")[1]

            result_list_for_familyNames.add(familyNames_store(name, phoneNumber))
            familyNames_adapter_holder!!.notifyDataSetChanged()

            counter++
        }
    }

    fun showMessageAfterSignup(msg_text: String, color: Int) {
        val st = StyleableToast.Builder(this)
                .text(msg_text)
                .textColor(Color.WHITE)
                .backgroundColor(color)
                .build()
        st.show()
    }

    //folder recyclerview
    fun attach_familyName_adapter_to_recyclerview() {
        val folder_recyclerView: RecyclerView = findViewById(R.id.familyNameRecyclerView)
        familyNames_recyclerview = folder_recyclerView
        val layoutManager = LinearLayoutManager(applicationContext)
        folder_recyclerView.layoutManager = layoutManager
        folder_recyclerView.setHasFixedSize(false)
        folder_recyclerView.isNestedScrollingEnabled = false
        familyNames_adapter_holder = familyNames_adapter()
        folder_recyclerView.adapter = familyNames_adapter_holder
    }

    inner class familyNames_store(familyMembername: String, familyMemberNumber: String) {
        private var inner_familyMemberName: String = familyMembername
        private var inner_familyMemberPhoneNumber: String = familyMemberNumber

        fun get_familyMemberName(): String {
            return inner_familyMemberName
        }

        fun get_familyMemberPhoneNumber(): String {
            return inner_familyMemberPhoneNumber
        }
    }

    private inner class familyNames_adapter() : RecyclerView.Adapter<familyNames_adapter.NumberViewHolder>() {

        override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): NumberViewHolder {
            val context = viewGroup.context
            val layoutIdForListItem = R.layout.pick_family_member_blueprint
            val inflater = LayoutInflater.from(applicationContext)
            val shouldAttachToParentImmediately = false

            val view = inflater.inflate(layoutIdForListItem, viewGroup, shouldAttachToParentImmediately)
            val viewHolder = NumberViewHolder(view)

            return viewHolder
        }

        override fun onBindViewHolder(holder: NumberViewHolder, position: Int) {
            var current_object = result_list_for_familyNames.get(position)
            holder.bind(current_object)
        }

        override fun getItemCount(): Int {
            return result_list_for_familyNames.size
        }

        inner class NumberViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            private var familyMemberNameButton: Button? = null

            init {
                familyMemberNameButton = itemView.findViewById(R.id.name)
            }

            fun bind(current_object: familyNames_store) {
                val family_member_name = current_object.get_familyMemberName()
                val family_member_phoneNumber = current_object.get_familyMemberPhoneNumber()

                familyMemberNameButton!!.setText(family_member_name)

                //onclick listener
                familyMemberNameButton!!.setOnClickListener {
                    proceedToNextStep(family_member_name, family_member_phoneNumber)
                }
            }

        }

    }

    fun proceedToNextStep(name: String, familyMemberPhoneNumber: String) {
        val intent = intent
        val b_args = intent.getBundleExtra("BUNDLE")
        val _object = (b_args.getSerializable("ARRAYLIST")) as java.io.Serializable
        val typeOfDocument = intent.getStringExtra("typeOfDocument")
        val category = intent.getStringExtra("categoryOfDocument")

        val i = Intent(this, previewImages::class.java)
        val args = Bundle()
        args.putSerializable("ARRAYLIST", _object)
        i.putExtra("familyMemberName", name)
        i.putExtra("BUNDLE", args)
        i.putExtra("typeOfDocument", typeOfDocument)
        i.putExtra("categoryOfDocument", category)
        i.putExtra("familyMemberPhoneNumber", familyMemberPhoneNumber)
        startActivity(i)
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
    }
}
