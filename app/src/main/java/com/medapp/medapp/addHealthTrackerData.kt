package com.medapp.medapp

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.res.Configuration
import android.graphics.Color
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.*
import com.jjoe64.graphview.GraphView
import com.jjoe64.graphview.series.DataPoint
import com.jjoe64.graphview.series.LineGraphSeries
import com.medapp.medapp.config.ServerConfig
import com.medapp.medapp.utils.left_drawer
import com.muddzdev.styleabletoastlibrary.StyleableToast
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.spinner
import org.jetbrains.anko.uiThread
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class addHealthTrackerData : AppCompatActivity() {
    private var optionName: String? = null
    private var phoneNumber: String? = null
    private var series_2: LineGraphSeries<DataPoint>? = null
    private var graph: GraphView? = null
    private var fromday: String? = "0"
    private var frommonth: String? = "0"
    private var fromyear: String? = "0"
    private var UserSysTime: String? = "0:0"
    private var unit: TextView? = null
    private var layoutBp: ConstraintLayout? = null
    private var layoutEye: ConstraintLayout? = null
    private var layoutHeight: ConstraintLayout? = null
    private var layoutCalories: ConstraintLayout? = null
    private var value: EditText? = null
    private var bp1: EditText? = null
    private var bp2: EditText? = null
    private var calorieCalculator: EditText? = null
    private var calories: EditText? = null
    private var spinnerLeft: Spinner? = null
    private var spinnerRight: Spinner? = null
    private var powerLeft: EditText? = null
    private var powerRight: EditText? = null
    private var height: EditText? = null
    private var spinnerHeightUnits: Spinner? = null
    private var heightCMS: Array<String>? = null
    private var heightFTS: Array<String>? = null
    private var TAG: String = "Activity_Name"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_health_tracker_data)
        Log.d(TAG, "Inside addHealth")
        //get phone number
        val loginInfo = applicationContext.getSharedPreferences("loginInfo", Context.MODE_PRIVATE)
        phoneNumber = loginInfo.getString("phoneNumber", "")

        left_drawer(this, this@addHealthTrackerData, findViewById(R.id.addHealthTrackerToolbar)!!).createNavigationDrawer()

        //option name
        val pI = intent
        optionName = pI.getStringExtra("optionName")

        Log.d("addHealthTrackerData", "Option: $optionName")

        value = findViewById(R.id.optionValue)
        layoutBp = findViewById(R.id.layout_bp)
        layoutCalories = findViewById(R.id.layout_calories)
        layoutEye = findViewById(R.id.layout_eye)
        layoutHeight = findViewById(R.id.layout_height)
        unit = findViewById(R.id.units)
        bp1 = findViewById(R.id.bp_1)
        bp2 = findViewById(R.id.bp_2)
        calorieCalculator = findViewById(R.id.calorieCalculator)
        calories = findViewById(R.id.calories)
        spinnerLeft = findViewById(R.id.spinnerLeft)
        spinnerRight = findViewById(R.id.spinnerRight)
        powerLeft = findViewById(R.id.powerLeft)
        powerRight = findViewById(R.id.powerRight)
        height = findViewById(R.id.height)
        spinnerHeightUnits = findViewById(R.id.spinnerHeightUnits)

        spinnerLeft!!.setSelection(0)
        spinnerRight!!.setSelection(0)
        spinnerHeightUnits!!.setSelection(0)

        val posNeg = getStringByLocal(this@addHealthTrackerData, R.array.heightUnits, "en")
        heightCMS = getStringByLocal(this@addHealthTrackerData, R.array.height_cms, "en")
        heightFTS = getStringByLocal(this@addHealthTrackerData, R.array.height_ft, "en")

        spinnerHeightUnits!!.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val selectedItem = posNeg[position]
                if (selectedItem == "cms") {
                    var layout: ConstraintLayout = layoutInflater.inflate(R.layout.height_picker_layout, null) as ConstraintLayout
                    var heightPicker: NumberPicker = layout.findViewById(R.id.heightPicker)

                    var length: Int = heightCMS!!.size

                    heightPicker.minValue = 0
                    heightPicker.maxValue = length - 1
                    heightPicker.displayedValues = heightCMS
                    heightPicker.descendantFocusability = NumberPicker.FOCUS_BLOCK_DESCENDANTS

                    var alertDialog: AlertDialog = AlertDialog.Builder(this@addHealthTrackerData).setPositiveButton("Submit", null)
                            .setNegativeButton("Cancel", null)
                            .setView(layout)
                            .setCancelable(false)
                            .create()

                    alertDialog.show()

                    alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener {
                        var value2: Int = heightPicker.value
                        registerOptionValue(optionName!!, value2.toString(), phoneNumber!!)
                        alertDialog.dismiss()
                    }

                    alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setOnClickListener {
                        alertDialog.dismiss()
                    }
                } else if (selectedItem == "feet") {
                    var layout: ConstraintLayout = layoutInflater.inflate(R.layout.height_picker_layout, null) as ConstraintLayout
                    var heightPicker: NumberPicker = layout.findViewById(R.id.heightPicker)

                    var length: Int = heightFTS!!.size

                    heightPicker.minValue = 0
                    heightPicker.maxValue = length - 1
                    heightPicker.displayedValues = heightFTS
                    heightPicker.descendantFocusability = NumberPicker.FOCUS_BLOCK_DESCENDANTS

                    var alertDialog: AlertDialog = AlertDialog.Builder(this@addHealthTrackerData).setPositiveButton("Submit", null)
                            .setNegativeButton("Cancel", null)
                            .setView(layout)
                            .setCancelable(false)
                            .create()

                    alertDialog.show()

                    alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener {
                        var valu: Int = heightPicker.value
                        var value2 = valu * 30.48
                        registerOptionValue(optionName!!, value2.toString(), phoneNumber!!)
                        alertDialog.dismiss()
                    }

                    alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setOnClickListener {
                        alertDialog.dismiss()
                    }
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }
        }
        calorieCalculator!!.setOnClickListener {
            val intent = Intent(this@addHealthTrackerData, calorieCalculatorClass::class.java)
            startActivity(intent)
        }

        if (optionName == getString(R.string.bp)) {
            layoutBp!!.visibility = View.VISIBLE
            unit!!.text = getString(R.string.unitBP)
        } else if (optionName == getString(R.string.weight)) {
            unit!!.text = getString(R.string.unitWeight)
            value!!.visibility = View.VISIBLE
        } else if (optionName == getString(R.string.power_of_glasses)) {
            layoutEye!!.visibility = View.VISIBLE
        } else if (optionName == getString(R.string.height)) {
            layoutHeight!!.visibility = View.VISIBLE
        } else if (optionName == getString(R.string.calories)) {
            layoutCalories!!.visibility = View.VISIBLE
            unit!!.text = getString(R.string.unitCalories)
        } else if (optionName == getString(R.string.sugar)) {
            unit!!.text = getString(R.string.unitSugar)
            value!!.visibility = View.VISIBLE
        } else if (optionName == getString(R.string.sleep)) {
            unit!!.text = getString(R.string.unitSleep)
            value!!.visibility = View.VISIBLE
        } else if (optionName == getString(R.string.pulse)) {
            unit!!.text = getString(R.string.unitPulse)
            value!!.visibility = View.VISIBLE
        }

        //set option name in layout
        findViewById<TextView>(R.id.optionName).text = optionName!!.toUpperCase()
        //on click option track button
        onClickTrackOptionButton()

        //date button function
        initDefaultSysDate()
        initDateButton()
        onClickDateButton()
    }

    private fun getStringByLocal(context: Activity, id: Int?, locale: String): Array<String> {
        val configuration = Configuration(resources.configuration)
        configuration.setLocale(Locale(locale))
        return context.createConfigurationContext(configuration).resources.getStringArray(id!!)
    }

    //save button ...
    private fun onClickTrackOptionButton() {
        findViewById<Button>(R.id.trackOptionButton).setOnClickListener {
            if (optionName == getString(R.string.bp)) {
                val valBP1: String = bp1!!.text.toString()
                val valBP2: String = bp2!!.text.toString()
                if (valBP1.isNotEmpty() && valBP2.isNotEmpty()) {
                    registerOptionValue("systolic", valBP1, phoneNumber!!)
                    registerOptionValue("diastolic", valBP2, phoneNumber!!)
                } else {
                    showMessageAfterSignup(getString(R.string.enter_your) + optionName, Color.RED)
                }
            } else if (optionName == getString(R.string.power_of_glasses)) {

                val powerLeftSpinner: String = spinnerLeft!!.selectedItem.toString()
                val powerRightSpinner: String = spinnerRight!!.selectedItem.toString()

                val powerL: String = powerLeft!!.text.toString()
                val powerR: String = powerRight!!.text.toString()

                if (powerL.isNotEmpty() && powerR.isNotEmpty()) {

                    val fPowerLeft: String = powerLeftSpinner + powerL
                    val fPowerRight: String = powerRightSpinner + powerR

                    registerOptionValue("powerL", fPowerLeft, phoneNumber!!)
                    registerOptionValue("powerR", fPowerRight, phoneNumber!!)
                } else {
                    showMessageAfterSignup(getString(R.string.enter_your) + optionName, Color.RED)
                }
            } else if (optionName == getString(R.string.calories)) {
                val calorieValue: String = calories!!.text.toString()
                if (calorieValue.isNotEmpty()) {
                    registerOptionValue(optionName!!, calorieValue, phoneNumber!!)
                } else {
                    showMessageAfterSignup(getString(R.string.enter_your) + optionName, Color.RED)
                }
            } else {
                val optionValue = findViewById<EditText>(R.id.optionValue).text.toString()
                if (optionValue.isNotEmpty()) {
                    registerOptionValue(optionName!!, optionValue, phoneNumber!!)
                } else {
                    showMessageAfterSignup(getString(R.string.enter_your) + optionName, Color.RED)
                }
            }
        }
    }

    private fun registerOptionValue(optionName: String, optionValue: String, phoneNumber: String) {
        doAsync {
            val client: OkHttpClient = OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build()

            val urlBuilder = HttpUrl.parse(ServerConfig().trackHealth())!!.newBuilder()
            urlBuilder.addQueryParameter("optionName", optionName)
            urlBuilder.addQueryParameter("optionValue", optionValue)
            urlBuilder.addQueryParameter("phoneNumber", phoneNumber)
            urlBuilder.addQueryParameter("day", fromday)
            urlBuilder.addQueryParameter("month", frommonth)
            urlBuilder.addQueryParameter("year", fromyear)
            urlBuilder.addQueryParameter("time", UserSysTime)
            urlBuilder.addQueryParameter("time_DatatypeLong", getTimeForArrDate(fromday!!, frommonth!!, fromyear!!))

            val url = urlBuilder.build().toString()

            val request = Request.Builder()
                    .url(url)
                    .build()

            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()

            uiThread {
                Log.d("mess", responce_string.toString())

                if (responce_string != "dataMissing") {
                    if (responce_string != "serverError") {
                        if (responce_string == "ok") {
                            showMessageAfterSignup(getString(R.string.values_saved_successfully), Color.GREEN)
                            //finish()
                        } else {
                            showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                        }
                    } else {
                        showMessageAfterSignup(getString(R.string.server_error), Color.RED)
                    }
                } else {
                    showMessageAfterSignup(getString(R.string.something_goes_wrong), Color.RED)
                }
            }
        }
    }

    fun showMessageAfterSignup(msg_text: String, color: Int) {
        val st = StyleableToast.Builder(this)
                .text(msg_text)
                .textColor(Color.WHITE)
                .backgroundColor(color)
                .build()
        st.show()
    }

    //init date button
    fun initDateButton() {
        findViewById<Button>(R.id.dateButton).text = "$fromday/$frommonth/$fromyear"
    }

    //onclick date button
    fun onClickDateButton() {
        findViewById<Button>(R.id.dateButton).setOnClickListener {
            createDatePicker()
        }
    }

    //create date picker
    fun createDatePicker() {
        val now = Calendar.getInstance()
        val dpd = DatePickerDialog.newInstance(
                object : DatePickerDialog.OnDateSetListener {
                    override fun onDateSet(view: DatePickerDialog?, year_: Int, monthOfYear: Int, dayOfMonth: Int) {
                        fromday = dayOfMonth.toString()
                        if (fromday!![0].toString() == "0") {
                            fromday!!.replaceFirst("0", "")
                        }
                        frommonth = (monthOfYear + 1).toString()
                        if (frommonth!![0].toString() == "0") {
                            frommonth!!.replaceFirst("0", "")
                        }
                        fromyear = year_.toString()

                        initDateButton()
                    }
                },
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        )
        dpd.show(fragmentManager, "Datepickerdialog")
    }

    //initiate default date and time
    fun initDefaultSysDate() {
        fromday = getDay()
        frommonth = getMonth()
        fromyear = getYear()
        UserSysTime = getUserSysTime()
    }

    //system date and time
    fun getDay(): String {
        val c: Calendar = Calendar.getInstance()
        val df: SimpleDateFormat = SimpleDateFormat("dd")
        val formattedDate: String = df.format(c.time).toString()
        if (formattedDate[0].toString() == "0") {
            formattedDate.replaceFirst("0", "")
        }
        return formattedDate
    }

    fun getMonth(): String {
        val c: Calendar = Calendar.getInstance()
        val df: SimpleDateFormat = SimpleDateFormat("MM")
        val formattedDate: String = df.format(c.time).toString()
        if (formattedDate[0].toString() == "0") {
            formattedDate.replaceFirst("0", "")
        }
        return formattedDate
    }

    fun getYear(): String {
        val c: Calendar = Calendar.getInstance()
        val df: SimpleDateFormat = SimpleDateFormat("yyyy")
        val formattedDate: String = df.format(c.time).toString()
        return formattedDate
    }

    fun getUserSysTime(): String {
        val c: Calendar = Calendar.getInstance()
        val df: SimpleDateFormat = SimpleDateFormat("HH:mm")
        val formattedDate: String = df.format(c.time).toString()
        return formattedDate
    }

    //get time for arr data
    fun getTimeForArrDate(day: String, month: String, year: String): String {
        val c = Calendar.getInstance()
        c.set(year.toInt(), month.toInt(), day.toInt(), 0, 0)
        return c.timeInMillis.toString()
    }
}
