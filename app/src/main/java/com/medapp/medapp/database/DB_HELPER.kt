package com.medapp.medapp.database

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.graphics.Color
import android.util.Log
import com.medapp.medapp.config.ServerConfig
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.util.concurrent.TimeUnit

class DB_HELPER(context: Context?) : SQLiteOpenHelper(context, "DB_1.db", null, 1) {
    var inner_context = context
    var TABLE_NAME_for_health_goals = "health_goals"
    var TABLE_NAME_for_health_goals_id = "_id"
    var TABLE_NAME_for_health_goals_healthGoals = "health_goals"
    var TABLE_NAME_for_health_goals_priority = "priority"
    var TABLE_NAME_for_health_goals_goalDay = "goalDay"
    var TABLE_NAME_for_health_goals_goalMonth = "goalMonth"
    var TABLE_NAME_for_health_goals_goalYear = "goalYear"
    var TABLE_NAME_for_health_goals_expiryFlag = "expiryFlag"

    var TABLE_NAME_for_health_insurance = "health_insurance"
    var TABLE_NAME_for_health_insurance_id = "_id"
    var TABLE_NAME_for_health_insurance_policyNumber = "policyNumber"
    var TABLE_NAME_for_health_insurance_Day = "renewDate_day"
    var TABLE_NAME_for_health_insurance_Month = "renewDate_month"
    var TABLE_NAME_for_health_insurance_Year = "renewDate_year"
    var TABLE_NAME_for_health_insurance_expiryFlag = "expiryFlag"

    var TABLE_NAME_for_nutrition = "nutrition"
    var TABLE_NAME_for_nutrition_id = "_id"
    var TABLE_NAME_for_nutrition_myPhoneNumber = "myPhoneNumber"
    var TABLE_NAME_for_nutrition_foodItems = "foodItems"
    var TABLE_NAME_for_nutrition_day = "day"
    var TABLE_NAME_for_nutrition_month = "month"
    var TABLE_NAME_for_nutrition_year = "year"
    var TABLE_NAME_for_nutrition_time = "time"
    var TABLE_NAME_for_nutrition_type = "type"
    var TABLE_NAME_for_nutrition_sync = "syncToOnline"

    override fun onCreate(db: SQLiteDatabase?) {
        /*HEALTH GOALS*/
        db!!.execSQL(
                "CREATE TABLE " + TABLE_NAME_for_health_goals + " ("
                        + TABLE_NAME_for_health_goals_id + " INTEGER, "
                        + TABLE_NAME_for_health_goals_healthGoals + " TEXT, "
                        + TABLE_NAME_for_health_goals_priority + " TEXT, "
                        + TABLE_NAME_for_health_goals_goalDay + " TEXT, "
                        + TABLE_NAME_for_health_goals_goalMonth + " TEXT, "
                        + TABLE_NAME_for_health_goals_goalYear + " TEXT, "
                        + TABLE_NAME_for_health_goals_expiryFlag + " TEXT); "
        )

        //load data
        //for health goals
        val loginInfo = inner_context!!.getSharedPreferences("loginInfo", Context.MODE_PRIVATE)
        val phoneNumber = loginInfo.getString("phoneNumber", "")
        loadInitDataFromServerToLocalDatabase_healthGoals(phoneNumber)

        /*HEALTH INSURANCE*/
        db.execSQL(
                "CREATE TABLE " + TABLE_NAME_for_health_insurance + " ("
                        + TABLE_NAME_for_health_insurance_id + " INTEGER, "
                        + TABLE_NAME_for_health_insurance_policyNumber + " TEXT, "
                        + TABLE_NAME_for_health_insurance_Day + " TEXT, "
                        + TABLE_NAME_for_health_insurance_Month + " TEXT, "
                        + TABLE_NAME_for_health_insurance_Year + " TEXT, "
                        + TABLE_NAME_for_health_insurance_expiryFlag + " TEXT); "
        )

        //load data
        //for health insurance
        loadInitDataFromServerToLocalDatabase_healthInsurance(phoneNumber)


        /*Nutrition*/
        db.execSQL(
                "CREATE TABLE " + TABLE_NAME_for_nutrition + " ("
                        + TABLE_NAME_for_nutrition_id + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                        + TABLE_NAME_for_nutrition_myPhoneNumber + " TEXT, "
                        + TABLE_NAME_for_nutrition_foodItems + " TEXT, "
                        + TABLE_NAME_for_nutrition_day + " TEXT, "
                        + TABLE_NAME_for_nutrition_month + " TEXT, "
                        + TABLE_NAME_for_nutrition_year + " TEXT, "
                        + TABLE_NAME_for_nutrition_time + " TEXT, "
                        + TABLE_NAME_for_nutrition_type + " TEXT, "
                        + TABLE_NAME_for_nutrition_sync + " TEXT); "
        )

    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db!!.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_for_health_goals)
    }


    /*HEALTH GOALS*/
    //load data from server to local database
    fun loadInitDataFromServerToLocalDatabase_healthGoals(phoneNumber: String) {
        doAsync {
            val client: OkHttpClient = OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build()

            val urlBuilder = HttpUrl.parse(ServerConfig().getOfflineHealthGoals())!!.newBuilder()
            urlBuilder.addQueryParameter("phoneNumber", phoneNumber)

            val url = urlBuilder.build().toString()

            val request = Request.Builder()
                    .url(url)
                    .build()

            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()

            uiThread {
                Log.d("mess", responce_string.toString())

                if (responce_string != "dataMissing") {
                    if (responce_string != "serverError") {
                        processResponse(responce_string)
                    } else {
                        //Server Error
                    }
                } else {
                    //Something goes wrong
                }
            }
        }
    }

    fun processResponse(responseString: String) {
        val chunkOfData = responseString.split("___")
        val size = chunkOfData.size
        var counter = 1

        while (counter < size) {
            val dataString = chunkOfData[counter]
            val healthGoal = dataString.split("*")[0]
            val priority = dataString.split("*")[1]
            val goalDay = dataString.split("*")[2]
            val goalMonth = dataString.split("*")[3]
            val goalYear = dataString.split("*")[4]
            val id = dataString.split("*")[5]
            val expiryFlag = dataString.split("*")[6]

            insertIntoHealthGoals(id.toInt(), healthGoal, priority, goalDay, goalMonth, goalYear, expiryFlag)

            counter++
        }

    }

    //insert functions
    fun insertIntoHealthGoals(
            id: Int,
            healthGoals: String,
            priority: String,
            goalDay: String,
            goalMonth: String,
            goalYear: String,
            expiryFlag: String): Boolean {

        val db = this.writableDatabase
        val contextvalues = ContentValues()
        contextvalues.put(TABLE_NAME_for_health_goals_id, id)
        contextvalues.put(TABLE_NAME_for_health_goals_healthGoals, healthGoals)
        contextvalues.put(TABLE_NAME_for_health_goals_priority, priority)
        contextvalues.put(TABLE_NAME_for_health_goals_goalDay, goalDay)
        contextvalues.put(TABLE_NAME_for_health_goals_goalMonth, goalMonth)
        contextvalues.put(TABLE_NAME_for_health_goals_goalYear, goalYear)
        contextvalues.put(TABLE_NAME_for_health_goals_expiryFlag, expiryFlag)
        val result = db.insert(TABLE_NAME_for_health_goals, null, contextvalues)

        return result.toInt() != (-1)
    }

    //get function
    fun getAllDataFromHealthGoals(): Cursor {
        val db = this.writableDatabase
        val c: Cursor = db.rawQuery("SELECT * FROM " + TABLE_NAME_for_health_goals, null)
        return c
    }

    //update function
    fun ActivateExpiryFlagInHealthGoals(healthGoals: String): Boolean {
        val db = this.writableDatabase
        val strSQL = "UPDATE " + TABLE_NAME_for_health_goals + " SET " + TABLE_NAME_for_health_goals_expiryFlag + " = 1 WHERE " + TABLE_NAME_for_health_goals_healthGoals + " = '" + healthGoals + "'"
        db.execSQL(strSQL)

        return true
    }


    /*HELATH INSURANCE*/
    //load data from server to local database
    fun loadInitDataFromServerToLocalDatabase_healthInsurance(phoneNumber: String) {
        doAsync {
            val client: OkHttpClient = OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build()

            val urlBuilder = HttpUrl.parse(ServerConfig().getOfflineHealthInsurance())!!.newBuilder()
            urlBuilder.addQueryParameter("phoneNumber", phoneNumber)

            val url = urlBuilder.build().toString()

            val request = Request.Builder()
                    .url(url)
                    .build()

            val response: Response = client.newCall(request).execute()
            val responce_string = response.body()!!.string()

            uiThread {
                Log.d("mess", responce_string.toString())

                if (responce_string != "dataMissing") {
                    if (responce_string != "serverError") {
                        processResponse_forHealthInsurance(responce_string)
                    } else {
                        //Server Error
                    }
                } else {
                    //Something goes wrong
                }
            }
        }
    }

    fun processResponse_forHealthInsurance(responseString: String) {
        val chunkOfData = responseString.split("___")
        val size = chunkOfData.size
        var counter = 1

        while (counter < size) {
            val dataString = chunkOfData[counter]
            val id = dataString.split("*")[0]
            val policyNumber = dataString.split("*")[1]
            val renew_day = dataString.split("*")[2]
            val renew_month = dataString.split("*")[3]
            val renew_year = dataString.split("*")[4]
            val expiryFlag = dataString.split("*")[5]


            insertIntoHealthInsurance(id.toInt(), policyNumber, renew_day, renew_month, renew_year, expiryFlag)

            counter++
        }

    }

    //insert functions
    fun insertIntoHealthInsurance(
            id: Int,
            policyNumber: String,
            renew_day: String,
            renew_month: String,
            renew_year: String,
            expiryFlag: String): Boolean {

        val db = this.writableDatabase
        val contextvalues = ContentValues()
        contextvalues.put(TABLE_NAME_for_health_insurance_id, id)
        contextvalues.put(TABLE_NAME_for_health_insurance_policyNumber, policyNumber)
        contextvalues.put(TABLE_NAME_for_health_insurance_Day, renew_day)
        contextvalues.put(TABLE_NAME_for_health_insurance_Month, renew_month)
        contextvalues.put(TABLE_NAME_for_health_insurance_Year, renew_year)
        contextvalues.put(TABLE_NAME_for_health_goals_expiryFlag, expiryFlag)
        val result = db.insert(TABLE_NAME_for_health_insurance, null, contextvalues)

        return result.toInt() != (-1)
    }

    //get function
    fun getAllDataFromHealthInsurance(): Cursor {
        val db = this.writableDatabase
        val c: Cursor = db.rawQuery("SELECT * FROM " + TABLE_NAME_for_health_insurance, null)
        return c
    }

    //update function
    fun ActivateExpiryFlagInHealthInsurance(policyNumber: String): Boolean {
        val db = this.writableDatabase
        val strSQL = "UPDATE " + TABLE_NAME_for_health_insurance + " SET " + TABLE_NAME_for_health_insurance_expiryFlag + " = 1 WHERE " + TABLE_NAME_for_health_insurance_policyNumber + " = '" + policyNumber + "'"
        db.execSQL(strSQL)

        return true
    }

    /*Nutrition*/
    //insert functions
    fun insertIntoNutrition(
            myPhoneNumber: String,
            foodItem: String,
            day: String,
            month: String,
            year: String,
            time: String,
            type: String,
            syncCode: String): Boolean {

        val db = this.writableDatabase
        val contextvalues = ContentValues()
        contextvalues.put(TABLE_NAME_for_nutrition_myPhoneNumber, myPhoneNumber)
        contextvalues.put(TABLE_NAME_for_nutrition_foodItems, foodItem)
        contextvalues.put(TABLE_NAME_for_nutrition_day, day)
        contextvalues.put(TABLE_NAME_for_nutrition_month, month)
        contextvalues.put(TABLE_NAME_for_nutrition_year, year)
        contextvalues.put(TABLE_NAME_for_nutrition_time, time)
        contextvalues.put(TABLE_NAME_for_nutrition_sync, syncCode)
        contextvalues.put(TABLE_NAME_for_nutrition_type, type)
        val result = db.insert(TABLE_NAME_for_nutrition, null, contextvalues)

        return result.toInt() != (-1)
    }

    //get function
    fun getAllUnsyncedDataFromNutrition(): Cursor {
        val db = this.writableDatabase
        val c: Cursor = db.rawQuery("SELECT * FROM " + TABLE_NAME_for_nutrition + " WHERE " + TABLE_NAME_for_nutrition_sync + "='0'", null)
        return c
    }

    //update function
    fun ActivateSyncFlagInNutrition(id: String): Boolean {
        val db = this.writableDatabase
        val strSQL = "UPDATE " + TABLE_NAME_for_nutrition + " SET " + TABLE_NAME_for_nutrition_sync + " = '1' WHERE " + TABLE_NAME_for_nutrition_id + " = '" + id + "'"
        db.execSQL(strSQL)

        return true
    }


}